module;

export module Mala.Container.FlatMap;

import <vector>;
import <map>;
import <algorithm>;

import Mala.Core.Allocator;

export namespace Mala::Container
{

/// <summary>
/// 맵 인터페이스를 가지는 시퀀스 컨테이너
/// 삽입 삭제가 자주 일어나지 않는 상황에서 사용
///
/// FlatMap::ValueType이 sort시 Key타입이 const면 대입 연산이 안되어서 const타입 제거
/// 다른 boost::flat_map등 구현체를 참고하여도 비슷한 상황
/// </summary>
template< typename TKey, typename TValue, typename TKeyComparer = std::map< TKey, TValue >::key_compare >
class FlatMap
{
public:
    /// <summary>
    /// 타입 재정의
    /// </summary>
    using KeyType              = TKey;
    using KeyComparer          = TKeyComparer;
    using ValueType            = std::pair< KeyType, TValue >;
    //using ValueType            = std::pair< const KeyType, TValue >; // 일반 맵과 다르게 key타입이 const타입이 아니다
    using BaseContainer        = std::vector< ValueType >;
    using SizeType             = typename BaseContainer::size_type;
    using Iterator             = typename FlatMap::BaseContainer::iterator;
    using ConstIterator        = typename FlatMap::BaseContainer::const_iterator;
    using ReverseIterator      = typename FlatMap::BaseContainer::reverse_iterator;
    using ConstReverseIterator = typename FlatMap::BaseContainer::const_reverse_iterator;
    using InsertReturnType     = std::pair< Iterator, bool >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    FlatMap() = default;
    FlatMap( std::initializer_list< ValueType >&& initList )
    {
        for ( const ValueType& valueType : initList )
        {
            Emplace( valueType );
        }
    }

    template< typename TIterator >
    FlatMap( TIterator begin, TIterator end )
    {
        while ( begin != end )
        {
            // 상입
            // 중복 검사는?

            ++begin;
        }

        // 정렬
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    template< typename... Args >
    InsertReturnType Emplace( Args&&... args )
    {
        ValueType target{ std::forward< Args >( args )... };
        auto findIter = Find( target.first );
        if( findIter == end() )
        {
            _container.emplace_back( std::move( target ) );
            _Sort();
            // 정렬이 들어가서 다시 Find가 필요하다.
            return { Find( target.first ), true };
        }
        else
        {
			findIter->second = std::move( target.second );
            return { findIter, false };
		}
    }

    void _Sort()
    {
        std::sort(
            _container.begin(),
            _container.end(),
            []( const ValueType& lhs, const ValueType& rhs )
            {
                return TKeyComparer{}( lhs.first, rhs.first );
            } );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    void Insert( const ValueType& value )
    {
        Emplace( value );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    void Insert( ValueType&& value )
    {
        Emplace( std::move( value ) );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    void Erase( const KeyType& key )
    {
        auto iter = Find( key );
        if ( iter == _container.end() )
            return;

        _container.erase( iter );
    }

    /// <summary>
    /// 원소 보유 여부를 반환한다
    /// </summary>
    constexpr bool Contains( const KeyType& value )
    {
        return Find( value ) != end();
    }

    /// <summary>
    /// 원소 보유 여부를 반환한다
    /// </summary>
    constexpr bool Contains( const KeyType& value ) const
    {
        return Find( value ) != cend();
    }

    /// <summary>
    /// 탐색한다
    /// </summary>
    constexpr Iterator Find( const KeyType& key )
    {
        auto iter = std::lower_bound(
            _container.begin(),
            _container.end(),
            *(ValueType*)( &key ), //ValueType{ key, TValue{} },
            []( const auto& lhs, const auto& rhs )
            { 
                return KeyComparer{}( lhs.first, rhs.first );
            } );

        return ( iter != end() ) && ( iter->first == key ) ? iter : end();
    }

    /// <summary>
    /// 탐색한다
    /// </summary>
    constexpr ConstIterator Find( const KeyType& key ) const
    {
        auto iter = std::lower_bound(
            _container.begin(),
            _container.end(),
            *(ValueType*)( &key ), //ValueType{ key, TValue{} },
            []( const auto& lhs, const auto& rhs )
            {
                return KeyComparer{}( lhs.first, rhs.first );
            } );

        return ( iter != end() ) && ( iter->first == key ) ? iter : end();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    Iterator begin()
    {
        return _container.begin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    Iterator end()
    {
        return _container.end();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    ConstIterator cbegin()
    {
        return _container.cbegin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    ConstIterator cend()
    {
        return _container.cend();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    Iterator rbegin()
    {
        return _container.rbegin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    ConstIterator rend()
    {
        return _container.rend();
    }

    /// <summary>
    /// 갯수를 반환한다
    /// </summary>
    constexpr SizeType size() const
    {
        return _container.size();
    }

    /// <summary>
    /// 갯수를 반환한다
    /// </summary>
    constexpr SizeType Size() const
    {
        return size();
    }

    /// <summary>
    /// 처음 상태로 되돌린다
    /// </summary>
    constexpr void Clear() const
    {
        _container.clear();
    }

    /// <summary>
    /// 처음 상태로 되돌린다
    /// </summary>
    constexpr bool Empty() const
    {
        return _container.emtpy();
    }


private:
    BaseContainer _container; // 내부 데이터 저장 컨테이너
};

};
