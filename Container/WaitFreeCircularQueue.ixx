module;

#include "../MalaMacro.h"
#include <memory>
#include <array>
#include <Windows.h>

export module Mala.Container.WaitfreeCircularQueue;

import Mala.Core.Types;
import Mala.Math.MathUtil;
import Mala.Container;
import Mala.Threading.Job;

NAMESPACE_BEGIN( Mala::Container )

template< class T >
class WaitFreeCircularQueue
{
	inline static constexpr i64 QUEUE_CAPACITY  { Mala::Math::PowOfTwo< 18 >() };
	inline static constexpr i64 QUEUE_SIZE_MASK { QUEUE_CAPACITY - 1           };
	inline static constexpr i64 CACHE_SIZE      { 64                           };

	using ElementArray = Array< T*, QUEUE_CAPACITY >;

public:
	/// <summary>
	/// ������
	/// </summary>
    WaitFreeCircularQueue() = default;
	WaitFreeCircularQueue( WaitFreeCircularQueue&& ) noexcept = delete;
	WaitFreeCircularQueue( const WaitFreeCircularQueue& ) = delete;
	~WaitFreeCircularQueue() = default;

    /// <summary>
    /// 
    /// </summary>
    void Enqueue( T* element )
	{
		i64 writePosition = InterlockedIncrement64( &_writer ) - 1;

		ASSERT_IF_FALSE( writePosition - _reader < QUEUE_CAPACITY ); ///< overflow

		_elements[ writePosition & QUEUE_SIZE_MASK ] = element;
	}

	/// <summary>
	/// 
	/// </summary>
	void push( T* element )
	{
		i64 writePosition = InterlockedIncrement64( &_writer ) - 1;

		ASSERT_IF_FALSE( writePosition - _reader < QUEUE_CAPACITY ); ///< overflow

		_elements[ writePosition & QUEUE_SIZE_MASK ] = element;
	}

	/// <summary>
	/// 
	/// </summary>
	T* Dequeue()
	{
		T* popVal = (T*)( InterlockedExchangePointer( (void**)&_elements[ _reader & QUEUE_SIZE_MASK ], nullptr ) );
		if ( popVal )
			InterlockedIncrement64( &_reader );

		return popVal;
	}

	i64 GetSize() const
	{
		return _writer - _reader;
	}

private:
	                     ElementArray _elements{};
	volatile ALIGN_CACHE i64 _reader  {};
	volatile ALIGN_CACHE i64 _writer  {};
};

NAMESPACE_END( Mala::Container )

