module;

export module Mala.Container.FlatSet;

import <vector>;
import <set>;
import <algorithm>;

import Mala.Core.Allocator;

export namespace Mala::Container
{

/// <summary>
/// 맵 인터페이스를 가지는 시퀀스 컨테이너
/// 삽입 삭제가 자주 일어나지 않는 상황에서 사용
/// </summary>
template< typename TValue, typename TValueComparer = std::set< TValue >::value_compare >
class FlatSet
{
public:
    /// <summary>
    /// 타입 재정의
    /// </summary>
    using ValueComparer        = TValueComparer;
    using ValueType            = TValue;
    using BaseContainer        = std::vector< ValueType >;
    using SizeType             = typename BaseContainer::size_type;
    using Iterator             = typename FlatSet::BaseContainer::iterator;
    using ConstIterator        = typename FlatSet::BaseContainer::const_iterator;
    using ReverseIterator      = typename FlatSet::BaseContainer::reverse_iterator;
    using ConstReverseIterator = typename FlatSet::BaseContainer::const_reverse_iterator;
    using InsertReturnType     = std::pair< Iterator, bool >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    FlatSet() = default;
    FlatSet( std::initializer_list< ValueType >&& initList )
    {
        for ( const ValueType& valueType : initList )
        {
            Emplace( valueType );
        }
    }
    template< typename TIterator >
    FlatSet( TIterator begin, TIterator end )
    {
        Emplace( begin, end );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    template< typename TIterator >
    void Emplace( TIterator begin, TIterator end )
    {
        _container.reserve( _container.size() + std::distance( begin, end ) );

        for ( TIterator iter = begin; iter != end; ++iter )
        {
            _container.emplace_back( *iter );
        }

        std::sort( _container.begin(), _container.end() );
    }

    template< typename TIterator >
    void emplace( TIterator begin, TIterator end )
    {
        Emplace( begin, end );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    template< typename... Args >
    InsertReturnType Emplace( Args&&... args )
    {
        ValueType target = { std::forward< Args >( args )... };

        auto findIter = Find( target );
        if ( findIter == end() )
        {
            auto iter = _container.emplace_back( std::forward< Args >( args )... );

            std::sort< Iterator >( _container.begin(), _container.end() );
            
            return { Find( target ), false };
        }
        else
        {
            *findIter = std::move( target );
            return { findIter, false };
        }
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    InsertReturnType Insert( const ValueType& value )
    {
        return Emplace( value );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    InsertReturnType insert( const ValueType& value )
    {
        return Emplace( value );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    InsertReturnType Insert( ValueType&& value )
    {
        return Emplace( std::move( value ) );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    InsertReturnType insert( ValueType&& value )
    {
        return Emplace( std::move( value ) );
    }

    /// <summary>
    /// 삽입한다
    /// </summary>
    void Erase( const ValueType& value )
    {
        auto iter = Find( value );
        if ( iter == _container.end() )
            return;

        _container.erase( iter );
    }

    /// <summary>
    /// 원소 보유 여부를 반환한다
    /// </summary>
    constexpr bool Contains( const ValueType& value ) const
    {
        return Find( value ) != end();
    }

    /// <summary>
    /// 원소 보유 여부를 반환한다
    /// </summary>
    constexpr bool Contains( const ValueType& value ) 
    {
        return Find( value ) != end();
    }

    /// <summary>
    /// 탐색한다
    /// </summary>
    constexpr Iterator Find( const ValueType& value )
    {
        auto iter = std::lower_bound( _container.begin(), _container.end(), value );
        if ( iter == end() )
            return end();

        if ( *iter == value )
            return iter;

        return _container.end();
    }

    /// <summary>
    /// 탐색한다
    /// </summary>
    constexpr ConstIterator Find( const ValueType& value ) const
    {
        auto iter = std::lower_bound( _container.begin(), _container.end(), value );
        return ( iter != end() ) && ( *iter == value ) ? iter : _container.end();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    Iterator begin()
    {
        return _container.begin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    Iterator end()
    {
        return _container.end();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    ConstIterator cbegin()
    {
        return _container.cbegin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    ConstIterator cend()
    {
        return _container.cend();
    }

    /// <summary>
    /// 처음 원소의 이터레이터를 반환한다
    /// </summary>
    ReverseIterator rbegin()
    {
        return _container.rbegin();
    }

    /// <summary>
    /// 마지막 다음 원소의 이터레이터를 반환한다
    /// </summary>
    ReverseIterator rend()
    {
        return _container.rend();
    }

    /// <summary>
    /// 갯수를 반환한다
    /// </summary>
    constexpr SizeType Size() const
    {
        return size();
    }

    /// <summary>
    /// 처음 상태로 되돌린다
    /// </summary>
    constexpr void Clear() const
    {
        _container.clear();
    }

    /// <summary>
    /// 처음 상태로 되돌린다
    /// </summary>
    constexpr bool Empty() const
    {
        return _container.emtpy();
    }

public:
    constexpr void clear() const { _container.clear(); }
    constexpr bool empty() const { return Empty(); }
    constexpr SizeType size() const { return size(); }

private:
    BaseContainer _container; // 내부 데이터 저장 컨테이너
};

/*
    Vector< int > vec{ 1, 3, 2, 4, 5, 0, 100, 52, 99, 3 };
    FlatSet< int > set( vec.begin(), vec.end() );

    auto iter = set.Find( 2 );
    if ( iter != set.end() )
        std::cout << "Find : " << *iter << std::endl;

    if( !set.Contains( 8 ) )
        std::cout << "Not Find 8" << std::endl;
*/

};
