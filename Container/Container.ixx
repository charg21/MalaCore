module;

#pragma once

export module Mala.Container;

import <memory>;
import <vector>;
import <array>;
import <list>;
import <forward_list>;
import <queue>;
import <deque>;
import <stack>;
import <set>;
import <map>;
import <unordered_set>;
import <unordered_map>;
import <tuple>;
import <concurrent_queue.h>;
import <concurrent_priority_queue.h>;

import Mala.Core.Allocator;

export import Mala.Container.FlatMap;
export import Mala.Container.FlatSet;
export import Mala.Container.LockfreeStack;
export import Mala.Container.StaticVector;
export import Mala.Container.String;
export import Mala.Container.WaitFreeQueue;

export namespace Mala::Container
{

template< typename T >
using ContainerAllocator = Mala::Core::StlAllocator< T >;

template< typename T >
using Vector = std::vector< T, ContainerAllocator< T > >;

template< typename T, size_t N >
using Array = std::array< T, N >;

template< typename T >
using ForwardList = std::forward_list< T, ContainerAllocator< T > >;

template< typename T >
using List = std::list< T, ContainerAllocator< T > >;

template< typename T >
using Set = std::set< T, ContainerAllocator< T > >;

template< typename T >
using Deque = std::deque< T, ContainerAllocator< T > >;

template< typename T >
using Queue = std::queue< T, Deque< T > >;

template< typename T >
using Stack = std::stack< T, Deque< T > >;

template< typename T, typename Container = Vector< T >, typename Pred = std::less< typename Container::value_type > >
using PriorityQueue = std::priority_queue< T, Container, Pred >;

template< typename K, typename V >
using Map = std::map< K, V, std::less< K >, ContainerAllocator< std::pair< const K, V > > >;

template< typename T >
using HashSet = std::unordered_set< T, std::hash< T >, std::equal_to< T >, ContainerAllocator< T > >;

template< typename K, typename V >
using HashMap = std::unordered_map< K, V, std::hash< K >, std::equal_to< K >, ContainerAllocator< std::pair< const K, V > > >;

template< typename... Types >
using Tuple = std::tuple< Types... >;

template< typename T >
using ConcurrentQueue = concurrency::concurrent_queue< T, ContainerAllocator< T > >;

template< typename T, typename Comparer = std::less< T > >
using ConcurrentPriorityQueue = concurrency::concurrent_priority_queue< T, Comparer, ContainerAllocator< T > >;

};
