export module Mala.Container.StaticVector;

import <array>;
import Mala.Core.Types;
import Mala.Core.TypeTraits;

export namespace Mala::Container
{

/// <summary>
/// 벡터의 인터페이스를 가진 정적 배열
/// 벡터같은 resize()가 일어나지 않는다.
/// </summary>
template< typename T, size_t Capacity, typename SizeT = size_t >
class StaticVector
{
public:
    using ValueType      = T;
    using SizeType       = usize;
    using Pointer        = T*;
    using Reference      = T&;
    using ConstPointer   = const T*;
    using ConstReference = const T&;

    /// <summary>
    /// 생성자
    /// </summary>
    StaticVector()
    : _size {}
    {
    }

    auto begin()
    {
        return _array.begin();
    }

    const auto cbegin() const
    {
        return _array.cbegin();
    }

    auto end()
    {
        return _array.begin() + _size;
    }

    const auto cend() const
    {
        return _array.cbegin() + _size;
    }

    void push_back( T&& src )
    {
        auto size = _size++;

        _array[ size ] = std::move( src );

        if ( size >= capacity() )
        {
            int* ptr = nullptr;
            *ptr = 0xDEADBEEF;
        }
    }

    void push_back( const T& src )
    {
        auto size = _size++;

        _array[ size ] = src;
    }

    void pop_back()
    {
        auto size = _size--;
        /// _array[ size ] = src;
    }

    /*T& back()
    {
        
    }*/

    constexpr size_t capacity() const
    {
        return Capacity;
    }

    constexpr size_t size() const
    {
        return _size;
    }

    constexpr bool is_full() const
    {
        return _size == Capacity;
    }

    Reference operator[]( size_t Index )
    {
        return _array[ Index ];
    }

    ConstReference operator[]( size_t Index ) const
    {
        return _array[ Index ];
    }

    void clear()
    {
        _size = 0;
    }

private:
    std::array< T, Capacity > _array; //<
    SizeT                     _size;  //<
};

    //template < size_t Capacity >
    //class static_bit_vector
    //{
    //    constexpr static size_t calc_capacity()
    //    {
    //             if constexpr ( Capacity <= 0 )
    //            return 0;
    //        else if constexpr ( Capacity <= 8 )
    //            return 1;
    //        else if constexpr ( Capacity <= 16 )
    //            return 2;
    //        else if constexpr ( Capacity <= 32 )
    //            return 4;
    //        else if constexpr ( Capacity <= 64 )
    //            return 8;
    //        else if constexpr ( ( Capacity % 8 ) > 0)
    //            return ( Capacity / 8 ) + 1;
    //        else
    //            return ( Capacity / 8 );
    //    }

    //    static constexpr size_t s_capacity = calc_capacity();
    //    std::array< unsigned char, s_capacity > _array;
    //};

    //template< typename T, size_t Capacity >
    //class warit_free_static_vector
    //{
    //    struct element
    //    {
    //        T    _data;
    //        bool _push_completed;
    //    };

    //public:
    //    warit_free_static_vector()
    //        : _size {}
    //    {
    //    }

    //    auto begin()
    //    {
    //        return _array.begin();
    //    }

    //    const auto cbegin() const
    //    {
    //        return _array.cbegin();
    //    }

    //    void push_back( T src )
    //    {
    //        size_t cur_size = _size += 1;

    //        _array[ cur_size ]._data          = src;
    //        _array[ cur_size ]._push_complted = true;
    //    }

    //    constexpr size_t GetCapacity() const
    //    {
    //        return Capacity;
    //    }

    //    T& operator[]( size_t index )
    //    {
    //        return _array[ index ];
    //    }

    //    // unsafe method
    //    auto end()
    //    {
    //        return _array.begin() + _size;
    //    }

    //    const auto cend() const
    //    {
    //        return _array.cbegin() + _size;
    //    }

    //private:
    //    std::array< element, Capacity > _array; //<
    //    std::atomic< size_t >           _size;  
    //};
} // namespace Mala::InnerContainer
