﻿module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <functional>
#include <Windows.h>

#include "../MalaMacro.h"

export module Mala.Container.LockfreeStack;

import <cstdint>;
import <type_traits>;

import Mala.Core.Types;

export namespace Mala::Container
{
    template< class T >
    struct tagged_ptr
    {
        inline static constexpr size_t TAG_MAX_INDEX { sizeof( void* ) / sizeof( uint16_t ) };
        inline static constexpr size_t TAG_SELECTOR  { TAG_MAX_INDEX - 1     };
        inline static constexpr size_t PTR_MASK      { 0x0000'FFFF'FFFF'FFFF };
        inline static constexpr size_t TAG_MASK      { 0xFFFF'0000'0000'0000 };
        inline static constexpr size_t LEFT_SHIFTER  { 16                    };
        inline static constexpr size_t RIGHT_SHIFTER { 48                    };
        inline static constexpr size_t TAG_VALUE_1   { 0x0001'0000'0000'0000 };

        static_assert( TAG_SELECTOR > 0, " Beep Beep!!" );

        volatile union
        {
            T*       _raw_ptr;
            size_t   _value;
            uint32_t _debug_view;
            uint16_t _pad_and_tag[ TAG_MAX_INDEX ];
        };

        tagged_ptr()
            : _value {}
        {
        }

        tagged_ptr( std::nullptr_t )
            : _raw_ptr {}
        {
        }

        tagged_ptr( T* in_raw_ptr )
            : _raw_ptr { in_raw_ptr }
        {
        }

        tagged_ptr( size_t value )
            : _value{ value }
        {
        }

        tagged_ptr( const tagged_ptr< T >& other )
            : _value { other._value }
        {
        }

        tagged_ptr( tagged_ptr< T >&& other )
            : _value { other._value }
        {
        }

        tagged_ptr< T >& operator=( const tagged_ptr< T >& other )
        {
            _value = other._value;

            return *this;
        }

        T* operator->()
        {
            return get_ptr();
        }

        const T* operator->() const
        {
            return get_ptr();
        }

        const T* get_ptr() const
        {
            return reinterpret_cast< T* >( _value & PTR_MASK );
        }

        T* get_ptr()
        {
            return reinterpret_cast< T* >( _value & PTR_MASK );
        }

        void set_ptr( T* in_ptr )
        {
            tagged_ptr< T > ptr_capture { _raw_ptr };

            _value = ( ptr_capture._pad_and_tag[ TAG_SELECTOR ] ) | (size_t)( in_ptr );
        }

        void set_ptr( size_t in_ptr )
        {
            size_t ptr_capture = _value;

            _value = ( ptr_capture.tag[ TAG_SELECTOR ] ) | ( in_ptr );
        }

        uint16_t get_tag()
        {
            return _pad_and_tag[ TAG_SELECTOR ];
        }

        void set_tag( uint16_t tag )
        {
            size_t ptr_capture  = _value;
            size_t extended_tag = (size_t)( tag );

            extended_tag <<= RIGHT_SHIFTER;

            _value = extended_tag | ( ptr_capture & PTR_MASK );
        }

        tagged_ptr< T > increase_tag()
        {
            size_t local_tagged_ptr__value = _value;

            /*
            tagged_ptr< T >* tagged_ptr_capture = ( tagged_ptr< T >* )&local_tagged_ptr__value;
            tagged_ptr_capture->ptr.pad_and_tag[ TAG_SELECTOR ] += 1;
            */

            uint16_t local_tag = (uint16_t)( local_tagged_ptr__value >> RIGHT_SHIFTER );
            uint64_t local_ptr = local_tagged_ptr__value & PTR_MASK;

            _value = ( (size_t)local_tag + 1 << 48 ) | local_ptr;

            return *this;
        }

        tagged_ptr< T > increase_tag_atomic()
        {
            return ::InterlockedAdd64( ( LONG64*)& _value, TAG_VALUE_1 );
        }
    }; // struct tag_ptr


    template< typename T, typename... Args >
    tagged_ptr< T > make_tagged_ptr( Args&&... args )
    {
        T* _raw_ptr = new T( std::forward< Args >( args )... );
        return tagged_ptr< T >( _raw_ptr );
    }

    template< typename T >
    class LockfreeStack
    {
    public:
        struct ALIGN_CACHE node_t
        {
            node_t* next;
            T       data;
        };

    public:
        LockfreeStack() 
			: _element_count {                             }
            , _top           { make_tagged_ptr< node_t >() }
        {
        }

        ~LockfreeStack()
        {
            unsafe_clear();
        }

		void unsafe_clear()
		{
			T t;
			while ( TryPop( t ) ) { }
		}

		void push( const T& src )
		{
            emplace( src );
		}

        void push( T&& src )
        {
            emplace( std::move( src ) );
        }

        template< class... Args >
        decltype( auto ) emplace( Args&&... args )
        {
            node_t* new_node = new node_t{ .next{ nullptr }, .data{ std::forward< Args >( args )... } };

            tagged_ptr< node_t > local_top{ _top }; // 탑 캡처

            tagged_ptr< node_t > new_top = new_node; // 새로 만든 노드를 탑 노드로 변환

            LOOP
            {
                if ( _top.get_tag() != local_top.get_tag() )
                {
                    local_top = _top;
                }
                else // 로컬탑과 현재탑이 같은... 아직까진 변경이 안된 경우...
                {
                    local_top._raw_ptr = _top._raw_ptr; // 탑 캡처

                    new_node->next = local_top.get_ptr(); // new_node next를 탑을 가리킨다.

                    new_top.set_tag( local_top.get_tag() + 1 ); // 새 노드의 tag를 0으로 변경

                    // 현재 탑이 캡처한 탑과 같은 경우 new_top을 새 탑으로 만든다.
                    if ( local_top._value != InterlockedCompareExchange(
                        &_top._value,
                        new_top._value,
                        local_top._value ) )
                        continue;
                    else
                        return;
                }
            }
        }

		////bool try_push( T data )
		////{
		////	node_t* new_node = new node_t{ nullptr, data };

		////	tagged_ptr< node_t > local_top = top;
		////	tagged_ptr< node_t > new_top   = make_tagged_ptr<node_t>( new_node, 
		////															  local_top.get_tag() + 1 );
		////	
		////	new_node->next = local_top.get_ptr();

		////	if ( top.get_tag() == local_top.get_tag() )
		////	{
		////		if ( local_top.ptr == InterlockedCompareExchange64( (int64_t*)&top.ptr, 
		////															new_top.ptr, 
		////															local_top.ptr ) )
		////		{
		////			return true;
		////		}
		////	}

		////	delete new_node;

		////	return false;
		////}

        bool TryPop( T& dest )
        {
            LOOP
            {
                tagged_ptr< node_t > local_top = _top; // 캡처 시작
                if ( !local_top->next ) 
                    return false;

                if ( _top.get_tag() != local_top.get_tag() )      // Test
                    continue;

                tagged_ptr< node_t > next_top( local_top->next ); // Setup
                next_top.set_tag( local_top.get_tag() + 1 );      

                // Atomic test
                if ( local_top._value != InterlockedCompareExchange( &_top._value, next_top._value, local_top._value ) )
                    continue;

                dest = local_top->data;

                delete local_top.get_ptr();

                return true;
            }
        }

        bool Empty()
        {
            tagged_ptr< node_t > local_top = _top;
            if ( !local_top->next )
                return false;

            return true;
        }

        bool TryPeek( T& dest )
        {
            tagged_ptr< node_t > local_top = _top; // 캡처
            if ( !local_top->next ) 
                return false;

            dest = local_top->data;
            return true;
        }

		size_t unsafe_size()
		{
			return _element_count;
		}

	private:
        ALIGN_CACHE tagged_ptr< node_t > _top;
        ALIGN_CACHE size_t			     _element_count;
	};


} // namespace Mala::InnerContainer::lockfree