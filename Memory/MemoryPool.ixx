module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Memory.MemoryPool;

import Mala.Core.Types;

export
{

enum
{
	SLIST_ALIGNMENT = 16
};

/*------------------
	MemoryHeader
-------------------*/

DECLSPEC_ALIGN( SLIST_ALIGNMENT )
struct MemoryHeader : public SLIST_ENTRY
{
	// [MemoryHeader][Data]

	MemoryHeader( i32 size ) : allocSize( size ) {}

	static void* AttachHeader( MemoryHeader* header, i32 size )
	{
		new( header )MemoryHeader( size );	// placement new
		return reinterpret_cast< void* >( ++header );
	}

	static MemoryHeader* DetachHeader( void* ptr )
	{
		MemoryHeader* header = reinterpret_cast< MemoryHeader* >( ptr ) - 1;
		return header;
	}

	i32 allocSize;
};

DECLSPEC_ALIGN( SLIST_ALIGNMENT )
class MemoryPool
{
public:
	MemoryPool( i32 allocSize );
	~MemoryPool();

	void Push( MemoryHeader* ptr );
	MemoryHeader* Pop();
private:
	SLIST_HEADER _header;
	i32 _allocSize = 0;
	Atomic< i32 > _useCount = 0;
	Atomic< i32 > _reserveCount = 0;
};

}