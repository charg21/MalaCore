module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include "../MalaMacro.h"
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Memory.MemoryPool:impl;

import Mala.Memory;
import Mala.Memory.MemoryPool;

MemoryPool::MemoryPool( i32 allocSize ) 
: _allocSize{ allocSize }
{
	::InitializeSListHead( &_header );
}

MemoryPool::~MemoryPool()
{
	// 메모리 비우기
	while ( auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) ) )
	{
		::_aligned_free( memory );
	}
}

void MemoryPool::Push( MemoryHeader* ptr )
{
	ptr->allocSize = 0;

	// Pool 메모리 반납
	::InterlockedPushEntrySList( &_header, static_cast< PSLIST_ENTRY >( ptr ) );
	_useCount.fetch_sub( 1 );
	_reserveCount.fetch_add( 1 );
}

MemoryHeader* MemoryPool::Pop()
{
	auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) );

	if ( !memory )
	{
		memory = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( _allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		ASSERT_CRASH( memory->allocSize == 0 );
		_reserveCount.fetch_sub( 1 );
	}

	_useCount.fetch_add( 1 );

	return memory;
}
