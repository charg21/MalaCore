export module Mala.Memory:impl;

#include "../MalaMacro.h"

import Mala.Memory;
import Mala.Memory.MemoryPool;
import Mala.Container;
import Mala.Core.CoreGlobal;
import Mala.Core.CoreTLS;

using namespace Mala::Container;

export
{

Memory::Memory()
{
	i32 size = 0;
	i32 tableIndex = 0;

	// 32씩 블록 나눠서 지정
	for ( size = 32; size <= 1024; size += 32 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 2048; size += 128 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 4096; size += 256 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}
}

Memory::~Memory()
{
	for ( MemoryPool* pool : _pools )
	{
		delete pool;
	}

	// _pools.clear();
}

void* Memory::Allocate( i32 size )
{
	MemoryHeader* header = nullptr;
	const i32 allocSize = size + sizeof( MemoryHeader );

#ifdef _STOMP
	header = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( allocSize ) );
#else
	if ( allocSize > MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 할당
		header = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		// 메모리 풀에서 꺼내온다
		header = _poolTable[ allocSize ]->Pop();
	}
#endif

	return MemoryHeader::AttachHeader( header, allocSize );
}

void Memory::Release( void* ptr )
{
	MemoryHeader* header = MemoryHeader::DetachHeader( ptr );

	Release( header, ptr );
}

void Memory::Release( MemoryHeader* header, void* ptr )
{
	const i32 allocSize = header->allocSize;
	//ASSERT_CRASH( allocSize > 0 );

#ifdef _STOMP
	StompAllocator::Release( header );
#else
	if ( allocSize > MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 해제
		::_aligned_free( header );
	}
	else
	{
		// 메모리 풀에 반납
		_poolTable[ allocSize ]->Push( header );
	}
#endif
}

MemoryCache::MemoryCache()
{
	i32 size = 0;
	i32 tableIndex = 0;

	// 32씩 블록 나눠서 지정
	for ( size = 32; size <= 1024; size += 32 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 2048; size += 128 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 4096; size += 256 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}
}

MemoryCache::~MemoryCache()
{
}

void* MemoryCache::Allocate( i32 size )
{
	MemoryHeader* header = nullptr;
	const i32 allocSize = size + sizeof( MemoryHeader );

#ifdef _STOMP
	header = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( allocSize ) );
#else
	if ( allocSize > Memory::MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 할당
		header = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		// 메모리 풀에서 꺼내온다
		auto* pool = _poolTable[ allocSize ];
		if ( pool->empty() )
			return GMemory->Allocate( size );

		// 메모리 풀에서 꺼내온다
		header = pool->back();
		pool->pop_back();
	}
#endif
	return MemoryHeader::AttachHeader( header, allocSize );
}

void MemoryCache::Release( void* ptr )
{
	MemoryHeader* header = MemoryHeader::DetachHeader( ptr );
	const i32 allocSize = header->allocSize;

	if ( allocSize > Memory::MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 해제
		::_aligned_free( header );
	}
	else
	{
		// 메모리 풀에 반납
		_poolTable[ allocSize ]->push_back( header );
		//return GMemory->Release( ptr );
	}
}

}