export module Mala.Memory;

#pragma once

import <vector>;
import Mala.Core.Types;
import Mala.Core.Allocator;
import Mala.Container.StaticVector;

using namespace Mala::Core;
using namespace Mala::Container;

export
{

class MemoryPool;
struct MemoryHeader;

class Memory
{
public:
	enum
	{
		// ~1024까지 32단위, ~2048까지 128단위, ~4096까지 256단위
		POOL_COUNT = ( 1024 / 32 ) + ( 1024 / 128 ) + ( 2048 / 256 ),
		MAX_ALLOC_SIZE = 4096,
	};

public:
	Memory();
	~Memory();

	void* Allocate( i32 size );
	void  Release( void* ptr );
	void  Release( MemoryHeader* header, void* ptr );

private:
	StaticVector< MemoryPool*, POOL_COUNT > _pools;

	// 메모리 크기 <-> 메모리 풀
	// 0(1) 빠르게 찾기 위한 테이블
	// 0~32
	MemoryPool* _poolTable[ MAX_ALLOC_SIZE + 1 ];
};

class MemoryCache
{
public:
	MemoryCache();
	~MemoryCache();

	void* Allocate( i32 size );
	void  Release( void* ptr );

private:
	StaticVector< std::vector< MemoryHeader* >*, Memory::POOL_COUNT > _pools;

	// 메모리 크기 <-> 메모리 풀
	// 0(1) 빠르게 찾기 위한 테이블
	// 0~32
	std::vector< MemoryHeader* >* _poolTable[ Memory::MAX_ALLOC_SIZE + 1 ];
};

template< typename Type, typename... Args >
Type* xnew( Args&&... args )
{
	Type* memory = static_cast< Type* >( PoolAllocator::Alloc( sizeof( Type ) ) );
	new( memory )Type( std::forward< Args >( args )... );

	return memory;
}

template< typename Type >
void xdelete( Type* obj )
{
	obj->~Type();
	PoolAllocator::Release( obj );
}

template< typename Type, typename... Args >
std::shared_ptr< Type > MakeShared( Args&&... args )
{
	return{ xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
}

template< typename Type, typename... Args >
std::atomic< std::shared_ptr< Type > > MakeAtomicShared( Args&&... args )
{
	return{ xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
}

template< typename Type, typename... Args >
std::unique_ptr< Type, void( * )( Type* ) > MakeUnique( Args&&... args )
{
	return{ xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
}

}
