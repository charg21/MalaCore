export module Mala.Reflection.Field:impl;

import Mala.Core.Types;
import Mala.Core.Concepts;
import Mala.Reflection.TypeInfo;
import Mala.Reflection.Field;
import "TypeInfoMacro.h";
import <string>;
import <map>;
import <typeinfo>;
import <vector>;
import <iostream>;

using namespace std;

export
{

const std::string_view& Field::GetName() const
{
	return _name;
}

const std::wstring_view& Field::GetNameW() const
{
	return _nameW;
}


const TypeInfo& Field::GetTypeInfo() const
{
	return _type;
}

Field::Field( TypeInfo& owner, const FieldInitializer& initializer )
: _name   { initializer._name    }
, _nameW  { initializer._nameW   }
, _type   { initializer._type    }
, _handler{ initializer._handler }
{
	owner.AddField( this );
}

} // namespace Mala::Reflection