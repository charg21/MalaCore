export module Mala.Reflection.Field;

import Mala.Core.Concepts;
import Mala.Core.Types;
import Mala.Core.EnumHelper;
import Mala.Core.Variant;
import Mala.Reflection.TypeInfo;
import "TypeInfoMacro.h";
import <concepts>;
import <string>;
import <map>;
import <typeinfo>;
import <vector>;

using namespace std;
using namespace Mala::Core;

export
{

using FieldName = std::string_view;
using FieldTypeName = std::string_view;

class FieldHandlerBase
{
	GENERATE_CLASS_TYPE_INFO( FieldHandlerBase )

public:
	virtual ~FieldHandlerBase() = default;
};

template< typename T >
class IFieldHandler : public FieldHandlerBase
{
	GENERATE_CLASS_TYPE_INFO( IFieldHandler )

	using ElementType = std::remove_all_extents_t< T >;

public:
	virtual ElementType& Get( void* object ) const = 0;
	virtual Variant GetVariant( void* object ) const { return Variant(); };// = 0;
	virtual void Set( void* object, const Variant& value ) const = 0;
	virtual void Set( void* object, const ElementType& value ) const = 0;
	
};

template< typename TClass, typename T >
class FieldHandler : public IFieldHandler< T >
{
	GENERATE_CLASS_TYPE_INFO( FieldHandler )

	using MemberPtr = T TClass::*;
	using ElementType = std::remove_all_extents_t< T >;

public:
	ElementType& Get( void* object ) const override
	{
		if constexpr ( std::is_array_v< T > )
		{
			static ElementType e;
			return e;
		}
		else
		{
			return static_cast< TClass* >( object )->*_ptr;
		}
	}

	//Variant GetVariant( void* object ) const override
	//{
	//	if constexpr( std::is_array_v< T > )
	//	{
	//		static ElementType e;
	//		return e;
	//	}
	//	else
	//	{
	//		/*static ElementType e;
	//		return e;*/
	//		return ( static_cast< TClass* >( object )->*_ptr );
	//	}
	//}

	virtual void Set( void* object, const ElementType& value ) const override
	{
		if constexpr ( std::is_array_v< T > )
		{

		}
		else if constexpr( std::is_copy_assignable_v< T > )
		{
			static_cast< TClass* >( object )->*_ptr = value;
		}
		else
		{

		}
	}

	virtual void Set( void* object, const Variant& value ) const override
	{

	}


	explicit FieldHandler( MemberPtr ptr ) 
	: _ptr( ptr ) 
	{
	}

private:
	MemberPtr _ptr = nullptr;
};

template< typename TClass, typename T >
class StaticFieldHandler : public IFieldHandler< T >
{
	GENERATE_CLASS_TYPE_INFO( StaticFieldHandler )

public:
	virtual T& Get( [[maybe_unused]] void* object ) const override
	{
		return *_ptr;
	}

	virtual void Set( [[maybe_unused]] void* object, const T& value ) const override
	{
		*_ptr = value;
	}

	virtual void Set( void* object, const Variant& value ) const override
	{

	}


	explicit StaticFieldHandler( T* ptr ) 
	: _ptr( ptr ) 
	{
	}

private:
	T* _ptr = nullptr;
};

enum class EFieldProperty : unsigned long long
{
	None = 0,

	IsValueType = 1 << 0,
	IsArray     = 1 << 1,
	IsEnum      = 1 << 2,
	IsPointer   = 1 << 3,
	IsReference = 1 << 4,
	IsConst     = 1 << 5,
	IsFunction  = 1 << 6,
	IsIterator  = 1 << 7,
	IsContainer = 1 << 8,
	IsVolatile  = 1 << 9,
	HasChild    = 1 << 10,

	CPtr  = IsConst | IsPointer,
	CRef  = IsConst | IsReference,
	CVRef = IsConst | IsVolatile | IsReference,
};

struct FieldInitializer
{
	const char* _name = nullptr;
	const wchar_t* _nameW = nullptr;
	const TypeInfo& _type;
	const FieldHandlerBase& _handler;
	const EFieldProperty _property = EFieldProperty::None;
};

class Field
{
public:
	const std::string_view& GetName() const;
	const std::wstring_view& GetNameW() const;
	const TypeInfo& GetTypeInfo() const;

	template< typename T >
	struct ReturnValueWrapper
	{
	public:
		explicit ReturnValueWrapper( T& value ) 
		: _value( &value )
		{
		}

		ReturnValueWrapper() = default;

		ReturnValueWrapper& operator=( const T& value )
		{
			*_value = value;
			return *this;
		}

		operator T& ()
		{
			return *_value;
		}

		T& Get()
		{
			return *_value;
		}

		const T& Get() const
		{
			return *_value;
		}

	private:
		T* _value = nullptr;
	};

	template< typename TClass, typename T >
	T& Get( TClass* object ) const
	{
		const TypeInfo& typeinfo = _handler.GetTypeInfo();
		if( typeinfo.IsA< FieldHandler< TClass, T > >() )
		{
			auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
			return concreteHandler.Get( object );
		}
		else if( typeinfo.IsA< StaticFieldHandler< TClass, T > >() )
		{
			auto concreteHandler = static_cast< const StaticFieldHandler< TClass, T >& >( _handler );
			return concreteHandler.Get( object );
		}
		else
		{
			auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
			return concreteHandler.Get( object );
		}
	}

	template< typename TClass, typename T >
	void Set( TClass* object, const T& value ) const
	{
		const TypeInfo& typeinfo = _handler.GetTypeInfo();
		if ( typeinfo.IsA< FieldHandler< TClass, T > >() )
		{
			auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
			concreteHandler.Set( object, value );
		}
		else if ( typeinfo.IsA< StaticFieldHandler< TClass, T > >() )
		{
			auto concreteHandler = static_cast< const StaticFieldHandler< TClass, T >& >( _handler );
			concreteHandler.Set( object, value );
		}
		else
		{
			auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
			concreteHandler.Set( object, value );
		}
	}

	//template< typename TClass, typename T >
	//void Set( TClass* object, const Variant& value ) const
	//{
	//	const TypeInfo& typeinfo = _handler.GetTypeInfo();
	//	if( typeinfo.IsA< FieldHandler< TClass, T > >() )
	//	{
	//		auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
	//		concreteHandler.Set( object, value );
	//	}
	//	else if( typeinfo.IsA< StaticFieldHandler< TClass, T > >() )
	//	{
	//		auto concreteHandler = static_cast< const StaticFieldHandler< TClass, T >& >( _handler );
	//		concreteHandler.Set( object, value );
	//	}
	//	else
	//	{
	//		auto concreteHandler = static_cast< const FieldHandler< TClass, T >& >( _handler );
	//		concreteHandler.Set( object, value );
	//	}
	//}

	Field( TypeInfo& owner, const FieldInitializer& initializer );

private:
	const std::string_view _name;
	const std::wstring_view _nameW;
	const TypeInfo& _type;
	const FieldHandlerBase& _handler;
	const EFieldProperty _property = EFieldProperty::None;

};

template< typename TClass, typename T, typename TPtr, TPtr ptr >
class FieldRegister
{
public:
	FieldRegister( const char* name, const wchar_t* nameW, TypeInfo& typeInfo )
	{
		EFieldProperty _property = EFieldProperty::None;
		//if constexpr ( std::is_array_v< T > || IsStdArray< T > )
		//{
		//	Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsArray );
		//}

		if constexpr ( std::is_enum_v< T > )
		{
			Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsEnum );
		}

		if constexpr ( std::is_pointer_v< T > )
		{
			Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsPointer );
		}

		if constexpr ( std::is_reference_v< T > )
		{
			Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsReference );
		}

		if constexpr ( std::is_function_v< T > )
		{
			Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsFunction );
		}

		//if constexpr ( std::_Is_iterator_v< T > )
		//{
		//	Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsIterator );
		//}

		if constexpr ( std::is_const_v< T > )
		{
			Mala::EnumHelper< EFieldProperty >::SetFlag( _property, EFieldProperty::IsConst );
		}

		/// 
		if constexpr ( std::is_member_pointer_v< TPtr > )
		{
			static FieldHandler< TClass, T > handler( ptr );
			static FieldInitializer intializer
			{
				._name = name,
				._nameW = nameW,
				._type = TypeInfo::GetStaticTypeInfo< T >(),
				._handler = handler,
				._property = _property
			};

			static Field field( typeInfo, intializer );
		}
		else
		{
			static StaticFieldHandler< TClass, T > handler( ptr );
			static FieldInitializer intializer
			{
				._name = name,
				._nameW = nameW,
				._type = TypeInfo::GetStaticTypeInfo< T >(),
				._handler = handler
			};

			static Field field( typeInfo, intializer );
		}
	}

private:
	static std::vector< Field >& GetProperties()
	{
		static std::vector< Field > properties;
		return properties;
	}
};


} // namespace Mala::Core