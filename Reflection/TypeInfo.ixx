export module Mala.Reflection.TypeInfo;

import Mala.Core.Concepts;
import Mala.Core.Types;
import Mala.Core.TypeTraits;
import Mala.Core.EnumHelper;
import Mala.Container.StaticVector;
import Mala.Container.String;
import <string>;
import <map>;
import <typeinfo>;
import <vector>;

using namespace std;
using namespace Mala::Container;

export
{

class Field;
class Method;
class TypeInfo;

template< typename T, typename U = void >
struct SuperClassTypeDeduction
{
	using Type = void;
};

/// 1. std::voie_t를 이용 SFINAE를 통해 ThisType 별칭이 존재하는 경우, 해당 Type의 ThisType을 사용
/// 2. T::ThisType을 통해 부모 타입을 알 수 있는 이유
/// 3. T의 부모로 부터 상속 받은 ThisType 클래스 정보를 사용하는 시점, 아직 자신의 ThisType 오버라이드 전
template< typename T >
struct SuperClassTypeDeduction< T, std::void_t< typename T::ThisType > >
{
	using Type = T::ThisType;
};

template< typename T >
concept HasSuper = requires
{
	typename T::Super;
} && !std::same_as< typename T::Super, void >;


template< typename T >
concept HasStaticTypeInfo = requires
{
	T::StaticTypeInfo();
};

template< typename T >
concept IsUnreflectTypeInfo = !HasStaticTypeInfo< T > && !HasStaticTypeInfo< std::remove_pointer_t< T > >;


enum class ETypeProperty : unsigned long long
{
	None = 0,

	IsValueType = 1 << 0,
	IsArray     = 1 << 1,
	IsEnum      = 1 << 2,
	IsPointer   = 1 << 3,
	IsReference = 1 << 4,
	IsConst     = 1 << 5,
	IsFunction  = 1 << 6,
	IsIterator  = 1 << 7,
	IsContainer = 1 << 8,
	IsVolatile  = 1 << 9,
	IsIterable  = 1 << 10,
	HasChild    = 1 << 11,

	CPtr  = IsConst | IsPointer,
	CRef  = IsConst | IsReference,
	CVRef = IsConst | IsVolatile | IsReference,
};


template< typename T >
struct TypeInfoInitializer
{
	TypeInfoInitializer( const char* name, const wchar_t* nameW )
	: _name { name  }
	, _nameW{ nameW }
	{
		if constexpr ( std::is_array_v< T > )// || IsStdArray< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsArray );
		}

		if constexpr ( std::is_enum_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsEnum );
		}

		if constexpr ( std::is_pointer_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsPointer );
		}

		if constexpr ( std::is_reference_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsReference );
		}

		if constexpr ( std::is_function_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsFunction );
		}

		if constexpr ( std::_Is_iterator_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsIterator );
		}

		if constexpr ( std::is_const_v< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsConst );
		}

		if constexpr( Iterable< T > )
		{
			Mala::EnumHelper< ETypeProperty >::SetFlag( _typeProperties, ETypeProperty::IsIterable );
		}

		if constexpr ( HasSuper< T > )
		{
			_super = &( typename T::Super::StaticTypeInfo() );
		}
		
	}

	const char* _name = nullptr;
	const wchar_t* _nameW = nullptr;
	const TypeInfo* _super = nullptr;
	ETypeProperty _typeProperties = ETypeProperty::None;
};


inline extern StaticVector< class TypeInfo*, 256 > GTypeInfoStorage{};

class TypeInfo
{
public:
	friend Method;
	friend Field;

	using FieldName = std::string_view;
	using FieldMap = std::map< FieldName, const Field* >;
	using FieldList = std::vector< const Field* >;

	using MethodName = std::string_view;
	using MethodMap = std::map< MethodName, const Method* >;
	using MethodList = std::vector< const Method* >;

public:
	template< typename T >
	explicit TypeInfo( const TypeInfoInitializer< T >& initializer )
	: _hashCode{ typeid( T ).hash_code() }
	, _name{ initializer._name }
	, _nameW{ initializer._nameW }
	, _fullName{ typeid( T ).name() }
	, _super{ initializer._super }
	, _typeProperties{ initializer._typeProperties }
	{
		if constexpr ( HasSuper< T > )
		{
			// Mala::EnumHelper< ETypeProperty >::SetFlag( _super->_typeProperties, ETypeProperty::HasChild );
			
			CollectSuperMethods();
			CollectSuperProperties();
			
		}
		
		RegisetrTypeInfo();
	}

	const TypeInfo* GetSuper() const;
	bool IsA( const TypeInfo& other ) const;
	bool IsChildOf( const TypeInfo& other ) const;
	const char* GetName() const;
	const wchar_t* GetNameW() const;
	bool IsArray() const;
	bool IsEnum() const;
	
	void AddField( const Field* property );
	const FieldList& GetFields() const;
	const Field* GetField( const char* name ) const;
	const Field* GetField( int index ) const;

	void AddMethod( const Method* method );
	const MethodList& GetMethods() const;
	const Method* GetMethod( const char* name ) const;
	const Method* GetMethod( int index ) const;

	template< typename T >
	bool IsA() const
	{
		return IsA( GetStaticTypeInfo< T >() );
	}

	template< typename T >
	bool IsChildOf() const
	{
		return IsChildOf( GetStaticTypeInfo< T >() );
	}

	//template< typename T > requires ( HasStaticTypeInfo< T > && Iterable< T > )
	//static const TypeInfo& GetStaticTypeInfo()
	//{
	//	using InnerType = typename T::value_type;

	//	return InnerType::StaticTypeInfo();
	//}

	template< typename T > requires ( HasStaticTypeInfo< T >&& !Iterable< T > )
	static const TypeInfo& GetStaticTypeInfo()
	{
		return T::StaticTypeInfo();
	}

	//template< HasStaticTypeInfo T >
	//static const TypeInfo& GetStaticTypeInfo()
	//{
	//	return T::StaticTypeInfo();
	//}

	template< typename T > requires ( std::is_pointer_v< T > || std::is_reference_v< T > ) && HasStaticTypeInfo< std::remove_pointer_t< T > >
	static const TypeInfo& GetStaticTypeInfo()
	{
		return std::remove_pointer_t< T >::StaticTypeInfo();
	}

	template< IsUnreflectTypeInfo T >
	static const TypeInfo& GetStaticTypeInfo()
	{
		static TypeInfo typeInfo{ TypeInfoInitializer< T >( "unreflected_type_variable", L"UB" )};
		return typeInfo;
	}

	template<>
	static const TypeInfo& GetStaticTypeInfo< bool >()
	{
		static TypeInfo typeInfo{ TypeInfoInitializer< bool >( "bool", L"bool" ) };
		return typeInfo;
	}
	template<>
	static const TypeInfo& GetStaticTypeInfo< int >()
	{
		static TypeInfo typeInfo{ TypeInfoInitializer< int >( "int", L"int" ) };
		return typeInfo;
	}
	template<>
	static const TypeInfo& GetStaticTypeInfo< float >()
	{
		static TypeInfo typeInfo{ TypeInfoInitializer< float >( "float", L"float" ) };
		return typeInfo;
	}
	template<>
	static const TypeInfo& GetStaticTypeInfo< String >()
	{
		static TypeInfo typeInfo{ TypeInfoInitializer< String >( "String", L"String" ) };
		return typeInfo;
	}

	size_t GetHashCode() const { return _hashCode; }

private:
	void CollectSuperMethods();
	void CollectSuperProperties();
	void RegisetrTypeInfo();

	size_t _hashCode;
	const char* _name{};
	const wchar_t* _nameW{};
	std::string_view _fullName;
	std::wstring_view _fullNameW;
	const TypeInfo* _super{};
	const ETypeProperty _typeProperties = ETypeProperty::None;

	FieldMap _fieldMap;
	FieldList _fields;

	MethodMap _methodMap;
	MethodList _methods;
};



} // namespace Mala::Core