export module Mala.Reflection.Method;


import Mala.Core.Concepts;
import Mala.Core.Types;
import Mala.Reflection.TypeInfo;
import "TypeInfoMacro.h";
import "MethodMacro.h";
import <string>;
import <map>;
import <typeinfo>;
import <vector>;
import <iostream>;


using namespace std;

export
{

class CallableBase
{
	GENERATE_CLASS_TYPE_INFO( CallableBase )

public:
	virtual ~CallableBase() = default;
};

template< typename TRet, typename... TArgs >
class ICallable : public CallableBase
{
	GENERATE_CLASS_TYPE_INFO( ICallable )

public:
	virtual TRet Invoke( void* caller, TArgs&&... args ) const = 0;
};

template< typename TClass, typename TRet, typename... TArgs >
class Callable : public ICallable< TRet, TArgs... >
{
	GENERATE_CLASS_TYPE_INFO( Callable )

	using FuncPtr = TRet( TClass::* )( TArgs... );

public:
	TRet Invoke( void* caller, TArgs&&... args ) const override
	{
		if constexpr ( std::same_as< TRet, void > )
		{
			( static_cast< TClass* >( caller )->*_ptr )( std::forward< TArgs >( args )... );
		}
		else
		{
			return ( static_cast< TClass* >( caller )->*_ptr )( std::forward< TArgs >( args )... );
		}
	}

	Callable( FuncPtr ptr ) 
	: _ptr( ptr ) 
	{
	}

private:
	FuncPtr _ptr = nullptr;
};


template< typename TClass, typename TRet, typename... TArgs >
class StaticCallable : public ICallable< TRet, TArgs... >
{
	GENERATE_CLASS_TYPE_INFO( StaticCallable )

	using FuncPtr = TRet( * )( TArgs... );

public:
	TRet Invoke( [[maybe_unused]] void* caller, TArgs&&... args ) const override
	{
		if constexpr ( std::same_as< TRet, void > )
		{
			( *_ptr )( std::forward< TArgs >( args )... );
		}
		else
		{
			return ( *_ptr )( std::forward< TArgs >( args )... );
		}
	}

	StaticCallable( [[maybe_unused]] TClass* owner, FuncPtr ptr )
	: _ptr( ptr ) 
	{}

private:
	FuncPtr _ptr = nullptr;
};

class Method
{
public:
	using TypeInfoList = std::vector< const TypeInfo* >;

public:
	const char* GetName() const
	{
		return _name;
	}

	const TypeInfo& GetReturnType() const
	{
		return *_returnType;
	}

	const TypeInfo& GetParameterType( size_t i ) const
	{
		return *_parameterTypes[ i ];
	}

	size_t NumParameter() const
	{
		return _parameterTypes.size();
	}

	template< typename TClass, typename TRet, typename... TArgs >
	TRet Invoke( void* caller, TArgs&&... args ) const
	{
		const TypeInfo& typeinfo = _callable.GetTypeInfo();
		if ( typeinfo.IsChildOf< Callable< TClass, TRet, TArgs...> >() )
		{
			auto concreateCallable = static_cast< const Callable< TClass, TRet, TArgs...>& >( _callable );
			if constexpr ( std::same_as< TRet, void > )
			{
				concreateCallable.Invoke( caller, std::forward< TArgs >( args )... );
			}
			else
			{
				return concreateCallable.Invoke( caller, std::forward< TArgs >( args )... );
			}
		}
		else if ( typeinfo.IsChildOf< StaticCallable< TClass, TRet, TArgs... > >() )
		{
			auto concreateCallable = static_cast< const StaticCallable< TClass, TRet, TArgs...>& >( _callable );
			if constexpr ( std::same_as< TRet, void > )
			{
				concreateCallable.Invoke( caller, std::forward< TArgs >( args )... );
			}
			else
			{
				return concreateCallable.Invoke( caller, std::forward< TArgs >( args )... );
			}
		}
		else
		{
			//assert( false && "Method::Invoke<TClass, TRet, TArgs...> - Invalied casting" );
			if constexpr ( !std::same_as< TRet, void > )
			{
				return {};
			}
		}
	}

	template< typename TRet, typename... TArgs >
	Method( TypeInfo& owner, [[maybe_unused]] TRet( *ptr )( TArgs... ), const char* name, const CallableBase& callable )
	: _name( name )
	, _callable( callable )
	{
		CollectFunctionSignature< TRet, TArgs... >();
		owner.AddMethod( this );
	}

	template <typename TClass, typename TRet, typename... TArgs>
	Method( TypeInfo& owner, [[maybe_unused]] TRet( TClass::* ptr )( TArgs... ), const char* name, const CallableBase& callable )
	: _name( name )
	, _callable( callable )
	{
		CollectFunctionSignature< TRet, TArgs... >();
		owner.AddMethod( this );
	}

private:
	template< typename TRet, typename... Args >
	void CollectFunctionSignature()
	{
		_returnType = &TypeInfo::GetStaticTypeInfo< TRet >();
		_parameterTypes.reserve( sizeof...( Args ) );

		( _parameterTypes.emplace_back( &TypeInfo::GetStaticTypeInfo< Args >() ), ... );
	}

	const TypeInfo* _returnType{};
	TypeInfoList _parameterTypes;
	const char* _name{};
	const CallableBase& _callable;
};

template< typename TClass, typename TPtr, TPtr ptr >
class MethodRegister
{
public:
	MethodRegister( const char* name, TypeInfo& typeInfo )
	{
		if constexpr ( std::is_member_function_pointer_v< TPtr > )
		{
			static Callable callable( ptr );
			static Method method( typeInfo, ptr, name, callable );
		}
		else
		{
			TClass* forDeduction = nullptr;
			static StaticCallable callable( forDeduction, ptr );
			static Method method( typeInfo, ptr, name, callable );
		}
	}
};

} // namespace Mala::Reflection