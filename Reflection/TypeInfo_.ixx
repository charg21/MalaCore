export module Mala.Reflection.TypeInfo:impl;

import Mala.Core.Types;
import Mala.Core.EnumHelper;
import Mala.Reflection.TypeInfo;
import Mala.Reflection.Field;
import Mala.Reflection.Method;

import <concepts>;
import <string>;
import <map>;
import <typeinfo>;
import <vector>;

using namespace std;

export
{

using FieldName = std::string_view;
using FieldTypeName = std::string_view;

const TypeInfo* TypeInfo::GetSuper() const
{
	return _super;
}

bool TypeInfo::IsA( const TypeInfo& other ) const
{
	if ( this == &other )
	{
		return true;
	}

	/// 다른 DLL의 같은 타입을 검사하기 위해 해시코드로 추가적으로 비교
	return _hashCode == other._hashCode;
}

bool TypeInfo::IsChildOf( const TypeInfo& other ) const
{
	if ( IsA( other ) )
	{
		return true;
	}

	for ( const TypeInfo* super = _super; super != nullptr; super = super->GetSuper() )
	{
		if ( super->IsA( other ) )
		{
			return true;
		}
	}

	return false;
}

const char* TypeInfo::GetName() const
{
	return _name;
}

const wchar_t* TypeInfo::GetNameW() const
{
	return _nameW;
}

bool TypeInfo::IsArray() const
{
	return Mala::EnumHelper< ETypeProperty >::HasFlag( _typeProperties, ETypeProperty::IsArray );
}

bool TypeInfo::IsEnum() const
{
	return Mala::EnumHelper< ETypeProperty >::HasFlag( _typeProperties, ETypeProperty::IsEnum );
}

void TypeInfo::AddField( const Field* field )
{
	_fields.emplace_back( field );
	_fieldMap.emplace( field->GetName(), field );
}

const TypeInfo::FieldList& TypeInfo::GetFields() const
{
	return _fields;
}

const Field* TypeInfo::GetField( const char* name ) const
{
	auto iter = _fieldMap.find( name );
	if ( iter != std::end( _fieldMap ) )
		return iter->second;
	
	return nullptr;
}

const Field* TypeInfo::GetField( int index ) const
{
	return _fields.at( index );
}

void TypeInfo::AddMethod( const Method* method )
{
	_methods.emplace_back( method );
	_methodMap.emplace( method->GetName(), method );
}

const TypeInfo::MethodList& TypeInfo::GetMethods() const
{
	return _methods;
}

const Method* TypeInfo::GetMethod( const char* name ) const
{
	auto iter = _methodMap.find( name );
	if ( iter != _methodMap.end() )
		return iter->second;

	return nullptr;
}

const Method* TypeInfo::GetMethod( int index ) const
{
	return _methods.at( index );
}

void TypeInfo::CollectSuperMethods()
{
	const MethodList& methods = _super->GetMethods();
	for ( const Method* method : methods )
	{
		AddMethod( method );
	}
}

void TypeInfo::CollectSuperProperties()
{
	const FieldList& fields = _super->GetFields();
	for ( const Field* field : fields )
	{
		AddField( field );
	}
}

void TypeInfo::RegisetrTypeInfo()
{
	auto& typeInfoStroage = GTypeInfoStorage;
	typeInfoStroage.push_back( this );
}

} // namespace Mala::Reflection