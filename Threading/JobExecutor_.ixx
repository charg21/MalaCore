export module Mala.Threading.JobExecutor:impl;

import <atomic>;

import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Threading.Job;
import Mala.Threading.JobExecutor;
import Mala.Threading.JobTimer;
import Mala.Threading.GlobalQueue;
import Mala.Memory;

using namespace Mala::Threading;

/// <summary>
/// 모든 작업을 실행한다
/// </summary>
void JobExecutor::Execute()
{
    LCurrentExecutor = this;

    for ( ;; )
    {
        int executeCount = 0;
        while ( auto* job = _jobQueue.Pop() )
        {
            job->Execute();
            xdelete( job );

            executeCount += 1;
        }

        if ( _remainJobCount.fetch_sub( executeCount ) == executeCount )
            break;
    }

    OnFlush();

    LCurrentExecutor = nullptr;
}

/// <summary>
/// 작업을 예약한다.
/// </summary>
void JobExecutor::DispatchJob( IJob* job )
{
    if ( _remainJobCount.fetch_add( 1 ) != 0 )
    {
        _jobQueue.Push( job );
    }
    else
    {
        _jobQueue.Push( job );

        if ( LCurrentExecutor )
        {
            /// 이렇게 되면 로컬 분배여서 
            // LExecuterList->emplace_back( shared_from_this() ) ;
            /// Gloabl Distributor
            GGlobalQueue->Push( shared_from_job_executor() );
        }
        else 
        {
            Execute();

            while ( !LExecuterList->empty() )
            {
                auto dispatcher = std::move( LExecuterList->front() );
                LExecuterList->pop_front();
                dispatcher->Execute();
            }
        }
    }
}

void JobExecutor::DispatchJob2( IJob* job )
{
    if ( _remainJobCount.fetch_add( 1 ) != 0 )
    {
        _jobQueue.Push( job );
    }
    else
    {
        _jobQueue.Push( job );
        GGlobalQueue->Push( shared_from_job_executor() );
    }
}

/// <summary>
/// 작업을 예약한다.
/// </summary>
void JobExecutor::DoAsyncAfter( int afterTick, IJob* job )
{
    if ( LJobTimer->GetMaxInterval() >= afterTick )
    {
        LJobTimer->Reserve( afterTick, shared_from_job_executor(), job );
    }
    else
    {
        // GJobTimer->Reserve( afterTick, shared_from_job_executor(), job );
    }
}



