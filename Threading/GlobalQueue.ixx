export module Mala.Threading.GlobalQueue;

#include "../MalaMacro.h"

import Mala.Core.Types;
import Mala.Container.WaitfreeCircularQueue;
import Mala.Memory;
import Mala.Threading.JobExecutor;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// �۷ι� ť
/// </summary>
class GlobalQueue
{
	/// <summary>
	/// 
	/// </summary>
	using JobQueueList = WaitFreeCircularQueue< JobExecutor >;

public:
	/// <summary>
	/// ������
	/// </summary>
	GlobalQueue() = default;

	/// <summary>
	/// �Ҹ���
	/// </summary>
	~GlobalQueue() = default;

	/// <summary>
	/// �����Ѵ�.
	/// </summary>
	void Push( JobExecutorRef jobQueue );
	void Push( JobExecutorPtr&& jobQueue );

	/// <summary>
	/// �����Ѵ�.
	/// </summary>
	JobExecutorPtr Pop();

private:
	/// <summary>
	/// 
	/// </summary>
	JobQueueList _jobQueues;
};

NAMESPACE_END( Mala::Threading )