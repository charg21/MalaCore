export module Mala.Threading.JobExecutor;

#pragma once


#include "../MalaMacro.h"


import Mala.Core.Types;
import Mala.Reflection;
import Mala.Memory;
import Mala.Threading.Job;


NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 작업 실행기
/// </summary>
class JobExecutor
{
    GENERATE_CLASS_TYPE_INFO( JobExecutor );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    JobExecutor() = default;

    /// <summary>
    /// 생성자
    /// </summary>
    virtual ~JobExecutor() = default;

    /// <summary>
    /// 작업을 실행한다.
    /// </summary>
    template< typename... Args, std::invocable< Args... > TLambda >
    void DoAsync( TLambda&& lambda, Args&&... args )
    {
        auto* job = xnew< LambdaJob< TLambda, Args... > >(
            std::forward< TLambda >( lambda ), 
            std::forward< Args >( args )... );

        DispatchJob( job );
    }

    /// <summary>
    /// 작업을 예약한다.
    /// </summary>
    template< typename... Args, std::invocable< Args... > TLambda >
    void DoAsyncAfter( int afterTick, TLambda&& lambda, Args&&... args )
    {
        auto* job = xnew< LambdaJob< TLambda, Args... > >(
            std::forward< TLambda >( lambda ), 
            std::forward< Args >( args )... );

        DoAsyncAfter( afterTick, job );
    }

    /// <summary>
    /// 작업을 예약한다.
    /// </summary>
    void DoAsyncAfter( int afterTick, IJob* job );

    /// <summary>
    /// 모든 작업을 실행한다
    /// </summary>
    void Execute();

    /// <summary>
    /// 
    /// </summary>
    void DispatchJob( IJob* job );
    void DispatchJob2( IJob* job );

    virtual void OnFlush() {}

    virtual std::shared_ptr< JobExecutor > shared_from_job_executor() = 0;

private:
    /// 작업 큐
    Field( _jobQueue )
    JobQueue _jobQueue;

    /// 작업 수
    Field( _remainJobCount )
    std::atomic< int64_t > _remainJobCount{};
};


class ExecutorDistributor
{
};

NAMESPACE_END( Mala::Threading )