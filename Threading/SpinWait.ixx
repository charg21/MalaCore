export module Mala.Threading.SpinWait;

import Mala.Core.Random;

export namespace Mala::Threading
{

/// <summary>
/// 임의의 난수 횟수 만큼 비지 웨이팅 진행
/// 주로 스레드간 경합이 일어나는 곳에서 짧은 시간동안 대기가 필요할때 사용
/// </summary>
/// <typeparam name="Limit"> 최대 스핀 카운트 </typeparam>
template< size_t Limit = 1000 >
class SpinWait
{
    static constexpr size_t _min = 10;

public:
    /// <summary>
    /// 랜덤한 숫자 만큼 비지 웨이팅을 한다
    /// 호출시 스핀 횟수가 배로 증가한다
    /// </summary>
    void SpinOnce()
    {
              size_t limitCountCapture = _limitCount;
        const size_t spinCount         = ( Mala::Core::FastRand() % limitCountCapture );

        limitCountCapture = limitCountCapture << 1;
        if ( limitCountCapture > Limit )
            limitCountCapture = Limit;

        _limitCount = limitCountCapture;

        for ( size_t count = 0; count < spinCount; count += 1 )
            ;
    }

    /// <summary>
    /// 스핀 카운트를 초기화한다
    /// </summary>
    void Reset()
    {
        _limitCount = _min;
    }

private:
    size_t _limitCount = _min; // 
};

}
