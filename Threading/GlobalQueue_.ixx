export module Mala.Threading.GlobalQueue:impl;

import Mala.Core.Types;
import Mala.Core.Crash;

import Mala.Threading.GlobalQueue;
import Mala.Container.WaitFreeQueue;

using namespace Mala::Threading;

/// <summary>
/// 삽입한다.
/// </summary>
void GlobalQueue::Push( JobExecutorRef executor )
{
	//_jobQueues.push( jobQueue );
	_jobQueues.push( executor.get() );
}

void GlobalQueue::Push( JobExecutorPtr&& executor )
{
	//_jobQueues.push( std::move( executor ) );
	_jobQueues.push( executor.get() );
}

/// <summary>
/// 제거한다.
/// </summary>
JobExecutorPtr GlobalQueue::Pop()
{
	JobExecutor* executor = _jobQueues.Dequeue();
	if ( !executor )
		return nullptr;

	return executor->shared_from_job_executor();
	//JobExecutorPtr jobQueue;
	//_jobQueues.try_pop( jobQueue );

	//return std::move( jobQueue );
}
