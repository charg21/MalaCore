export module Mala.Threading.ThreadManager;

#include "../MalaMacro.h"

import <atomic>;
import <thread>;
import <functional>;

import Mala.Core.Types;
import Mala.Core.Crash;
import Mala.Core.CoreTLS;
import Mala.Memory;
import Mala.Windows;

import Mala.Threading.Thread;
import Mala.Threading.JobWheelTimer;
import Mala.Container.WaitFreeQueue;

using namespace Mala::Windows;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 스레드 관리자
/// </summary>
class ThreadManager
{
protected:
    using ThreadList = Mala::Container::WaitFreeQueue< std::thread >;
    using ThreadJob = std::function< void( void ) >;

public:
    /// <summary>
    /// 
    /// </summary>
    ThreadManager();
    ~ThreadManager();
    ThreadManager( const ThreadManager& other )     = delete;
    ThreadManager( ThreadManager&& other ) noexcept = delete;

    /// <summary>
    /// TLS를 초기화한다
    /// </summary>
    static void InitTLS();

    /// <summary>
    /// 
    /// </summary>
    static void DestroyTLS();

    /// <summary>
    /// 
    /// </summary>
    static void DoGlobalQueueWork();

    /// <summary>
    /// 
    /// </summary>
    static void DistibuteReservedJobs();

    /// <summary>
    /// 
    /// </summary>
    void Join();

    /// <summary>
    /// 실행한다.
    /// </summary>
    void Launch( ThreadJob threadJob );
    template< typename TThread >
    void Launch();

    virtual bool Initialize() { return true; };

    /// <summary>
    /// 스레드 식별자를 발급한다
    /// </summary>
    i32 GenerateThreadId();

//private:
    /// 스레드 목록
    ThreadList _threads;

    /// 스레드 최대 갯수
    size_t _capacity;

    /// 스레드 식별자 발급기
    std::atomic< i32 > _threadIdGenerator{ -1 };
};

template< typename TThread >
void ThreadManager::Launch()
{
    _threads.Emplace( TThread( [ = ]()
    {
        InitTLS();
        DestroyTLS();
    } ) );
}

NAMESPACE_END( Mala::Threading )
