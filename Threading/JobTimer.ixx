#include "../MalaMacro.h"

export module Mala.Threading.JobTimer;

import <vector>;
import <queue>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Job;
import Mala.Threading.JobExecutor;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Windows;
using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// Job비교 연산자 정의
/// </summary>
struct JobComparer
{
    bool operator()( const std::pair< u64, JobTimerElement >& lhs, const std::pair< u64, JobTimerElement >& rhs ) const
    {
        return lhs.first > rhs.first;
    }
};

/// <summary>
/// 특정 시간 이후 예약한 작업을 실행하는 객체
/// </summary>
class JobTimer
{
    using TickType         = u64;
    using TimeJob          = std::pair< TickType, JobTimerElement >;
    using TimeJobVector    = Vector< TimeJob >;
    using TimeJobContainer = ConcurrentPriorityQueue< TimeJob, JobComparer >;

public:
    /// <summary>
    /// 작업을 예약한다
    /// </summary>
    void Reserve( u64 afterTick, JobExecutorPtr&& executable, IJob* job )
    {
        _jobContainer.push( TimeJob{ GetTick() + afterTick, { std::move( executable ), job } } );
    }

    /// <summary>
    /// 작업을 예약한다
    /// </summary>
    void Reserve( u64 afterTick, JobExecutorRef executable, IJob* job )
    {
        _jobContainer.push( TimeJob{ GetTick() + afterTick, { executable, job } } );
    }

    /// <summary>
    /// 실행한다
    /// </summary>
    bool Fetch( size_t now ) const
    {
        if ( _jobContainer.empty() )
            return false;

        //auto& top = _jobContainer.top();
        //if ( top.first > now )
        //    return false;

        return true;
    }

    /// <summary>
    /// 작업을 분배한다
    /// </summary>
    void Distribute( usize now )
    {
        while ( Fetch( now ) )
        {
            //auto& element = _jobContainer.top().second;
            //element._jobExecutable->DispatchJob2( element._job );
            //_jobContainer.pop();
        }
    }

    /// <summary>
    /// 갱신한다
    /// </summary>
    void Update( i64 now = GetTick() )
    {
        Distribute( now );
    }

private:
    USE_LOCK;

    TimeJobContainer _jobContainer;
};

NAMESPACE_END( Mala::Threading )