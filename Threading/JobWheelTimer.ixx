#include "../MalaMacro.h"

export module Mala.Threading.JobWheelTimer;

import <mutex>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Job;
import Mala.Threading.Lock;
import Mala.Math.MathUtil;
import Mala.Windows;

using namespace Mala::Container;
using namespace Mala::Windows;
using namespace Mala::Math;

NAMESPACE_BEGIN( Mala::Threading )

struct JobTimerElement
{
    JobExecutorPtr _executor;
    IJob* _job;
};

/// <summary>
/// 작업 타임 휠
/// </summary>
template< Lockable TLock = NullLock, usize TickInterval = 16, usize SlotPow = 12 >
class JobWheelTimer
{
public:
    /// <summary>
    /// 상수
    /// </summary>
    inline static constexpr usize SLOT_CAPACITY  = PowOfTwo< SlotPow >(); // 0b1000'0000'...
    inline static constexpr usize SLOT_SIZE_MASK = SLOT_CAPACITY - 1;     // 0b0111'1111'...
    inline static constexpr usize MAX_TICK_INTERVAL = TickInterval * SLOT_CAPACITY; 

    /// <summary>
    /// 타입 재정의
    /// </summary>
    using Slot     = Deque< JobTimerElement >;
    using Slots    = Array< Slot, SLOT_CAPACITY >;
    using TickType = usize;
    using LockType = TLock;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    JobWheelTimer()
    : _lastFlushedTick { GetTick() }
    , _lastFlushedIndex{           }
    {
    }

    /// <summary>
    /// 작업을 예약한다.
    /// </summary>
    void Reserve( TickType afterTick, JobExecutorPtr&& executor, IJob* job )
    {
        const usize slotIndex = afterTick / TickInterval;

        const usize insertSlotIndex = ( slotIndex + _lastFlushedIndex ) & SLOT_SIZE_MASK;

        if constexpr ( !IsNullLock< LockType >::value )
        {
            EXCLUSIVE_LOCK( _lock )

            _slots[ insertSlotIndex ].emplace_back( std::move( executor ), job );
        }
        else
        {
            _slots[ insertSlotIndex ].emplace_back( std::move( executor ), job );
        }
    }

    /// <summary>
    /// 작업을 비운다.
    /// </summary>
    void Distribute( TickType now = GetTick() )
    {
        const TickType tickGap = now - _lastFlushedTick;
        if ( tickGap < TickInterval )
            return;

        const usize popCount = tickGap / TickInterval;
        
        /// Get Timeout Slot...
        if constexpr ( !IsNullLock< LockType >::value )
        {
            EXCLUSIVE_LOCK( _lock );

            PopAndDistribute( popCount );
        }
        else
        {
            PopAndDistribute( popCount );
        }
    }

    /// <summary>
    /// 작업을 빼내고 분배한다.
    /// </summary>
    void PopAndDistribute( usize popCount )
    {
        for ( usize i{}; i < popCount; i += 1 )
        {
            usize nextSlotIndex = ( i + _lastFlushedIndex ) & SLOT_SIZE_MASK;
            Slot& slot = _slots[ nextSlotIndex ];
            
            int distributedJobCount{};
            int jobCount = slot.size();

            while ( distributedJobCount < jobCount )
            {
                for ( ; distributedJobCount < jobCount; distributedJobCount += 1 )
                {
                    auto& element = slot[ distributedJobCount ];
                    element._executor->DispatchJob2( element._job );
                }

                jobCount = slot.size();
            }

            slot.clear();
        }

        _lastFlushedTick  += ( popCount * TickInterval );
        _lastFlushedIndex += popCount;
    }

    /// <summary>
    /// 틱 인터벌을 반환한다.
    /// </summary>
    [[ nodiscard ]] static constexpr TickType GetInterval()
    {
        return TickInterval;
    }

    /// <summary>
    /// 최대 틱 인터벌을 반환한다.
    /// </summary>
    [[ nodiscard ]] static constexpr TickType GetMaxInterval()
    {
        return MAX_TICK_INTERVAL;
    }

private:
    LockType _lock;             ///< 슬롯 락
    Slots    _slots;            ///< 슬롯 목록
    TickType _lastFlushedTick;  ///< 마지막 실행된 틱
    TickType _lastFlushedIndex; ///< 마지막 실행된 인덱스
};

/*

import Mala.Core.DateTime;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.Job;

int main()
{
    constexpr usize Max = 64;

    Mala::Threading::JobWheelTimer jtw;
    
    Mala::Threading::Job job = [ & ]()
    {
        for ( int i = 0; i < 64; i += 1 )
        {
            auto sec = i * 1000;
            jtw.DoAsyncAfter(     
                sec,
                [ &, sec, i ]
                {
                    std::wcout << L"zz[ " << i << " ] : " << Mala::Core::DateTime::Now().ToString() << std::endl;
                    if ( i == 63 )
                        job();
                } );
        }
    };
    
    job();
    
    for ( ;; )
    {
        jtw.Dispatch();
    }

    return 0;
}
*/


NAMESPACE_END( Mala::Threading )