export module Mala.Threading.ThreadManager:impl;

#include "../MalaMacro.h"

import <atomic>;
import <thread>;
import <functional>;

import Mala.Core.Types;
import Mala.Core.Crash;
import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Memory;
import Mala.Windows;

import Mala.Threading.Lock;
import Mala.Threading.GlobalQueue;
import Mala.Threading.Thread;
import Mala.Threading.ThreadManager;
import Mala.Threading.JobWheelTimer;
import Mala.Threading.JobExecutor;
import Mala.Container.WaitFreeQueue;

using namespace Mala::Windows;
using namespace Mala::Threading;


/// <summary>
/// 생성자
/// </summary>
ThreadManager::ThreadManager()
{
    InitTLS();
}

/// <summary>
/// 소멸자
/// </summary>
ThreadManager::~ThreadManager()
{
    DestroyTLS();
}

/// <summary>
/// 실행한다
/// </summary>
void ThreadManager::Launch( ThreadJob threadJob )
{
    // WRITE_LOCK;

    _threads.Emplace( ( [ = ]()
    {
        InitTLS();
        threadJob();
        DestroyTLS();
    } ) );
}

void ThreadManager::InitTLS()
{
    static Atomic< u32 > sThreadId = -1;
    LThreadId = sThreadId.fetch_add( 1 );

    LJobTimer = xnew< JobWheelTimer< NullLock, 16, 12 > >();
    LExecuterList = xnew< std::deque< JobExecutorPtr > >();
}

void ThreadManager::DestroyTLS()
{
    xdelete( LJobTimer );
    xdelete( LExecuterList );
}

void ThreadManager::DoGlobalQueueWork()
{
    for ( ;; )
    {
        //u64 now = GetTick();
        //if ( now > LEndTickCount )
        //    break;

        auto jobQueue = GGlobalQueue->Pop();
        if ( !jobQueue )
            break;

        jobQueue->Execute();
    }
}

void ThreadManager::DistibuteReservedJobs()
{
    const u64 now = GetTick();
    LJobTimer->Distribute( now );
    //GJobTimer->Distribute( now );
}

void ThreadManager::Join()
{
    _threads.ForEach( []( auto& thread )
    {
        //Mala::Core::CrashIfNull( thread );
    
        thread.join();
    } );
}

/// <summary>
/// 스레드 식별자를 발급한다
/// </summary>
i32 ThreadManager::GenerateThreadId()
{
    return ++_threadIdGenerator;
}
