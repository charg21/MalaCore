export module Mala.Threading.ThreadPool;

import <vector>;
import <functional>;
import <concurrent_queue.h>;

import Mala.Core.Crash;
import Mala.Threading.Thread;
import Mala.Threading.ThreadManager;
import Mala.Container.LockfreeStack;
import Mala.Container.WaitFreeQueue;

using namespace Concurrency;
using namespace std;

export namespace Mala::Threading
{
class ThreadPool;

class WaitableThread : public Mala::Threading::Thread
{
    using Base         = Mala::Threading::Thread;
    using Job          = std::function< void() >;
    using JobContainer = Concurrency::concurrent_queue< Job >;

public:
    WaitableThread( ThreadPool& owner );
    virtual ~WaitableThread() = default;

    void Wait();
    void SetVictim( bool isVictim );

    virtual void OnStart() override;

protected:
    ThreadPool&  _owner;        //< pool
    bool         _isVictim;     //< 희생양인지 여부( 바쁘게 일할 누군가 하나 )
    size_t       _timeoutValue; //< 타입아웃 값
};

/// <summary>
/// 스레드 풀
/// </summary>
class ThreadPool : public Mala::Threading::ThreadManager
{
    friend class WaitableThread;

    using Base         = Mala::Threading::ThreadManager;
    using Job          = std::function< void() >;
    using JobContainer = Concurrency::concurrent_queue< Job >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    ThreadPool( const size_t maxThread );
    ThreadPool( const ThreadPool& other ) = delete;
    ThreadPool( ThreadPool&& other ) noexcept = delete;

    /// <summary>
    /// 초기화한다
    /// </summary>
    virtual bool Initialize() override final;
    constexpr size_t Capacity() const noexcept;

    /// <summary>
    /// 작업 실행을 예약한다.
    /// ( 한개의 스레드가 깨어나 작업을 획득하여 처리 )
    /// </summary>
    void Execute( Job&& inJob );

    /// <summary>
    /// 작업 실행을 예약한다.
    /// ( 모든 스레드가 깨어나 작업을 획득하여 처리 )
    /// </summary>
    void ExecuteAll( Job&& inJob );

    /// <summary>
    /// 모든 스레드를 깨운다
    /// </summary>
    void WakeAll(){}

    /// <summary>
    /// 한 스레드를 깨운다
    /// </summary>
    void WakeOne(){}

    Job Take();

    /// <summary>
    /// 한 스레드를 깨운다
    /// </summary>
    void add_thread( WaitableThread* );
    
protected:
    JobContainer    _jobs; //< 작업 목록
    size_t          _key;
};

} // namespace Mala::Net

namespace Mala::Threading
{

/// <summary>
/// 생성자
/// </summary>
ThreadPool::ThreadPool( const size_t max_thread )
: Base{}
{
    // _factory = [ this ](){ return new WaitableThread{ *this }; };
}

/// <summary>
/// 초기화한다
/// </summary>
bool ThreadPool::Initialize()
{
    bool victim = true;

    for ( int n{}; n < _capacity; n += 1 )
    {
        //WaitableThread* thread = static_cast< WaitableThread* >( _factory() );
        //Mala::Core::CrashIfFalse( thread );
        //
        //thread->SetVictim( victim );
        //
        //add_thread( thread );
        //
        //thread->Start();

        //victim = false;
    }

    return true;
}

/// <summary>
/// 스레드를 추가한다
/// </summary>
void ThreadPool::add_thread( WaitableThread* in_thread )
{
    // _threads.Enqueue( in_thread );
}

/// <summary>
/// 
/// </summary>
constexpr size_t ThreadPool::Capacity() const noexcept
{
    return 0;// _capacity;
}

}