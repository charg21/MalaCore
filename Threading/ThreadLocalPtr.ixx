module;

#include "../MalaMacro.h"

export module Mala.Threading.ThreadLocalPtr;

import <atomic>;
import <functional>;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// ThreadLocal을 사용할 때 사용할 컨텍스트
/// 추후 공용으로 사용이 필요하면 위치를 옮긴다 
/// </summary>
class ThreadLocalContext
{
public:
    static int IssueId() 
    { 
        return ++_idGenerator; 
    }

    static int GetThreadId() 
    { 
        return _threadId; 
    }

    inline static std::atomic< int > _idGenerator{ -1 };

    inline static thread_local int _threadId{ IssueId() };
};

/// <summary>
/// 스레드 로컬 포인터( 최대 갯수 제한 ) 
/// C#의 ThreadLocal, boost의 thread_specific_ptr와 비슷한 기능 간단한 구현체
/// </summary>
template< typename T >
class ThreadLocalPtr
{
    using Factory = std::function< T*() >;
    using Deleter = std::function< void( T* ) >;

public:
    ThreadLocalPtr(
        size_t    capacity,
        Factory&& factory = [](){ return new T(); },
        Deleter&& deleter = []( T* rhs ){ delete rhs; } )
    : _ptrs   { capacity, nullptr    }
    , _factory{ std::move( factory ) }
    , _deleter{ std::move( deleter ) }
    {
    }

    ThreadLocalPtr( const ThreadLocalPtr& rhs ) = delete;
    ThreadLocalPtr( ThreadLocalPtr&& rhs ) noexcept = delete;

    ~ThreadLocalPtr()
    {
        for ( auto ptr : _ptrs )
        {
            if ( !ptr )
                continue;

            if ( _deleter )
                _deleter( ptr );
        }
    }

    T*& GetPtrRef()
    {
        auto threadId = ThreadLocalContext::GetThreadId();
        return _ptrs[ threadId ];
    }

    T* Get()
    {
        auto& ptr = GetPtrRef();
        if ( !ptr )
            ptr = _factory();

        return ptr;
    }

    void Reset( T* value = nullptr )
    {
        T*& ptr = GetPtrRef();
        ptr = value;
    }

    auto Capacity()
    {
        return _ptrs.capacity();
    }

    auto* operator->()
    {
        return Get();
    }

    auto* operator->() const
    {
        return Get();
    }

    auto& operator*()
    {
        return *get();
    }

    auto& operator*() const 
    {
        return *get();
    }

    T* get()
    {
        return Get();
    }

    operator bool() const
    {
        return Get();
    }

private:
    /// <summary>
    /// 포인터 목록
    /// </summary>
    std::vector< T* > _ptrs;

    /// <summary>
    /// 팩토리
    /// </summary>
    Factory _factory;

    /// <summary>
    /// 딜리터
    /// </summary>
    Deleter _deleter;
};

NAMESPACE_END( Mala::Threading )

/*

/// <summary>
/// 프로그램 진입접
/// </summary>
int main()
{
    ThreadLocalPtr< int > threadLocalPtr( [] { return new int{}; }, 10 );
    auto& n = threadLocalPtr.GetRef();
    n = 20;

    std::thread t( [ & ]
    {
        auto& n = threadLocalPtr.GetRef();
        n = 30;
    } );

    n = 40;

    for( ;; )
    {
    }
}

*/