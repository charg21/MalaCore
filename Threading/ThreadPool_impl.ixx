module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
#include <synchapi.h>
#include "../MalaMacro.h"

#pragma comment( lib, "Synchronization.lib" )

export module Mala.Threading.ThreadPool:impl;

import Mala.Threading.ThreadPool;

namespace Mala::Threading
{

WaitableThread::WaitableThread( class ThreadPool& owner )
: Base         {          }
, _owner       { owner    }
, _isVictim    { false    }
, _timeoutValue{ INFINITE }
{
}

void WaitableThread::OnStart()
{
    LOOP
    {
        // 잡에 캡처등으로 포함된 객체의 생명주기에 영향을 줄 수 있어 
        // 루프내에서 생성자 소멸자가 호출되도록 하였습니다.
        Job outJob;

        while ( _owner._jobs.try_pop( outJob ) )
            outJob();

        // 모든 스레드가 현재 문맥인 상태에서 잡이 들어온다면 잡이 남아 있는 상태에서 블럭이 가능하다!
        // 생각 나는 해결 방법
        // 1. 모든 스레드에 타임아웃을 걸어 잡이 쌓이는걸 방지
        // 2. 한 스레드에만 타임아웃을 건다. ( 1번보다 컨텍스트 스위칭 빈도 감소, 적은 케이스에 대해 최소비용 처리 )

        // 이미 잡이 생성된 경우가 있을 수 있으니 아래로 옮긴다.
        Wait();
    }
}

void WaitableThread::SetVictim( bool isVictim )
{
    _isVictim = isVictim;

    _timeoutValue = isVictim ? 16 : INFINITE;
}

void WaitableThread::Wait()
{
    size_t keyCapture = _owner._key;

    // 현재 스레드를 Block 상태로 변경한다.
    ::WaitOnAddress( &_owner._key, &keyCapture, sizeof( _owner._key ), _timeoutValue );
}

void ThreadPool::Execute( Job&& job )
{
    // 작업을 추가후
    _jobs.push( std::move( job ) );

    // 한개의 스레드를 깨웁니다.
    ::WakeByAddressSingle( &_key );
}

void ThreadPool::ExecuteAll( Job&& job )
{
    // 작업을 추가후
    _jobs.push( std::move( job ) );

    // 한개의 스레드를 깨웁니다.
    ::WakeByAddressAll( &_key );
}

ThreadPool::Job ThreadPool::Take()
{
    Job outJob;

    while ( _jobs.try_pop( outJob ) )
        return std::move( outJob );
}

}
