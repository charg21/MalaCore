module; // module 정의

#include "../MalaMacro.h"

import <stddef.h>; 
import <atomic>; 
import <functional>; 
import <type_traits>;

export module Mala.Threading.Job;

import Mala.Core.Crash;

NAMESPACE_BEGIN( Mala::Threading )

/// <summary>
/// 작업
/// </summary>
using Job = std::function< void() >;

/// <summary>
/// 
/// </summary>
struct JobEntry
{
	JobEntry* volatile _next{};
};

/// <summary>
/// 작업 노드 엔트리
/// </summary>
struct IJob
{
	/// <summary>
	/// 소멸자
	/// </summary>
	virtual ~IJob() = default;

	/// <summary>
	/// 실행한다
	/// </summary>
	virtual void Execute()
	{};

	/// <summary>
	/// 실행한다
	/// </summary>
	void operator()()
	{
		Execute();
	}

	JobEntry _entry;
};

/// <summary>
/// 람다를 래핑한 잡 객체
/// </summary>
template< typename TLambda, typename... TArgs >
struct LambdaJob : public IJob
{
public:
	/// <summary>
	/// 인자 타입
	/// </summary>
	using Args = std::tuple< TArgs... >;

public:
	/// <summary>
	/// 생성자
	/// </summary>
	LambdaJob( TLambda&& lambda, TArgs&&... args )
	: _lambda{ std::forward< TLambda >( lambda )  }
	, _args  { std::forward< TArgs >( args )...   }
	{
	}

	/// <summary>
	/// 소멸자
	/// </summary>
	virtual ~LambdaJob() override final = default;

	/// <summary>
	/// 실행한다
	/// </summary>
	virtual void Execute() override final
	{
		std::apply( _lambda, _args );
	}

private:
	/// <summary>
	/// 람다 인스턴스
	/// </summary>
	TLambda _lambda;

	/// <summary>
	/// 메서드의 인자
	/// </summary>
	Args _args;
};

#define OFFSET_OF( type, field )                                          \
  ( reinterpret_cast< intptr_t >( &( reinterpret_cast< type* >( 4 )->field ) ) - 4 )

class JobQueue
{
public:
	///// <summary>
	///// 
	///// </summary>
	//int64_t _offset{ OFFSET_OF( class IJob, _entry ) };

	/// <summary>
	/// 
	/// </summary>
	constexpr inline static int64_t _offset{ 8 };

public:
	JobQueue()
	: _tail{ &_stub }
	{
		_head = &_stub;
		Mala::Core::CrashIfFalse( _head.is_lock_free() );
	}

	/// <summary>
	/// 
	/// </summary>
	~JobQueue() = default;

	/// <summary>
	/// 
	/// </summary>
	void Push( IJob* newJob )
	{
		auto* prevEntry = reinterpret_cast< JobEntry* >( std::atomic_exchange_explicit(
			&_head,
			&newJob->_entry, 
			std::memory_order_acq_rel ) );

		prevEntry->_next = &( newJob->_entry );
	}

	IJob* Pop()
	{
		JobEntry* tail = _tail;
		JobEntry* next = tail->_next;

		if ( tail == &_stub )
		{
			if ( !next )
				return nullptr;

			_tail = next;
			tail = next;
			next = next->_next;
		}

		if ( next )
		{
			_tail = next;

			return reinterpret_cast< IJob* >( reinterpret_cast< int64_t >( tail )- _offset );
		}

		JobEntry* head = _head;
		if ( tail != head )
			return nullptr;

		_stub._next = nullptr;
		
		auto* prevEntry = reinterpret_cast< JobEntry* >( std::atomic_exchange_explicit( 
			&_head,
			&_stub, 
			std::memory_order_acq_rel ) );

		prevEntry->_next = &_stub;

		next = tail->_next;
		if ( next )
		{
			_tail = next;

			return reinterpret_cast< IJob* >( reinterpret_cast< int64_t >( tail ) - _offset );
		}

		return nullptr;
	}

	void Flush()
	{
		auto* job = Pop();
		while ( job )
		{
			job->Execute();
			delete job;
			job = Pop();
		}
	}

private:
	/// <summary>
	/// 헤드
	/// </summary>
	std::atomic< JobEntry* > _head;

	/// <summary>
	/// 테일
	/// </summary>
	JobEntry* _tail;

	/// <summary>
	/// 더미
	/// </summary>
	JobEntry _stub;
};

/*

struct Human
{
	void Do()
	{
		std::cout << "A" << std::endl;
	}
};

void Do()
{
	std::cout << "B" << std::endl;
}

void Do1( int n )
{
	std::cout << "B " << n << std::endl;
}

/// <summary>
/// 프로그램 진입접
/// </summary>
int main()
{
	FunctionJob func( &Do );
	func();

	FunctionJob func1( &Do1, 1 );
	func1();

	Human h;
	MethodJob memFunc( &h, &Human::Do);
	memFunc();
}

*/


NAMESPACE_END( Mala::Threading )