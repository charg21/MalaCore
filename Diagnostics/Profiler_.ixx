module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

export module Mala.Diagnostics.Profiler:impl;

import <string_view>;
import <unordered_map>;

import Mala.Diagnostics.Profiler;
import Mala.Container.WaitFreeQueue;

export namespace Mala::Diagnostics
{
//// 통합한다.
//void CentralProfiler::Merge()
//{
//    Reset();
//
//    _threadProfilers.ForEach( [ this ]( ThreadProfiler* eachThreadProfiler )
//    {
//        eachThreadProfiler->_recordList.ForEach( [ this ]( ProfilerRecord* each_record )
//        {
//            ProfilerRecord& record = _records[ each_record->_name ];
//            if ( !record._counter )
//                record._name = each_record->_name;
//
//            record._counter     += each_record->_counter;
//            record._total_ticks += each_record->_total_ticks;
//        } );
//    } );
//}
//
//// 
//void CentralProfiler::Reset()
//{
//    for ( auto& [ _, record ] : _records )
//    {
//        record._counter     = 0;
//        record._total_ticks = 0;
//    }
//}
//
//void CentralProfiler::ResetAndMerge()
//{
//    Reset();
//    Merge();
//}
//
//void CentralProfiler::Register( ThreadProfiler* profiler )
//{
//    _threadProfilers.Emplace( profiler );
//}
//
//void CentralProfiler::Log()
//{
//    for ( auto& [ _, record ] : _records )
//        OnLog( record );
//}
//
//// 생성자
//ThreadProfiler::ThreadProfiler()
//{
//    // central profiler에 등록
//    GProfiler.Register( this );
//}
//
//void ThreadProfiler::Add( const std::wstring_view& name, long long diff )
//{
//    //const ProfilerRecord& record = _records.find(name);
//    auto record = _records.find( name );
//    if ( record == _records.end() )
//    {
//        record->second._name = name;
//        _recordList.Emplace( &record->second );
//    }
//
//    record->second._counter     += 1;
//    record->second._total_ticks += diff;
//}
//
//ProfilerHelper::ProfilerHelper( std::wstring_view function_name )
//: _functionName{ function_name }
//, _ticks       { 0             }
//{
//    ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &_ticks ) );
//}
//
//ProfilerHelper::~ProfilerHelper()
//{
//    long long prev_tick = _ticks;
//    ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &_ticks ) );
//
//    tick diff = _ticks - prev_tick;
//
//    LThreadProfiler.Add( _functionName, diff );
//}
//    
}