module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

#include <Psapi.h>
#include <pdh.h>
#pragma comment( lib, "pdh" )

#include "../MalaMacro.h"

export module Mala.Diagnostics.PerformanceCounter;

import <iostream>;
import <vector>;
import <array>;
import <string>;
import <string_view>;

export namespace Mala::Diagnostics
{

/// <summary>
/// Ref: https://learn.microsoft.com/ko-kr/windows/win32/perfctrs/collecting-performance-data
/// </summary>
class PerformanceCounter
{
	enum class QueryType
	{
		CpuTotalUsage,            //< cpu 사용률
		SystemCallPerSec,         //< 초당 시스템콜
		ConetextSwitchPerSec,     //< 초당 컨텍스트 스위칭 발생 횟수
		
		AvailableMbytes,
		PoolNonpagedBytes,        //< 논페이지드 메모리풀의 크기
		PoolPagedBytes,           //< 
		PrivateBytes,             //< 프로세스 유저 할당 메모리
		WorkingSet,               //< 프로세스 작업 메모리
		PrivilegedTime,           //< 커널 타임
		UserTime,				  //< 유저 타임
		PrecessorTime, 		      //< 프로세서 사용률

		BytesReceivedPerSec,      //< 초당 수신
		BytesSentPerSec,          //< 초당 송신

		//DiskFreeSapceRate,      /// 사용 가능한 공간 비율

		Max
	};

	static constexpr int Max = (int)( QueryType::Max );

	using HQueries      = std::array< PDH_HQUERY, Max >;
	using HCounters     = std::array< PDH_HCOUNTER, Max >;
	using CounterValues = std::array< PDH_FMT_COUNTERVALUE, Max >;

	struct Context
	{
			  int               _fmt;
		      std::wstring		_path;
		const std::wstring_view _forDebug;
	};

	// PDH 추가시 직접 추가할 부분
	std::array< Context, Max > _pdhs
	{
		/// Processor
		Context{ PDH_FMT_DOUBLE, L"\\Processor(_Total)\\% Processor Time", L"Precessor Time" }, // 프로세서 전체 사용률

		/// System
		Context{ PDH_FMT_LARGE,  L"\\System\\System Calls/sec"            , L"System Calls/sec" }, // 초당 시스템콜 수
		Context{ PDH_FMT_LARGE,  L"\\System\\Context Switches/sec"        , L"Context Switch/sec" }, // 초당 컨텍스트 스위칭 횟수

		/// Memory
		Context{ PDH_FMT_LARGE, L"\\Memory\\Available MBytes", L"vailable MBytes" }, // 

		/// Process
		Context{ PDH_FMT_LARGE,  L"\\Process({})\\Pool Nonpaged Bytes", L"Pool Nonpaged Bytes" }, // 논페이지드 메모리 사용량
		Context{ PDH_FMT_LARGE,  L"\\Process({})\\Pool Paged Bytes"   , L"Pool Paged Bytes"    }, // 페이지드 메모리 사용량
		Context{ PDH_FMT_LARGE,  L"\\Process({})\\Private Bytes"      , L"Private Bytes"       }, // 프로세스 유저 할당 메모리
		Context{ PDH_FMT_LARGE,  L"\\Process({})\\Working Set"        , L"Working Set"         }, // 프로세스 작업 메모리, 물리 메모리에 사용 되는
		Context{ PDH_FMT_DOUBLE, L"\\Process({})\\% Privileged Time"  , L"Privileged Time"     },
		Context{ PDH_FMT_DOUBLE, L"\\Process({})\\% User Time"        , L"User Time"           },
		Context{ PDH_FMT_DOUBLE, L"\\Process({})\\% Processor Time"   , L"Processor Time"      },

		/// Process V2( 동일 프로세스 네임에 대해 PDH 쿼리가 필요한 경우 PID로 구분 )

		// NIC
		Context{ PDH_FMT_LARGE, L"\\Network Interface(*)\\Bytes Received/sec", L"Bytes Received/sec" },
		Context{ PDH_FMT_LARGE, L"\\Network Interface(*)\\Bytes Sent/sec"    , L"Bytes Sent/sec"     },
		// context{ PDH_FMT_DOUBLE, L"\\Process(Core)\\% Privilged Time"     },

		/* Thread
		L"\\Process(Core)\\Thread Count"		*/
	};

public:
	PerformanceCounter() = default;

	void Init( const std::wstring& processName )
	{
		_processName = processName;

		_nativeHandle = GetCurrentProcess();

		PrepareCpuInfo();
		PrepareMemory();
		PreparePdh();

		MakeStaticQuery();
	}

	void Refresh()
	{
		/// 전체 메모리 정보
		GlobalMemoryStatusEx( &_ms );

		/// 해당 프로세스 메모리 정보
		GetProcessMemoryInfo( GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&_pmc, sizeof( _pmc ) );
	}

	long long GetNonpagedSystemMemorySize64()
	{
		return GetValue< long long, true >( QueryType::PoolNonpagedBytes );
	}

	float GetNonpagedSystemMemorySize64AsMb()
	{
		return GetNonpagedSystemMemorySize64() / 1024.0 / 1024.0;
	}

	float GetWorkingSet64AsMb()
	{
		return GetWorkingSet64() / 1024.0 / 1024.0;
	}

	long GetNonpagedSystemMemorySize()
	{
		return GetValue< long, true >( QueryType::PoolNonpagedBytes );
	}

	long long GetWorkingSet64()
	{
		return GetValue< long long, false >( QueryType::WorkingSet );
	}

	long GetWorkingSet()
	{
		return GetValue< long, false >( QueryType::WorkingSet );
	}

	long GetContextSwitch()
	{
		return GetValue< long, false >( QueryType::ConetextSwitchPerSec );
	}

	long long GetSystemCallPerSec()
	{
		return GetValue< long long >( QueryType::SystemCallPerSec );
	}

	double GetCpuTotalUsage()
	{
		return GetValue< double >( QueryType::CpuTotalUsage );
	}

	double GetPrecessorTime()
	{
		return GetValue< double >( QueryType::PrecessorTime );
	}

	double GetUserTime()
	{
		return GetValue< double >( QueryType::UserTime );
	}

	double GetPrivilegedTime()
	{
		return GetValue< double >( QueryType::PrivilegedTime );
	}

	double GetKernelTime()
	{
		return GetValue< double >( QueryType::PrivilegedTime );
	}

	long long GetBytesReceivedPerSec()
	{
		return GetValue< long long >( QueryType::BytesReceivedPerSec );
	}

	long long GetBytesSentPerSec()
	{
		return GetValue< long long >( QueryType::BytesSentPerSec );
	}

private:
	void PrepareCpuInfo()
	{
		SYSTEM_INFO system_info;
		GetSystemInfo( &system_info );

		_concurrent_thread_count = system_info.dwNumberOfProcessors;

		HKEY handleKey;
		wchar_t cupName[ 255 ]{}; // processorName
		DWORD cname_size;
		DWORD cspeedSize;
		DWORD cupSpeed;	//

		RegOpenKeyExW( HKEY_LOCAL_MACHINE, L"Hardware\\Description\\System\\CentralProcessor\\0", 0, KEY_QUERY_VALUE, &handleKey );
		RegQueryValueExW( handleKey, L"ProcessorNameString", NULL, NULL, (LPBYTE)cupName, &cname_size );
		RegQueryValueExW( handleKey, L"~MHz", NULL, NULL, (LPBYTE)&cupSpeed, &cspeedSize ); // CPU Speed
		RegCloseKey( handleKey );
	}

	void PrepareMemory()
	{
		// process memory 
		GetProcessMemoryInfo( GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&_pmc, sizeof( _pmc ) );

		/// 전체 가상 메모리 정보
		_ms.dwLength = sizeof( MEMORYSTATUSEX );
		GlobalMemoryStatusEx( &_ms );
	}

	void PreparePdh()
	{
		std::wstring fmt( L"{}" );

		for ( int i = 0; i < Max; ++i )
		{
			// 쿼리 객체 초기화
			PdhOpenQuery( NULL, NULL, &_queries[ i ] );

			std::wstring path( _pdhs[ i ]._path );
			if ( auto pos = path.find( L"{}" ); std::wstring_view::npos != pos )
			{
				std::wstring newPath( path.data() );
				std::wstring processName( _processName.begin(), _processName.end() );

				newPath.replace( newPath.find( fmt ), fmt.size(), processName );
				newPath.push_back( NULL );

				path = std::move( newPath );

				_pdhs[ i ]._path = path;
			}

			// 바인드
			PdhAddCounterW( _queries[ i ], path.data(), NULL, &_counters[ i ] );

			// 최수 데이터 정보 수집
			PDH_FUNCTION result = ::PdhCollectQueryData( _queries[ i ] );
			if ( ERROR_SUCCESS != result )
				CORE_LOG( "%ws index : %d, os error : %d [path: %ws]", _pdhs[ i ]._forDebug.data(), i, GetLastError(), path.data() );

			// MSDN 참고시, ::PdhCollectQueryData() 2회를 호출해야 쿼리 타입이 있어 전부 2회씩 호출
			result = ::PdhCollectQueryData( _queries[ i ] );
			if( ERROR_SUCCESS != result )
				CORE_LOG( "%ws index : %d, os error : %d [path: %ws]", _pdhs[ i ]._forDebug.data(), i, GetLastError(), path.data() );

			// 값 변환
			::PdhGetFormattedCounterValue( _counters[ i ], _pdhs[ i ]._fmt, nullptr, &_values[ i ] );
		}
	}

	bool MakeStaticQuery()
	{
		/// 쿼리 유효성 검증 return 0이면 정상
		for ( int i = 0; i < Max; ++i )
		{
			if ( ERROR_SUCCESS != PdhValidatePathW( _pdhs[ i ]._path.data() ) )
			{
				printf( "cur Idx : %d Get Lset Error : %d, %ws", i, GetLastError(), _pdhs[ i ]._path.data() );
				return false;
			}
		}

		return true;
	}

	template< typename T, bool UseCollect = true >
	auto GetValue( QueryType queryType )
	{
		int i = static_cast< int >( queryType );
	
		if constexpr( UseCollect )
		{
			if( ERROR_SUCCESS != ::PdhCollectQueryData( _queries[ i ] ) )
				CORE_LOG( "%ws index : %d, os error : %d", _pdhs[ i ]._forDebug.data(), i, GetLastError() );
		}
	
		::PdhGetFormattedCounterValue( _counters[ i ], _pdhs[ i ]._fmt, nullptr, &_values[ i ] );
	
		if constexpr( std::is_same_v< T, double > )
			return _values[ i ].doubleValue;
		else if constexpr( std::is_same_v< T, long > )
			return _values[ i ].longValue;
		else// if constexpr( std::is_same_v< T, long long > )
			return _values[ i ].largeValue;
		//return _values[ i ].doubleValue;
	}

private:
	MEMORYSTATUSEX             _ms;
	PROCESS_MEMORY_COUNTERS_EX _pmc;
	HANDLE                     _nativeHandle;
	HQueries                   _queries;
	HCounters                  _counters;
	CounterValues              _values;
	std::wstring               _processName;
	int			               _concurrent_thread_count;
};

} // namespace Mala::diagnostics
