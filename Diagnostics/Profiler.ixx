export module Mala.Diagnostics.Profiler;

#include "../MalaMacro.h";
import <string_view>;
import <unordered_map>;

import Mala.Container.WaitFreeQueue;
import Mala.Core.Delegate;
import Mala.Core.Types;

NAMESPACE_BEGIN( Mala::Diagnostics )

///// <summary>
///// 전방 선언
///// </summary>
//
///// <summary>
///// 프로파일링 기록 구조체
///// </summary>
//class ProfilerRecord
//{
//public:
//    std::wstring_view _name;        //< 함수명
//    usize             _counter;     //< 실행 횟수
//    usize             _total_ticks; //< 실행 시간
//};
//
///// <summary>
///// 
///// </summary>
//class ThreadProfiler;
//
///// <summary>
///// 
///// </summary>
//class CentralProfiler
//{
//    // 스레드 세이프하게 스레드별 기록을 접근하기 위한 컨테이너
//    using ThreadProfilers = Mala::Container::WaitFreeQueue< ThreadProfiler* >;
//
//    // 레코드 기록 맵
//    using RecordMap = std::unordered_map< std::wstring_view, ProfilerRecord >;
//
//    // 출력용 델리게이트
//    using on_log = Mala::Core::Delegate< void, ProfilerRecord& >;
//
//public:
//    // 통합한다.
//    void Merge();
//
//    /// <summary>
//    /// 초기상태로 되돌린다.
//    /// </summary>
//    void Reset();
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void ResetAndMerge();
//
//    /// <summary>
//    /// 로컬 프로파일러를 등록한다
//    /// </summary>
//    /// <param name="profiler">로컬 프로파일러의 포인터</param>
//    void Register( ThreadProfiler* profiler );
//    
//    /// <summary>
//    /// 기록한다
//    /// </summary>
//    void Log();
//
//    on_log          OnLog;            //< 로깅 
//
//private:
//    ThreadProfilers _threadProfilers; //< 스레드별 프로파일러
//    RecordMap       _records;         //< 기록
//};
//
//using Profiler = CentralProfiler;
//
//// extern inline CentralProfiler GProfiler{};
//
///// <summary>
///// 스레드 프로파일러
///// </summary>
//class ThreadProfiler
//{
//    /// <summary>
//    /// 
//    /// </summary>
//    friend class CentralProfiler;
//
//    // 
//    using RecordMap  = std::unordered_map< std::wstring_view, ProfilerRecord >; 
//
//    // central profiler에서 락 없이 thread-safe하게 레코드에 접근하기 위한 컨테이너
//    using RecordList = Mala::Container::WaitFreeQueue< ProfilerRecord* >;
//
//public:
//    /// <summary>
//    /// 생성자
//    /// </summary>
//    ThreadProfiler();
//    ThreadProfiler( const ThreadProfiler& other ) = delete;
//    ThreadProfiler( ThreadProfiler&& other ) = delete;
//
//    /// <summary>
//    /// 소멸자
//    /// </summary>
//    ~ThreadProfiler() 
//    {
//
//    }//= default; 
//
//    /// <summary>
//    /// 
//    /// </summary>
//    void Add( const std::wstring_view& name, long long tick );
//
//private:
//    id64       _id;         //< 식별자
//    RecordMap  _records;    //< 
//    RecordList _recordList; //< 레코드 목록
//};
//
//// inline thread_local ThreadProfiler LThreadProfiler{};
//
///// <summary>
///// 
///// </summary>
//class ProfilerHelper
//{
//    using tick = long long;
//
//public:
//    /// <summary>
//    /// 생성자
//    /// </summary>
//    ProfilerHelper( std::wstring_view function_name );
//
//    /// <summary>
//    /// 소멸자
//    /// </summary>
//    ~ProfilerHelper();
//
//private:
//    std::wstring_view _functionName; //< 함수 이름
//    tick              _ticks;        //< 시간 시간 틱( nano second )
//};

NAMESPACE_END( Mala::Diagnostics )
