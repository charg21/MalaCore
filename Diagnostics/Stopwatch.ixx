module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include "../MalaMacro.h";
#include <Windows.h>

export module Mala.Diagnostics.Stopwatch;

import Mala.Core.Crash;
import Mala.Core.TimeSpan;

NAMESPACE_BEGIN( Mala::Diagnostics )

class Stopwatch
{
    // https://www.sysnet.pe.kr/2/0/13035
    //struct frequency_initializer
    //{
    //    frequency_initializer()
    //    {
    //        Mala::core::Assert_If_False( QueryPerformanceFrequency( (LARGE_INTEGER*)&stopwatch::_frequency ) );

    //        stopwatch::_frequency = 1000'0000;

    //        stopwatch::_ms_frequency     = _frequency / 1000;
    //        stopwatch::_us_frequency     = _frequency / 100'0000;
    //        stopwatch::_ns_100_frequency = _frequency / 1000'0000;
    //    }
    //};

public:
    /// <summary>
    /// 생성자
    /// </summary>
	Stopwatch() 
	{
        //static frequency_initializer init_once;
        Reset();
	}

    /// <summary>
    /// 초기 상태로 되돌린다
    /// </summary>
    void Reset()
    {
        _started = false;

        _elapsed_counter = 0;
    }

    /// <summary>
    /// 시작한다
    /// </summary>
    void Start()
    {
        if ( _started )
            return;

        _started = true;

        ::QueryPerformanceCounter( (LARGE_INTEGER*)&_current_counter );
    }

    /// <summary>
    /// 재시작한다
    /// </summary>
    void Restart()
    {
        Reset();
        Start();
    }

    /// <summary>
    /// 갱신한다
    /// </summary>
    void Update()
    {
        const auto prev_counter = _current_counter;

        ::QueryPerformanceCounter( (LARGE_INTEGER*)&_current_counter );

        _elapsed_counter += ( _current_counter - prev_counter );
    }

    /// <summary>
    /// 정지한다
    /// </summary>
    void Stop()
    {
        if ( !_started )
            return;

        _started = false;

        Update();
    }

    /// <summary>
    /// 블럭킹 상태로 멈춘다
    /// </summary>
    void Sleep( const int sleepTime )
    {
        Sleep( sleepTime );
    }

    /// <summary>
    /// 지나간 시간 간격을 구한다
    /// </summary>
    const Mala::Core::TimeSpan GetElapsed()
    {
        if ( _started )
            Update();

        return _elapsed_counter;
    }

    /// <summary>
    /// 지나간 시간 틱을 구한다
    /// </summary>
    constexpr unsigned long long GetElapsedTicks()
    {
        if ( _started )
            Update();

        return _elapsed_counter;
    }

    /// <summary>
    /// 지나간 시간 초를 구한다
    /// </summary>
	constexpr size_t GetElapsedSeconds()
	{
        return GetElapsedTicks() / _frequency;
	}

    /// <summary>
    /// 지나간 시간 밀리초를 구한다
    /// </summary>
	constexpr size_t GetElapsedMilliSeconds()
	{
        return GetElapsedTicks() / _ms_frequency;
	}

    /// <summary>
    /// 지나간 시간 마이크로초를 구한다
    /// </summary>
	constexpr size_t GetElapsedMicroSeconds()
	{
        return GetElapsedTicks() / _us_frequency;
	}

    /// <summary>
    /// 지나간 시간 나노초를 구한다
    /// </summary>
	constexpr float GetElapsedNanoseconds()
	{
        return GetElapsedTicks() / _ns_100_frequency;
	}

public:
    bool               _started;         // 시작 여부
    unsigned long long _current_counter; // 현재 틱 카운터
    unsigned long long _elapsed_counter; // 마지막 틱 카운터

    inline static constexpr __int64 _frequency       { 1000'0000              };
    inline static constexpr __int64 _ms_frequency    { _frequency / 1000      };
    inline static constexpr __int64 _us_frequency    { _frequency / 100'0000  };
    inline static constexpr __int64 _ns_100_frequency{ _frequency / 1000'0000 };
};

NAMESPACE_END( Mala::Diagnostics )
