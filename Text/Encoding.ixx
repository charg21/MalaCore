module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

export module Mala.Text;


import <type_traits>;
import <string>;
import <string_view>;
import Mala.Container;

using namespace std;

export 
{

enum class EEncoding
{
    Utf8,
    Utf16,

    Max
};

namespace Mala::Text::Utf8
{
    String ToUtf16( const char* utf8, size_t stringLength )
    {
        if ( stringLength == 0 )
			return L"";

        int ret = ::MultiByteToWideChar( CP_UTF8, 0, utf8, stringLength, nullptr, 0 );
        if ( 0 == ret )
            return L"";

        String utf16;
        utf16.resize( ret );

        ret = ::MultiByteToWideChar( CP_UTF8, 0, utf8, stringLength, (wchar_t*)utf16.c_str(), ret );

        return std::move( utf16 );
    }

    String ToUtf16( const std::string& utf8 )
    {
        return ToUtf16( utf8.c_str(), utf8.size() );
    }

    String ToUtf16( const char* utf8 )
    {
        return ToUtf16( utf8, strlen( utf8 ) );
    }
}

namespace Mala::Text::Utf16
{
    std::string ToUtf8( const wchar_t* utf16, size_t wstringLength )
    {
        if ( wstringLength == 0 )
            return "";

        int ret = ::WideCharToMultiByte( CP_UTF8, 0, utf16, wstringLength, nullptr, 0, nullptr, nullptr );
        if ( 0 == ret )
            return "";

        std::string utf8;
        utf8.resize( ret );

        ::WideCharToMultiByte( CP_UTF8, 0, utf16, wstringLength, (char*)utf8.c_str(), ret, nullptr, nullptr );

        return std::move( utf8 );
    }

    std::string ToUtf8( StringRef utf16 )
    {
        return ToUtf8( utf16.c_str(), utf16.size() );
    }

    std::string ToUtf8( const wchar_t* utf16 )
    {
        return ToUtf8( utf16, wcslen( utf16 ) );
    }
}


} // namespace Mala::core