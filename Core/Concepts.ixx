module;

import <memory>;
import <iterator>;
import <array>;
export import <concepts>;
export import <type_traits>;

export module Mala.Core.Concepts;
import Mala.Core.TypeTraits;

export
{

template< typename T >
concept IsStdArray = requires( T a ) 
{
    typename T::value_type;

    requires std::is_same_v< T, std::array< typename T::value_type, std::tuple_size_v< T > > >;
};

//template< typename T >
//concept ArrayType = std::is_same_v< T, std::array< typename T::value_type, T::size() > >;
template< typename T >
concept SharedPtrType = std::is_same_v< T, std::shared_ptr< typename T::element_type > >;
template< typename T >
concept WeakPtrType = std::is_same_v< T, std::weak_ptr< typename T::element_type > >;
template< typename T >
concept UniquePtrType = std::is_same_v< T, std::unique_ptr< typename T::element_type > >;
template< typename T >
concept EnumType = std::is_enum_v< T >;
template< typename T >
concept DefaultValueType = IsDefaultTypeV< T >;

template< typename T >
concept HasMax = requires( T t ) 
{ 
    T::Max; 
};

template< typename T >
concept Iterable = requires( T a ) 
{
    { std::begin( a ) } -> std::input_or_output_iterator;
    { std::end( a ) } -> std::input_or_output_iterator;
};

}

template< typename TLockable >
concept Lockable = requires( TLockable loackbale )
{
    loackbale.Lock();
    loackbale.Unlock();
};

template< typename TRunner >
concept Runnable = requires( TRunner runner )
{
    runner.Run();
};

template< typename TAsyncRunner >
concept AsyncRunnable = requires( TAsyncRunner runner )
{
    runner.RunAsync();
};