export module Mala.Core.Random;

import Mala.Core.Types;

export namespace Mala::Core
{
    
/// <summary>
/// 간단한 연산으로 난수를 반환한다
/// </summary>
usize FastRand()
{
    static alignas( 64 ) usize seed = 'fast';

    seed = 214013 * seed + 2531011;

    return ( seed >> 16 ) & 0x7FFF;
}

}