export module Mala.Core.TimeSpan;

import Mala.Core.Types;

export namespace Mala::Core
{

/// <summary>
/// 시간 간격을 나타내는 구조체
/// </summary>
struct TimeSpan
{
    static constexpr usize TICK_PER_100NS{ 1 };

    static constexpr usize TICK_PER_US   { TICK_PER_100NS * 10    }; 
    static constexpr usize TICK_PER_MS   { TICK_PER_US    * 1000  };
    static constexpr usize TICK_PER_SEC  { TICK_PER_MS    * 1000  };

    static constexpr usize TICK_PER_MIN  { TICK_PER_SEC  * 60 };
    static constexpr usize TICK_PER_HOUR { TICK_PER_MIN  * 60 };
    static constexpr usize TICK_PER_DAY  { TICK_PER_HOUR * 24 };


    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="tick"> 시간 틱( ms ) </param>
    TimeSpan( const usize tick = 0 )
    : _ticks{ tick }
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="hours">   시간 </param>
    /// <param name="mins">    분 </param>
    /// <param name="seconds"> 초 </param>
    TimeSpan( i32 hours, i32 mins, i32 seconds )
    {
        usize totalTick = ( hours * TICK_PER_HOUR );

        totalTick += ( mins    * TICK_PER_MIN );
        totalTick += ( seconds * TICK_PER_SEC );

        _ticks = totalTick;
    }

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="hours"> 일 </param>
    /// <param name="hours"> 시간 </param>
    /// <param name="mins"> 분 </param>
    /// <param name="seconds"> 초 </param>
    /// <param name="miliseconds"> 초 </param>
    TimeSpan( i32 days, i32 hours, i32 mins, i32 seconds, i32 miliseconds )
    {
        usize totalTick = ( days * TICK_PER_DAY );

        totalTick += ( hours       * TICK_PER_HOUR );
        totalTick += ( mins        * TICK_PER_MIN  );
        totalTick += ( seconds     * TICK_PER_SEC  );
        totalTick += ( miliseconds * TICK_PER_MS   );

        _ticks = totalTick;
    }

#pragma region Get Methods

    /// <summary>
    /// 일수에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetDays() const
    {
        return _ticks / TICK_PER_DAY;
    }

    /// <summary>
    /// 시간에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetHours() const
    {
        const usize hour_tick = _ticks % TICK_PER_DAY;

        return hour_tick / TICK_PER_HOUR;
    }

    /// <summary>
    /// 분에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetMinutes() const
    {
        const usize min_tick = _ticks % TICK_PER_HOUR;

        return min_tick / TICK_PER_MIN;
    }

    /// <summary>
    /// 초에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetSeconds() const
    {
        const usize sec_tick = _ticks % TICK_PER_MIN;

        return sec_tick / TICK_PER_SEC;
    }

    /// <summary>
    /// 밀리초에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetMilliseconds() const
    {
        const usize msTick = _ticks % TICK_PER_SEC;

        return msTick / TICK_PER_MS;
    }

    /// <summary>
    /// 마이크로초에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetMicroseconds() const
    {
        const usize us_tick = _ticks % TICK_PER_MS;

        return us_tick / TICK_PER_US;
    }

    /// <summary>
    /// 나노초에 해당하는 부분을 반환한다
    /// </summary>
    i32 GetNanoseconds() const
    {
        const usize ns_tick = _ticks % TICK_PER_US;

        return ns_tick / TICK_PER_US * 100;
    }
#pragma endregion 

#pragma region GetTotal Methods

    /// <summary>
    /// 전체 일수를 반환한다
    /// </summary>
    i32 GetTotalDays() const
    {
        return _ticks / TICK_PER_DAY;
    }

    /// <summary>
    /// 전체 시간을 반환한다
    /// </summary>
    i32 GetTotalHours() const
    {
        return _ticks / TICK_PER_HOUR;
    }

    /// <summary>
    /// 전체 분을 반환한다
    /// </summary>
    i32 GetTotalMinutes() const
    {
        return _ticks / TICK_PER_MIN;
    }

    /// <summary>
    /// 전체 초를 반환한다
    /// </summary>
    i32 GetTotalSeconds() const
    {
        return _ticks / TICK_PER_SEC;
    }

    /// <summary>
    /// 전체 밀리초를 반환한다
    /// </summary>
    i32 GetTotalMilliseconds() const
    {
        return _ticks / TICK_PER_MS;
    }

    /// <summary>
    /// 전체 마이크로초를 반환한다
    /// </summary>
    i32 GetTotalMicroseconds() const
    {
        return _ticks / TICK_PER_US;
    }

    /// <summary>
    /// 전체 나노초를 반환한다
    /// </summary>
    i32 GetTotalNanoseconds() const
    {
        return _ticks / TICK_PER_US * 100;
    }

#pragma endregion 

    const bool operator==( const TimeSpan& rhs ) const
    {
        return _ticks == rhs._ticks;
    }

    const bool operator!=( const TimeSpan& rhs ) const
    {
        return _ticks != rhs._ticks;
    }

    const bool operator<=( const TimeSpan& rhs ) const
    {
        return _ticks <= rhs._ticks;
    }

    const bool operator<( const TimeSpan& rhs ) const
    {
        return _ticks < rhs._ticks;
    }

    const bool operator>=( const TimeSpan& rhs ) const
    {
        return _ticks >= rhs._ticks;
    }

    const bool operator>( const TimeSpan& rhs ) const
    {
        return _ticks > rhs._ticks;
    }

    TimeSpan& operator+=( const TimeSpan& rhs )
    {
        _ticks += rhs._ticks;

        return *this;
    }

    TimeSpan operator+( const TimeSpan& rhs ) const
    {
        return _ticks + rhs._ticks;
    }

    TimeSpan& operator-=( const TimeSpan& rhs )
    {
        _ticks -= rhs._ticks;

        return *this;
    }

    TimeSpan operator-( const TimeSpan& rhs ) const
    {
        return _ticks - rhs._ticks;
    }

    static TimeSpan FromTicks( usize ticks )
    {
        return TimeSpan{ ticks };
    }

    static TimeSpan FromHours( i32 hours )
    {
        return TimeSpan{ hours, 0, 0 };
    }

    static TimeSpan FromMinutes( i32 mins )
    {
        return TimeSpan{ 0, mins, 0 };
    }

    static TimeSpan FromSeconds( i32 secs )
    {
        return TimeSpan{ 0, 0, secs };
    }

    usize _ticks;
};

bool operator==( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks == rhs._ticks;
}

bool operator!=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks != rhs._ticks;
}

bool operator<=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks <= rhs._ticks;
}

bool operator<( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks < rhs._ticks;
}

bool operator>=( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks >= rhs._ticks;
}

bool operator>( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks > rhs._ticks;
}

TimeSpan operator+( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks + rhs._ticks;
}

TimeSpan operator-( const TimeSpan& lhs, const TimeSpan& rhs )
{
    return lhs._ticks - rhs._ticks;
}

inline TimeSpan operator""_day( usize day )
{
    return TimeSpan{ (i32)( day ), 0, 0, 0, 0 };
}

inline TimeSpan operator""_hour( usize hour )
{
    return TimeSpan{ (i32)( hour ), 0, 0 };
}

inline TimeSpan operator""_min( usize min )
{
    return TimeSpan{ 0, (i32)( min ), 0 };
}

inline TimeSpan operator""_sec( usize sec )
{
    return TimeSpan{ 0, 0, (i32)( sec ) };
}

}

