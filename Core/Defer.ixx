export module Mala.Core.Defer;

import <memory>;

export namespace Mala::Core
{

/// <summary>
/// 실행을 지연 시키는 객체
/// 현재 스코프의 마지막에 실행된다
/// </summary>
template< typename TJob >
class Defer
{
    static_assert( std::is_invocable< TJob >::value, "T is non functional in defer" );

public:
    Defer( TJob&& job )
    : _job { std::move( job ) }
    {
    }

    Defer( const TJob& job )
    : _job{ job }
    {
    }

    Defer( const Defer& other )     = delete;        
    Defer( Defer&& other ) noexcept = delete;

    ~Defer()
    {
        _job();
    }

private:
    TJob _job;
};

/// <summary>
/// 스레드간 작업 실행을 지연 시키는 객체
/// 최종적으로 객체가 소멸 되는 시점에 작업이 실행된다
/// </summary>
template< typename TJob >
class SharedDefer
{
    static_assert( std::is_invocable< TJob >::value, "T is non functional in shared defer" );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    SharedDefer( const TJob& job )
    : _job{ nullptr, [ job ]( void* p ){ job(); } }
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    SharedDefer( TJob&& job )
    : _job{ nullptr, [ job = std::move( job ) ]( void* p ) { job(); }}
    {
    }
    
private:
    std::shared_ptr< void > _job;
};

} // namespace Mala::Core