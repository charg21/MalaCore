export module Mala.Core.Counter;

import <atomic>;
import Mala.Core.Types;

using namespace std;

namespace Mala::Core
{

/// <summary>
/// 카운터 인터페이스를 가진 객체
/// </summary>
template< typename TCounter >
concept ICounter = requires( TCounter counter )
{
    counter.Increment();
    counter.Decrement();
    counter.Add();
    counter.Sub();
    counter.GetValue();
};

struct NullCounter
{
    isize Increment()
    {
        return 0;
    }

    isize Decrement()
    {
        return 0;
    }

    void Add( isize value )
    {

    }

    void Sub( isize value )
    {

    }

    isize GetValue()
    {
        return 0;
    }
};

struct Counter
{
    isize Increment()
    {
        return ++_value;
    }
    
    isize Decrement()
    {
        return --_value;
    }

    void Add( isize value )
    {
        _value += value;
    }

    void Sub( isize value )
    {
        _value -= value;
    }
    
    isize GetValue()
    {
        return _value;
    }

    isize _value{};
};

struct AtomicCounter
{
    isize Increment()
    {
        return ++_value;
    }

    isize Decrement()
    {
        return --_value;
    }

    void Add( isize value )
    {
        _value += value;
    }

    void Sub( isize value )
    {
        _value -= value;
    }
    
    isize GetValue()
    {
        return _value;
    }

    atomic< isize > _value{};
};

}