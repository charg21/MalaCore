export module Mala.Core.IniParser;

import <string>;
import <string_view>;
import <cwctype>;
import <clocale>;

import Mala.Io.File;
import Mala.Container.String;

export namespace Mala::Core
{
    class IniParser
    {
        inline static constexpr size_t CharSize = sizeof( wchar_t );

        enum InvalidCharacter : char
        {
            Tab            = 0x09, //'	'
            LineFeed       = 0x0A, // '\n'
            CarriageReturn = 0x0D, // '\r'
            Space          = 0x20, // ' '
        };

    public:
        IniParser( const std::wstring_view& fileName );
        IniParser( StringRef fileName );
        IniParser() = default;
        ~IniParser();

        bool LoadFile( const std::wstring_view& fileName );

        template< typename T >
        bool TryGet( const std::wstring_view& fieldName, T& out_str, T defaultValue );

    private:
        template< typename T >
        void _assign_value( T& out_param, wchar_t* value_word );
        // skip invalid character
        bool SkipInvalidChar( wchar_t** bufferPtr );
        // skip valid character
        bool SkipWord( wchar_t** bufferPtr );
        // skip current line
        bool SkipCurrentLine( wchar_t** bufferPtr );
        // skip current line -> find '\r'
        bool SkipMultipleLinesAnnotation( wchar_t** bufferPtr );
        bool SkipAnnotation( wchar_t** bufferPtr );
        bool skip_and_extract_word_if_find_word( wchar_t** buffer_ptr, wchar_t* out_wordBuffer, size_t out_buf_capacity );
        void Crash();
        bool is_invalid_char( char c );
        

    private:
        wchar_t*     buffer{};
        wchar_t*     _bufferEnd{};
        size_t       _bufferLength{};
        std::wstring _text;
    };

    template<>
    inline void IniParser::_assign_value< float >( float& out_param, wchar_t* value_word )
    {
        out_param = _wtof( value_word );
    }

    template<>
    inline void IniParser::_assign_value< double >( double& out_param, wchar_t* value_word )
    {
        out_param = _wtof( value_word );
    }

    template< typename T >
    inline void IniParser::_assign_value( T& outParam, wchar_t* value_word )
    {
        static_assert( std::numeric_limits< T >::is_integer, "T type is must integer type" );

        if constexpr ( true == std::numeric_limits< T >::is_signed )
            outParam = wcstof( value_word, nullptr );
        else
            outParam = wcstol( value_word, nullptr, 10 );
    }

    template<>
    inline void IniParser::_assign_value< bool >( bool& out_param, wchar_t* value_word )
    {
        // normalize
        // Example ) TRUE -> true
        // Example ) TrUE -> true
        // Example ) TruE -> true
        std::wstring value_copy = value_word;
        for ( wchar_t& c : value_copy )
            c = towlower( c );
        if ( 0 == wcscmp( L"false", value_word ) )
            out_param = false;
        else if ( 0 == wcscmp( L"true", value_word ) )
            out_param = true;
    }

    template<>
    inline void IniParser::_assign_value< String >( String& outParam, wchar_t* value_word )
    {
        outParam = ( value_word + 1 ); // "str"로 들어오기 떄문에 버퍼 +1로  str"
        outParam.pop_back();           // str" 에서 한글자 뺴기 str
    }

    template< typename T >
    inline bool IniParser::TryGet( const std::wstring_view& fieldName, T& out, T defaultValue )
    {
        wchar_t* curBuffer { buffer };
        wchar_t  wordBuffer[ 512 ] {};

        for ( ;; )
        {
            do
            {
                // 필드 명 찾고 추출
                if ( !skip_and_extract_word_if_find_word( &curBuffer, wordBuffer, sizeof( wordBuffer ) ) )
                    break;

                // 필드명이 맞는지 확인
                int forDebug = wcscmp( wordBuffer, fieldName.data() );
                if ( 0 != forDebug )//wcscmp( wordBuffer, fieldName.data() ) )
                    break;

                // 	'=' 를 찾음.
                if ( !skip_and_extract_word_if_find_word( &curBuffer, wordBuffer, sizeof( wordBuffer ) ) )
                    break;

                if ( L'=' != wordBuffer[ 0 ] )
                    break;

                // value 탐색;
                if ( !skip_and_extract_word_if_find_word( &curBuffer, wordBuffer, sizeof( wordBuffer ) ) )
                    break;

                _assign_value< T >( out, wordBuffer );

                return true;

            } while ( false );

            if ( !SkipCurrentLine( &curBuffer ) )
            {
                out = defaultValue;

                return false;
            }
        }

        out = defaultValue;

        return false;
    }

    IniParser::IniParser( const std::wstring_view& fileName /* = nullptr */ )
    : buffer        {}
    , _bufferEnd    {}
    , _bufferLength {}
    {
        if ( !fileName.empty() )
            LoadFile( fileName );
    }

    IniParser::IniParser( StringRef fileName )
    : buffer       {}
    , _bufferEnd   {}
    , _bufferLength{}
    {
        if( !fileName.empty() )
            LoadFile( fileName.c_str() );
    }

    IniParser::~IniParser()
    {
        //if ( nullptr != buffer )
        //    delete[] buffer;
    }

    bool IniParser::LoadFile( const std::wstring_view& fileName )
    {
        _text = Mala::Io::File::ReadAllText( fileName );

        _bufferLength = _text.length();

        // 파일의 내용을 읽어 옴.
        buffer                  = _text.data();//new wchar_t[ buffer_length + 1 ];
        _bufferEnd              = &buffer[ _bufferLength ];
        buffer[ _bufferLength ] = Space;

        return true;
    }

    bool IniParser::SkipInvalidChar( wchar_t** buffer_ptr )
    {
        wchar_t* curBuffer = *buffer_ptr;

        for ( ;; )
        {
            // 유효하지 않은 문자 찾기.
            const char cur_char = *curBuffer;
            if ( true == is_invalid_char( cur_char ) )
            {
                curBuffer += 1;
            }
            else if ( '/' == cur_char ) // '/' 주석의 시작 찾음. 주석이 있나.
            {
                const char next_char = curBuffer[ 1 ];
                if ( '/' == next_char ) // "//" 주석인지
                {
                    if ( !SkipCurrentLine( &curBuffer ) )
                        return false;
                }
                // "/*"주석인지 확인해야함.
                else if ( '*' == next_char )
                {
                    if ( !SkipMultipleLinesAnnotation( &curBuffer ) )
                        return false;
                }
                else //  '/' 이거 하나만 달랑 있는 경우  // 문법 문제 assrtion
                {
                    const wchar_t* msg = L"Invalid Syntax!";
                    Crash();
                }
            }
            else
            {
                break;
            }

            if ( curBuffer >= _bufferEnd ) // 끝까지 봤는데 없다면?
            {
                *buffer_ptr = curBuffer;

                return false;
            }
        }

        *buffer_ptr = curBuffer;

        return true;
    }

    bool IniParser::SkipWord( wchar_t** buffer_ptr )
    {
        wchar_t* curBuffer = *buffer_ptr;

        for ( ;; )
        {
            char cur_char = *curBuffer;

            // 문자열인 경우
            if ( '"' == cur_char )
            {
                size_t offset { 1 };

                // " 문자열 끝을 찾음.
                for ( ; '"' != curBuffer[ offset ]; offset += 1 )
                {
                    // 버퍼 넘어서 검사 방지
                    // curBuffer[ offset ] -> "str" X "str
                    if ( _bufferEnd + 1 <= &curBuffer[ offset ] )
                        return false;
                }

                // +1로 "str"
                curBuffer += ( offset + 1 );

                continue;
            }
            // 무시할 문자도 아니고.
            else if ( !is_invalid_char( cur_char ) )
            {
                // 주석도 아님.	 리틀엔디안 '//' 그대로 '/*'은 '*/'
                unsigned short cur_short = *(unsigned short*)&curBuffer;
                if ( '//' != cur_short && '*/' != cur_short )
                {
                    ++curBuffer;
                    // 끝까지 찾아도 없으면 false
                    if ( _bufferEnd + 1 <= curBuffer )
                        return false;
                }
            }
            else
            {
                *buffer_ptr = curBuffer;

                return true;
            }
        }
    }

    /////////////////////////////////////////////////////////
    // brief  skip current line
    // param  pointer of buffer
    // return true if successful, otherwise false if buffer end reached
    /////////////////////////////////////////////////////////
    bool IniParser::SkipCurrentLine( wchar_t** buffer_ptr )
    {
        wchar_t*  curBuffer   = *buffer_ptr;
        size_t    offset       = 0;
        wchar_t*  buf_end_next = this->_bufferEnd + 1;

        for ( ; LineFeed != curBuffer[ offset ]; offset += 1 )
            if ( buf_end_next <= &curBuffer[ offset ] )
                return false;

        *buffer_ptr = curBuffer + offset + sizeof( InvalidCharacter::LineFeed );

        return true;
    }

    bool IniParser::SkipMultipleLinesAnnotation( wchar_t** buffer_ptr )
    {
        wchar_t*  curBuffer   = *buffer_ptr;
        size_t    offset       = 2;
        wchar_t*  buf_end_next = this->_bufferEnd + 1;

        // 리틀 엔디안 CPU이기 떄문에 2byte 자료형을 통해 찾기 위해
        // [*][/]가 아니라 [/][*]를 검색
        for ( ; '/*' != *(short*)&curBuffer[ offset ]; offset += 1 )
            if ( buf_end_next <= &curBuffer[ offset ] )
                return false;

        // 2byte "*/"
        *buffer_ptr += ( offset + 2 );

        return true;
    }

    // 버퍼의 이동 및 문자열, 길이를 줌.
    bool IniParser::skip_and_extract_word_if_find_word( wchar_t** buffer_ptr, wchar_t* out_wordBuffer, size_t out_buf_capacity )
    {
        wchar_t*  curBuffer     = *buffer_ptr;
        wchar_t*  first_cahr_ptr = nullptr;
        size_t word_length    = 0;

        // 단어 시작점을 찾음.
        if ( SkipInvalidChar( &curBuffer ) )
        {
            first_cahr_ptr = curBuffer;

            // 단어 끝 부분을 찾음.  key = value;
            if ( SkipWord( &curBuffer ) )
            {
                *buffer_ptr = curBuffer;
                word_length = static_cast< size_t >( curBuffer - first_cahr_ptr );

                if ( word_length >= out_buf_capacity )
                    word_length = out_buf_capacity - 1; // 마지막 널문자까지..

                // copy word
                memcpy( out_wordBuffer, first_cahr_ptr, word_length * CharSize );
                out_wordBuffer[ word_length ] = NULL;

                return true;
            }
        }

        return false;
    }

    void IniParser::Crash()
    {
        *( (int*)( nullptr ) ) = 0xDEADBEEF;
    }

    bool IniParser::is_invalid_char( char c )
    {
        switch ( c )
        {
        case InvalidCharacter::Tab:
        case InvalidCharacter::LineFeed:
        case InvalidCharacter::CarriageReturn:
        case InvalidCharacter::Space:
            return true;
        }

        return false;
    }

    bool IniParser::SkipAnnotation( wchar_t** buffer_ptr )
    {
        wchar_t* curBuffer = *buffer_ptr;
        char  cur_char   = curBuffer[ 0 ];

        if ( '/' == cur_char ) // '/' 주석의 시작 찾음. 주석이 있나.
        {
            const char next_char = curBuffer[ 1 ];
            if ( '/' == next_char ) // "//" 주석인지
            {
                if ( !SkipCurrentLine( &curBuffer ) )
                    return false;
                else
                    return true;
            }
            // "/*"주석인지 확인해야함.
            else if ( '*' == next_char )
            {
                if ( !SkipMultipleLinesAnnotation( &curBuffer ) )
                    return false;
                else
                    return true;
            }
            else //  '/' 이거 하나만 달랑 있는 경우  // 문법 문제 assrtion
            {
                const wchar_t* msg = L"Invalid Syntax!";
                Crash();
                return false;
            }
        }

        return false;
    }

}; // namespace Mala

