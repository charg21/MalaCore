export module Mala.Core.CoreGlobal;

#pragma once

import <memory>;
import Mala.Core.Types;
import Mala.Net;


#define BEGIN_NAMESPACE( NAMESPACE ) export namespace NAMESPACE{
#define END_NAMESPACE( NAMESPACE ) }

BEGIN_NAMESPACE( Mala::Threading )

inline extern class ThreadManager*        GThreadManager{};
inline extern class GlobalQueue*          GGlobalQueue{};
inline extern class JobTimer*             GJobTimer{};
//inline extern class LogRecordQueue< 16 >* GLogQueue{};

END_NAMESPACE( Mala::Threading )

BEGIN_NAMESPACE( Mala::Net )

inline extern class SendBufferManager*                    GSendBufferManager{};
inline extern       std::shared_ptr< Mala::Net::NetCore > GNetCore{};

END_NAMESPACE( Mala::Net )


BEGIN_NAMESPACE( Mala::Db )
END_NAMESPACE( Mala::Db )


BEGIN_NAMESPACE( Mala::Diagnostics )

inline extern class PerformanceCounter* GPerformanceCounter{};

END_NAMESPACE( Mala::Diagnostics )


export
{

inline extern class Memory*            GMemory{};

}

