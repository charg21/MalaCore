export module Mala.Core.Dump;

import Mala.Windows;

namespace Mala::Core
{

export struct Dump
{
    static void Register();
};

void myPurecallHandler();

void myInvalidParamaterHandler(
    const wchar_t*     expression,
    const wchar_t*     function,
    const wchar_t*     filfile,
    unsigned int       line,
    unsigned long long pReserved );

void signalHandler( int error );

long __stdcall exception_filter( EXCEPTION_POINTERS* e );

}