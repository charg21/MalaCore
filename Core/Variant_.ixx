export module Mala.Core.Variant:impl;

import <string>;
import <vector>;

import Mala.Core.Variant;
import Mala.Core.Concepts;
import Mala.Core.DateTime;
import Mala.Container;
import Mala.Container.String;
import Mala.Reflection.TypeInfo;
import Mala.Text;

using namespace Mala::Core;
using namespace Mala::Container;


Variant::Variant( EVarType type )
: _type{ type }
{
    switch ( type )
    {
        case EVarType::Bool:
        case EVarType::Int:
        case EVarType::Long:      
        case EVarType::Single:    
        case EVarType::Double:
        case EVarType::Timestamp:
        case EVarType::DateTime:
            _long = 0;
            break;

        case EVarType::String:    
            _string = new std::string();
            break;

        case EVarType::WString:
            _wstring = new String();
            break;
    }
}

Variant::Variant( const Variant& rhs )
: _type{ rhs._type }
{
    switch ( rhs._type )
    {
        case EVarType::Bool:
        case EVarType::Int:
        case EVarType::Long:      
        case EVarType::Single:    
        case EVarType::Double:
        case EVarType::Timestamp:
        case EVarType::DateTime:
            _long = rhs._long;
            break;

        case EVarType::String:    
            _string = new std::string( *rhs._string );
            break;

        case EVarType::WString:
            _wstring = new String( *rhs._wstring );
            break;
    }
}

Variant::Variant( const TypeInfo& typeInfo, const String& value )
:_type{ ToVarType( typeInfo ) }
{
    Set( value );
}

Variant::~Variant()
{
    switch ( _type )
    {
    case EVarType::String:
        delete _string;
        break;
    case EVarType::WString:
        delete _wstring;
        break;
    }
}


Variant::Variant( const wchar_t* v )
: _type { EVarType::WString }
{
    _wstring = new String{ v };
}

Variant::Variant( bool value )
: _type { EVarType::Bool }
, _short{ value          }
{
}

Variant::Variant( short value )
: _type { EVarType::Short }
, _short{ value           }
{
}

Variant::Variant( unsigned short value )
: _type  { EVarType::UShort }
, _ushort{ value            }
{
}

Variant::Variant( int value )
: _type{ EVarType::Int }
, _int { value         }
{
}

Variant::Variant( unsigned int value )
: _type{ EVarType::UInt }
, _uint{ value          }
{
}

Variant::Variant( long long value )
: _type{ EVarType::Long }
, _long{ value          }
{
}

Variant::Variant( unsigned long long value )
: _type { EVarType::ULong }
, _ulong{ value           }
{
}

Variant::Variant( float value )
: _type  { EVarType::Single }
, _single{ value            }
{
}

Variant::Variant( double value )
: _type  { EVarType::Double }
, _double{ value            }
{
}

Variant::Variant( std::string&& value )
: _type{ EVarType::String }
{
    _string = new std::string{ std::move( value ) };
}

Variant::Variant( const std::string& value )
: _type{ EVarType::String }
{
    _string = new std::string{ value };
}

Variant::Variant( const char* value )
: _type{ EVarType::String }
{
    _string = new std::string{ value };
}

Variant::Variant( String&& value )
: _type{ EVarType::WString }
{
    _wstring = new String{ std::move( value ) };
}

Variant::Variant( StringRef value )
: _type{ EVarType::WString }
{
    _wstring = new String{ value };
}

void Variant::operator=( const Variant& other )
{
    switch ( _type )
    {
    case EVarType::Bool:
    case EVarType::Int:
    case EVarType::Long:
    case EVarType::ULong:
    case EVarType::Single:
    case EVarType::Double:
    case EVarType::Timestamp:
    case EVarType::DateTime:
        _long = other._long;
        break;

    case EVarType::String:
        _string = new std::string( *other._string );
        break;

    case EVarType::WString:
        _wstring = new String( *other._wstring );
        break;
    }
}

void Variant::Set( const char* valueStr )
{
    switch ( _type )
    {
    case EVarType::Bool:
        if ( valueStr == "false" )
            _bool = false;
        else
            _bool = true;
        break;

    case EVarType::Int:
        _int = std::stoi( valueStr );
        break;

    case EVarType::Long:
        _long = std::stoll( valueStr );
        break;

    case EVarType::Single:
        _single = std::stof( valueStr );
        break;

    case EVarType::Double:
        _double = std::stod( valueStr );
        break;

    case EVarType::String:
        *_string = valueStr;
        break;

    case EVarType::DateTime:
    case EVarType::Timestamp:
        //_dateTime = Mala::Core::DateTime( valueStr );
        _dateTime = DateTime_( valueStr );
        break;

    case EVarType::WString:
        *_wstring = Mala::Text::Utf8::ToUtf16( valueStr );
        break;
    }
}

void Variant::Set( const std::string& valueStr )
{
    switch ( _type )
    {
    case EVarType::Bool:
        if ( valueStr == "false" )
            _bool = false;
        else
            _bool = true;
        break;

    case EVarType::Int:
        _int = std::stoi( valueStr );
        break;

    case EVarType::Long:
        _long = std::stoll( valueStr );
        break;

    case EVarType::Single:
        _single = std::stof( valueStr );
        break;

    case EVarType::Double:
        _double = std::stod( valueStr );
        break;

    case EVarType::String:
        *_string = valueStr;
        break;

    case EVarType::DateTime:
    case EVarType::Timestamp:
        //_dateTime = Mala::Core::DateTime( valueStr );
        _dateTime = DateTime_( valueStr );
        break;

    case EVarType::WString:
        *_wstring = Mala::Text::Utf8::ToUtf16( valueStr );
        break;

    }
}

void Variant::Set( StringRef valueStr )
{
    switch ( _type )
    {
    case EVarType::Bool:
        if ( valueStr == L"false" )
            _bool = false;
        else
            _bool = true;
        break;

    case EVarType::Int:
        _int = stoi( valueStr );
        break;

    case EVarType::Long:
        _long = stoll( valueStr );
        break;

    case EVarType::Single:
        _single = stof( valueStr );
        break;

    case EVarType::Double:
        _double = stod( valueStr );
        break;

    case EVarType::String:
        //*_string = valueStr;
        break;

    case EVarType::DateTime:
    case EVarType::Timestamp:
        //_dateTime = Mala::Core::DateTime( valueStr );
        _dateTime = DateTime_( valueStr.c_str() );
        break;

    case EVarType::WString:
        *_wstring = valueStr;
        break;

    }
}


const std::string Variant::to_string()
{
    switch ( _type )
    {
    case EVarType::Bool:
        return _bool ? "true" : "false";

    case EVarType::Int:
        return std::to_string( _int );

    case EVarType::Long:
        return std::to_string( _long );

    case EVarType::Single:
        return std::to_string( _single );

    case EVarType::Double:
        return std::to_string( _double );

    case EVarType::String:
        return "\"" + *_string + "\"";

    case EVarType::DateTime:
    case EVarType::Timestamp:
        return "\"" + _dateTime.ToAnsiString() + "\"";

    case EVarType::WString:
        break;

    }

    return "";
}

String Variant::ToString()
{
    switch ( _type )
    {
    case EVarType::Bool:
        return _bool ? L"true" : L"false";

    case EVarType::Short:
        return to_wstring( _short );

    case EVarType::UShort:
        return to_wstring( _ushort );

    case EVarType::Int:
        return to_wstring( _int );

    case EVarType::Long:
        return to_wstring( _long );

    case EVarType::ULong:
        return to_wstring( _ulong );

    case EVarType::Single:
        return to_wstring( _single );

    case EVarType::Double:
        return to_wstring( _double );

    case EVarType::String:
        //return L"\"" + *_string + L"\"";
        break;

    case EVarType::DateTime:
    case EVarType::Timestamp:
        return _dateTime.ToString();

    case EVarType::WString:
        return L"\"" + *_wstring + L"\"";
        break;

    }

    return L"";
}

const TypeInfo& Variant::GeTypeInfo()
{
    switch ( _type )
    {
    case EVarType::Bool:
        return TypeInfo::GetStaticTypeInfo< bool >();
    case EVarType::Short:
        return TypeInfo::GetStaticTypeInfo< short >();
    case EVarType::UShort:
        return TypeInfo::GetStaticTypeInfo< unsigned short >();
    case EVarType::Int:
        return TypeInfo::GetStaticTypeInfo< int >();
    case EVarType::UInt:
        return TypeInfo::GetStaticTypeInfo< unsigned int >();
    case EVarType::Long:
        return TypeInfo::GetStaticTypeInfo< long long >();
    case EVarType::ULong:
        return TypeInfo::GetStaticTypeInfo< unsigned long long >();
    case EVarType::Single:
        return TypeInfo::GetStaticTypeInfo< float >();
    case EVarType::Double:
        return TypeInfo::GetStaticTypeInfo< double >();
    case EVarType::DateTime:
    case EVarType::Timestamp:
        return TypeInfo::GetStaticTypeInfo< DateTime >();
    case EVarType::String:
    case EVarType::WString:
        return TypeInfo::GetStaticTypeInfo< String >();
    }

    /// 타입을지정하지 않으면 int로 인식되듯이... 추후 수정 예정
    return TypeInfo::GetStaticTypeInfo< int >();
}

EVarType Variant::ToVarType( const TypeInfo& typeInfo )
{
    static std::unordered_map< const TypeInfo*, EVarType > typeMap
    {
        { &TypeInfo::GetStaticTypeInfo< bool >(), EVarType::Bool },
        { &TypeInfo::GetStaticTypeInfo< int >(), EVarType::Int },
        { &TypeInfo::GetStaticTypeInfo< unsigned int >(), EVarType::UInt },
        { &TypeInfo::GetStaticTypeInfo< short >(), EVarType::Short },
        { &TypeInfo::GetStaticTypeInfo< unsigned short >(), EVarType::UShort },
        { &TypeInfo::GetStaticTypeInfo< float >(), EVarType::Single },
        { &TypeInfo::GetStaticTypeInfo< double >(), EVarType::Double },
        { &TypeInfo::GetStaticTypeInfo< unsigned long long >(), EVarType::ULong },
        { &TypeInfo::GetStaticTypeInfo< long long >(), EVarType::Long },
		{ &TypeInfo::GetStaticTypeInfo< String >(), EVarType::String },
		{ &TypeInfo::GetStaticTypeInfo< DateTime >(), EVarType::DateTime },
    };

    auto iter = typeMap.find( &typeInfo );
    if ( iter != typeMap.end() )
        return iter->second;

    return EVarType::Int;
}

Variant::operator bool() const
{
    return _bool;
}

Variant::operator char() const
{
    return _char;
}

Variant::operator short() const
{
    return _short;
}

Variant::operator unsigned short() const
{
    return _ushort;
}

Variant::operator int() const
{
    return _int;
}
Variant::operator unsigned int() const
{
    return _uint;
}
Variant::operator long long() const
{
    return _long;
}
Variant::operator unsigned long long() const
{
    return _ulong;
}
Variant::operator float() const
{
    return _single;
}
Variant::operator double() const
{
    return _double;
}