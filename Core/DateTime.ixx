export module Mala.Core.DateTime;

import <time.h>;
import Mala.Core.Types;
import Mala.Core.TimeSpan;
import Mala.Container.String;

export namespace Mala::Core
{

enum class EDateFormat
{
    YMDHmS, // 년월일시분초
    MDHmS,  // 월일시분초
    DHmS,   // 일시분초
    HmS,    // 시분초
};

/// <summary>
/// 시간 날짜 정보 객체
/// </summary>
struct DateTime
{
    /// <summary>
    /// 현재 시간에 대한 DateTime 객체를 반환한다
    /// </summary>
    static DateTime Now()
    {
        DateTime now;
        ::_time64( &now._sec );

        return now;
    }

    //static const DateTime Today()
    //{
    //    DateTime now;

    //    return now;
    //}


    /// <summary>
    /// 생성자
    /// </summary>
    DateTime() = default;

    // "YYYY-MM-DD HH:MM:SS" 형식만 지원
    DateTime( const std::string& dateString )
    {
        if ( dateString.size() < 19 )
            return;
        
        const char* date = dateString.data();

        tm when
        {
            .tm_sec  = ( date[ 17 ] - L'0' ) * 10 +
                ( date[ 18 ] - L'0' ) * 1,

            .tm_min  = ( date[ 14 ] - L'0' ) * 10 +
                ( date[ 15 ] - L'0' ) * 1,

            .tm_hour = ( date[ 11 ] - L'0' ) * 10 +
                ( date[ 12 ] - '0' ) * 1,

            .tm_mday = ( date[ 8 ] - L'0' ) * 10 +
                ( date[ 9 ] - L'0' ) * 1,

            .tm_mon  = ( ( date[ 5 ] - L'0' ) * 10 +
                ( date[ 6 ] - L'0' ) * 1 ) - 1,

            .tm_year = ( ( date[ 0 ] - L'0' ) * 1000 +
                ( date[ 1 ] - L'0' ) * 100 +
                ( date[ 2 ] - L'0' ) * 10  +
                ( date[ 3 ] - L'0' ) * 1     ) - 1900,
        };

        _sec = ::_mktime64( &when );
    }

    DateTime( const std::wstring& dateString )
    {
        if ( dateString.size() < 19 )
            return;
        
        const wchar_t* date = dateString.data();

        tm when
        {
            .tm_sec  = ( date[ 17 ] - L'0' ) * 10 +
                ( date[ 18 ] - L'0' ) * 1,

            .tm_min  = ( date[ 14 ] - L'0' ) * 10 +
                ( date[ 15 ] - L'0' ) * 1,

            .tm_hour = ( date[ 11 ] - L'0' ) * 10 +
                ( date[ 12 ] - '0' ) * 1,

            .tm_mday = ( date[ 8 ] - L'0' ) * 10 +
                ( date[ 9 ] - L'0' ) * 1,

            .tm_mon  = ( ( date[ 5 ] - L'0' ) * 10 +
                ( date[ 6 ] - L'0' ) * 1 ) - 1,

            .tm_year = ( ( date[ 0 ] - L'0' ) * 1000 +
                ( date[ 1 ] - L'0' ) * 100 +
                ( date[ 2 ] - L'0' ) * 10  +
                ( date[ 3 ] - L'0' ) * 1     ) - 1900,
        };

        _sec = ::_mktime64( &when );
    }

    /// <summary>
    /// 생성자
    /// </summary>
    DateTime( const i32 year, const i32 month, const i32 days, const i32 hours, const i32 mins, const i32 seconds )
    {
        tm when
        {
            .tm_sec  = seconds,
            .tm_min  = mins,
            .tm_hour = hours,
            .tm_mday = days,
            .tm_mon  = month - 1,
            .tm_year = year - 1900,
        };

        _sec = _mktime64( &when );
    }

    /// <summary>
    /// 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS, typename T = String >
    auto ToString() const
    {
        if  constexpr ( std::is_same< String, T >::value )
        {
            return ToWString< TFormat >();
        }
        else
        {
            return ToAnsiString< TFormat >();
        }
    }

    /// <summary>
    /// Ansi 스트링을 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS >
    std::string ToAnsiString() const
    {
        tm tm;
        _localtime64_s( &tm, &_sec );

        char buf[ 20 ];
        
        if constexpr ( TFormat == EDateFormat::YMDHmS )
        {
            sprintf_s( buf, "%04d-%02d-%02d %02d:%02d:%02d", 
                tm.tm_year + 1900, 
                tm.tm_mon + 1, 
                tm.tm_mday,
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }
        else if constexpr ( TFormat == EDateFormat::HmS )
        {
            sprintf_s( buf, "%02d:%02d:%02d", 
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }

        return std::move( std::string( buf, 19 ) );
    }

    /// <summary>
    /// 와이드 스트링을 문자열을 반환한다
    /// </summary>
    template< EDateFormat TFormat = EDateFormat::YMDHmS >
    String ToWString() const
    {
        tm tm;
        _localtime64_s( &tm, &_sec );

        wchar_t buf[ 20 ];
        if constexpr ( TFormat == EDateFormat::YMDHmS )
        {
            swprintf_s( buf, L"%04d-%02d-%02d %02d:%02d:%02d",
                tm.tm_year + 1900, 
                tm.tm_mon + 1, 
                tm.tm_mday,
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }
        else if constexpr ( TFormat == EDateFormat::HmS )
        {
            swprintf_s( buf, L"%02d:%02d:%02d",
                tm.tm_hour,
                tm.tm_min,
                tm.tm_sec );
        }

        return std::move( String( buf, 19 ) );
    }

    DateTime& operator+=( const TimeSpan& rhs )
    {
        _sec += ( rhs._ticks / TimeSpan::TICK_PER_SEC );

        return *this;
    }

    DateTime& operator+( const TimeSpan& rhs )
    {
        _sec += ( rhs._ticks / TimeSpan::TICK_PER_SEC );

        return *this;
    }

    DateTime& operator-=( const TimeSpan& rhs )
    {
        _sec -= ( rhs._ticks / TimeSpan::TICK_PER_SEC );

        return *this;
    }

    DateTime& operator-( const TimeSpan& rhs )
    {
        _sec -= ( rhs._ticks / TimeSpan::TICK_PER_SEC );

        return *this;
    }

    time_t _sec; // 초를 나태내는 시간 구조체
};

const bool operator==( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec == rhs._sec;
}

const bool operator!=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec != rhs._sec;
}

const bool operator<=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec <= rhs._sec;
}

const bool operator<( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec < rhs._sec;
}

const bool operator>=( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec >= rhs._sec;
}

const bool operator>( const DateTime& lhs, const DateTime& rhs )
{
    return lhs._sec > rhs._sec;
}

const TimeSpan operator-( const DateTime& lhs, const DateTime& rhs )
{
    return { ( static_cast< u64 >( lhs._sec ) - static_cast< u64 >( rhs._sec ) )
        * TimeSpan::TICK_PER_SEC };
}

const TimeSpan operator+( const DateTime& lhs, const DateTime& rhs )
{
    return { ( static_cast< u64 >( lhs._sec ) + static_cast< u64 >( rhs._sec ) )
        * TimeSpan::TICK_PER_SEC };
}

}