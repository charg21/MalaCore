﻿export module Mala.Core.Format;

export import <format>;

import Mala.Container.String;

using namespace std;

export 
{
    
template< int = 0 > // improves throughput, see GH-2329
_NODISCARD String xvformat( const StringView _Fmt, const std::wformat_args _Args ) 
{
    String _Str;
    _Str.reserve( _Fmt.size() + _Args._Estimate_required_capacity() );
    _STD vformat_to( back_insert_iterator{ _Str }, _Fmt, _Args );
    return _Str;
}

template< class... _Types >
_NODISCARD String xformat( const wformat_string<_Types...> _Fmt, _Types&&... _Args ) 
{
    return xvformat( _Fmt.get(), _STD make_wformat_args( _Args... ) );
}


}



