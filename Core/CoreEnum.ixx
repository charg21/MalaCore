export module Mala.Core.CoreEnum;

#pragma once

import <type_traits>;

export namespace Mala
{

/// <summary>
/// 서비스 타입 열거형
/// </summary>
enum class EServiceType
{
	Server = 1 << 0,
	Client = 1 << 1,

	All = ~0
};

/// <summary>
/// 
/// </summary>
enum class EDisconnectReason
{
	Kickout,
};

}