export module Mala.Core.Global:impl;

import Mala.Core.CoreGlobal;
import Mala.Diagnostics.PerformanceCounter;
import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Threading.ThreadManager;
import Mala.Threading.GlobalQueue;
import Mala.Threading.JobTimer;
import Mala.Threading.JobWheelTimer;
import Mala.Log;
import Mala.Memory;

using namespace Mala::Net;
using namespace Mala::Threading;
using namespace Mala::Diagnostics;

/// <summary>
/// 
/// </summary>
class CoreGlobal
{
public:
	/// <summary>
	/// 持失切
	/// </summary>
	CoreGlobal()
	{
		GMemory            = new Memory();

		// GLog               = xnew< ThreadManager >();
		GThreadManager     = xnew< ThreadManager >();
		GSendBufferManager = xnew< SendBufferManager >();
		GGlobalQueue       = xnew< GlobalQueue >();
		GJobTimer          = xnew< JobTimer >();
		
		GPerformanceCounter = xnew< PerformanceCounter >();

		SocketHelper::Initialize();
#ifndef USE_ASIO
#endif
	}

	/// <summary>
	/// 社瑚切
	/// </summary>
	~CoreGlobal()
	{
		xdelete( GThreadManager );
		xdelete( GSendBufferManager );
		xdelete( GGlobalQueue );
		xdelete( GJobTimer );

		delete GMemory;
	}
};

CoreGlobal GCoreGlobal;