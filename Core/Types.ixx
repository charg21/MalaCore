export module Mala.Core.Types;

#pragma once

#include "../MalaMacro.h"

//export import Mala.Container.String;

export import <atomic>;
export import <memory>;
export import <atomic>;
export import <string_view>;

export
{

/// <summary>
/// 기본 타입 정의
/// </summary>
using i8    = char;
using i16   = short;
using i32   = int;
using i64   = long long;
using isize = long long;

using u8    = unsigned char;
using u16   = unsigned short;
using u32   = unsigned int;
using u64   = unsigned long long;
using usize = unsigned long long;

using f32 = float;
using f64 = double;

// byte
using byte_t = unsigned char;
using byte = unsigned char;
using BYTE   = unsigned char;

using id8  = unsigned char;
using id16 = unsigned short;
using id32 = unsigned int;
using id64 = unsigned long long;

//  count 
using count8  = char;
using count16 = short;
using count32 = long;
using count64 = long long;

// tick
using tick32 = unsigned long;
using tick64 = unsigned long long;

// flag
using flag_t = unsigned long long;
 
using StringView    = std::wstring_view;
using StringViewRef = const StringView&;

using ai8 = std::atomic< char >;
using ai16 = std::atomic< short >;
using ai32 = std::atomic< int >;
using ai64 = std::atomic< long long >;
using aisize = std::atomic< long long >;


template< typename T >
using Atomic = std::atomic< T >;


namespace Mala::Net
{

// TODO
USING_SHARED_PTR( AsioCore );
USING_SHARED_PTR( AsioEvent );
USING_SHARED_PTR( AsioObject );
USING_SHARED_PTR( AsioSession );
USING_SHARED_PTR( AsioService );
USING_SHARED_PTR( AsioClientService );
USING_SHARED_PTR( AsioServerService );
USING_SHARED_PTR( AsioListener );

USING_SHARED_PTR( IocpCore );
USING_SHARED_PTR( IocpEvent );
USING_SHARED_PTR( IocpObject );
USING_SHARED_PTR( IocpSession );
USING_SHARED_PTR( IocpService );
USING_SHARED_PTR( IocpClientService );
USING_SHARED_PTR( IocpServerService );
USING_SHARED_PTR( IocpListener );

USING_SHARED_PTR( RioCore );
USING_SHARED_PTR( RioEvent );
USING_SHARED_PTR( RioObject );
USING_SHARED_PTR( RioSession );
USING_SHARED_PTR( RioService );
USING_SHARED_PTR( RioClientService );
USING_SHARED_PTR( RioServerService );
USING_SHARED_PTR( RioListener );

USING_SHARED_PTR( NetCore );

USING_SHARED_PTR( SendBuffer );
USING_SHARED_PTR( SendBufferChunk );

USING_SHARED_PTR( PacketSession );

}

namespace Mala::Threading
{

USING_SHARED_PTR( JobExecutor );

}

namespace Mala::Db
{

USING_SHARED_PTR( TransactionExecutor );
USING_SHARED_PTR( DbModel );

}

}
