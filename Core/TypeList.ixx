export module Mala.Core.TypeList;

import <tuple>;
import Mala.Core.Types;

export
{

#pragma region TypeList
template< typename... T >
struct TypeList;

template< typename T, typename U >
struct TypeList< T, U >
{
	using Head = T;
	using Tail = U;
};

template< typename T, typename... U >
struct TypeList< T, U... >
{
	using Head = T;
	using Tail = TypeList< U... >;
};
#pragma endregion

#pragma region Length
template< typename T >
struct Length;

template<>
struct Length< TypeList<> >
{
	enum { value = 0 };
};

template< typename T, typename... U >
struct Length< TypeList< T, U... > >
{
	enum { value = 1 + Length< TypeList< U... > >::value };
};
#pragma endregion

#pragma region TypeAt
template< typename TL, i32 Index >
struct TypeAt;

template< typename Head, typename... Tail >
struct TypeAt< TypeList< Head, Tail... >, 0 >
{
	using Result = Head;
};

template< typename Head, typename... Tail, i32 Index >
struct TypeAt< TypeList< Head, Tail... >, Index >
{
	using Result = typename TypeAt< TypeList< Tail... >, Index - 1 >::Result;
};

#pragma endregion

#pragma  region IndexOf
template< typename TL, typename T >
struct IndexOf;

template< typename... Tail, typename T >
struct IndexOf< TypeList< T, Tail... >, T >
{
	enum { value = 0 };
};

template< typename T >
struct IndexOf< TypeList<>, T >
{
	enum { value = -1 };
};

template< typename Head, typename... Tail, typename T >
struct IndexOf< TypeList< Head, Tail...>, T >
{
private:
	enum { temp = IndexOf< TypeList< Tail...>, T >::value };

public:
	enum { value = ( temp == -1 ) ? -1 : temp + 1 };
};


#pragma endregion

#pragma region ToTypeList

template< typename Tuple >
struct ToTypeListInternal;

template< typename... T >
struct ToTypeListInternal< std::tuple< T... > >
{
	using Result = TypeList< T... >;
};

template< typename Tuple >
using ToTypeList = typename ToTypeListInternal< Tuple >::Result;

#pragma endregion

#pragma region ToTuple

template< typename TL >
struct ToTuple;

template< typename... Types >
struct ToTuple< TypeList< Types...> >
{
	using Result = std::tuple< typename std::decay< Types >::type... >;
};

template< typename TL, typename... Args >
typename ToTuple< TL >::Result MakeTuple( Args&&... args )
{
	return typename ToTuple< TL >::Result{ std::forward< Args >( args )... };
}


template < int Index, typename T, typename S >
void ForeachTupleUsingIndex( T&& t, S&& f )
{
    if constexpr ( Index < std::tuple_size_v< std::decay_t< T > > )
    {
        f( std::get< Index >( t ) );

        ForeachTupleUsingIndex< Index + 1 >( t, std::forward< S >( f ) );
    }
}

template< typename... Ts, typename S >
void ForeachTuple( const std::tuple< Ts... >& t, S&& s )
{
    ForeachTupleUsingIndex< 0 >( t, std::forward< S >( s ) );
}

template< typename... T, typename F >
void ForeachTupleType( const std::tuple< T... >& t, F&& f )
{
    ( f( ( T* )nullptr ), ... );
}
#pragma endregion

}
