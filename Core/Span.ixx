﻿export module Mala.Core.Span;

import <memory>;

export namespace Mala::Core
{
    template < typename T >
    struct Span
    {
        T*     _data;
        size_t _size;
    };

    struct ReadonlySpan
    {
        const char*  _buffer;
        const size_t _size;
        size_t _reader;

        void Read( void* data, const size_t size )
        {
            if ( _reader + size > _size ) // idx니깐 같아져도 문제가 생김.
                *( (int*)( nullptr ) ) = 0xDEADBEEF;

            std::memcpy( data, &_buffer[ _reader ], size );

            _reader += (unsigned short)( size );
        }
    };

}



