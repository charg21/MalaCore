export module Mala.Core.Delegate;

import <memory>;
import <functional>;
import <vector>;

export namespace Mala::Core
{

template< typename TReturn = void, typename... Args >
class Delegate
{
public:
    /// <summary>
    /// 콜백 타입 정의 
    /// </summary>
    using Functor    = std::function< TReturn( Args... ) >;

    /// <summary>
    /// 콜백 타입 목록 정의 
    /// </summary>
    using FunctorList = std::vector< Functor >;

    /// <summary>
    /// 생성자
    /// </summary>
    Delegate() = default;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~Delegate() = default;

    /// <summary>
    /// 복사 생성자
    /// </summary>
    Delegate( const Delegate& other )
    {
        for ( auto& func : other._funcList )
            Bind( func );
    }

    /// <summary>
    /// 생성자
    /// </summary>
    void Bind( const Functor& functor )
    {
        _funcList.emplace_back( functor );
    }

    /// <summary>
    /// 생성자
    /// </summary>
    void Bind( Functor&& functor )
    {
        _funcList.emplace_back( functor );
    }

    /// <summary>
    /// += 연산자 재정의
    /// </summary>
    template< typename TReturn, typename Object >
    void BindDynamic( Object* typeThis, TReturn Object::*mem_func )
    {
        _funcList.emplace_back( [ typeThis, mem_func ]() { return ( typeThis->*mem_func )(); });
    }

    /// <summary>
    /// += 연산자 재정의
    /// </summary>
    Delegate& operator+=( Delegate& other )
    {
        for ( auto& func : other._funcList )
            bind( func );

        return *this;
    }

    /// <summary>
    /// ()연산자 재정의
    /// </summary>
    decltype( auto ) operator()( Args&&... args )
    {
        return Invoke( std::forward< Args >( args )... );
    }

    /// <summary>
    /// 실행한다
    /// </summary>
    decltype( auto ) Invoke( Args&&... args )
    {
        if constexpr ( std::is_same< TReturn, void >::value )
        {
            for ( auto& func : _funcList )
                func( std::forward< Args... >( args )... );
        }
        else
        {
            TReturn result{};

            for ( auto& func : _funcList )
                result = func( std::forward< Args... >( args )... );

            return result;
        }
    }


    /// <summary>
    /// 특정함수를 제거
    /// </summary>
    void Unbind( const Functor& func )
    {
        std::erase_if( _funcList,
            [ &func ]( const Functor& functor ) 
            { 
                return functor.target_type() == func.target_type();
            } );
    }

public:
    FunctorList _funcList; // 콜백 목록
};


#define BIND_DYNAMIC( MEM_FUNC ) BindDynamic( this, MEM_FUNC );

}
