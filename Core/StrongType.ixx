module;

export module Mala.Core.StrongType;
import Mala.Core.Concepts;

export
{

struct StrongTypeTag {};

template< typename T, typename TTag >
struct StrongType
{
public:
    using UnderlyingType = T;
    using TagType        = TTag;

    StrongType() = default;
    StrongType( T value ) : _value{ value } {}

public:
    T _value{};
};

template< DefaultValueType T, typename TTag >
struct StrongValueType : public StrongType< T, TTag >
{
    static_assert( sizeof( T ) <= sizeof( double ), "" );

public:
    using BaseType       = StrongType< T, TTag >;

    using UnderlyingType = typename BaseType::UnderlyingType;
    using TagType        = typename BaseType::TagType;

    using BaseType::_value;

    bool operator==( StrongValueType rhs ) const
    {
        return BaseType::_value == rhs.BaseType::_value;
    }

    bool operator<( StrongValueType rhs ) const
    {
        return BaseType::_value < rhs.BaseType::_value;
    }

    bool operator<=( StrongValueType rhs ) const
    {
        return BaseType::_value <= rhs.BaseType::_value;
    }

    bool operator>( StrongValueType rhs ) const
    {
        return BaseType::_value > rhs.BaseType::_value;
    }

    bool operator>=( StrongValueType rhs ) const
    {
        return BaseType::_value >= rhs.BaseType::_value;
    }
};

namespace std
{

template< typename ValueType, typename Tag >
struct hash< StrongValueType< ValueType, Tag > >
{
	size_t operator()( const StrongValueType< ValueType, Tag >& rhs ) const noexcept
	{
        return std::hash< StrongValueType< ValueType, Tag >::UnderlyingType >{}( rhs._value );
	}
};

} // namespace std

//#define USING_STRONG_TYPE( Name, Base ) \
//struct Name ## StrongTypeTag : public StrongTypeTag {}; \
//using Name = StrongType< Base, Name ## StrongTypeTag >;
//
//    USING_STRONG_TYPE( C, int );
}
