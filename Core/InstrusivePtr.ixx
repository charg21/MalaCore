export module Mala.Core.InstrusivePtr;

import <memory>;
import <type_traits>;

import Mala.Core.Types;

export namespace Mala::Core
{

/// <summary>
/// 
/// </summary>
struct DefaultDeleter
{
    template< typename T >
    static void Delete( T* ptr ) noexcept
    {
        delete ptr;
    }
};

struct TRefCountableSuper
{};

/// <summary>
/// 
/// </summary>
/// <typeparam name="Counter"></typeparam>
/// <typeparam name="Deleter"></typeparam>
template< typename Counter, typename Deleter = DefaultDeleter >
struct TRefCountableBase : public TRefCountableSuper
{
    using DeleterType = Deleter;
    using CounterType = Counter;

    TRefCountableBase()
    : _refCount{ 1 }
    {
    }

    virtual ~TRefCountableBase() {}

    i32 AddRef()
    {
        return ++_refCount;
    }

    i32 GetRefCount()
    {
        return _refCount;
    }

    i32 ReleaseRef()
    {
        auto refCount = --_refCount;
        if ( !refCount )
            Deleter::Delete( this );

        return refCount;
    }

    Counter _refCount;
};

/// <summary>
/// 
/// </summary>
/// <typeparam name="Deleter"></typeparam>
template< typename Deleter >
using TRefCountable      = TRefCountableBase< i32, Deleter >;
template< typename Deleter >
using TAtomicRefCountable = TRefCountableBase< std::atomic< i32 >, Deleter >;

using RefCountable       = TRefCountable< DefaultDeleter >;
using AtomicRefCountable = TAtomicRefCountable< DefaultDeleter >;

template< typename TRefCountable >
concept IRefCountable = requires( TRefCountable refCountable )
{
    refCountable.AddRef();
    refCountable.ReleaseRef();
    /*{ refCountable.AddRef()     } -> std::convertible_to< i32 >;
    { refCountable.ReleaseRef() } -> std::convertible_to< i32 >;*/
};

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
template< typename T >
struct IsRefCountable
{
    //using CounterType = typename T::CounterType;
    //using DeleterType = typename T::DeleterType;

    inline static constexpr bool value = std::is_base_of< TRefCountableSuper, T >::value;
        //std::is_base_of< TRefCountableBase< CounterType, DeleterType >, T >::value;
};

//using Rc  = RefCountable;
//using Arc = AtomicRefCountable;

/// <summary>
/// 침습형 레퍼런스 카운트를 사용하는 스마트 포인터
/// </summary>
/// <typeparam name="T"> 타입 </typeparam>
template< typename T >
class InstrusivePtr
{
    //static_assert( IsRefCountable< T >::value,"T Must Derived Type From TRefCountable" );

public:
    using element_type   = T;
    using pointer_type   = T*;
    using reference_type = T&;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    InstrusivePtr()
    : _refCountable{}
    {
    }

    InstrusivePtr( nullptr_t )
    : _refCountable{}
    {
    }

    InstrusivePtr( T* refCount )
    : _refCountable{ refCount }
    {
        if ( !refCount )
            return;

        refCount->AddRef();
    }

    InstrusivePtr( T* refCount, bool )
    : _refCountable{ refCount }
    {
    }

    InstrusivePtr( const InstrusivePtr& other )
    : _refCountable{ other._refCountable }
    {
        if ( !other._refCountable )
            return;

        other._refCountable->AddRef();
    }

    InstrusivePtr( InstrusivePtr&& other ) noexcept
    : _refCountable{ other._refCountable }
    {
        other._refCountable = nullptr;
    }

    template< typename U >
    InstrusivePtr( const InstrusivePtr< U >& other )
    : _refCountable{ static_cast< T* >( other._refCountable ) }
    {
        if ( !other._refCountable )
            return;

        other._refCountable->AddRef();
    }

    template< typename U >
    InstrusivePtr( InstrusivePtr< U >&& other ) noexcept
    : _refCountable{ static_cast< T* >( other._refCountable ) }
    {
        other._refCountable = nullptr;
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~InstrusivePtr()
    {
        if ( _refCountable )
            _refCountable->ReleaseRef();

        _refCountable = nullptr;
    }

    InstrusivePtr& operator=( nullptr_t )
    {
        Release();

        return *this;
    }

    InstrusivePtr& operator=( const InstrusivePtr& other )
    {
        if ( this == &other )
            return *this;

        Release();
        Set( other._refCountable );

        return *this;
    }

    InstrusivePtr& operator=( InstrusivePtr&& other ) noexcept
    {
        Release();

        _refCountable = other._refCountable; 
        other._refCountable = nullptr;

        return *this;
    }

    i32 GetRefCount()
    {
        if ( !_refCountable )
            return 0;

        return _refCountable->GetRefCount();
    }

    const i32 GetRefCount() const
    {
        if ( !_refCountable )
            return 0;

        return _refCountable->GetRefCount();
    }

    T* Get()
    {
        return _refCountable;
    }

    T* Get() const
    {
        return _refCountable;
    }

    T* operator->()
    {
        return Get();
    }

    T* operator->() const
    {
        return Get();
    }

    bool operator!()
    {
        return _refCountable == nullptr;
    }

    const bool operator!() const
    {
        return _refCountable == nullptr;
    }

    bool operator==( const InstrusivePtr& other ) const
    {
        return _refCountable == other._refCountable;
    }

    bool operator!=( const InstrusivePtr& other ) const
    {
        return _refCountable != other._refCountable;;
    }

    inline void Release()
    {
        if ( !_refCountable )
            return;

        _refCountable->ReleaseRef();
        _refCountable = nullptr;
    }
       
    inline void Set( T* ptr )
    {
        _refCountable = ptr;
        if ( ptr )
            ptr->AddRef();
    }
    
    friend static bool operator==( const InstrusivePtr& lhs, const InstrusivePtr& rhs );
    friend static bool operator!=( const InstrusivePtr& lhs, const InstrusivePtr& rhs );
    template< typename Type >
    friend class InstrusivePtr;
    template< typename To, typename From >
    friend InstrusivePtr< To > TypeCast( InstrusivePtr< From >& ptr );

private:
    T* _refCountable;
};

template< typename T >
using TPtr = InstrusivePtr< T >;

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="lhs"></param>
/// <param name="rhs"></param>
/// <returns></returns>
template< typename T >
bool operator==( const InstrusivePtr< T >& lhs, const InstrusivePtr< T >& rhs )
{
    return lhs._refCountable == rhs._refCountable;
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="lhs"></param>
/// <param name="rhs"></param>
/// <returns></returns>
template< typename T >
bool operator!=( const InstrusivePtr< T >& lhs, const InstrusivePtr< T >& rhs )
{
    return lhs._refCountable != rhs._refCountable;
}

/// <summary>
/// 
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="...Args"></typeparam>
/// <param name="...args"></param>
/// <returns></returns>
template< typename T, typename... Args >
InstrusivePtr< T > MakeInstrusivePtr( Args&&... args )
{
    return InstrusivePtr< T >( new T( std::forward< Args >( args )... ), true );
}

template< typename T, typename... Args >
TPtr< T > MakeTPtr( Args&&... args )
{
    return TPtr< T >( new T( std::forward< Args >( args )...), true );
}

/// <summary>
/// 침습형 스마트 포인터 클래스 
/// </summary>
/// <typeparam name="T"></typeparam>
template< typename T >
struct IsInstrusivePtr
{
    using element_type   = typename T::element_type;

    inline static constexpr bool value =
        std::is_same< IsInstrusivePtr< element_type >, T >::value;
};

/// <summary>
/// 침습형 스마트 포인터 클래스 
/// </summary>
/// <typeparam name="T"></typeparam>
template< typename T >
inline constexpr bool IsInstrusivePtrV = IsInstrusivePtr< T >::value;

template< typename T >
concept InstrusivePtrType = std::is_same_v< T, InstrusivePtr< typename T::element_type > >;
template< typename T >
concept TPtrType = std::is_same_v< T, TPtr< typename T::element_type > >;


//{
//    requires std::is_same_v< T, InstrusivePtr< typename T::element_type > >;
//};

}
//struct Actor : public RefCountable
//{
//    virtual ~Actor()
//    {
//        std::cout << "Hello World!\n";
//    }
//};
//
