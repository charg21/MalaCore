export module Mala.Core.EnumHelper;

#pragma once

import <type_traits>;

export namespace Mala
{

template< typename T > concept Enumerable = std::is_enum_v< T >;
template< typename T > concept Enumeration = std::is_enum_v< T >;

template< Enumerable TEnum >
struct EnumHelper
{
	static void SetFlag( TEnum& value, TEnum flag )
	{
		value = (TEnum)( static_cast< std::underlying_type_t< TEnum > >( value ) | 
			static_cast< std::underlying_type_t< TEnum > >( flag ) ) ;
	}

	static bool HasFlag( const TEnum value, const TEnum flag )
	{
		return static_cast< std::underlying_type_t< TEnum > >( value ) & 
			   static_cast< std::underlying_type_t< TEnum > >( flag );
	}

	static void ForEachEnum( const TEnum value )
	{
	}

};

}