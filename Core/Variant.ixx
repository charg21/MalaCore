export module Mala.Core.Variant;

import <string>;
import <vector>;

import Mala.Core.Concepts;
import Mala.Core.DateTime;
import Mala.Container;

using namespace Mala::Container;

/// <summary>
/// 빌드 실패로 임시 코드
/// </summary>
export using DateTime_ = Mala::Core::DateTime;

export class TypeInfo;

export namespace Mala::Core
{

template< typename T >
concept PodVarType = 
std::is_same_v< T, bool >           ||
std::is_same_v< T, short >          ||
std::is_same_v< T, unsigned short > ||
std::is_same_v< T, int >            ||
std::is_same_v< T, unsigned int >   ||
std::is_same_v< T, long >           ||
std::is_same_v< T, unsigned long >  ||
std::is_same_v< T, float >          ||
std::is_same_v< T, double >;         


///// <summary>
///// 액터 타입 타입 리스트 정의
///// </summary>
//using VarTypeList = TypeList< bool, i16, u16, i32, u32, i64, u64, float, double, std::string, std::wstring >;
enum class EVarType : unsigned char
{
    Bool,
    Short,
    UShort,
    Int,
    UInt,
    Long,
    ULong,
    Single,
    Double,
    String,
    WString,
    Timestamp,
    DateTime,
};

//struct Table;
struct Variant
{
public:
    /// <summary>
    /// 생성자
    /// </summary>
    Variant( EVarType type = EVarType::Long );
    Variant( const Variant& rhs );
    Variant( const TypeInfo& typeInfo, const String& value );
    ~Variant();

    template< std::size_t N >
    Variant( char( &value )[ N ] );
    template< std::size_t N >
    Variant( wchar_t( &value )[ N ] );

    Variant( const wchar_t* v );
    Variant( bool value );
    Variant( short value );
    Variant( unsigned short value );
    Variant( int value );
    Variant( unsigned int value );
    Variant( long long value );
    Variant( unsigned long long value );
    Variant( float value );
    Variant( double value );
    Variant( std::string&& value );
    Variant( const std::string& value );
    Variant( const char* value );
    Variant( String&& value );
    Variant( StringRef value );
    template< EnumType T >
    Variant( T enumValue );

    void operator=( const Variant& other );
    
    void Set( const char* value_string );
    void Set( const std::string& value_string );
    void Set( const String& valueString );
    
    const std::string to_string();
    String ToString();

    const TypeInfo& GeTypeInfo();
    static EVarType ToVarType( const TypeInfo& typeInfo );

    operator bool() const;
    operator char() const;
    operator short() const;
    operator unsigned short() const;
    operator int() const;
    operator unsigned int() const;
    operator long long() const;
    operator unsigned long long() const;
    operator float() const;
    operator double() const;
    template< EnumType T >
    operator T() const;

    const EVarType _type;

    union
    {
        bool                  _bool;
        char                  _char;
        short                 _short;
        unsigned short        _ushort;
        int                   _int;
        unsigned int          _uint;
        long long             _long;
        unsigned long long    _ulong;
        float                 _single;
        double                _double;
        std::string*          _string;
        String*               _wstring;
        char*                 _bytes;
        DateTime_             _dateTime;
    };
};

/// <summary>
/// Variant 타입 컨테이너 정의
/// </summary>
using VariantVector = Vector< Variant >;
using VariantList   = List< Variant >;

template< std::size_t N >
Variant::Variant( char( &value )[ N ] )
: _type{ EVarType::String }
{
    _string = new std::string{ value };
}

template< std::size_t N >
Variant::Variant( wchar_t( &value )[ N ] )
: _type{ EVarType::WString }
{
    _wstring = new std::wstring{ value };
}

template< EnumType T >
Variant::Variant( T enumValue )
: _type{ EVarType::ULong }
{
    new( this ) Variant( std::underlying_type_t< T >( _ulong ) );
}

template< EnumType T >
Variant::operator T() const
{
    return static_cast< T >( static_cast< std::underlying_type_t< T > >( _ulong ) );
}

}

