export module Mala.Core.Log2;

import Mala.Container.String;

import <iostream>;

export namespace Mala::Core
{

enum class ELogLevel;

extern inline ELogLevel GLogLevelFilter;// = ELogLevel::Debug;

enum class ELogLevel
{
    Debug,
    Info,
    Warring,
    Critical,
};

struct LogRecord
{

};

class Logger
{
public:
    template< ELogLevel TLogLevel, typename... Args >
    static void Log( const wchar_t* fmt, Args&&... args )
    {
        if ( TLogLevel < GLogLevelFilter )
          return;
    }

    LogRecord _redords;
};

//class console_log
//{
//    template< typename... Args >
//    void log( std::string_view& fmt, Args&&... args )
//    {
//        ::printf( fmt.data(), std::forward< Args >( args )... );
//    }
//};


} // namespace Mala::Net