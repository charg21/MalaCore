export module Mala.Math.Vector3Int:impl;

import Mala.Math;
import <cmath>;

namespace Mala::Math
{

constexpr Vector3Int::Vector3Int( const Vector2Int& rhs )
: _x{ rhs._x }
, _y{ rhs._y }
, _z{ 0 }
{
}

/*constexpr */Vector3Int::Vector3Int( i32 x, i32 y, i32 z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

/*constexpr */Vector3Int::Vector3Int( float x, float y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector3Int::Magnitude()
{
    return std::sqrtf( SqrMagnitude() );;
}

i32 Vector3Int::SqrMagnitude()
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

float Distance( const Vector3Int& lhs, const Vector3Int& rhs )
{
    return Vector3Int( lhs - rhs ).Magnitude();
}

Vector3Int operator+( const Vector3Int& lhs, const Vector3Int& rhs )
{
    return Vector3Int{ lhs._x + rhs._x, lhs._y + rhs._y, lhs._z + rhs._z };
}

Vector3Int operator-( const Vector3Int& lhs, const Vector3Int& rhs )
{
    return Vector3Int{ lhs._x - rhs._x, lhs._y - rhs._y, lhs._z - rhs._z };
}

Vector3Int operator*( const Vector3Int& lhs, i32 multiple )
{
    return Vector3Int{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector3Int operator*( i32 multiple, const Vector3Int& rhs )
{
    return Vector3Int{ rhs._x * multiple, rhs._y * multiple, rhs._z * multiple };
}

Vector3Int operator/( const Vector3Int& lhs, i32 d )
{
    return Vector3Int{ lhs._x / d, lhs._y / d, lhs._z / d };
}

Vector3Int operator/( i32 d, const Vector3Int& rhs )
{
    return Vector3Int{ rhs._x / d, rhs._y / d, rhs._z / d };
}

bool operator!=( const Vector3Int& lhs, const Vector3Int& rhs )
{
    return ( lhs._compareValue1 != rhs._compareValue1 ) || 
        ( lhs._compareValue2 != rhs._compareValue2 );
}

void operator*=( Vector3Int& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
    lhs._z *= multiple;
}

const Vector3Int Vector3Int::Zero { 0, 0, 0 };
const Vector3Int Vector3Int::Unit { 1, 1, 0 };
const Vector3Int Vector3Int::UnitX{ 1, 0, 0 };
const Vector3Int Vector3Int::UnitY{ 0, 1, 0 };
const Vector3Int Vector3Int::UnitZ{ 0, 0, 1 };

}