export module Mala.Math.Vector3;

import Mala.Core.Types;
import Mala.Math.Base;

export namespace Mala::Math
{

/// <summary>
/// 3차원 실수형 좌표를 나타내는 객체
/// </summary>
struct Vector3 final : public TVectorBase< float, 3 >
{
    friend Vector3 operator+( const Vector3& lhs, const Vector3& rhs );
    friend Vector3 operator-( const Vector3& lhs, const Vector3& rhs );
    friend Vector3 operator*( const Vector3& lhs, f32 multiple );
    friend Vector3 operator*( const Vector3& lhs, i32 multiple );
    friend Vector3 operator*( i32 multiple, const Vector3& rhs );
    friend Vector3 operator/( const Vector3& lhs, i32 d );
    friend Vector3 operator/( i32 d, const Vector3& rhs );

    using BaseType = float;

    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector3() = default;
    explicit constexpr Vector3( float x, float y, float z );
    explicit constexpr Vector3( int x, int y, float z );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude() const;
    float Size() const;
    
    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    float SqrMagnitude() const;
    float SizeSquared() const;

    /// <summary>
    /// 두 벡터간의 내적값을 반환한다.
    /// </summary>
    constexpr float Dot( const Vector3& vec ) const
    {
        return _x * vec._x + _y * vec._y + _z * vec._z;
    }

    constexpr Vector3 Cross( const Vector3& vec ) const
    {
        return Vector3
        {
            _y * vec._z - _z * vec._y,
            _z * vec._x - _x * vec._z,
            _x * vec._y - _y * vec._x
        };
    }

    ///
    Vector3 FromYaw( float yaw );

    /// <summary>
    /// 정규화 한다
    /// </summary>
    void Normalize();
    [[ nodiscard ]] Vector3 GetNormalize() const;

    const Vector3& operator+=( const Vector3& rhs );

public:
    static const Vector3 One;
    static const Vector3 Zero;
    static const Vector3 Unit;
    static const Vector3 UnitX;
    static const Vector3 UnitY;
    static const Vector3 UnitZ;
    static const Vector3 Up;
    static const Vector3 Down;
    static const Vector3 Left;
    static const Vector3 Right;

public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
            BaseType _z;
        };

        struct
        {
            size_t   _compareValue; 
            BaseType _z;
        };

        ScalarType _scalars{};
    };
};

Vector3 operator*( const Vector3& lhs, i32 multiple );
Vector3 operator+( const Vector3& lhs, const Vector3& rhs );
Vector3 operator-( const Vector3& lhs, const Vector3& rhs );
Vector3 operator*( const Vector3& lhs, i32 multiple );
Vector3 operator*( i32 multiple, const Vector3& rhs );
Vector3 operator/( const Vector3& lhs, i32 d );
Vector3 operator/( i32 d, const Vector3& rhs );

bool operator==( const Vector3& lhs, const Vector3& rhs );
bool operator!=( const Vector3& lhs, const Vector3& rhs );
void operator*=( Vector3& lhs, i32 multiple );


}
