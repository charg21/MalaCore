export module Mala.Math.Vector3:impl;

import Mala.Math;
import <cmath>;

namespace Mala::Math
{

constexpr Vector3::Vector3( float x, float y, float z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

constexpr Vector3::Vector3( int x, int y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector3::Magnitude() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector3::Size() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector3::SqrMagnitude() const
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

float Vector3::SizeSquared() const
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

void Vector3::Normalize()
{
    *this = GetNormalize();
}

Vector3 Vector3::GetNormalize() const
{
    float squareSum = SizeSquared();
    if ( squareSum == 1.f )
        return *this;

    if ( squareSum == 0.f )
        return Vector3::Zero;

    float invLength = Math::InvSqrt( squareSum );
    return Vector3{ _x * invLength, _y * invLength, _z * invLength };
}

const Vector3& Vector3::operator+=( const Vector3& rhs )
{
    _x += rhs._x;
    _y += rhs._y;
    _z += rhs._z;

    return *this;
}

Vector3 Vector3::FromYaw( float yawDegree )
{
    const float yawRadian = Deg2Rad( yawDegree );
    return Vector3{ std::sin( yawRadian ), 0.f, std::cos( yawRadian ) };
}

const Vector3 Vector3::One  {  1.0f, 1.0f,  1.0f };
const Vector3 Vector3::Zero {  0.0f, 0.0f,  0.0f };
const Vector3 Vector3::Unit {  1.0f, 1.0f,  0.0f };
const Vector3 Vector3::UnitX{  1.0f, 0.0f,  0.0f };
const Vector3 Vector3::UnitY{  0.0f, 1.0f,  0.0f };
const Vector3 Vector3::UnitZ{  0.0f, 0.0f,  1.0f };
const Vector3 Vector3::Up   {  0.0f, 0.0f,  1.0f };
const Vector3 Vector3::Down {  0.0f, 0.0f, -1.0f };
const Vector3 Vector3::Left { -1.0f, 0.0f,  0.0f };
const Vector3 Vector3::Right{  1.0f, 0.0f,  0.0f };


Vector3 operator+( const Vector3& lhs, const Vector3& rhs )
{
    return Vector3{ lhs._x + rhs._x, lhs._y + rhs._y, lhs._z + rhs._z };
}

Vector3 operator-( const Vector3& lhs, const Vector3& rhs )
{
    return Vector3{ lhs._x - rhs._x, lhs._y - rhs._y, lhs._z - rhs._z };
}

Vector3 operator*( const Vector3& lhs, f32 multiple )
{
    return Vector3{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector3 operator*( const Vector3& lhs, i32 multiple )
{
    return Vector3{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector3 operator*( i32 multiple, const Vector3& rhs )
{
    return Vector3{ rhs._x * multiple, rhs._y * multiple, rhs._z * multiple };
}

Vector3 operator/( const Vector3& lhs, i32 d )
{
    return Vector3{ lhs._x / d, lhs._y / d, lhs._z / d };
}

Vector3 operator/( i32 d, const Vector3& rhs )
{
    return Vector3{ rhs._x / d, rhs._y / d, rhs._z / d };
}

bool operator==( const Vector3& lhs, const Vector3& rhs )
{
    return ( lhs._compareValue == rhs._compareValue ) && ( lhs._z == rhs._z );
}

bool operator!=( const Vector3& lhs, const Vector3& rhs )
{
    return true;/* (lhs._compareValue1 != rhs._compareValue1) ||
        ( lhs._compareValue2 != rhs._compareValue2 );*/
}

void operator*=( Vector3& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
    lhs._z *= multiple;
}

}