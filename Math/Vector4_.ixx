export module Mala.Math.Vector4:impl;

import Mala.Math;
import <cmath>;

namespace Mala::Math
{

constexpr Vector4::Vector4( float x, float y, float z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

constexpr Vector4::Vector4( int x, int y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector4::Magnitude()
{
    return std::sqrtf( SqrMagnitude() );
}

int Vector4::SqrMagnitude()
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

const Vector4& Vector4::operator+=( const Vector4& rhs )
{
    _x += rhs._x;
    _y += rhs._y;
    _z += rhs._z;

    return *this;
}

const Vector4 Vector4::Zero{ 0.0f, 0.0f, 0.0f };
const Vector4 Vector4::Unit{ 1.0f, 1.0f, 0.0f };


Vector4 operator+( const Vector4& lhs, const Vector4& rhs )
{
    return Vector4{ lhs._x + rhs._x, lhs._y + rhs._y, lhs._z + rhs._z };
}

Vector4 operator-( const Vector4& lhs, const Vector4& rhs )
{
    return Vector4{ lhs._x - rhs._x, lhs._y - rhs._y, lhs._z - rhs._z };
}

Vector4 operator*( const Vector4& lhs, i32 multiple )
{
    return Vector4{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector4 operator*( i32 multiple, const Vector4& rhs )
{
    return Vector4{ rhs._x * multiple, rhs._y * multiple, rhs._z * multiple };
}

Vector4 operator/( const Vector4& lhs, i32 d )
{
    return Vector4{ lhs._x / d, lhs._y / d, lhs._z / d };
}

Vector4 operator/( i32 d, const Vector4& rhs )
{
    return Vector4{ rhs._x / d, rhs._y / d, rhs._z / d };
}

bool operator!=( const Vector4& lhs, const Vector4& rhs )
{
    return true;/* (lhs._compareValue1 != rhs._compareValue1) ||
        ( lhs._compareValue2 != rhs._compareValue2 );*/
}

void operator*=( Vector4& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
    lhs._z *= multiple;
}

}