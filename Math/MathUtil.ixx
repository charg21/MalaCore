export module Mala.Math.MathUtil;

import <numeric>;
import <cmath>;
import Mala.Core.Types;
import Mala.Math.Unit;
import Mala.Math.Vector2;


using namespace std;

export namespace Mala::Math
{

/// <summary>
/// 연산이 가능한 컨셉트를 정의한다
/// </summary>
template< typename T >
concept Calculable = requires( T param )
{
    requires std::is_integral_v< T > || std::is_floating_point_v< T >;
    requires !std::is_same_v< bool, T >;
    requires std::is_arithmetic_v< decltype( param + 1 ) >;
    requires !std::is_pointer_v< T >;
};

/// <summary>
/// 특정값을 min, max 범위내로 설정해준다
/// </summary>
auto Clamp( Calculable auto min, Calculable auto value , Calculable auto max )
{
    if ( min > value )
    {
        return min;
    }

    if ( max < value )
    {
        return max;
    }

    return value;
}

/// <summary>
/// 
/// </summary>
auto Repeat( Calculable auto value, Calculable auto max )
{
    if ( max < value )
    {
        return max;
    }

    return value;
}

/// <summary>
/// 
/// </summary>
auto Cos( float value, float max )
{
    if ( max < value )
    {
        return max;
    }

    return value;
}

/// <summary>
/// 컴파일 타임에 Base의 N승을 구해 반환한다.
/// </summary>
template< usize Base, usize N >
consteval usize Pow()
{
    if constexpr ( N == 0 )
        return 1;
    else
        return Base * Pow< Base, N - 1 >();
}

template< i64 N >
consteval i64 PowOfTwo()
{
    if constexpr ( N == 0 )
        return 1;
    else
        return 2 * PowOfTwo< N - 1 >();
}

/// <summary>
/// 컴파일 타임에 N이 2의 임의의 x승 여부를 반환한다.
/// </summary>
template< usize N >
consteval bool IsPowOfTwo()
{
    return N && !( N & ( N - 1 ) );
}

constexpr float Deg2Rad( float degree )
{
    return degree * PI / 180.f;
}

constexpr float Rad2Deg( float radian )
{
    return radian * 180.f * InvPI;
}

constexpr int TruncToInt( float value )
{
	return (int)( value );
}

constexpr int RountToInt( float value )
{
	return TruncToInt( roundf( value ) );
}

constexpr int FloorToInt( float value )
{
	return TruncToInt( floorf( value ));
}

int CeilToInt( float value )
{
	return TruncToInt( ceilf( value ) );
}

float Vector2ToYaw( const Vector2 &v )
{
    const float yawRadian = std::atan2( v._y, v._x );
    return Rad2Deg( yawRadian );
}

/// <summary>
/// src와 dest 사이에 선형 보간을 진행한다.
/// </summary>
template< class T >
constexpr T Lerp( const T& src, const T& dest, float alpha )
{
	return (T)( src + alpha * ( dest - src ) );
}

template< class T >
constexpr T Max( const T a, const T b )
{
	return ( a >= b ) ? a : b;
}

template< class T >
constexpr T Min( const T a, const T b )
{
	return ( a <= b ) ? a : b ;
}

template< class T >
T constexpr Max3( const T a, const T b, const T c )
{
	return Max( Max( a, b ), c );
}

template< class T >
T constexpr Min3( const T a, const T b, const T c )
{
	return Min( Min( a, b ), c );
}

template< class T >
constexpr T Abs( const T a )
{
	return (a >= (T)( 0 ) ) ? a : -a;
}

template< class T >
constexpr T Clamp( const T x, const T min, const T max )
{
	return x < min ? min : x < max ? x : max;
}

template<class T>
constexpr T Square( const T num )
{
    return num * num;
}

constexpr bool EqualsInTolerance( float InFloat1, float InFloat2, float InTolerance = SmallNumber )
{
	return Math::Abs( InFloat1 - InFloat2 ) <= InTolerance;
}

// 언리얼 엔진 코드에서 가져옴. 지정된 각도에 대한 두 삼각함수를 함께 가져오는 함수. 
constexpr void GetSinCosRad( float& outSin, float& outCos, float radian )
{
	// Copied from UE4 Source Code
	// Map Value to y in [-pi,pi], x = 2*pi*quotient + remainder.
	float quotient = ( InvPI * 0.5f ) * radian;
	if ( radian >= 0.0f )
	{
		quotient = (float)( (int)( quotient + 0.5f ) );
	}
	else
	{
		quotient = (float)( (int)( quotient - 0.5f ) );
	}
	float y = radian - ( 2.0f * PI ) * quotient;

	// Map y to [-pi/2,pi/2] with sin(y) = sin(Value).
	float sign = 0.f;
	if ( y > HalfPI )
	{
		y = PI - y;
		sign = -1.0f;
	}
	else if ( y < -HalfPI )
	{
		y = -PI - y;
		sign = -1.0f;
	}
	else
	{
		sign = +1.0f;
	}

	float y2 = y * y;

	// 11-degree minimax approximation
	outSin = (((((-2.3889859e-08f * y2 + 2.7525562e-06f) * y2 - 0.00019840874f) * y2 + 0.0083333310f) * y2 - 0.16666667f) * y2 + 1.0f) * y;

	// 10-degree minimax approximation
	float p = ((((-2.6051615e-07f * y2 + 2.4760495e-05f) * y2 - 0.0013888378f) * y2 + 0.041666638f) * y2 - 0.5f) * y2 + 1.0f;
	outCos = sign * p;
}

constexpr void GetSinCos( float& outSin, float& outCos, float degree )
{
	if ( degree == 0.f )
	{
		outSin = 0.f;
		outCos = 1.f;
	}
	else if ( degree == 90.f )
	{
		outSin = 1.f;
		outCos = 0.f;
	}
	else if ( degree == 180.f )
	{
		outSin = 0.f;
		outCos = -1.f;
	}
	else if ( degree == 270.f )
	{
		outSin = -1.f;
		outCos = 0.f;
	}
	else
	{
		GetSinCosRad( outSin, outCos, Math::Deg2Rad( degree ) );
	}
}

float FMod( float x, float y )
{
	if ( fabsf( y ) <= SmallNumber )
	{
		return 0.f;
	}

	const float quotient = (float)( TruncToInt( x / y ) );
	float intPortion = y * quotient;
	if ( fabsf( intPortion ) > fabsf( x ) )
	{
		intPortion = x;
	}

	return ( x - intPortion );
}

#include <immintrin.h>
// 언리얼 엔진 코드에서 가져옴. 고속 역제곱근 공식
float InvSqrt( float folatValue )
{
	// Performs two passes of Newton-Raphson iteration on the hardware estimate
	//    v^-0.5 = x
	// => x^2 = v^-1
	// => 1/(x^2) = v
	// => F(x) = x^-2 - v
	//    F'(x) = -2x^-3

	//    x1 = x0 - F(x0)/F'(x0)
	// => x1 = x0 + 0.5 * (x0^-2 - Vec) * x0^3
	// => x1 = x0 + 0.5 * (x0 - Vec * x0^3)
	// => x1 = x0 + x0 * (0.5 - 0.5 * Vec * x0^2)
	//
	// This final form has one more operation than the legacy factorization (X1 = 0.5*X0*(3-(Y*X0)*X0)
	// but retains better accuracy (namely InvSqrt(1) = 1 exactly).

	const __m128 fOneHalf = _mm_set_ss( 0.5f );
	__m128 Y0, X0, X1, X2, FOver2;
	float temp;

	Y0     = _mm_set_ss( folatValue );
	X0     = _mm_rsqrt_ss( Y0 );	// 1/sqrt estimate (12 bits)
	FOver2 = _mm_mul_ss( Y0, fOneHalf );

	// 1st Newton-Raphson iteration
	X1 = _mm_mul_ss( X0, X0 );
	X1 = _mm_sub_ss( fOneHalf, _mm_mul_ss( FOver2, X1 ) );
	X1 = _mm_add_ss( X0, _mm_mul_ss( X0, X1 ) );

	// 2nd Newton-Raphson iteration
	X2 = _mm_mul_ss( X1, X1 );
	X2 = _mm_sub_ss( fOneHalf, _mm_mul_ss( FOver2, X2 ) );
	X2 = _mm_add_ss( X1, _mm_mul_ss( X1, X2 ) );

	_mm_store_ss( &temp, X2 );
	return temp;
}

} // namespace Mala::Math