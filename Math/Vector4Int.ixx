export module Mala.Math.Vector4Int;

import Mala.Core.Types;
import Mala.Math.Base;
import Mala.Math.Vector2Int;

export namespace Mala::Math
{

/// <summary>
/// 3차원 정수형 좌표를 나타내는 객체
/// </summary>
struct Vector4Int final : public TVectorBase< i32, 3 >
{
    friend Vector4Int operator+( const Vector4Int& lhs, const Vector4Int& rhs );
    friend Vector4Int operator-( const Vector4Int& lhs, const Vector4Int& rhs );
    friend Vector4Int operator*( const Vector4Int& lhs, i32 multiple );
    friend Vector4Int operator*( i32 multiple, const Vector4Int& rhs );
    friend Vector4Int operator/( const Vector4Int& lhs, i32 d );
    friend Vector4Int operator/( i32 d, const Vector4Int& rhs );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector4Int() = default;
    constexpr Vector4Int( const Vector2Int& rhs );
    /*explicit constexpr */Vector4Int( i32 x, i32 y, i32 z );
    /*explicit constexpr */Vector4Int( float x, float y, float z );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude();

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    i32 SqrMagnitude();

public:
    static const Vector4Int Zero;
    static const Vector4Int Unit;

public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
            BaseType _z;
        };

        ScalarType _scalars{};

        struct
        {
            long long _compareValue1;
            i32       _compareValue2;
        };
    };
};

Vector4Int operator*( const Vector4Int& lhs, i32 multiple );
Vector4Int operator+( const Vector4Int& lhs, const Vector4Int& rhs );
Vector4Int operator-( const Vector4Int& lhs, const Vector4Int& rhs );
Vector4Int operator*( const Vector4Int& lhs, i32 multiple );
Vector4Int operator*( i32 multiple, const Vector4Int& rhs );
Vector4Int operator/( const Vector4Int& lhs, i32 d );
Vector4Int operator/( i32 d, const Vector4Int& rhs );

bool operator!=( const Vector4Int& lhs, const Vector4Int& rhs );
void operator*=( Vector4Int& lhs, i32 multiple );

}
