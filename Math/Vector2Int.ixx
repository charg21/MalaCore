export module Mala.Math.Vector2Int;

import Mala.Core.Types;
import Mala.Math.Base;

export namespace Mala::Math
{

/// <summary>
/// 2차원 정수형 좌표를 나타내는 객체
/// </summary>
/// <reference>
/// https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Vector2Int.cs
/// </reference>
struct Vector2Int final : public TVectorBase< i32, 2 >
{
    friend Vector2Int operator+( const Vector2Int& lhs, const Vector2Int& rhs );
    friend Vector2Int operator-( const Vector2Int& lhs, const Vector2Int& rhs );
    friend Vector2Int operator*( const Vector2Int& lhs, i32 multiple );
    friend Vector2Int operator*( i32 multiple, const Vector2Int& rhs );
    friend Vector2Int operator/( const Vector2Int& lhs, i32 d );
    friend Vector2Int operator/( i32 d, const Vector2Int& rhs );
    friend float Distance( const Vector2Int& lhs, const Vector2Int& rhs );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector2Int() = default;
    /*explicit constexpr */constexpr Vector2Int( i32 x, i32 y )
    : _x{ x }
    , _y{ y }
    {}
    /*explicit constexpr */Vector2Int( float x, float y );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude();

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    i32 SqrMagnitude();

    /// <summary>
    /// 값을 제한한다
    /// </summary>
    void Clamp( const Vector2Int& min, const Vector2Int& max );

    /// <summary>
    /// 
    /// </summary>
    bool IsInRange( const Vector2Int& rhs );


public:
    static const Vector2Int Zero;
    static const Vector2Int Unit;


public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
        };

        ScalarType _scalars{};
        long long  _compareValue;
    };
};

Vector2Int operator*( const Vector2Int& lhs, i32 multiple );
Vector2Int operator+( const Vector2Int& lhs, const Vector2Int& rhs );
Vector2Int operator-( const Vector2Int& lhs, const Vector2Int& rhs );
Vector2Int operator*( const Vector2Int& lhs, i32 multiple );
Vector2Int operator*( i32 multiple, const Vector2Int& rhs );
Vector2Int operator/( const Vector2Int& lhs, i32 d );
Vector2Int operator/( i32 d, const Vector2Int& rhs );

bool operator==( const Vector2Int& lhs, const Vector2Int& rhs );
bool operator!=( const Vector2Int& lhs, const Vector2Int& rhs );
void operator*=( Vector2Int& lhs, i32 multiple );

}
