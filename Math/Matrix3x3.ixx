export module Mala.Math.Matrix3x3;

import <array>;
import <vector>;

import Mala.Math.Vector3;

namespace Mala::Math
{

struct Matrix3x3
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	constexpr Matrix3x3() = default;
	explicit constexpr Matrix3x3( const Vector3& InCol0, const Vector3& InCol1, const Vector3& col2 ) 
	: _cols{ InCol0, InCol1, col2 }
	{ 
	}

//	// 연산자 
//	const Vector3& operator[](BYTE InIndex) const;
//	Vector3& operator[](BYTE InIndex);
//
//	Matrix3x3 operator*(float InScalar) const;
//	Matrix3x3 operator*(const Matrix3x3& InMatrix) const;
//	Vector3 operator*(const Vector3& InVector) const;
//	friend Vector3 operator*=(Vector3& InVector, const Matrix3x3& InMatrix)
//	{
//		InVector = InMatrix * InVector;
//		return InVector;
//	}
//	Vector2 operator*(const Vector2& InVector) const;
//	friend Vector2 operator*=(Vector2& InVector, const Matrix3x3& InMatrix)
//	{
//		InVector = InMatrix * InVector;
//		return InVector;
//	}
//
//	// 멤버함수 
//	Matrix2x2 ToMatrix2x2() const;
//	void SetIdentity();
//	Matrix3x3 Transpose() const;
//
//	std::vector<std::string> ToStrings() const;

//	// 정적멤버변수 
public:
	static const Matrix3x3 Identity;
	inline static constexpr size_t Rank = 3;

	std::array< Vector3, Rank > _cols;// = { Vector3::UnitX, Vector3::UnitY, Vector3::UnitZ };
};

const Matrix3x3 Matrix3x3::Identity{ Vector3::UnitX, Vector3::UnitY, Vector3::UnitZ };

//void Matrix3x3::SetIdentity()
//{
//	*this = Matrix3x3::Identity;
//}
//
//Matrix3x3 Matrix3x3::Transpose() const
//{
//	return Matrix3x3(
//		Vector3(Cols[0].X, Cols[1].X, Cols[2].X),
//		Vector3(Cols[0].Y, Cols[1].Y, Cols[2].Y),
//		Vector3(Cols[0].Z, Cols[1].Z, Cols[2].Z)
//	);
//}
//
//const Vector3& Matrix3x3::operator[](BYTE InIndex) const
//{
//	assert(InIndex < Rank);
//	return Cols[InIndex];
//}
//
//Vector3& Matrix3x3::operator[](BYTE InIndex)
//{
//	assert(InIndex < Rank);
//	return Cols[InIndex];
//}
//
//Matrix3x3 Matrix3x3::operator*(float InScalar) const
//{
//	return Matrix3x3(
//		Cols[0] * InScalar,
//		Cols[1] * InScalar,
//		Cols[2] * InScalar
//	);
//}
//
//Matrix3x3 Matrix3x3::operator*(const Matrix3x3 &InMatrix) const
//{
//	Matrix3x3 transposedMatrix = Transpose();
//	return Matrix3x3(
//		Vector3(transposedMatrix[0].Dot(InMatrix[0]), transposedMatrix[1].Dot(InMatrix[0]), transposedMatrix[2].Dot(InMatrix[0])),
//		Vector3(transposedMatrix[0].Dot(InMatrix[1]), transposedMatrix[1].Dot(InMatrix[1]), transposedMatrix[2].Dot(InMatrix[1])),
//		Vector3(transposedMatrix[0].Dot(InMatrix[2]), transposedMatrix[1].Dot(InMatrix[2]), transposedMatrix[2].Dot(InMatrix[2]))
//	);
//
//}
//
//Vector3 Matrix3x3::operator*(const Vector3& InVector) const
//{
//	Matrix3x3 transposedMatrix = Transpose();
//	return Vector3(
//		transposedMatrix[0].Dot(InVector),
//		transposedMatrix[1].Dot(InVector),
//		transposedMatrix[2].Dot(InVector)
//	);
//}
//
//Vector2 Matrix3x3::operator*(const Vector2& InVector) const
//{
//	Vector3 v3(InVector);
//	v3 *= *this;
//
//	return v3.ToVector2();
//}
//
//Matrix2x2 Matrix3x3::ToMatrix2x2() const
//{
//	return Matrix2x2(Cols[0].ToVector2(), Cols[1].ToVector2());
//}
//
}