export module Mala.Math.Transform:impl;

import Mala.Math;
import Mala.Math.Matrix4x4;

using namespace Mala::Math;

//namespace Mala::Math
//{

Transform::Transform( const Vector3& position )
: _position{ position }
{
}

Transform::Transform( const Vector3& position, const Quaternion& rotation )
: _position{ position }
, _rotation{ rotation }
{
}

Transform::Transform( const Vector3& position, const Quaternion& rotation, const Vector3& scale )
: _position( position )
, _rotation( rotation )
, _scale   ( scale    ) 
{
}

void Transform::SetPosition( const Vector3& position )
{
    _position = position;
}

void Transform::AddPosition( const Vector3& deltaPosition )
{
    _position += deltaPosition; 
}

//Transform::Transform( const Matrix4x4& matrix )
//{
//}


//}