export module Mala.Math.Vector2Int:impl;

import Mala.Math;
//import Mala.Math.VectorInt2;

namespace Mala::Math
{

const Vector2Int Vector2Int::Zero{ 0, 0 };
const Vector2Int Vector2Int::Unit{ 1, 1 };

/*constexpr */Vector2Int::Vector2Int( float x, float y )
: _x( x )
, _y( y )
{}

float Vector2Int::Magnitude()
{
    return std::sqrtf( SqrMagnitude() );
}

i32 Vector2Int::SqrMagnitude()
{
    return ( _x * _x ) + ( _y * _y );
}

void Vector2Int::Clamp( const Vector2Int& min, const Vector2Int& max )
{
    _x = Mala::Math::Clamp( min._x, _x, max._x );
    _y = Mala::Math::Clamp( min._y, _y, max._y );
}

Vector2Int operator+( const Vector2Int& lhs, const Vector2Int& rhs )
{
    return Vector2Int{ lhs._x + rhs._x, lhs._y + rhs._y };
}

Vector2Int operator-( const Vector2Int& lhs, const Vector2Int& rhs )
{
    return Vector2Int{ lhs._x - rhs._x, lhs._y - rhs._y };
}

Vector2Int operator*( const Vector2Int& lhs, i32 multiple )
{
    return Vector2Int{ lhs._x * multiple , lhs._y * multiple };
}

Vector2Int operator*( i32 multiple, const Vector2Int& rhs )
{
    return Vector2Int{ rhs._x * multiple , rhs._y * multiple };
}

Vector2Int operator/( const Vector2Int& lhs, i32 d )
{
    return Vector2Int{ lhs._x / d, lhs._y / d };
}

Vector2Int operator/( i32 d, const Vector2Int& rhs )
{
    return Vector2Int{ rhs._x / d, rhs._y / d };
}

float Distance( const Vector2Int& lhs, const Vector2Int& rhs )
{
    return Vector2Int( lhs - rhs ).Magnitude();
}

bool operator==( const Vector2Int& lhs, const Vector2Int& rhs )
{
    return lhs._compareValue == rhs._compareValue;
}

bool operator!=( const Vector2Int& lhs, const Vector2Int& rhs )
{
    return lhs._compareValue != rhs._compareValue;
}

void operator*=( Vector2Int& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
}

}