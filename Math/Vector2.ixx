export module Mala.Math.Vector2;

import Mala.Math.Base;

export namespace Mala::Math
{

/// <summary>
/// 2차원 실수형 좌표를 나타내는 객체
/// </summary>
struct Vector2 final : public TVectorBase< float, 2 >
{
    using BaseType = float;

    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector2() = default;
    /*explicit constexpr*/ Vector2( float x, float y = 0.f );
    /*explicit constexpr*/ Vector2( int x, int y = 0 );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude() const;
    float Size() const;

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    float SqrMagnitude() const;
    float SizeSquared() const;

    /// <summary>
    /// 
    /// </summary>
    Vector2 Normalized();

    /// <summary>
    /// 정규화 한다
    /// </summary>
    void Normalize();
    [[ nodiscard ]] Vector2 GetNormalize() const;

    float Angle() const;
	float AngleInDegree() const;


    static Vector2 FromYaw( float yaw );

public:
    static const Vector2 Zero;
    static const Vector2 Unit;
    static const Vector2 UnitX;
    static const Vector2 UnitY;
    static const Vector2 Up;
    static const Vector2 Down;
    static const Vector2 Left;
    static const Vector2 Right;

public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
        };

        ScalarType _scalars{};
    };
};

}
