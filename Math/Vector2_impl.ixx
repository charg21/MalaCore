export module Mala.Math.Vector2:impl;

import Mala.Math;
import Mala.Math.Vector2;

namespace Mala::Math
{

/*constexpr */Vector2::Vector2( float x, float y )
: _x{ x }
, _y{ y }
{}

/*constexpr */Vector2::Vector2( int x, int y )
: _x( x )
, _y( y )
{}

float Vector2::Magnitude() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector2::Size() const
{
    return std::sqrtf( SqrMagnitude() );
}

float Vector2::SqrMagnitude() const
{
    return ( _x * _x ) + ( _y * _y );
}

float Vector2::SizeSquared() const
{
    return ( _x * _x ) + ( _y * _y );
}

void Vector2::Normalize()
{
    *this = GetNormalize();
}

Vector2 Vector2::GetNormalize() const
{
    float squareSum = SizeSquared();
    if ( squareSum == 1.f )
        return *this;

    if ( squareSum == 0.f )
        return Vector2::Zero;

    float invLength = Math::InvSqrt( squareSum );
    return Vector2{ _x * invLength, _y * invLength };
}

float Vector2::Angle() const
{
    return atan2f( _y, _x );
}

float Vector2::AngleInDegree() const
{
    return Math::Rad2Deg( atan2f( _y, _x ) );
}

Vector2 Vector2::FromYaw( float yawDegree )
{
    const float yawRadian = Deg2Rad( yawDegree );
    return Vector2{ std::sin( yawRadian ), std::cos( yawRadian ) };
}

const Vector2 Vector2::Zero {  0.0f,  0.0f };
const Vector2 Vector2::Unit {  1.0f,  1.0f };
const Vector2 Vector2::UnitX{  1.0f,  0.0f };
const Vector2 Vector2::UnitY{  0.0f,  1.0f };
const Vector2 Vector2::Up   {  0.0f,  1.0f };
const Vector2 Vector2::Down {  0.0f, -1.0f };
const Vector2 Vector2::Left { -1.0f,  0.0f };
const Vector2 Vector2::Right{  1.0f,  0.0f };

}