export module Mala.Math.Transform;

import <array>;
import Mala.Math.Vector2;
import Mala.Math.Vector3;
import Mala.Math.Unit;
import Mala.Math.Quaternion;
import Mala.Math.Matrix4x4;

// struct Matrix4x4;

export namespace Mala::Math
{

struct Transform
{
private:
    Vector3    _position;
    Quaternion _rotation;
    Vector3    _scale = Vector3::One;

public:
    /// <summary>
    /// ������
    /// </summary>
    constexpr Transform() = default;
    Transform( const Vector3& position );
    Transform( const Vector3& position, const Quaternion& rotation );
    Transform( const Vector3& position, const Quaternion& rotation, const Vector3& scale );
    //Transform( const Matrix4x4& matrix )
    //{}

    void Translate( const Vector3& translation )
    {
        _position += translation;
    }

    void Translate( float x, float y, float z )
    {
        Translate( Vector3{ x, y, z } );
    }

    void Rotate( const Vector3& eulers )
    {
        auto eulerRot = Quaternion::Euler( eulers._x, eulers._y, eulers._z );

        //_rotation = _rotation * 
    }

public: 
	void SetPosition( const Vector3& position );
	void AddPosition( const Vector3& deltaPosition );

};


} // namespace Mala::Math