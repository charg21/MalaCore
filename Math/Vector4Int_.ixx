export module Mala.Math.Vector4Int:impl;

import Mala.Math;
import <cmath>;

namespace Mala::Math
{

constexpr Vector4Int::Vector4Int( const Vector2Int& rhs )
: _x{ rhs._x }
, _y{ rhs._y }
, _z{ 0 }
{
}

/*constexpr */Vector4Int::Vector4Int( i32 x, i32 y, i32 z )
: _x{ x }
, _y{ y }
, _z{ z }
{}

/*constexpr */Vector4Int::Vector4Int( float x, float y, float z )
: _x( x )
, _y( y )
, _z( z )
{}

float Vector4Int::Magnitude()
{
    return std::sqrtf( SqrMagnitude() );
}

i32 Vector4Int::SqrMagnitude()
{
    return ( _x * _x ) + ( _y * _y ) + ( _z * _z );
}

float Distance( const Vector4Int& lhs, const Vector4Int& rhs )
{
    return Vector4Int( lhs - rhs ).Magnitude();
}

Vector4Int operator+( const Vector4Int& lhs, const Vector4Int& rhs )
{
    return Vector4Int{ lhs._x + rhs._x, lhs._y + rhs._y, lhs._z + rhs._z };
}

Vector4Int operator-( const Vector4Int& lhs, const Vector4Int& rhs )
{
    return Vector4Int{ lhs._x - rhs._x, lhs._y - rhs._y, lhs._z - rhs._z };
}

Vector4Int operator*( const Vector4Int& lhs, i32 multiple )
{
    return Vector4Int{ lhs._x * multiple, lhs._y * multiple, lhs._z * multiple };
}

Vector4Int operator*( i32 multiple, const Vector4Int& rhs )
{
    return Vector4Int{ rhs._x * multiple, rhs._y * multiple, rhs._z * multiple };
}

Vector4Int operator/( const Vector4Int& lhs, i32 d )
{
    return Vector4Int{ lhs._x / d, lhs._y / d, lhs._z / d };
}

Vector4Int operator/( i32 d, const Vector4Int& rhs )
{
    return Vector4Int{ rhs._x / d, rhs._y / d, rhs._z / d };
}

bool operator!=( const Vector4Int& lhs, const Vector4Int& rhs )
{
    return ( lhs._compareValue1 != rhs._compareValue1 ) || 
        ( lhs._compareValue2 != rhs._compareValue2 );
}

void operator*=( Vector4Int& lhs, i32 multiple )
{
    lhs._x *= multiple;
    lhs._y *= multiple;
    lhs._z *= multiple;
}

const Vector4Int Vector4Int::Zero{ 0, 0, 0 };
const Vector4Int Vector4Int::Unit{ 1, 1, 0 };

}