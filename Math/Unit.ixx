export module Mala.Math.Unit;

export namespace Mala::Math
{

constexpr float PI         { 3.14159265358979323846f };
constexpr float TwoPI      { 2.f * PI                };
constexpr float HalfPI     { 1.57079632679f          };
constexpr float InvPI      { 0.31830988618f          }; // 원주율 역수 값
constexpr float SmallNumber{ 1.e-8f                  }; 
//constexpr float Deg2Rad    { PI / 180.0f             };

} // namespace Mala::Math