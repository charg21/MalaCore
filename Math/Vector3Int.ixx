export module Mala.Math.Vector3Int;

import Mala.Core.Types;
import Mala.Math.Base;
import Mala.Math.Vector2Int;

export namespace Mala::Math
{

/// <summary>
/// 3차원 정수형 좌표를 나타내는 객체
/// </summary>
struct Vector3Int final : public TVectorBase< i32, 3 >
{
    friend Vector3Int operator+( const Vector3Int& lhs, const Vector3Int& rhs );
    friend Vector3Int operator-( const Vector3Int& lhs, const Vector3Int& rhs );
    friend Vector3Int operator*( const Vector3Int& lhs, i32 multiple );
    friend Vector3Int operator*( i32 multiple, const Vector3Int& rhs );
    friend Vector3Int operator/( const Vector3Int& lhs, i32 d );
    friend Vector3Int operator/( i32 d, const Vector3Int& rhs );

public:
    /// <summary>
    /// 생성자
    /// </summary>
    constexpr Vector3Int() = default;
    constexpr Vector3Int( const Vector2Int& rhs );
    /*explicit constexpr */Vector3Int( i32 x, i32 y, i32 z );
    /*explicit constexpr */Vector3Int( float x, float y, float z );

    /// <summary>
    /// 벡터의 길이를 반환한다
    /// </summary>
    float Magnitude();

    /// <summary>
    /// 벡터의 제곱 길이를 반환한다
    /// </summary>
    i32 SqrMagnitude();

public:
    static const Vector3Int Zero;
    static const Vector3Int Unit;
    static const Vector3Int UnitX;
    static const Vector3Int UnitY;
    static const Vector3Int UnitZ;

public:
    union
    {
        struct
        {
            BaseType _x;
            BaseType _y;
            BaseType _z;
        };

        ScalarType _scalars{};

        struct
        {
            long long _compareValue1;
            i32       _compareValue2;
        };
    };
};

Vector3Int operator*( const Vector3Int& lhs, i32 multiple );
Vector3Int operator+( const Vector3Int& lhs, const Vector3Int& rhs );
Vector3Int operator-( const Vector3Int& lhs, const Vector3Int& rhs );
Vector3Int operator*( const Vector3Int& lhs, i32 multiple );
Vector3Int operator*( i32 multiple, const Vector3Int& rhs );
Vector3Int operator/( const Vector3Int& lhs, i32 d );
Vector3Int operator/( i32 d, const Vector3Int& rhs );

bool operator!=( const Vector3Int& lhs, const Vector3Int& rhs );
void operator*=( Vector3Int& lhs, i32 multiple );

}
