export module Mala.Math.Matrix2x2;

import <array>;
import <vector>;
import <string>;

import Mala.Core.Types;
import Mala.Math.Vector2;

using BYTE = unsigned char;

namespace Mala::Math
{

struct Matrix2x2
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	constexpr Matrix2x2() = default;
	explicit constexpr Matrix2x2( const Vector2& col0, const Vector2& col1 ) 
	: _cols{ col0, col1 }
	{ 
	}

	/// <summary>
	/// 연산자
	/// </summary>
	const Vector2& operator[]( i32 dex ) const;
	Vector2& operator[]( i32 dex );
	Matrix2x2 operator*(float Scalar) const;
	Matrix2x2 operator*( const Matrix2x2& matrix ) const;
	Vector2 operator*( const Vector2& vector ) const;
	friend Vector2 operator*=( Vector2& vector, const Matrix2x2& matrix )
	{
		vector = matrix * vector;
		return vector;
	}

	// 멤버함수 
	void SetIdentity();
	Matrix2x2 Transpose() const;

	std::vector< std::string > ToStrings() const;

public:
	static const Matrix2x2 Identity;
	static constexpr i32 Rank = 2;

public:
	std::array< Vector2, Rank > _cols = { Vector2::UnitX, Vector2::UnitY };
};

const Matrix2x2 Matrix2x2::Identity{ 0.0f, 0.0f };

void Matrix2x2::SetIdentity()
{
	*this = Matrix2x2::Identity;
}

const Vector2& Matrix2x2::operator[]( i32 dex ) const
{
	//assert(dex < Rank);
	return _cols[ dex ];
}

Matrix2x2 Matrix2x2::Transpose() const
{
	return Matrix2x2
	{
		Vector2{ _cols[ 0 ]._x, _cols[ 1 ]._x },
		Vector2{ _cols[ 0 ]._y, _cols[ 1 ]._y }
	};
}



Vector2& Matrix2x2::operator[]( i32 dex )
{
	//assert(dex < Rank);
	return _cols[ dex ];
}

//Matrix2x2 Matrix2x2::operator*(float Scalar) const
//{
//	return Matrix2x2(
//		_cols[0] * Scalar,
//		_cols[1] * Scalar
//	);
//}

//Matrix2x2 Matrix2x2::operator*(const Matrix2x2 &Matrix) const
//{
//	Matrix2x2 transposedMatrix = Transpose();
//	return Matrix2x2(
//		Vector2(transposedMatrix[0].Dot(Matrix[0]), transposedMatrix[1].Dot(Matrix[0])),
//		Vector2(transposedMatrix[0].Dot(Matrix[1]), transposedMatrix[1].Dot(Matrix[1]))
//	);
//}
//
//Vector2 Matrix2x2::operator*(const Vector2& Vector) const
//{
//	Matrix2x2 transposedMatrix = Transpose();
//	return Vector2(
//		transposedMatrix[0].Dot(Vector),
//		transposedMatrix[1].Dot(Vector)
//	);
//}

}