export module Mala.Math.Quaternion;

import <array>;
import Mala.Math.Vector3;

export namespace Mala::Math
{

struct Quaternion
{
    /// <summary>
    /// ������
    /// </summary>
    constexpr Quaternion() = default;
    explicit constexpr Quaternion( float x, float y, float z, float w );
    explicit constexpr Quaternion( float yaw, float pitch, float roll );
    explicit constexpr Quaternion( const Vector3& axis, float angleDegree );

    float _x;
    float _y;
    float _z;
    float _w; // real part

    void Normalize()
    {
        const float squareSum = _x * _x + _y * _y + _z * _z + _w * _w;
    }

    Quaternion& Inverse()
    {
        Quaternion::Inverse( *this );

        return *this;
    }

    [[ nodiscard ]] static Quaternion Euler( float x, float y, float z )
    {
    }

    [[ nodiscard ]] static Quaternion Inverse( const Quaternion& rotation )
    {
    }

    constexpr float RealPart() const 
    { 
        return _w; 
    }

    constexpr Vector3 ImaginaryPart() const 
    { 
        return Vector3{ _x, _y, _z }; 
    }

};


} // namespace Mala::Math