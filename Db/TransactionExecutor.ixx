export module Mala.Db.TxExecutor;

#include "../MalaMacro.h"

import <memory>;
import <string>;
import <string_view>;
import <vector>;
import <deque>;
import <functional>;
import <unordered_set>;

import Mala.Core.Crash;
import Mala.Container.WriteFreeQueue;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.TxContext;
import Mala.Threading.JobExecutor;

using namespace Mala::Threading;
using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

class TransactionExecutor
//    : public std::enable_shared_from_this< TransactionExecutor >
{
    friend class TransactionContext;

public:
    using TxContextQueue = WriteFreeQueue< TxContextPtr >;
    using ResultJob      = std::function< void( bool ) >;
    using Job            = std::function< void() >;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TransactionExecutor( JobExecutor& executor )
    : _executor{ executor }
    {
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~TransactionExecutor() = default;

    /// <summary>
    /// 트랜잭션을 실행한다
    /// </summary>
    void ExecuteTx( TxContextPtr&& tx, ResultJob&& resultJob )
    {
        tx->_txState   = ETxContextState::WaitingToExecute;
        tx->_owner     = shared_from_tx_executor();
        tx->_resultJob = std::move( resultJob );

        _txContexts.Enqueue( std::move( tx ) );

        TryExecuteTxAll();
    }

private:
    /// <summary>
    /// 
    /// </summary>
    void TryExecuteTxAll()
    {
        i64 executedTx{};

        LOOP
        {
            if ( _isExecuting.exchange( true ) )
                return;

            executedTx += ExecuteTxAll();
            _isExecuting = false;

            if ( _txContexts.Empty() )
                return;
        }
    }

    /// <summary>
    ///
    /// </summary>
    int ExecuteTxAll()
    {
        i64 totalExecutedCount = 0;

        std::deque< TxContextPtr > txContextList;

        LOOP
        {
            _txContexts.TakeAll( txContextList );
            if ( txContextList.empty() )
                return totalExecutedCount;

            while( !txContextList.empty() )
            {
                TxContextPtr txPtr = std::move( txContextList.back() );
                txContextList.pop_back();

                Execute( [ txPtr = std::move( txPtr ) ]()
                {
                    txPtr->Execute();
                } );
                
                totalExecutedCount += 1;
            }
        }

        return 0;
    }

    virtual void Execute( Job&& job )
    {
        _executor.DoAsync( std::move( job ) );
    }

    virtual void Execute( const Job& job )
    {
        _executor.DoAsync( job );
    }

    virtual std::shared_ptr< TransactionExecutor > shared_from_tx_executor() = 0;

protected:
    /// <summary>
    /// 트랜잭션 작업 큐
    /// </summary>
    TxContextQueue _txContexts;

    /// <summary>
    /// 실행중 여부
    /// </summary>
    std::atomic< bool > _isExecuting;

    /// <summary>
    /// 작업 실행기
    /// </summary>
    JobExecutor& _executor;
};

/// <summary>
/// 트랜잭션 실행기 타입 재정의
/// </summary>
using TxExecutor    = Mala::Db::TransactionExecutor;
using TxExecutorPtr = std::shared_ptr< TxExecutor >;
using TxExecutorRef = const TxExecutorPtr&;

NAMESPACE_END( Mala::Db )
