export module Mala.Db.ColumnInfo:impl;

import <string>;
import <string_view>;

import Mala.Db.ColumnInfo;

using namespace Mala::Db;

ColumnInfo::ColumnInfo( const TableInfo& table, int columnNo )
: _table   { table    }
, _columnNo{ columnNo }
{
}
