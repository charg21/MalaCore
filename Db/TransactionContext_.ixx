export module Mala.Db.TransactionContext:impl;

import <iostream>;

import Mala.Core.Defer;
import Mala.Memory;
import Mala.Db.DbConnection;
import Mala.Db.TxContext;
import Mala.Db.TxExecutor;
import Mala.Threading.JobExecutor;

using namespace Mala::Db;


void TransactionContext::Execute()
{
    switch( _txState )
    {
    case ETxContextState::Created:
        // TODO: 로그
        break;

    case TxState::WaitingToExecute: 
        Execute_LogicJob();
        break;

    case TxState::Executing:
        Execute_DbJob();
        break;

    case TxState::ExecutingDb: 
        Execute_Sync();
        break;

    case TxState::ExecutingSync:
        Execute_Completed();
        break;

    case ETxContextState::Completed:
    case ETxContextState::Canceled:
        break;

    case TxState::Failed:
        Execute_Failed();
        break;
    }
}

void TransactionContext::Cancel()
{
    _txState = TxState::Canceled;
}

void TransactionContext::ChangeState( TxState newState )
{
    _txState = newState;
}

void TransactionContext::Execute_LogicJob()
{
    if ( _txState != TxState::WaitingToExecute )
    {
        // TODO 로그
        return;
    }

    _txState = ETxContextState::Executing;

    ExecuteDbJob( [ tx = TxContextPtr( this ) ]()
    { 
        tx->Execute();
    } );
}

void TransactionContext::Execute_DbJob()
{
    if ( _txState != TxState::Executing )
    {
        // TODO 로그
        return;
    }

    _txState    = ETxContextState::ExecutingDb;
    _connection = GetConnection();
    _connection->BeginTransaction();

    bool result = [ this ]()
    {
        bool result = true;
        for ( auto& dbJob : _dbJobList )
        {
            result = dbJob( _connection );
            if ( !result )
                return false;
        }

        // Pre DbJob ( In DbThread )
        size_t affectedRowCount = 0;
        for ( auto& [ queryType, syncable ] : _modelList )
        {
            switch ( queryType )
            {
            case EQueryType::Update:
                result = syncable->Update( _connection );
                if ( !result )
                    return false;

                continue;
            case EQueryType::Insert:
                result = syncable->Insert( _connection );
                if ( !result )
                    return false;

                continue;
            case EQueryType::Delete:
                affectedRowCount += syncable->Delete( _connection );
                continue;

            case EQueryType::Select: break;
            case EQueryType::Upsert: break;
            default:
                break;
            }
        }

        for ( auto& dbJob : _lazyDbJobList )
        {
            result = dbJob( _connection );
            if ( !result )
                return false;
        }

        return true;
    }();

    if ( !result )
    {
        _txState = TxState::Failed;
        _connection->Rollback();
    }
    else
    {
        _connection->Commit();
    }

    ReleaseConnection( _connection );

    _owner->Execute( [ tx = TxContextPtr( this ) ]() { tx->Execute(); } );
}

void TransactionContext::Execute_Sync()
{
    if ( _txState != TxState::ExecutingDb )
    {
        // TODO 로그
        return;
    }

    _txState = TxState::ExecutingSync;
    for ( auto& [ queryType, model ] : _modelList )
    {
        switch ( queryType )
        {
        case EQueryType::Update:
        case EQueryType::Insert:
        case EQueryType::Delete:
        case EQueryType::Select:
            model->Sync( queryType );
        case EQueryType::Upsert: 
            // TODO: 미구현 로그
            break;

        default: break;
        }
    }

    Execute();
}

void TransactionContext::Execute_Completed()
{
    _txState = ETxContextState::Completed;

    for ( const auto& job : _completionJobList )
        job();

    _resultJob( true );
}

void TransactionContext::Execute_Failed()
{
    _txState = ETxContextState::Failed;

    _resultJob( false );
}

bool TransactionContext::IsCompleted() const
{
    return _txState == ETxContextState::Completed;
}

bool TransactionContext::IsEmpty() const
{
    return _txState == ETxContextState::Created;
}

bool TransactionContext::IsFailed() const
{
    return _txState == ETxContextState::Failed;
}

bool TransactionContext::IsCanceled() const
{
    return _txState == ETxContextState::Canceled;
}

void TransactionContext::SetDbJobExecutor( DbJobExecutor&& executor )
{
    ExecuteDbJob = executor;
}

TxContextPtr TransactionContext::New()
{
    return MakeShared< TxContext >();
}

void TransactionContext::SetDbConnectionAllocator( DbConnectionAllocator&& allocator )
{
    GetConnection = allocator;
}

void TransactionContext::SetDbConnectionDeallocator( DbConnectionDeleter&& deleter )
{
    ReleaseConnection = deleter;
}
