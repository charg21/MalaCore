export module Mala.Db.ColumnInfo; // data or db

#include "../MalaMacro.h"

import <string>;
import <string_view>;

import Mala.Core.Variant;
import Mala.Db.Type;

using namespace Mala::Core;

NAMESPACE_BEGIN( Mala::Db )

struct IndexInfo;
struct TableInfo;

// 컬럼에 관한 정보
struct ColumnInfo
{
    // 생성자
    ColumnInfo( const TableInfo& table, int fieldNo );

    const TableInfo&           _table;              //< 테이블
          EVarType             _type;               //< type
          int                  _columnNo;           //< 몇번째 컬럼인지 정보         
          std::string_view     _name;               //< 컬럼명
          std::wstring_view    _name_w;             //< 컬럼명
          bool                 _isInPrimaryIndex;   //< pk에 포함 되는지 여부
          bool                 _isInSecondaryIndex; //< 인덱스 구성요소에 포함되는지 여부
          bool                 _notNull;            //< default not_null is false
          Mala::Core::Variant  _defaultValue;       //< 기본 값
};

NAMESPACE_END( Mala::Db )
