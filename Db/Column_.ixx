export module Mala.Db.Column:impl; // data or db

import Mala.Db.Column;
import Mala.Db.ColumnInfo;

import Mala.Core.Variant;
import Mala.Db.ColumnInfo;

using namespace Mala::Db;

Column::Column( ColumnInfo* info )
: _var { info->_type }
, _info{ info        }
{
};

void Column::operator=( Column& other )
{
    _var = other._var;
}
