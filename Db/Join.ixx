export module Mala.Db.Join; // data or 

#include "../MalaMacro.h"

import <array>;
//import <string>;
import <string_view>;
import <vector>;
import <functional>;
import <iostream>;

//import <format>;
import Mala.Container.String;

#define UTF16
#ifdef UTF16
using QueryBuffer     = String;
using QueryBufferView = std::wstring_view;
#define _U(STR) (L##STR)
#else
using QueryBuffer     = std::string;
using QueryBufferView = std::string_view;
#define _U(STR) (STR)
#endif // UNICODE

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.Table;
import Mala.Db.Type;

NAMESPACE_BEGIN( Mala::Db )

//template< typename TQueryMakeable >
//concept QueryMakeable = requires( TQueryMakeable queryMakeable )
//{
//    //queryMakeable.Select();
//};

///// <summary>
///// 전방 선언
///// </summary>
//template< typename T1, typename T2, EJoinType Type >
//struct Join;

/// <summary>
/// 두 테이블간 Join
/// </summary>
template< typename T1, typename T2, EJoinType Type = EJoinType::Left >
struct Join
{
    friend class QueryMaker;

    using ColumnWithInfo = std::pair< Mala::Db::Column*, const Mala::Db::ColumnInfo* >;
    using BindColumnList     = std::vector< ColumnWithInfo >;

    static_assert( std::is_base_of< DbModel, T1 >::value, "T1 must derived from DbModel" );
    static_assert( std::is_base_of< DbModel, T2 >::value, "T2 must derived from DbModel" );

    using t1 = T1;
    using t2 = T2;


public:
    /// <summary>
    /// 생성자
    /// </summary>
    Join() = default;

    template< typename T >
    T& Get() = delete;

    template<>
    [[ nodiscard ]] T1& Get< T1 >()
    {
        return _t1;
    }

    template<>
    [[ nodiscard ]] T2& Get< T2 >()
    {
        return _t2;
    }

    template< typename T >
    T& As() = delete;

    template<>
    T1& As< T1 >()
    {
        return _t1;
    }

    template<>
    T2& As< T2 >()
    {
        return _t2;
    }

    template< typename T >
    constexpr QueryBufferView Alias() = delete;
    
    template<>
    [[ nodiscard ]] constexpr QueryBufferView Alias< T1 >()
    {
        return _U( "t1" );
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView Alias< T2 >()
    {
        return _U( "t2" );
    }

    template< typename T >
    constexpr QueryBufferView AliasDot()
    {
        if constexpr ( std::is_base_of< T, T1 >::value )
            return AliasDot< T1 >();
        else if constexpr ( std::is_base_of< T, T2 >::value )
            return AliasDot< T2 >();
        else
            return "";// __FUNCSIG__;
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView AliasDot< T1 >()
    {
        return _U( "t1." );
    }

    template<>
    [[ nodiscard ]] constexpr QueryBufferView AliasDot< T2 >()
    {
        return _U( "t2." );
    }

    [[ nodiscard ]] bool Select( Mala::Db::DbConnection* connection )
    {
        _connection = connection;

        if ( !_t1.HasBindingColumn() && !_t2.HasBindingColumn() )
            return "";

        QueryBuffer join_query = std::move( MakeQuery() );
        if ( !connection->Query( join_query ) )
            return false;

        /*if ( !FetchRow() )
            return false;*/

        return true;
    }

    template< typename T >
    const bool IsNull() = delete;
    
    template<>
    const bool IsNull< T1 >()
    {
        return _columnCount < _t1_bind_columns.size();
    }

    template<>
    const bool IsNull< T2 >()
    {
        return _columnCount < ( _t1_bind_columns.size() + _t2_bind_columns.size() );
    }

    template< typename T >
    BindColumnList& get_bind_columns() = delete;

    template<>
    BindColumnList& get_bind_columns< T1 >()
    {
        return _t1_bind_columns;
    }

    template<>
    BindColumnList& get_bind_columns< T2 >()
    {
        return _t2_bind_columns;
    }

    template< typename T >
    std::string make_binding_string()
    {
        std::string _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !get< T >().IsBindingColumn( col->_info->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_info->_name;
            _query += ", ";

            get_bind_columns< T >().emplace_back( col, col->_info );
        }

        return std::move( _query );
    }

    template< typename T >
    QueryBuffer make_binding_wstring()
    {
        QueryBuffer _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !Get< T >().IsBindingColumn( col->_info->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_info->_name_w;
            _query += L", ";

            get_bind_columns< T >().emplace_back( col, col->_info );
        }

        return std::move( _query );
    }

    QueryBuffer get_binding_wstring()
    {
        QueryBuffer _query = make_binding_wstring< T1 >() + make_binding_wstring< T2 >();
        
        return std::move( _query );
    }

    QueryBuffer MakeQuery()
    {
        /*
        select t1.col1, t2.col2
        from Table1 as t1
        ( left )join Table2 as t2
        on t1.col1 = t2.col2
        */

        const TableInfo& t1_table = _t1._table;

        QueryBuffer _query;
        _query.reserve( _max_query_length );

        _query += L"select ";
        _query += make_binding_wstring< T1 >();
        _query += make_binding_wstring< T2 >();

        _query.resize( _query.size() - 2 );
        _query += ' ';

        _query += L"from ";
        _query += _connection->_schema_w;
        _query += L'.';
        _query += _t1.GetTableName();
        _query += L" as ";
        _query += Alias< T1 >();

        if constexpr ( Type == EJoinType::Inner )
            _query += L" join ";
        else if constexpr ( Type == EJoinType::Left )
            _query += L" left join ";

        _query += _connection->_schema_w;
        _query += L'.';
        _query += _t2.GetTableName();
        _query += L" as ";
        _query += Alias< T2 >();

        if ( HasOnCondition() )
            _query += _onCondition;

        if ( HasWhereCondition() )
            _query += _whereCondition;

        if ( HasGroupByCondition() )
            _query += _groupByCondition;

        if ( HasLimitCondition() )
        {
            _query += L" limit ";
            _query += std::to_wstring( _limit );
        }

        _max_query_length = std::max( _max_query_length, _query.size() );

        return std::move( _query );
    }

    template< typename T >
    int SyncBindingColumn()
    {
        auto row = _connection->GetRow();

        if ( row[ _columnCount ] == nullptr )
            return _columnCount;

        for ( auto& [ column, meta_data ] : get_bind_columns< T >() )
        {
            if ( meta_data->_isInPrimaryIndex || meta_data->_isInSecondaryIndex )
                Get< T >().SetKeyColumn( meta_data->_columnNo );

            column->_var.Set( row[ _columnCount ] );
            _columnCount += 1;
        }

        return _columnCount;
    }

    [[ nodiscard ]] constexpr bool FetchRow()
    {
        if ( !_connection->Fetch() )
            return false;

        _columnCount = 0;

        _columnCount = SyncBindingColumn< T1 >();
        _columnCount += SyncBindingColumn< T2 >();

        return true;
    }

    Join& And()
    {
        QueryBuffer& condition_string = get_condition_string();
        condition_string += _U( " and " );

        return *this;
    }

    Join& Or()
    {
        QueryBuffer& condition_string = get_condition_string();
        condition_string += _U( " or " );

        return *this;
    }

    Join& On()
    {
        _onCondition += _U( " on " );

        _lastConditionOperator = EConditionOperator::On;

        return *this;
    }

    Join& Where()
    {
        _whereCondition += _U( " where " );

        _lastConditionOperator = EConditionOperator::Where;

        return *this;
    }

    Join& GroupBy()
    {
        _groupByCondition += _U( " group by " );

        _lastConditionOperator = EConditionOperator::GroupBy;

        return *this;
    }

    template< typename Column, typename Iterator >
    Join& NotIn( Iterator begin, Iterator end )
    {
        std::string condition;
        for ( ; begin != end; ++begin )
        {
            if constexpr ( std::is_same_v< Iterator::value_type, QueryBuffer > )
                condition += "\"" + ( *begin ) + "\"";
            else
                condition += std::to_string( *begin );

            condition += ", ";
        }

        condition.resize( condition.size() - 2 );

        return NotIn< Column >( std::move( condition ) );
    }

    template< typename Column, typename Iterator >
    Join& _in( Iterator begin, Iterator end )
    {
        std::string condition;

        for ( ; begin != end; ++begin )
        {
            if constexpr ( std::is_same_v< Iterator::value_type, std::string > )
                condition += "\"" + ( *begin ) + "\"";
            else
                condition += std::to_string( *begin );

            condition += ", ";
        }

        condition.resize( condition.size() - 2 );

        return _in< Column >( std::move( condition ) );
    }

    template< typename Column >
    Join& _not_in( const QueryBuffer& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        QueryBuffer& condition_string = get_condition_string();

        condition_string += AliasDot< Column::DbType >();
        condition_string += Column::name_view.data();
        condition_string += _U( " not in ( " );
        condition_string += condition;
        condition_string += _U( " )" );
        return *this;
    }

    template< typename Column >
    Join& _in( const QueryBuffer& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        QueryBuffer& condition_string = get_condition_string();

        condition_string += AliasDot< Column::DbType >();
        condition_string += Column::name_view.data();
        condition_string += _U( " in ( " );
        condition_string += condition;
        condition_string += _U( " )" );

        return *this;
    }

    template< typename T1Column, typename T2Column >
    Join& Equal()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );

        return _compare_two_column< T1Column, T2Column >( _U( " = " ) );
    }

    template< typename Column, typename Condition >
    Join& Equal( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );
        
        return _compare< Column >( _U( " = " ), std::forward< Condition >( condition ) );
    }

    template< typename T1Column, typename T2Column >
    Join& NotEqual()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );
        return _compare_two_column< T1Column, T2Column >( _U( " <> " ) );
    }

    template< typename Column, typename Condition >
    Join& NotEqual( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        return _compare< Column >( _U( " <> " ), std::forward< Condition >( condition ) );
    }

    template< typename T1Column, typename T2Column >
    Join& Greater()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );
        return _compare_two_column< T1Column, T2Column >( _U( " > " ) );
    }

    template< typename Column, typename Condition >
    Join& Greater( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        return _compare< Column >( _U( " > " ), std::forward< Condition >( condition ) );
    }

    template< typename T1Column, typename T2Column >
    Join& GreaterOrEqual()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );

        return _compare_two_column< T1Column, T2Column >( _U( " >= " ) );
    }

    template< typename Column, typename Condition >
    Join& GreaterOrEqual( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        return _compare< Column >( _U( " >= " ), std::forward< Condition >( condition ) );
    }

    template< typename T1Column, typename T2Column >
    Join& Less()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );

        return _compare_two_column< T1Column, T2Column >( _U( " < " ) );
    }

    template< typename Column, typename Condition >
    Join& Less( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        return _compare< Column >( _U( " < " ), std::forward< Condition >( condition ) );
    }

    template< typename T1Column, typename T2Column >
    Join& LessOrEqual()
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );

        return _compare_two_column< T1Column, T2Column >( _U( " <= " ) );
    }

    template< typename Column, typename Condition >
    Join& LessOrEqual( Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        return _compare< Column >( _U( " <= " ), std::forward< Condition >( condition ) );
    }

    Join& Limit( size_t count = 1 )
    {
        _limit = count;

        return *this;
    }

    template< typename T1Column, typename T2Column >
    Join& _compare_two_column( QueryBufferView operation )
    {
        static_assert( std::is_base_of_v< NullColumn, T1Column >, "T1Column must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Column >, "T2Column must derived from NullColumn" );

        // typename T1Column::Type forDeclType;
        QueryBuffer& condition_string = get_condition_string();
        condition_string += AliasDot< T1Column::DbType >();
        condition_string += T1Column::name_view_w;
        condition_string += operation;
        condition_string += AliasDot< T2Column::DbType >();
        condition_string += T2Column::name_view_w;

        return *this;
    }

    template< typename Column, typename Condition >
    Join& _compare( QueryBufferView operation, Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Column >, "Column must derived from NullColumn" );

        QueryBuffer& condition_string = get_condition_string();
        condition_string += AliasDot< Column::DbType >();
        condition_string += Column::name_view_w;

        condition_string += operation;

        if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, QueryBuffer > )
        {
            condition_string += '\"';
            condition_string += condition;
            condition_string += '\"';
        }
        else if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, std::string > )
        {
            //condition_string += '\"';
            //condition_string += condition;
            //condition_string += '\"';
        }
        else
        {
            condition_string += std::to_wstring( condition );
        }

        return *this;
    }

    [[ nodiscard ]] QueryBuffer& get_condition_string()
    {
        switch ( _lastConditionOperator )
        {
        case EConditionOperator::On:
            return _onCondition;
        case EConditionOperator::Where:
            return _whereCondition;
        case EConditionOperator::GroupBy:
            return _groupByCondition;
        }
    }

    [[ nodiscard ]] constexpr bool HasOnCondition() const
    {
        return _onCondition.size() > 4; // " on "
    }

    [[ nodiscard ]] constexpr bool HasGroupByCondition() const
    {
        return _onCondition.size() > 10; // " group by "
    }

    [[ nodiscard ]] constexpr bool HasWhereCondition() const
    {
        return _onCondition.size() > 7; // " where "
    }

    [[ nodiscard ]] constexpr bool HasLimitCondition() const
    {
        return _limit > 0;
    }

#pragma region JoinWhere

    struct JoinWhere
    {
        JoinWhere( Join& join )
        : _join{ join }
        {
            join._onCondition += _U(" where ");

            join._lastConditionOperator = EConditionOperator::Where;
        }

        template< typename Column, typename Condition >
        Join& Equal( Condition&& condition )
        {
            return _join.Equal< Column >( std::forward< Condition >( condition ) );
        }

        template< typename Column, typename Condition >
        Join& NotEqual( Condition&& condition )
        {
            return _join.NotEqual< Column >( std::forward< Condition >( condition ) );
        }

        Join& _join;
    };

    JoinWhere Where_()
    {
        return JoinWhere( *this );
    }

#pragma endregion

#pragma region JoinOn

    /*join.On_().Equal< User::PlayerId, Player::Id >()
          .Where_().Equal< User::Account >( account )
          .And_().Equal< User::Password >( password )
          .Limit( 1 );*/

    struct JoinOn
    {
        JoinOn( Join& join )
        : _join{ join }
        {
            join._onCondition += _U(" on ");

            join._lastConditionOperator = EConditionOperator::On;
        }

        template< typename T1Column, typename T2Column >
        Join& Equal()
        {
            return _join.Equal< T1Column, T2Column >();
        }

        template< typename T1Column, typename T2Column >
        Join& NotEqual()
        {
            return _join.NotEqual< T1Column, T2Column >();
        }

        template< typename T1Column, typename T2Column >
        Join& Greater()
        {
            return _join.Greater< T1Column, T2Column >();
        }

        template< typename T1Column, typename T2Column >
        Join& GreaterOrEqual()
        {
            return _join.GreaterOrEqual< T1Column, T2Column >();
        }

        template< typename T1Column, typename T2Column >
        Join& Less()
        {
            return _join.Less< T1Column, T2Column >();
        }

        template< typename T1Column, typename T2Column >
        Join& LessOrEqual()
        {
            return _join.LessOrEqual< T1Column, T2Column >();
        }

        Join& _join;
    };

    JoinOn On_()
    {
        return JoinOn( *this );
    }

#pragma endregion JoinOn

private:
    T1 _t1; //<
    T2 _t2; //<
    
    QueryBuffer             _onCondition;       //< 
    QueryBuffer             _whereCondition;    //<
    QueryBuffer             _groupByCondition;  //< 
    size_t                  _limit = 0;         //< Limit 조건( 기본값 0 )
    EConditionOperator      _lastConditionOperator = EConditionOperator::On; //

    Mala::Db::DbConnection* _connection;

    BindColumnList          _t1_bind_columns;
    BindColumnList          _t2_bind_columns;

    int _columnCount = 0;

    inline static size_t     _max_query_length = 64;
};

//template< typename T1 >
//T1& Join::Get< T1 >()
//{
//    return _t1;
//}
//template< typename T1, typename T2, EJoinType Type >
//[[ nodiscard ]] T1& Join::Get< T1 >()
//{
//    return _t1;
//}

////import Mala.db.query_maker;

//template< typename T1, typename T2, EJoinType Type >
//std::wstring join< T1, T2, Type >::MakeQuery()
//{
//    return L"";
//}

NAMESPACE_END( Mala::Db )
