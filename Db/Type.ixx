export module Mala.Db.Type; // Db

#include "../MalaMacro.h"

NAMESPACE_BEGIN( Mala::Db )

/// <summary>
/// 디비 자료 타입
/// </summary>
enum class EDbType : unsigned char
{
    Bool,
    Short,
    UShort,
    Int,
    UInt,
    Long,
    ULong,
    Single,
    Double,
    
    String,
    WString,
    Timestamp,
    DateTime,
};

/// <summary>
/// 조인 타입
/// </summary>
enum class EJoinType : unsigned char
{
    Left, // 레프터 조인
    Inner,// 이너 조인
    Self, // 셀프 조인
};

/// <summary>
/// 쿼리 타입
/// </summary>
enum class EQueryType : unsigned char
{
    Insert, // 인서트
    Update, // 업데이트
    Upsert, // 업서트
    Delete, // 딜리트
    Select, // 셀렉트
    Max
};

/// <summary>
/// 디비 결과
/// </summary>
enum class EDbResult
{
    Success,      //< 성공
    QueryFailure, //< 쿼리 실패
    NoData,       //< 데이터 없음
};

/// <summary>
/// 
/// </summary>
enum class EComparer
{
    Equal,
    NotEqual,
    Greater,
    Less,
    In,
    NotIn,
};

/// <summary>
/// 
/// </summary>
enum class ELogicalOperator
{
    Or,
    And,
    Not
};

/// <summary>
/// 
/// </summary>
enum class EConditionOperator
{
    On,
    GroupBy,
    Not,
    Where,

    Max
};

NAMESPACE_END( Mala::Db )
