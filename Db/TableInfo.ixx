export module Mala.Db.Table; // data or db

#include "../MalaMacro.h"

import <array>;
import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Db.Type;
import Mala.Container.String;

NAMESPACE_BEGIN( Mala::Db )

//struct DbModel;
class ColumnInfo;
class IndexInfo;

//template< size_t N >
//consteval size_t make_filled_bit_mask()
//{
//    static_assert( N < 64, "N must smaller than 64" );

//    return ( 1 << N ) | make_filled_bit_mask< N - 1 >();
//}

//template<>
//consteval size_t make_filled_bit_mask< 0 >()
//{
//    return 1;
//}

struct TableInfo
{
    using ColumnInfoList = std::vector< ColumnInfo* >;
    using IndexList     = std::vector< IndexInfo* >;

    //enum EQueryType
    //{
    //    Insert,
    //    Update,
    //    Upsert,
    //    Delete,
    //    Select,
    //    max
    //};

    size_t         _columnCapacity;
    std::string    _name;
    String         _name_w;
    ColumnInfoList _columnInfos;
    IndexList      _indexs;
    size_t         _allFieldBitMask;
    //std::function< struct db_syncable*() >    _factory_job;
    
    // ����
    std::array< int, (size_t)( EQueryType::Max ) > max_query_length { 20, 20, 20, 20, 20 };

    constexpr static size_t pk_index = 0;
};

NAMESPACE_END( Mala::Db )
