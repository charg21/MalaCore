export module Mala.Db.LeftJoin;

#include "../MalaMacro.h"

import <array>;
import <string>;
import <string_view>;
import <vector>;
import <functional>;
import <iostream>;
import <tuple>;

#define UTF16
#ifdef UTF16
using QueryBuffer     = std::wstring;
using QueryBufferView = std::wstring_view;
#define _U(STR) (L##STR)
#else
using QueryBuffer     = std::string;
using QueryBufferView = std::string_view;
#define _U(STR) (STR)
#endif // UNICODE


import Mala.Core.TypeList;
import Mala.Core.Types;

import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.Table;
import Mala.Db.Type;

NAMESPACE_BEGIN( Mala::Db )

enum class EComparer2
{
    Equal,
    NotEqual,
    Greater,
    Less,
    In,
    NotIn,
};

enum class ELogicalOperator2
{
    Or,
    And,
    Not
};

enum class EConditionOperator2
{
    On,
    GroupBy,
    Not,
    Where,

    max
};

//template< typename TQueryMakeable >
//concept QueryMakeable = requires( TQueryMakeable queryMakeable )
//{
//    //queryMakeable.Select();
//};

/// <summary>
/// 전방 선언
/// </summary>
//template< typename T1, typename T2, EJoinType Type >
//struct LeftJoin;

// 재귀로 확인하느 코드

/// <summary>
/// 두 테이블간 Join
/// </summary>
template< typename... Types >
struct LeftJoin
{
    friend class QueryMaker;

    using DbTableTypeList    = TypeList< Types... >;
    using DbTableTuple       = std::tuple< Types... >;
    constexpr static size_t Capacity = Length< DbTableTypeList >::value;


    using ColumnWithInfo     = std::pair< Mala::Db::Column*, const Mala::Db::ColumnInfo* >;
    using BindColumnList     = std::vector< ColumnWithInfo >;
    using BindColumnLists    = std::array< BindColumnList, Capacity >;
    using QueryBufferViews   = std::array< QueryBufferView, 10 >;

    /// <summary>
    /// 필수
    /// </summary>
    using T1 = TypeAt< DbTableTypeList, 0 >::Result;
 /*   
    static_assert( std::is_base_of< DbModel, T1 >::value, "T1 must derived from DbModel" );
    static_assert( std::is_base_of< DbModel, T2 >::value, "T2 must derived from DbModel" );
    using t1 = T1;
    using t2 = T2;
*/

private:
    DbTableTuple _tables;

    QueryBuffer             _onCondition;       //< 
    QueryBuffer             _whereCondition;    //<
    QueryBuffer             _groupByCondition;  //< 
    size_t                  _limit = 0;         //< Limit 조건( 기본값 0 )
    EConditionOperator2     _lastConditionOperator = EConditionOperator2::On; //
    Mala::Db::DbConnection* _connection;
    BindColumnLists         _bindFieldLists;
    int                     _columnCount = 0;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    LeftJoin() = default;

    template< typename T >
    [[ nodiscard ]] T& Get()
    {
        return std::get< T >( _tables );
        //return std::get< IndexOf< DbTableTypeList, T >::value >( _tables );
    }

    template< typename T >
    [[ nodiscard ]] const T& Get() const
    {
        return std::get< T >( _tables );
        //return std::get< IndexOf< DbTableTypeList, T >::value >( _tables );
    }

    template< typename T >
    [[ nodiscard ]] T& As()
    {
        return std::get< IndexOf< DbTableTypeList, T >::value >( _tables );
    }

    template< typename T >
    [[ nodiscard ]] const T& As() const
    {
        return std::get< IndexOf< DbTableTypeList, T >::value >( _tables );
    }

    template< typename T >
    constexpr bool IsNull() = delete;

    //template< typename T >
    //constexpr bool IsNull() = delete
    //{
    //    return true;
    //}

    template< typename T >
    [[ nodiscard ]] constexpr QueryBufferView Alias()
    {
        constexpr static QueryBufferViews views
        {
            _U( "t1" ),
            _U( "t2" ),
            _U( "t3" ),
            _U( "t4" ),
            _U( "t5" ),
            _U( "t6" ),
            _U( "t7" ),
            _U( "t8" ),
            _U( "t9" ),
            _U( "t10" )
        };

        return views[ IndexOf< DbTableTypeList, T >::value ];
    }

    template< typename T >
    [[ nodiscard ]] constexpr QueryBufferView AliasDot()
    {
        constexpr static QueryBufferViews views
        {
            _U( "t1." ),
            _U( "t2." ),
            _U( "t3." ),
            _U( "t4." ),
            _U( "t5." ),
            _U( "t6." ),
            _U( "t7." ),
            _U( "t8." ),
            _U( "t9." ),
            _U( "t10." )
        };

        return views[ IndexOf< DbTableTypeList, T >::value ];
    }

    template< typename T >
    [[ nodiscard ]] BindColumnList& GetBindFields()
    {
        return _bindFieldLists[ IndexOf< DbTableTypeList, T >::value ];
    }

    template< typename T >
    [[ nodiscard ]] QueryBuffer MakeBindingString()
    {
        QueryBuffer _query;

        for ( Column* col : Get< T >()._columnList )
        {
            if ( !Get< T >().IsBindingColumn( col->_metadata->_columnNo ) )
                continue;

            _query += AliasDot< T >();
            _query += col->_metadata->_name_w;
            _query += L", ";

            GetBindFields< T >().emplace_back( col, col->_metadata );
        }

        return std::move( _query );
    }

    template< typename Tuple, std::size_t IndexInfo = 0 >
    struct TupleIteration1
    {
        static void Execute( const Tuple& t, QueryBuffer& buffer, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size<Tuple>::value )
            {
                buffer += join.MakeBindingString< TypeAt< DbTableTypeList, IndexInfo >::Result >();

                TupleIteration1< Tuple, IndexInfo + 1 >::Execute( t, buffer, join );
            }
        }
    };

    QueryBuffer MakeQuery()
    {
        /*
        select t1.col1, t2.col2
        from Table1 as t1
        ( left )join Table2 as t2
        on t1.col1 = t2.col2
        */

        QueryBuffer _query;
        //_query.reserve( _max_query_length );

        _query += L"select ";
        TupleIteration1< decltype( _tables ) >::Execute( _tables, _query, *this );

        _query.resize( _query.size() - 2 );
        _query += ' ';

        _query += L"from ";
        _query += L"test";// _connection->_schema_w;
        _query += L'.';
        _query += Get< T1 >().GetTableName();
        _query += L" as ";
        _query += Alias< T1 >();

        _query += L" left join ";
        //if constexpr ( Type == EJoinType::Inner )
        //    _query += L" join ";
        //else if constexpr ( Type == EJoinType::Left )
        _query += L"test";// _connection->_schema_w;
        _query += L'.';
        _query += Get< T1 >().GetTableName();
        _query += L" as ";
        _query += Alias< T1 >();

        if ( HasOnCondition() )
            _query += _onCondition;

        if ( HasWhereCondition() )
            _query += _whereCondition;

        if ( HasGroupByCondition() )
            _query += _groupByCondition;

        if ( HasLimitCondition() )
        {
            _query += L" limit ";
            _query += std::to_wstring( _limit );
        }

        //_max_query_length = std::max( _max_query_length, _query.size() );

        return std::move( _query );
    }

    [[ nodiscard ]] constexpr bool Select( Mala::Db::DbConnection* connection )
    {
        _connection = connection;

        //if ( !_t1.HasBindingColumn() && !_t2.HasBindingColumn() )
        //    return "";

        //QueryBuffer join_query = std::move( MakeQuery() );
        //if ( !connection->Query( join_query ) )
        //    return false;

        ///*if ( !FetchRow() )
        //    return false;*/

        return true;
    }

    [[ nodiscard ]] constexpr bool HasOnCondition() const
    {
        return _onCondition.size() > 4; // " on "
    }

    [[ nodiscard ]] constexpr bool HasGroupByCondition() const
    {
        return _onCondition.size() > 10; // " group by "
    }

    [[ nodiscard ]] constexpr bool HasWhereCondition() const
    {
        return _onCondition.size() > 7; // " where "
    }

    [[ nodiscard ]] constexpr bool HasLimitCondition() const
    {
        return _limit > 0;
    }

    template< typename Tuple, std::size_t IndexInfo = 0 >
    struct TupleIteration2
    {
        static void Execute( const Tuple& t, QueryBuffer& buffer, LeftJoin& join )
        {
            if constexpr ( IndexInfo < std::tuple_size< Tuple >::value )
            {
                buffer += join.MakeBindingString< TypeAt< DbTableTypeList, IndexInfo >::Result >();

                TupleIteration2< Tuple, IndexInfo + 1 >::Execute( t, buffer, join );
            }
        }
    };

    template< typename T >
    int SyncBindingField()
    {
        auto row = _connection->GetRow();

        if ( row[ _columnCount ] == nullptr )
            return _columnCount;

     /*   for ( auto& [ column, meta_data ] : get_bind_columns< T >() )
        {
            if ( meta_data->_isInPrimaryIndex || meta_data->_isInSecondaryIndex )
                Get< T >().SetKeyColumn( meta_data->_columnNo );

            column->_var.Set( row[ _columnCount ] );
            _columnCount += 1;
        }*/

        return _columnCount;
    }

    [[ nodiscard ]] constexpr bool FetchRow()
    {
        if ( !_connection->Fetch() )
            return false;

        _columnCount = 0;

        // _columnCount = SyncBindingField< T1 >();
        // _columnCount += SyncBindingField< T2 >();

        return true;
    }

    [[ nodiscard ]] QueryBuffer& GetConditionString()
    {
        switch ( _lastConditionOperator )
        {
        case EConditionOperator2::On:      return _onCondition;
        case EConditionOperator2::Where:   return _whereCondition;
        case EConditionOperator2::GroupBy: return _groupByCondition;
        }
    }

    template< typename T1Field, typename T2Field >
    LeftJoin& _CompareTwoField( QueryBufferView operation )
    {
        static_assert( std::is_base_of_v< NullColumn, T1Field >, "T1Field must derived from NullColumn" );
        static_assert( std::is_base_of_v< NullColumn, T2Field >, "T2Field must derived from NullColumn" );

        QueryBuffer& condition_string = GetConditionString();
        condition_string += AliasDot< T1Field::DbType >();
        condition_string += T1Field::name_view_w;
        condition_string += operation;
        condition_string += AliasDot< T2Field::DbType >();
        condition_string += T2Field::name_view_w;

        return *this;
    }

    template< typename Field, typename Condition >
    LeftJoin& _CompareCondition( QueryBufferView operation, Condition&& condition )
    {
        static_assert( std::is_base_of_v< NullColumn, Field >, "Field must derived from NullColumn" );

        QueryBuffer& condition_string = GetConditionString();
        condition_string += AliasDot< Field::DbType >();
        condition_string += Field::name_view_w;
        condition_string += operation;

        if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, QueryBuffer > )
        {
            condition_string += '\"';
            condition_string += condition;
            condition_string += '\"';
        }
        else if constexpr ( std::is_same_v< std::remove_cvref< Condition >::type, std::string > )
        {
            //condition_string += '\"';
            //condition_string += condition;
            //condition_string += '\"';
        }
        else
        {
            condition_string += std::to_wstring( condition );
        }

        return *this;
    }

    struct LeftJoinWhere
    {
        LeftJoinWhere( LeftJoin& join )
        : _join{ join }
        {
            join._onCondition += _U(" where ");

            join._lastConditionOperator = EConditionOperator2::Where;
        }

        template< typename Field, typename Condition >
        LeftJoin& Equal( Condition&& condition)
        {
            return _join._CompareCondition< Field >( _U( " = " ), std::forward< Condition >( condition ) );
        }

        template< typename Field, typename Condition >
        LeftJoin& NotEqual( Condition&& condition )
        {
            return _join._CompareCondition< Field >( _U( " != " ), std::forward< Condition >( condition ) );
        }

        LeftJoin& _join;
    };

    struct LeftJoinOn
    {
        LeftJoinOn( LeftJoin& join )
        : _join{ join }
        {
            join._onCondition += _U( " on " );

            join._lastConditionOperator = EConditionOperator2::On;
        }

        template< typename T1Field, typename T2Field >
        LeftJoin& Equal()
        {
            return _join._CompareTwoField< T1Field, T2Field >( _U( "=" ) );
        }

        template< typename T1Field, typename T2Field >
        LeftJoin& NotEqual()
        {
            return _join._CompareTwoField< T1Field, T2Field >( _U( "!=" ) );
        }

        LeftJoin& _join;
    };

    LeftJoinOn On()
    {
        return LeftJoinOn( *this );
    }

    LeftJoinWhere Where()
    {
        return LeftJoinWhere( *this );
    }

};

NAMESPACE_END( Mala::Db )
