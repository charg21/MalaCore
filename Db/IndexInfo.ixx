export module Mala.Db.IndexInfo; // data or db

#include "../MalaMacro.h"

import <string>;
import <string_view>;
import <vector>;

import Mala.Core.Types;
import Mala.Container.String;

NAMESPACE_BEGIN( Mala::Db )

struct ColumnInfo;
struct TableInfo;

/// <summary>
/// 
/// </summary>
struct IndexInfo 
{
    /// <summary>
    /// Ÿ�� ������
    /// </summary>
    using ColumnInfoList = std::vector< const ColumnInfo* >;

    std::string_view   _name;        //< �ε��� ��
    StringView         _name_w;      //< �ε��� ��
    flag_t             _mask;        //< �ε��� ����ũ
    ColumnInfoList     _columnInfos; //< 
};

NAMESPACE_END( Mala::Db )
