export module Mala.Db.TxContext;

#include "../MalaMacro.h"

import <memory>;
import <functional>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Memory;
import Mala.Db.Type;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

/// <summary>
/// 트랜잭션 컨텍스트 상태
/// </summary>
enum class ETxContextState
{
    Created,          //< 최초 상태
    WaitingToExecute, //< 실행하기 위해 대기중,( 예약된 상태 )
    Executing,        //< 실행중 로직 작업
    ExecutingDb,      //< 실행중 Db 작업
    ExecutingSync,    //< 실행중 동기화 작업
    Completed,        //< 완료됨
    Canceled,         //< 작업 취소 상태, 요청으로 인해 취소
    Failed,           //< 작업중 실패
};

using TxState = ETxContextState;
class TransactionExecutor;

template< typename TState >
struct IFsm{};

/// <summary>
/// 트랜잭션 작업 문맥 인터페이스
/// </summary>
struct ITransactionContext {};

class TransactionContext;
using TxExecutorPtr = std::shared_ptr< TransactionExecutor >;
 //using TxExecutorRef = const TxExecutorPtr&;
using TxContextPtr = std::shared_ptr< TransactionContext >;
using TxContextRef = const TxContextPtr&;


struct DbConnection;

struct FsmState
{
    virtual void Execute( TxContextRef tx ) = 0;
};

struct LogicJobState : public FsmState
{
    virtual void Execute( TxContextRef tx ) override final
    {
	    
    }
};

struct SyncState : public FsmState
{
    virtual void Execute( TxContextRef tx ) override final
    {

    }
};

struct CompletedState
{
    virtual void Execute()
    {

    }
};

/// <summary>
/// 트랜잭션 작업 문맥
/// Db를 이용한 STM( Software Transactional Memory )을 구현하기 위한 인터페이스
/// </summary>
class TransactionContext final
    : public ITransactionContext
    , public IFsm< ETxContextState >
    , public std::enable_shared_from_this< TransactionContext >
    //, public AtomicRefCountable
{
    friend class TransactionExecutor;

public:
    // Job
    using DbJob     = std::function< bool( DbConnection* connection ) >;
    using DbJobList = Mala::Container::Deque< DbJob >;
    using Job       = std::function< void() >;
    using JobList   = Mala::Container::Deque< Job >;
    using ResultJob = std::function< void( bool ) >;

    /// <summary>
    /// 
    /// </summary>
    using DbConnectionAllocator = std::function< DbConnection*() >;
    using DbConnectionDeleter   = std::function< void( DbConnection* ) >;
    using DbJobExecutor         = std::function< void( Job&& ) >;

    using DbModelRawPtr    = DbModel*;
    using DbModelPtr       = std::unique_ptr< DbModel >;
    using DbModelPtrList   = Mala::Container::Vector< std::pair< EQueryType, DbModelPtr > >;
    using DbModelSet       = HashSet< DbModel* >;


private:
    // 임시 인터페이스
    inline static DbConnectionAllocator GetConnection     = []() { return nullptr; };
    inline static DbConnectionDeleter   ReleaseConnection = []( DbConnection* ) { };
    inline static DbJobExecutor         ExecuteDbJob      = []( Job&& ) {};

public:
    static void SetDbConnectionAllocator  ( DbConnectionAllocator&& allocator );
    static void SetDbConnectionDeallocator( DbConnectionDeleter&& deleter     );
    static void SetDbJobExecutor          ( DbJobExecutor&& executor          );

    /// <summary>
    /// 생성한다
    /// </summary>
    /// <returns></returns>
    static TxContextPtr New();

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TransactionContext() = default;
    TransactionContext( const TransactionContext& other ) = delete;
    TransactionContext( TransactionContext&& other ) noexcept = delete;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~TransactionContext() = default;

    /// <summary>
    /// 실행한다
    /// </summary>
    void Execute();

    /// <summary>
    /// 취소한다
    /// </summary>
    void Cancel();

    template< typename TDbJob >
    void AddDbJob( TDbJob&& dbJob );
    template< typename TJob >
    void AddJob( TJob&& job );

    bool IsCompleted() const;
    bool IsCanceled()  const;
    bool IsEmpty()     const;
    bool IsFailed()    const;

public:
    template< typename DbModelType >
    DbModelType* CopyForUpdate( std::shared_ptr< DbModelType >& origin );
    template< typename DbModelType >
    DbModelType* CopyForDelete( std::shared_ptr< DbModelType >& origin );
    template< typename DbModelType, typename... Args >
    DbModelType* MakeForInsert( Args&&... args );

    // For Fsm
    void ChangeState( TxState newState );

private:
    void Execute_LogicJob();
    void Execute_DbJob();
    void Execute_Sync();
    void Execute_Completed();
    void Execute_Failed();

public:
    /// 식별자
    id64 _transactionId;

    /// DbModel 목록
    DbModelPtrList _modelList;

    /// 커넥션
    DbConnection* _connection;

    /// Db 작업 목록
    DbJobList _dbJobList;

    /// 지연된 Db 작업 목록
    DbJobList _lazyDbJobList;

    /// 트랜잭션 실행기 오너
    TxExecutorPtr _owner;

    /// 완료 작업 목록
    JobList _completionJobList;

    /// 결과 작업
    ResultJob _resultJob;

    /// 상태
    ETxContextState _txState = ETxContextState::Created;
};

template< typename TDbJob >
void TransactionContext::AddDbJob( TDbJob&& dbJob )
{
    _dbJobList.emplace_back( std::forward< TDbJob >( dbJob ) );
}

template< typename TJob >
void TransactionContext::AddJob( TJob&& job )
{
    _completionJobList.emplace_back( std::forward< TJob >( job ) );
}

template< typename DbModelType >
DbModelType* TransactionContext::CopyForUpdate( std::shared_ptr< DbModelType >& origin )
{
/*
    User      user;
    TxContext txContext;

    User* userCopy = txContext.CopyForUpdate( user );
    userCopy->SetLastPlayTime( DateTime::Now() );

    user->ExecuteTx( txContext, []( bool txResult ){} );
*/
    static_assert( std::is_base_of< DbModel, DbModelType >::value, "" );

    auto  copy      = std::make_unique< DbModelType >( *origin );
    auto* copyPtr   = copy.get();

    copy->_original = origin;

    _modelList.emplace_back( EQueryType::Update, std::move( copy ) );

    return copyPtr;
}

template< typename DbModelType >
DbModelType* TransactionContext::CopyForDelete( std::shared_ptr< DbModelType >& origin )
{
    static_assert( std::is_base_of< DbModel, DbModelType >::value, "" );

    auto  copy    = std::make_unique< DbModelType >( *origin );
    auto* copyPtr = copy.get();

    copy->_original = origin;

    _modelList.emplace_back( EQueryType::Delete, std::move( copy ) );

    return copyPtr;
}

template< typename DbModelType, typename... Args >
DbModelType* TransactionContext::MakeForInsert( Args&&... args )
{
    /*
        TxContext txContext;
        Player*   player = txContext.MakeForInsert< Player >();

        player->SetKeyId       ( 1               );
        player->SetLastPlayTime( DateTime::Now() );

        player->ExecuteTx( txContext, []( bool txResult ){} );
    */
    static_assert( std::is_base_of< DbModel, DbModelType >::value, "" );

    auto  origin    = std::make_unique< DbModelType >( std::forward< Args >( args )... );
    auto* originPtr = origin.get();

    _modelList.emplace_back( EQueryType::Insert, std::move( origin ) );

    return originPtr;
}

/// <summary>
/// 트랜잭션 실행기 타입 재정의
/// </summary>
using TxContext        = Mala::Db::TransactionContext;
using TxContextPtr     = std::shared_ptr< TxContext >;
using TxContextRef     = const TxContextPtr&;
using LogicTransaction = TxContext;

NAMESPACE_END( Mala::Db )
