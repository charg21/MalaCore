module;

#include "../../external/mysql/mysql.h"
#include <string>
#include <iostream>
#include <functional>

#pragma comment( lib , "libmysql" )

#include "../MalaMacro.h"

export module Mala.Db.DbConnection;

import Mala.Container.LockfreeStack;
import Mala.Container.String;
import Mala.Text;
import Mala.Db.Type;


NAMESPACE_BEGIN( Mala::Db )


class DbConnection
{
public:
    DbConnection() = default;
    ~DbConnection() = default;

    // https://mariadb.com/kb/en/mariadb-connectorc-types-and-definitions/

    bool Connect(
        const std::string& host,
        const std::string& user,
        const std::string& password,
        StringRef schema,
        const int port )
    {
        _connection = mysql_init( nullptr );

        unsigned int opt_ssl = SSL_MODE_DISABLED;
        mysql_options( _connection, MYSQL_OPT_SSL_MODE, &opt_ssl );

        _schema   = Mala::Text::Utf16::ToUtf8( schema );
        _schema_w = schema;
        
        auto schema_utf8 = Mala::Text::Utf16::ToUtf8( schema );

        _connection = mysql_real_connect(
            _connection,
            host.c_str(),
            user.c_str(),
            password.c_str(),
            schema_utf8.c_str(),
            port,
            nullptr,
            0 );

        if ( _connection == nullptr )
        {
            printf( "Mysql connection error : %s\n", mysql_error( _connection ) );
            _error_code = 1;
            return false;
        }

        //set_character_set();
        //else
        //{
        //	printf( "Mysql connection error : %s\n", mysql_get_ssl_cipher( _connection ) );
        //}
        
        return true;
    }

    void Disconnect()
    {
        if ( _connection == nullptr )
            return;

        mysql_close( _connection );
    }

    bool Fetch()
    {
        if ( !_results )
            return false;
        
        _cursor = mysql_fetch_row( _results );
        if ( !_cursor )
            return false;
        //{
        //    std::cout << mysql_error( _connection ) << std::endl;
        //    std::cout << mysql_errno( _connection ) << std::endl;

        //}

        return true;
    }

    bool Query( const std::string& queryString )
    {
        int result = mysql_real_query( _connection, queryString.c_str(), queryString.size() );
        if ( 0 != result )
        {
            printf( "Mysql connection error %d : %s, query: %s\n", mysql_errno( _connection ), mysql_error( _connection ), queryString.c_str() );
            _error_code = 1;
            return false;
        }

        // 결과 영향받은 로우수
        if ( IsSelectQuery( queryString ) )
        {
            _results = mysql_store_result( _connection );
            _affected_row_count = mysql_num_rows( _results );
            _affected_column_count = mysql_num_fields( _results );
        }
        else
        {
            _affected_row_count = mysql_affected_rows( _connection );
        }

        return true;
    }

    const static bool IsSelectQuery( const std::string& query )
    {
        return 0 == strncmp( query.c_str(), "SELECT", 6 );
    }

    const static bool IsSelectQuery( const std::wstring& query )
    {
        return 0 == wcsncmp( query.c_str(), L"SELECT", 6 );
    }

    bool Query( StringRef query_wstring )
    {
        std::string queryString = std::move( Mala::Text::Utf16::ToUtf8( query_wstring ) );

        return Query( queryString );
    }

    MYSQL_ROW GetRow()
    {
        return _cursor;
    }

    void Reset()
    {
        if ( _results )
        {
            mysql_free_result( _results );
            _results = nullptr;
        }

        _affected_row_count = 0;
    }

    bool BeginTransaction()
    {
        return 0 == mysql_autocommit( _connection, false );
        //return Query( "START TRANACTION" );
    }

    bool SetAutoCommit( bool auto_mode )
    {
        return 0 == mysql_autocommit( _connection, auto_mode );
    }

    bool Commit()
    {
        return 0 == mysql_commit( _connection );

        //return set_autocommit( true );
    }

    bool Rollback()
    {
        //return set_autocommit( true );
        return 0 == mysql_rollback( _connection );
    }

    ////
    // 주의 Utf16, utf32는 사용이 불가능하다
    /// <summary>
    /// 
    /// </summary>
    void set_character_set()
    {
        mysql_set_character_set( _connection, "Utf16" );
    }

    std::wstring_view get_schema_w()
    {
        return _schema_w;
    }

public:
    MYSQL*            _connection         = nullptr;
    MYSQL_RES*        _results            = nullptr;
    MYSQL_ROW	      _cursor;
    int               _error_code            = 0;
    int               _affected_row_count    = 0;
    int               _affected_column_count = 0;

    std::string_view  _schema;
    std::wstring_view _schema_w;
};

class DbConnectionPool
{
    using ConnectionContanier = Mala::Container::LockfreeStack< DbConnection* >;

public:
    bool Connect(
        StringRef host,
        StringRef user,
        StringRef password,
        StringRef schema,
        const int           port,
        size_t              connectionCount = 10 )
    {
        _host     = Mala::Text::Utf16::ToUtf8( host     );
        _user     = Mala::Text::Utf16::ToUtf8( user     );
        _password = Mala::Text::Utf16::ToUtf8( password );
        _schema   = Mala::Text::Utf16::ToUtf8( schema   );

        _schema_w = schema;
        _port     = port;

        std::vector< DbConnection* > connections;
        for ( int n{}; n < connectionCount; n += 1 )
        {
            DbConnection* newConnection = GetOrDefault();
            if ( !newConnection )
            {
                return false;
            }

            connections.push_back( newConnection );
        }

        for ( auto* connection : connections )
        {
            Release( connection );
        }

        return true;
    }

    DbConnection* Get()
    {
        DbConnection* outDbConnection;

        if ( _connections.TryPop( outDbConnection ) )
            return outDbConnection;

        return nullptr;
    }

    DbConnection* GetOrDefault()
    {
        DbConnection* outDbConnection;

        if ( _connections.TryPop( outDbConnection ) )
            return outDbConnection;

        outDbConnection = new DbConnection();
        if ( !outDbConnection->Connect( _host, _user, _password, _schema_w, _port ) )
        {
            delete outDbConnection;
            return nullptr;
        }

        return outDbConnection;
    }

    void Release( DbConnection* connection )
    {
        connection->Reset();

        _connections.push( connection );
    }

    void ForEachDbConnection( std::function< void( DbConnection* ) > job )
    {
    }

private:
    ConnectionContanier _connections;
    std::string			_host     = "127.0.0.1";
    std::string			_user     = "root";
    std::string			_password = "1q2w3e4r";
    std::string			_schema   = "test";
    String	            _schema_w = L"test";
    int                 _port;
};

NAMESPACE_END( Mala::Db )
