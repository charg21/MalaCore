export module Mala.Db.DbModel:impl; // data or db

#include "../MalaMacro.h"

import <string>;
import <string_view>;
import <vector>;
import <deque>;
import <functional>;

import Mala.Core.Crash;
import Mala.Core.Types;
import Mala.Core.Variant;

import Mala.Db.DbModel;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.DbConnection;
import Mala.Db.IndexInfo;
import Mala.Db.QueryMaker;
import Mala.Db.Table;
import Mala.Db.Type;
import Mala.Db.TxContext;

using namespace Mala::Db;


// DbModel
// db type https://ksr930.tistory.com/43?category=910675
DbModel::DbModel( TableInfo& table )
: _table         { table }
, _columnUpdated {       }
, _columnBinding {       }
, _columnKeySetup{       }
{
}

DbModel::DbModel( DbModel& origin )
: _table{ origin._table }
{
    origin.CopyTo( *this );
}

//DbModel::~DbModel()
//{
//}

MALA_NODISCARD bool DbModel::Delete( DbConnection* connection )
{
    auto deleteQuery = Mala::Db::QueryMaker::MakeDeleteQuery( *this, connection );
    if ( !connection->Query( deleteQuery ) )
        return false;

    ResetUpdateFlag();

    return connection->_affected_row_count > 0;
}

MALA_NODISCARD bool DbModel::Insert( DbConnection* connection )
{
    auto insertQuery = Mala::Db::QueryMaker::MakeInsertQuery( *this, connection );
    if ( !connection->Query( insertQuery ) )
        return false;

    ResetUpdateFlag();

    return connection->_affected_row_count > 0;
}

MALA_NODISCARD i64 DbModel::Update( DbConnection* connection )
{
    if ( !connection )
        return -1;

    auto update_query = Mala::Db::QueryMaker::MakeUpdateQuery( *this, connection );
    if ( !connection->Query( update_query ) )
        return -1;

    ResetUpdateFlag();

    // 쿼리 성공 이후 변경이 안된 경우에 _affected_row_count == 0 인경우도 성공으로 간주
    return connection->_affected_row_count;
}

MALA_NODISCARD bool DbModel::Select( DbConnection* connection )
{
    if ( !connection )
        return false;
    
    auto selectQuery = Mala::Db::QueryMaker::MakeSelectQuery( *this, connection );
    if ( !connection->Query( selectQuery ) )
        return false;

    if ( !connection->Fetch() )
        return false;

    i32 column_count = 0;
    for ( Column* col : _columnList )
    {
        // todo: bind 여부로 필요한 컬럼만
        col->_var.Set( connection->GetRow()[ column_count ] );
        column_count += 1;
    }

    return column_count > 0;
}

//MALA_NODISCARD const bool is_all_column_updated()
//{
//    return _columnUpdated == 1;
//}

void DbModel::ResetUpdateFlag()
{
    _columnUpdated = 0;
}

void DbModel::SetUpdateColumn( i32 columnNo )
{
    _columnUpdated |= ( 1ULL << columnNo );
}

void DbModel::SetBindColumn( i32 columnNo )
{
    _columnBinding |= ( 1ULL << columnNo );
}

void DbModel::SetKeyColumn( i32 columnNo )
{
    _columnKeySetup |= ( 1ULL << columnNo );
}

void DbModel::SetUpdateAndSetupKeyColumn( i32 columnNo )
{
    _columnUpdated  |= ( 1ULL << columnNo );
    _columnKeySetup |= ( 1ULL << columnNo );
}

MALA_NODISCARD bool DbModel::IsUpdateColumn( i32 no ) const
{
    const size_t mask = ( 1ULL << no );
    const bool result = 1ULL <= ( _columnUpdated & ( mask ) );
    return result;
}

MALA_NODISCARD bool DbModel::IsBindingColumn( i32 no ) const
{
    const size_t mask = ( 1ULL << no );
    const bool result = 1ULL <= ( _columnBinding & ( mask ) );
    return result;
}

MALA_NODISCARD bool DbModel::IsValidIndex( const IndexInfo* index ) const
{
    const size_t maskedResult = _columnKeySetup & index->_mask;

    return maskedResult == index->_mask;
}

bool DbModel::select_list( std::deque< DbModel > list ,DbConnection* connection )
{
    if ( !connection )
        return false;

    //list
    /*std::string select_query = Mala::db::query_maker::make_select_query( *this, connection );
    if ( !connection->Query( select_query ) )
        return false;

    if ( !connection->Fetch() )
        return false;

    i32 column_count = 0;
    for ( column* col : _columnList )
    {
        col->_var.set( connection->GetRow()[ column_count ] );
        column_count += 1;
    }*/

    return 1;// column_count > 0;
}

void DbModel::Sync( EQueryType queryType )
{
}

MALA_NODISCARD bool DbModel::HasUpdateColumn() const
{
    return _columnUpdated > 0;
}

MALA_NODISCARD bool DbModel::HasBindingColumn() const
{
    return _columnBinding > 0;
}

MALA_NODISCARD bool DbModel::HasKeySetupColumn() const
{
    return _columnKeySetup > 0;
} 

void DbModel::Reset()
{
    ResetUpdateFlag();
    ResetBinding();
}

void DbModel::BindAllColumn()
{
    _columnBinding = 0b11111111'11111111'11111111'11111111'11111111'11111111'11111111'11111111;
}

void DbModel::ResetBinding()
{
    _columnBinding = 0;
}

const std::string& DbModel::get_table_name()
{
    return _table._name;
}

StringRef DbModel::GetTableName() const
{
    return _table._name_w;
}

void DbModel::CopyTo( DbModel& target ) const
{
    target._columnBinding  = _columnBinding;
    target._columnUpdated  = _columnUpdated;
    target._columnKeySetup = _columnKeySetup;

    for ( i32 index{}; index < _columnList.size(); index += 1 )
    {
        *target._columnList[ index ] = *_columnList[ index ];
    }
}

void DbModel::CopyToOnlyUpdateColumn( DbModel& target ) const
{
    target._columnBinding  = _columnBinding;
    target._columnUpdated  = _columnUpdated;
    target._columnKeySetup = _columnKeySetup;

    for ( i32 index{}; index < _columnList.size(); index += 1 )
    {
        if ( IsUpdateColumn( index ) )
            *target._columnList[ index ] = *_columnList[ index ];
    }
}

