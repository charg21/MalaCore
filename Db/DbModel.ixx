export module Mala.Db.DbModel; // data or db

#include "../MalaMacro.h"

import <memory>;
import <string>;
import <string_view>;
import <vector>;
import <deque>;
import <functional>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Container.String;
import Mala.Db.Type;

using namespace Mala::Container;

NAMESPACE_BEGIN( Mala::Db )

struct QueryMakeable        {};
struct TransactionExecutable{};
struct NullColumn           {};
struct NullIndex            {};

class  DbConnection;
struct DbModel;
struct TableInfo;
struct Column;
struct IndexInfo;

using QueryBuildable = QueryMakeable;

/// <summary>
/// Db 동기화 객체
/// </summary>
struct DbModel : public QueryMakeable
{
    using ColumnList = Vector< Column* >;

    /// <summary>
    /// 생성자
    /// </summary>
    DbModel( TableInfo& table );
    DbModel( DbModel& origin );

    /// <summary>
    /// 소멸자
    /// </summary>
    virtual ~DbModel() = default;

    /// <summary>
    /// Db 동작
    /// </summary>
    MALA_NODISCARD bool Delete( DbConnection* connection );
    MALA_NODISCARD bool Insert( DbConnection* connection );
    MALA_NODISCARD i64  Update( DbConnection* connection );
    MALA_NODISCARD bool Select( DbConnection* connection );
    MALA_NODISCARD void Upsert( DbConnection* connection ) {}

    //MALA_NODISCARD const bool is_all_column_updated()
    //{
    //    return _columnUpdated == 1;
    //}

    void ResetUpdateFlag();
    void SetUpdateColumn( i32 columnNo );
    void SetBindColumn( i32 columnNo );
    void SetKeyColumn( i32 columnNo );
    void SetUpdateAndSetupKeyColumn( i32 columnNo );

    MALA_NODISCARD bool IsUpdateColumn( i32 no ) const;
    MALA_NODISCARD bool IsBindingColumn( i32 no ) const;
    MALA_NODISCARD bool IsValidIndex( const IndexInfo* index ) const;
    MALA_NODISCARD bool HasUpdateColumn() const;
    MALA_NODISCARD bool HasBindingColumn() const;
    MALA_NODISCARD bool HasKeySetupColumn() const;

    static bool select_list( Deque< DbModel > list, DbConnection* connection );

    /// <summary>
    /// 동기화 한다
    /// </summary>
    virtual void Sync( EQueryType queryType );

    /// <summary>
    /// 객체를 초기 상태로 되돌린다
    /// </summary>
    void Reset();

    /// <summary>
    /// 
    /// </summary>
    void BindAllColumn();
    void ResetBinding();

    const std::string& get_table_name();
    const String& GetTableName() const;

    void Clone();

    void CopyTo( DbModel& target ) const;
    void CopyToOnlyUpdateColumn( DbModel& target ) const;

    const TableInfo&   _table;          //< 테이블 정보
          flag_t       _columnUpdated;  //< 컬럼 갱신 플래그
          flag_t       _columnBinding;  //< DB 데이터 세팅 플래그
          flag_t       _columnKeySetup; //< 키 세팅 플래그
          ColumnList   _columnList;     //< 
          DbModelPtr   _original;       //< 
};

    
NAMESPACE_END( Mala::Db )





