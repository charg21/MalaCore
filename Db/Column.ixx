export module Mala.Db.Column; // data or db

#include "../MalaMacro.h"

import Mala.Core.Variant;

using namespace Mala::Core;

NAMESPACE_BEGIN( Mala::Db )

class ColumnInfo;

// 컬럼
struct Column
{
    // 생성자
    Column( ColumnInfo* metadata );
    // 복사 생성자
    Column( const Column& field );

    void operator=( Column& field );

          Variant     _var;  //< 값
    const ColumnInfo* _info; //< 컬럼 정보
};

NAMESPACE_END( Mala::Db )
