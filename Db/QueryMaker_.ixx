export module Mala.Db.QueryMaker:impl;

import <string>;

import Mala.Container.String;
import Mala.Db.QueryMaker;
import Mala.Db.DbConnection;
import Mala.Db.DbModel;
import Mala.Db.IndexInfo;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.Join;
//import Mala.Db.Type;

#define UTF16
#ifdef UTF16
#define _U(STR) (L##STR)
#else
#define _U(STR) (STR)
#endif // UNICODE

using namespace Mala::Db;

//QueryMaker::QueryMaker()
//{
//    //_buffer.reserve( 30 );
//}

/// <summary>
/// 이미 생성된 객체 pk 가지고 있음.
/// </summary>
const String QueryMaker::MakeUpdateQuery( const DbModel& syncable, DbConnection* connection )
{
    /*
        UPDATE table_name
        SET column1 = value1, column2 = value2, ...
        WHERE condition;
    */
    if ( !syncable.HasUpdateColumn() )
        return { _U( "can't find update column...." ) };
    
    const TableInfo& table = syncable._table;

    String queryBuffer;

    queryBuffer.reserve( 13 + connection->_schema_w.size() + table._name.size() );
    queryBuffer += _U("update ");
    queryBuffer += connection->_schema_w;
    queryBuffer += _U( "." );
    queryBuffer += table._name_w;
    queryBuffer += _U( " set " );

    /// set 문
    bool isAllField = true;
    for ( Column* col : syncable._columnList )
    {
        const ColumnInfo* columnInfo = col->_info;
        if ( !syncable.IsUpdateColumn( columnInfo->_columnNo ) )
        {
            isAllField = false;
            continue;
        }

        // 해당 컬럼이 업데이트 됐는지
        // pass;
        // else
        queryBuffer += columnInfo->_name_w;
        queryBuffer += _U( " = " );
        queryBuffer += col->_var.ToString();
        queryBuffer += _U( "," );
    }

    queryBuffer.pop_back();
    queryBuffer.push_back( _U( ' ' ) ); // remove ' ';

    // pk 부터 체크.
    for ( const IndexInfo* index : table._indexs )
    {
        if ( !syncable.IsValidIndex( index ) )
            continue;
        /// where
        queryBuffer += _U( "where " );

        for ( const ColumnInfo* columnInfo : index->_columnInfos )
        {
            Column* col = syncable._columnList[ columnInfo->_columnNo ];

            queryBuffer += columnInfo->_name_w;
            queryBuffer += _U( " = " );
            queryBuffer += col->_var.ToString();
            queryBuffer += _U( " and " );
        }

        queryBuffer.resize( queryBuffer.size() - 5 );

        break;
    }

    queryBuffer += _U( ';' );;

    return queryBuffer;
}

// note db_syncable 객체에 PK 설정이 필요함
const String QueryMaker::MakeInsertQuery( const DbModel& syncable, DbConnection* connection )
{
    ///*
    //    INSERT INTO table_name (column1, column2, column3, ...)
    //    VALUES (value1, value2, value3, ...);

    //    INSERT INTO table_name
    //    VALUES (value1, value2, value3, ...);
    //*/
    String _query_buffer;
    _query_buffer += _U( "insert into " );
    _query_buffer += connection->_schema_w;
    _query_buffer += _U( "." );
    _query_buffer += syncable._table._name_w;
    _query_buffer += _U( " values ( " );

    int inserted_count = 0;
    for ( /*todo: const */ Column* col : syncable._columnList )
    {
        _query_buffer += col->_var.ToString();
        _query_buffer += _U( ", " );

        inserted_count += 1;
    }

    if ( inserted_count == 0 )
    {
        _query_buffer = _U( "error..." );
        return _query_buffer;
    }

    _query_buffer.resize( _query_buffer.size() - 2 );
    _query_buffer += _U( " );" );

    return _query_buffer;
}

// note db_syncable 객체에 PK 설정이 필요함
const String QueryMaker::make_insert_query2( DbConnection* connection )
{
    DbModel* syncable = *_syncable_set.begin();
    /*if ( !syncable )
        return "null db syncable";*/
    
    String  _buffer;
    _buffer += _U( "insert into " );
    _buffer += connection->_schema_w;
    _buffer += _U( "." );
    _buffer += syncable->_table._name_w;
    _buffer += _U( " values ( " );

    int inserted_count = 0;
    for ( /*todo: const */ Column* col : syncable->_columnList )
    {
        _buffer += col->_var.ToString();
        _buffer += _U( ", " );

        inserted_count += 1;
    }

    if ( inserted_count == 0 )
    {
        _buffer = _U( "error..." );
        return _buffer;
    }

    _buffer.resize( _buffer.size() - 2 );
    _buffer += _U( " );" );;

    return _buffer;
}

/// bulk insert query를 만든다
const String QueryMaker::MakeBulkInsertQuery( std::unordered_set< DbModel* >& syncable_set, DbConnection* connection )
{
    if ( syncable_set.empty() )
        return _U( "empty object set" );

    const DbModel* syncable = *syncable_set.begin();

    String _query_buffer;

    _query_buffer += _U( "insert into " );
    _query_buffer += connection->_schema_w;
    _query_buffer += _U( "." );
    _query_buffer += syncable->_table._name_w;
    _query_buffer += _U( " values " );

    int inserted_count = 0;
    for ( DbModel* syncable : syncable_set )
    {
        _query_buffer += _U( "( " );

        for ( /*todo: const */ Column* col : syncable->_columnList )
        {
            _query_buffer += col->_var.ToString();
            _query_buffer += _U( ", " );

            inserted_count += 1;
        }

        _query_buffer.resize( _query_buffer.size() - 2 );
        _query_buffer += _U( " ), " );
    }

    _query_buffer.resize( _query_buffer.size() - 2 );
    _query_buffer += _U( ';' );

    return _query_buffer;
}

const String QueryMaker::MakeUpsertQuery( const DbModel& syncable, DbConnection* connection )
{
    /*
    INSERT INTO 테이블명 (userId, name, age)
    values (1, '엄준식', 25)
    ON DUPLICATE KEY UPDATE name = '엄준식', age = 19;
    */
    String _query_buffer;

    /*_query_buffer += */MakeInsertQuery( syncable, connection );
    _query_buffer.pop_back();
    _query_buffer += _U( " on duplicate key update " );

    const TableInfo& table = syncable._table;

    for ( const IndexInfo* index : table._indexs )
    {
        if ( !syncable.IsValidIndex( index ) )
            continue;

        for ( const ColumnInfo* column_meta : index->_columnInfos )
        {
            Column* col = syncable._columnList[ column_meta->_columnNo ];

            _query_buffer += column_meta->_name_w;
            _query_buffer += _U( " = " );
            _query_buffer += col->_var.ToString();
            _query_buffer += _U( ", " );
        }

        break;
    }


    int updated_column_count = 0;
    for ( /* todo : const */Column* col : syncable._columnList )
    {
        if ( !syncable.IsUpdateColumn( col->_info->_columnNo ) )
            continue;

        // 해당 컬럼이 업데이ㅡ 됐는지
        // pass;
        // else
        _query_buffer += col->_info->_name_w;
        _query_buffer += _U( " = " );
        _query_buffer += col->_var.ToString();
        _query_buffer += _U( ", " );

        updated_column_count += 1;
    }

    if ( updated_column_count > 0 )
    {
        _query_buffer.resize( _query_buffer.size() - 2 );
        _query_buffer.push_back( _U( ' ' ) ); // remove ' ';
    }
    else
    {
        _query_buffer = _U( "can't find updated column...." );
        return _query_buffer;
    }

    _query_buffer.resize( _query_buffer.size() - 2 );
    _query_buffer.push_back( _U( ';' ) ); // remove ','

    return _query_buffer;
}

const String QueryMaker::MakeSelectQuery( const DbModel& syncable, DbConnection* connection )
{
    String queryBuffer;

    //static std::wstring_view query_format = _U( "select * from %s where %s;" );
    queryBuffer += _U( "select * from " );
    queryBuffer += connection->_schema_w;
    queryBuffer += _U( "." );
    queryBuffer += syncable._table._name_w;
    queryBuffer += _U( " where " );

    const TableInfo& table = syncable._table;

    // pk 부터 체크.
    for ( const IndexInfo* index : table._indexs )
    {
        if ( !syncable.IsValidIndex( index ) )
            continue;

        for ( const ColumnInfo* columnMeta : index->_columnInfos )
        {
            Column* col = syncable._columnList[ columnMeta->_columnNo ];

            queryBuffer += columnMeta->_name_w;
            queryBuffer += _U( " = " );
            queryBuffer += col->_var.ToString();
            queryBuffer += _U( " and " );
        }

        break;
    }

    // remove " and "
    queryBuffer.resize( queryBuffer.size() - 5 );
    queryBuffer += _U( ';' );

    return queryBuffer;
}

// must has key...
const String QueryMaker::MakeDeleteQuery( const DbModel& syncable, DbConnection* connection )
{
    /*
    DELETE FROM table_name WHERE condition;
    */
    String queryBuffer;
    queryBuffer += _U( "delete from " );
    queryBuffer += connection->_schema_w;
    queryBuffer += _U( "." );
    queryBuffer += syncable._table._name_w;
    queryBuffer += _U( " where " );

    // make condition, using pk
    int keyFieldCount = 0;

    const TableInfo& table = syncable._table;
    // pk 부터 체크.
    for ( const IndexInfo* index : table._indexs )
    {
        if ( !syncable.IsValidIndex( index ) )
            continue;

        for ( const ColumnInfo* column_meta : index->_columnInfos )
        {
            Column* col = syncable._columnList[ column_meta->_columnNo ];

            queryBuffer += column_meta->_name_w;
            queryBuffer += _U( " = " );
            queryBuffer += col->_var.ToString();
            queryBuffer += _U( " and " );

            keyFieldCount += 1;
        }

        break;
    }

    if ( keyFieldCount == 0 )
    {
        queryBuffer.resize( queryBuffer.size() - 7 );
        queryBuffer.push_back( _U( ';' ) ); //< remove ' '
        return queryBuffer;
    }

    // remove " and "
    queryBuffer.resize( queryBuffer.size() - 5 );
    queryBuffer.push_back( _U( ';' ) ); //< remove ' '

    return queryBuffer;
}
