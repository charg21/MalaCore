export module Mala.Db.QueryMaker;

#include "../MalaMacro.h"

import <string>;
import <unordered_set>;
import <vector>;

import Mala.Container.String;
import Mala.Db.Join;
import Mala.Db.Type;
import Mala.Db.DbConnection;

NAMESPACE_BEGIN( Mala::Db )

struct DbModel;

enum class EJoinType : unsigned char;
template< typename T1, typename T2, EJoinType JoinType >
struct Join;// < T1, T2, EJoinType >;

// 쿼리 생성기
struct QueryMaker
{
    // 컴파일 에러
    //template< typename T1, typename T2, EJoinType EJoinType >
    //friend struct Join;

    using SyncableSet  = std::unordered_set< DbModel* >;
    using SyncableList = std::vector< DbModel* >;
    using QueryBuffer  = std::wstring;

    QueryMaker() = default;

    // update 쿼리를 생성한다
    static const String MakeUpdateQuery( const DbModel& syncable, DbConnection* connection );

    // insert 쿼리를 생성한다
    static const String MakeInsertQuery( const DbModel& syncable, DbConnection* connection );

    // bulk insert 쿼리를 생성한다
    static const String MakeBulkInsertQuery( std::unordered_set< DbModel* >& syncable_set, DbConnection* connection );

    // upsert 쿼리를 생성한다
    static const String MakeUpsertQuery( const DbModel& syncable, DbConnection* connection );

    // select 쿼리를 생성한다
    static const String MakeSelectQuery( const DbModel& syncable, DbConnection* connection );

    // delete 쿼리를 생성한다
    static const String MakeDeleteQuery( const DbModel& syncable, DbConnection* connection );

    // insert 쿼리를 생성한다
    const String make_insert_query2( DbConnection* connection );

    void Clear() 
    {}

    void Add( DbModel* syncable )
    {
        if ( !syncable )
            return;

        if ( _syncable_set.insert( syncable ).second )
            _syncable_list.push_back( syncable );
    }

public:
    SyncableSet  _syncable_set;
    SyncableList _syncable_list;
    //QueryBuffer  _buffer;
};

NAMESPACE_END( Mala::Db )
