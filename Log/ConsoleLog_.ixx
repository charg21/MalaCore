module;

export module Mala.Log.ConsoleLog:impl;

import Mala.Log.ConsoleLog;

import <stdio.h>;
import <memory>;

/// <summary>
/// 콘솔 로그 출력 클래스
/// </summary>
ConsoleLog::ConsoleLog()
{
	_stdOut   = ::GetStdHandle( STD_OUTPUT_HANDLE );
	_stdError = ::GetStdHandle( STD_ERROR_HANDLE );
}

/// <summary>
/// 소멸자
/// </summary>
ConsoleLog::~ConsoleLog()
{
	// ::CloseHandle( _stdOut   );
	// ::CloseHandle( _stdError );
}

/// <summary>
/// 표준 출력 디바이스에 로그를 작성한다
/// </summary>
void ConsoleLog::WriteStdOut( EColor color, const wchar_t* format, ... ) const
{
	if ( !format )
		return;

	SetColor< true >( color );

	va_list ap;
	va_start( ap, format );
	::vwprintf( format, ap );
	va_end( ap );

	fflush( stdout );

	SetColor< true >( EColor::White );
}

	/// <summary>
	/// 표준 오류 디바이스에 로그를 작성한다
	/// </summary>
	void ConsoleLog::WriteStdErr( EColor color, const wchar_t* format, ... ) const
	{
		wchar_t buffer[ 4096 ];

		if ( !format )
			return;

		SetColor< false >( color );

		va_list ap;
		va_start( ap, format );
		::vswprintf_s( buffer, 4096, format, ap );
		va_end( ap );

		::fwprintf_s( stderr, buffer );
		fflush( stderr );

		SetColor< false >( EColor::White );
	}
