export module Log:impl;

#include "../MalaMacro.h";

import Mala.Core.Types;
import Mala.Container.String;
import Mala.Log.ConsoleLog;
import Mala.Log;

import ThreadLocal;

using namespace Mala::Log;

namespace LogWriter
{

void _Write( String&& log )
{
    GLogQueue->Emplace( LThreadId, std::move( log ) );
}

void Flush()
{
    //static wchar_t sLogBuffer[ 4096 ];
    //static usize   sLogIndex = 0;

    GLogQueue->ConsumeAll( []( const LogRecord& log )
    {
        //log._message.c_str();
        //sLogBuffer[ 0 ] = L'a';

        CONSOLE_LOG( White, log._message.c_str() );
        // Mala::Core::WriteLog( Mala::Core::EColor::White, log._message.c_str() );
    } );
}

};

