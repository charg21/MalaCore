module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>

export module Mala.Log.ConsoleLog;

#include "../MalaMacro.h";

export import <stdio.h>;
import <memory>;


NAMESPACE_BEGIN( Mala::Log )

#ifdef _WIN32

/// <summary>
/// 콘솔 출력 색상 열거형 타입
/// </summary>
enum class EColor
{
	Black,
	White  = FOREGROUND_RED   | FOREGROUND_GREEN | FOREGROUND_BLUE,
	Yellow = FOREGROUND_RED   | FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	Purple = FOREGROUND_RED   | FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
	Red    = FOREGROUND_RED   | FOREGROUND_INTENSITY,
	Mint   = FOREGROUND_GREEN | FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
	Green  = FOREGROUND_GREEN | FOREGROUND_INTENSITY,
	Blue   = FOREGROUND_BLUE  | FOREGROUND_INTENSITY,
};

#else
#endif

/// <summary>
/// 콘솔 로그 출력 클래스
/// </summary>
class ConsoleLog
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	ConsoleLog()
	{
		_stdOut   = ::GetStdHandle( STD_OUTPUT_HANDLE );
		_stdError = ::GetStdHandle( STD_ERROR_HANDLE );
	}

	/// <summary>
	/// 소멸자
	/// </summary>
	~ConsoleLog()
	{
		// ::CloseHandle( _stdOut   );
		// ::CloseHandle( _stdError );
	}

public:
	/// <summary>
	/// 표준 출력 디바이스에 로그를 작성한다
	/// </summary>
    void WriteStdOut( EColor color, const wchar_t* format, ... ) const
	{
		if ( !format )
			return;

		SetColor< true >( color );

		va_list ap;
		va_start( ap, format );
		::vwprintf( format, ap );
		va_end( ap );

		fflush( stdout );

		SetColor< true >( EColor::White );
	}

	/// <summary>
	/// 표준 오류 디바이스에 로그를 작성한다
	/// </summary>
	void WriteStdErr( EColor color, const wchar_t* format, ... ) const
	{
		wchar_t buffer[ 4096 ];

		if ( !format )
			return;

		SetColor< false >( color );

		va_list ap;
		va_start( ap, format );
		::vswprintf_s( buffer, 4096, format, ap );
		va_end( ap );

		::fwprintf_s( stderr, buffer );
		fflush( stderr );

		SetColor< false >( EColor::White );
	}

	/// <summary>
	/// 표준 디바이스에 색상을 지정한다
	/// </summary>
    template< bool IsStdOut >
	void SetColor( EColor color ) const
	{
		HANDLE outputHandle{};
		if constexpr ( IsStdOut )
		{
			outputHandle = _stdOut;
		}
		else
		{
			outputHandle = _stdError;
		}

		::SetConsoleTextAttribute( outputHandle, static_cast< WORD >( color ) );
	}

	HANDLE _stdOut;   //< 표준 출력 디바이스
	HANDLE _stdError; //< 표준 오류 디바이스
};

/// <summary>
/// 로그를 작성한다.
/// </summary>
/// <typeparam name="Args"> 가변 인자 목록 </typeparam>
template< typename... Args >
void WriteLog( EColor color, const wchar_t* fmt, Args&&... args )
{
	static ConsoleLog sConsoleLog;

	sConsoleLog.WriteStdOut( color, fmt, std::forward< Args >( args )... );
}

NAMESPACE_END( Mala::Log )