﻿export module Mala.Net.SendBuffer:impl;

#include "../MalaMacro.h"

import <concepts>;
import <memory>;
import <string>;
import <iostream>;

import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.Types;

import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Buffers.ObjectPool;
import Mala.Threading.Lock;
import Mala.Memory;
import Mala.Windows;

using namespace Mala::Buffers;

export namespace Mala::Net
{

/// <summary>
/// 생성자
/// </summary>
SendBuffer::SendBuffer( SendBufferChunkRef owner, BYTE* buffer, u32 allocSize )
: _owner{ owner }
, _buffer{ buffer }
, _allocSize{ allocSize }
{
}

/// <summary>
/// 생성자
/// </summary>
SendBuffer::SendBuffer( SendBufferChunkPtr&& owner, BYTE* buffer, u32 allocSize )
: _owner{ std::move( owner ) }
, _buffer{ buffer }
, _allocSize{ allocSize }
{
}

///// </summary>
//SendBuffer::~SendBuffer()
//{
//	auto useCount = _owner.use_count();
//	ASSERT_CRASH( useCount != 0 );
//
//	std::cout << "[~SendBuffer] 스레드 ID" << LThreadId << "Owner Use Count" << _owner.use_count() << std::endl;
//	_owner.reset();
//	_buffer = nullptr;
//	_allocSize = 0;
//	_writeSize = 0;
//}

void SendBuffer::Close( u32 writeSize )
{
	ASSERT_CRASH( _allocSize >= writeSize );
	_writeSize = writeSize;
	_owner->Close( writeSize );
}

RIO_BUFFERID SendBuffer::BufferId()
{
	return _owner->BufferId();
}

i64 SendBuffer::BufferOffset()
{
	i64 offset = (char*)_buffer - (char*)&_owner->_buffer[ 0 ];

	return offset;
}

bool SendBuffer::Merge( SendBufferRef sendBuffer )
{
	if ( BufferId() != sendBuffer->BufferId() )
		return false;

	if ( BufferOffset() + WriteSize() != sendBuffer->BufferOffset() )
		return false;

	_writeSize += sendBuffer->WriteSize();

	return true;
}

SendBufferChunk::~SendBufferChunk()
{
}

void SendBufferChunk::Reset()
{
	_open = false;
	_usedSize = 0;
}

SendBufferPtr SendBufferChunk::Open( u32 allocSize )
{
	ASSERT_CRASH( allocSize <= SEND_BUFFER_CHUNK_SIZE );
	ASSERT_CRASH( _open == false );

	if ( allocSize > FreeSize() )
		return nullptr;

	_open = true;
	
	return ::MakeShared< SendBuffer >( std::move( shared_from_this() ), Buffer(), allocSize );
}

void SendBufferChunk::Close( u32 writeSize )
{
	ASSERT_CRASH( _open == true );
	_open = false;
	_usedSize += writeSize;
}
//
SendBufferPtr SendBufferManager::Open( u32 size )
{
	if ( !LSendBufferChunk )
	{
		LSendBufferChunk = Pop();
		LSendBufferChunk->Reset();
	}

	ASSERT_CRASH( LSendBufferChunk->IsOpen() == false );

	if ( LSendBufferChunk->FreeSize() < size )
	{
		LSendBufferChunk = Pop();
		LSendBufferChunk->Reset();
	}

	// cout << "Free : " << LSendBufferChunk->FreeSize() << endl;

	return LSendBufferChunk->Open( size );
}

SendBufferChunkPtr SendBufferManager::Pop()
{
	{
		WRITE_LOCK;

		if ( !_sendBufferChunks.empty() )
		{
			SendBufferChunkPtr sendBufferChunk = _sendBufferChunks.back();
			_sendBufferChunks.pop_back();

			return std::move( sendBufferChunk );
		}
	}
	
	return SendBufferChunkPtr( xnew< SendBufferChunk >(), PushGlobal );
}

void SendBufferManager::Push( SendBufferChunkRef buffer )
{
	WRITE_LOCK;

	_sendBufferChunks.emplace_back( buffer );
}

void SendBufferManager::Push( SendBufferChunkPtr&& buffer )
{
	WRITE_LOCK;

	_sendBufferChunks.emplace_back( std::move( buffer ) );
}

void SendBufferManager::PushGlobal( SendBufferChunk* buffer )
{
	GSendBufferManager->Push( SendBufferChunkPtr( buffer, PushGlobal ) );
}

} 
