module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>
#include "../MalaMacro.h"

#pragma comment( lib, "ws2_32" )

export module Mala.Net.IocpEvent:impl;

import Mala.Net.IocpEvent;

using namespace Mala::Net;

IocpEvent::IocpEvent( EEventType type )
: eventType{ type }
{
	Reset();
}

void IocpEvent::Reset()
{
	OVERLAPPED::Internal = 0;
	OVERLAPPED::InternalHigh = 0;

	//OVERLAPPED::Offset = 0;     // 비동기 파일 IO에서 사용
	//OVERLAPPED::OffsetHigh = 0; //( 예약 공간 )

	OVERLAPPED::hEvent = 0;  // 이벤트를 사용하지 않는다면 무조건 0
}
