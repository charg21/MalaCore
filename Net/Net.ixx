module;

export module Mala.Net;

//export import Mala.Net.Acceptor;
export import Mala.Net.Buffer;
//export import Mala.Net.Connector;
export import Mala.Net.DisconnectReason;
export import Mala.Net.EndPoint;
export import Mala.Net.IocpCore;
export import Mala.Net.IocpObject;
export import Mala.Net.IocpEvent;
export import Mala.Net.IocpSession;
export import Mala.Net.IocpService;
export import Mala.Net.RioEvent;
export import Mala.Net.RioCore;
export import Mala.Net.RioSession;
export import Mala.Net.RioService;
export import Mala.Net.SocketHelper;
export import Mala.Net.SendBuffer;
export import Mala.Net.RecvBuffer;


import Mala.Core.TypeTraits;

export namespace Mala::Net
{

inline constexpr bool UseRio{ true };

using NetService        = ConditionalT< UseRio, RioService, IocpService >;
using NetServerService  = ConditionalT< UseRio, RioServerService, IocpServerService >;
using NetSessionFactory = ConditionalT< UseRio, RioSessionFactory, IocpSessionFactory >;
using NetCore           = ConditionalT< UseRio, RioCore, IocpCore >;
using NetSession        = ConditionalT< UseRio, RioSession, IocpSession >;

}