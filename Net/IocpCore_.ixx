module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>
#include "../MalaMacro.h"

#pragma comment( lib, "ws2_32" )

export module Mala.Net.IocpCore:impl;

import <memory>;

import Mala.Core.Types;
import Mala.Net.IocpCore;
import Mala.Net.IocpEvent;
import Mala.Net.IocpObject;
import Mala.Windows;

export namespace Mala::Net
{

IocpCore::IocpCore()
{
	_iocpHandle = ::CreateIoCompletionPort( INVALID_HANDLE_VALUE, 0, 0, 0 );
	ASSERT_CRASH( _iocpHandle != INVALID_HANDLE_VALUE );
}

IocpCore::~IocpCore()
{
	::CloseHandle( _iocpHandle );
}

bool IocpCore::Register( IocpObjectRef iocpObject )
{
	return ::CreateIoCompletionPort( iocpObject->GetHandle(), _iocpHandle, /*key*/0, 0 );
}

bool IocpCore::Dispatch( u32 timeoutMs )
{
	DWORD       numofBytes{};
	ULONG_PTR   key{};
	IocpObject* iocpObject = nullptr;	//등록한 iocpObject와  <- 해당 Object의 데이터 오염 문제가 발생 할 수 있음.
	IocpEvent*  iocpEvent = nullptr;	//어떤 이벤트가 호출됬는지 아는 iocpEvent를 out으로 보냄
	
	if ( ::GetQueuedCompletionStatus( 
		_iocpHandle, 
		OUT & numofBytes, 
		OUT reinterpret_cast< PULONG_PTR >( &key ),
		OUT reinterpret_cast< LPOVERLAPPED* >( &iocpEvent ), 
		timeoutMs ) ) 
	{
		//복원
		IocpObjectPtr iocpObject = iocpEvent->owner;
		iocpObject->Dispatch( iocpEvent, numofBytes );
	}
	else 
	{
		i32 errorCode = ::WSAGetLastError();
		switch ( errorCode ) 
		{
		case WAIT_TIMEOUT:
			//타임 아웃은 에러가 아님,
			return false;
		default:
			//TODO : 로그 찍기.
			IocpObjectPtr iocpObject = iocpEvent->owner;
			iocpObject->Dispatch( iocpEvent, numofBytes );
			break;
		}
	}
	
	return true;
}

void IocpCore::Initialize( i32 threadCount )
{
}

}