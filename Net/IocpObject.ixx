export module Mala.Net.IocpObject;

import <memory>;

import Mala.Core.Types;
import Mala.Windows;

export namespace Mala::Net
{
/// <summary>
/// 
/// </summary>
class IocpObject : public std::enable_shared_from_this< IocpObject >
{
public:
	virtual HANDLE GetHandle() abstract;
	virtual void Dispatch( class IocpEvent* iocpEvent, i32 numofBytes ) abstract;
};

}
