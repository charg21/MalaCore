export module Mala.Net.PacketHeader;

import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;

class StreamBuffer;

export namespace Mala::Net
{
#pragma pack( push ,1 )

struct PacketHeader
{
    unsigned short _length; /// 길이
    unsigned short _type;   /// 패킷의 타입

    bool Serialize( Mala::Net::StreamBuffer* stream )
    {
        unsigned short* length = (unsigned short*)( stream->get_writer_buffer() );
        stream->Write( &_length, sizeof( _length ) );
        stream->Write( &_type,   sizeof( _type   ) );

        auto result = OnSerialize( stream );

        *length = (size_t)( stream->get_writer_buffer() ) - (size_t)(length);

        return result;
    }

    bool Deserialize( Mala::Net::StreamBuffer* stream )
    {
        stream->Read( &_length, sizeof( _length ) );
        stream->Read( &_type,   sizeof( _type   ) );

        return OnDeserialize( stream );
    }

    bool OnSerialize( Mala::Net::StreamBuffer* stream )
    {
        return false;
    }

    bool OnDeserialize( Mala::Net::StreamBuffer* stream )
    {
        return false;
    }
};

#pragma pack (pop)
} 

