//module;
//
//#ifndef WIN32_LEAN_AND_MEAN
//#define WIN32_LEAN_AND_MEAN
//#endif

//#include <Windows.h>

//#include <winsock2.h>
//#include <iphlpapi.h>
//#include <stdio.h>
//#include <string>

//#pragma comment(lib, "iphlpapi.lib")

export module Mala.Net.PortChecker;

import <string>;
import <iostream>;
//import Mala.Windows.Cmd;

export namespace Mala::Net
{

class PortChecker
{
public:
    enum EWellKnownPort
    {
        Reserved = 0,
        Tcpmux = 1,
        EchoProtocol = 7,
        Discard = 9,

        Max
    };

    static bool CanConnect( unsigned short tcp_port )
    {
        std::wstring query = L"netstat -n | findstr TCP | findstr :";
        query += std::move( std::to_wstring( tcp_port ) );

        ///*auto result = */Mala::Windows::Cmd::Query( query );
        //if ( !result.first )
        //    return false;

        //std::string& query_result = result.second;
        //std::string  src_port     = std::move( query_result.substr( query_result.find( ":" ) + 1, 5 ) );
        ////long parsed_src_port    = std::stoi( src_port, nullptr, 10 );

        //if ( std::string::npos == src_port.find( std::move( std::to_string( tcp_port ) ) ) )
        //    return true;

        return false;
    }
};

}