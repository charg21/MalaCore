module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
#include <WS2tcpip.h>

#include "../MalaMacro.h"

export module Mala.Net.RioCore:impl;

import <iostream>;

import Mala.Core.Crash;
import Mala.Core.CoreTLS;
import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Net.Buffer;
import Mala.Net.RioCore;
import Mala.Net.RioEvent;
import Mala.Net.RioSession;
import Mala.Net.SocketHelper;
import Mala.Net.IocpEvent;
import Mala.Net.IocpObject;
import Mala.Windows;
import Mala.Threading.ThreadManager;

using namespace Mala::Net;

RioCore::RioCore()
: _concurrentThreadCount{}
{
}

RioCore::~RioCore()
{
    for ( auto iocpHandle : _iocpHandles )
    {
        ::CloseHandle( iocpHandle );
    }

    _iocpHandles.clear();
}

void RioCore::Initialize( i32 threadCount )
{
    _concurrentThreadCount = threadCount;
    _iocpHandles.resize( threadCount );
    _completionQueues.resize( threadCount );

    for ( i32 i{}; i < threadCount; i += 1 )
    {
        _iocpHandles[ i ] = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 1 );
        ASSERT_CRASH( _iocpHandles[ i ] != NULL );

        OVERLAPPED overlapped{};
        RIO_NOTIFICATION_COMPLETION completionType{};
        completionType.Type               = RIO_IOCP_COMPLETION;
        completionType.Iocp.IocpHandle    = _iocpHandles[ i ];
        completionType.Iocp.CompletionKey = (void*)( CK_RIO );
        completionType.Iocp.Overlapped    = &overlapped;

        _completionQueues[ i ] = SocketHelper::RioFunctionTable.RIOCreateCompletionQueue( MAX_CQ_SIZE, &completionType );
        ASSERT_CRASH( _completionQueues[ i ] != RIO_INVALID_CQ_ );

        int notifyResult = SocketHelper::RioFunctionTable.RIONotify( _completionQueues[ i ] );
        ASSERT_CRASH( notifyResult == ERROR_SUCCESS );
    }
}

bool RioCore::Register( RioObjectRef rioObject )
{
    /// TODO: CQ 로드밸런싱하여 배정 되도록 수정
    static std::atomic< int > sThreadId = 0;
    rioObject->_threadId = sThreadId.fetch_add( 1 ) % _concurrentThreadCount;

    if ( auto rioSession = std::dynamic_pointer_cast< RioSession >( rioObject ) )
    {
        /// create socket RQ
        auto requestQueue = SocketHelper::RioFunctionTable.RIOCreateRequestQueue(
            rioSession->GetSocket(),
            MAX_RECV_RQ_SIZE_PER_SOCKET,
            1,
            MAX_SEND_RQ_SIZE_PER_SOCKET,
            1,
            GetCompletionQueue( rioObject->GetThreadId() ),
            GetCompletionQueue( rioObject->GetThreadId() ),
            NULL );
        ASSERT_CRASH( requestQueue != RIO_INVALID_RQ_ );
        rioObject->_requestQueue = requestQueue;
    }

    /// 컴플리션 포트 등록
    HANDLE resultHandle = ::CreateIoCompletionPort( 
        rioObject->GetHandle(), 
        _iocpHandles[ rioObject->GetThreadId() ], 
        /*completionKey*/0, 
        0 );

    return resultHandle == _iocpHandles[ rioObject->GetThreadId() ];
}

bool RioCore::Register( IocpObjectRef iocpObject )
{
    /// 컴플리션 포트 등록
    HANDLE resultHandle = ::CreateIoCompletionPort(
        iocpObject->GetHandle(),
        _iocpHandles[ 0 ],
        /*completionKey*/0,
        0 );

    return resultHandle;
}

bool RioCore::Dispatch( u32 timeoutMs )
{
    auto threadId = LThreadId;

    RIO_CQ      cq = _completionQueues[ threadId ];
    HANDLE		iocpHandle = _iocpHandles[ threadId ];

    DWORD		numOfBytes{};
    ULONG_PTR	key{};
    IocpEvent*  iocpEvent{};

    if ( ::GetQueuedCompletionStatus(
        iocpHandle,
        OUT &numOfBytes,
        OUT reinterpret_cast< PULONG_PTR >( &key ),
        OUT reinterpret_cast< LPOVERLAPPED* >( &iocpEvent ),
        timeoutMs ) )
    {
        if ( CK_RIO != key && iocpEvent != nullptr )
        {
            IocpObjectPtr iocpObject = iocpEvent->owner;
            iocpObject->Dispatch( iocpEvent, numOfBytes );
        }
        else
        {
            RIORESULT results[ MAX_RIO_RESULT ];
            ULONG numResults = SocketHelper::RioFunctionTable.RIODequeueCompletion( cq, results, MAX_RIO_RESULT );
            if ( 0 == numResults || RIO_CORRUPT_CQ_CODE == numResults )
            {
                ASSERT_CRASH( false );
            }

            if ( numResults > ( MAX_RIO_RESULT / 2 ) )
            {
                std::cout << "NumResults Count: " << numResults << std::endl;
            }

            /// Completion 완료 후 CompletionQueue 사용 가능 통지
            auto notifyResult = SocketHelper::RioFunctionTable.RIONotify( cq );
            ASSERT_CRASH( notifyResult == ERROR_SUCCESS );

            for ( ULONG i = 0; i < numResults; ++i )
            {
                auto* rioEvent = reinterpret_cast< RioEvent* >( results[ i ].RequestContext );
                auto rioObject = rioEvent->owner;
                ULONG transferred = results[ i ].BytesTransferred;
                if ( !transferred )
                {
                    std::cout  << "transferred Is Zero: " << results[ i ].Status << std::endl;
                    continue;
                }

                rioObject->Dispatch( rioEvent, transferred );
            }
        }
    }
    else
    {
        i32 errorCode = ::WSAGetLastError();
        switch ( errorCode )
        {
        case WAIT_TIMEOUT:
            return false;
        default:
            //TODO : 로그 찍기.
            auto rioEvent = (RioEvent*)( iocpEvent );
            RioObjectPtr rioObject = rioEvent->owner;
            rioObject->Dispatch( rioEvent, numOfBytes );
            break;
        }
    }

    return true;
}

