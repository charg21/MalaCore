export module Mala.Net.RecvBuffer;

#include "../MalaMacro.h"

import Mala.Core.Types;
import Mala.Windows;
import Mala.Container;

using namespace Mala::Container;

export namespace Mala::Net
{
	using BYTE = unsigned char;

/// <summary>
/// RecvBuffer
/// </summary>
class RecvBuffer
{
	enum 
	{ 
		BUFFER_COUNT = 10,
		BUFFER_CAPACITY = 65536
	};

public:
	/// <summary>
	/// ������
	/// </summary>
	RecvBuffer( i32 bufferSize, bool useRio = false );
	~RecvBuffer();

	void Clean();
	bool OnRead( i32 numOfBytes );
	bool OnWrite( i32 numOfBytes );

	BYTE* ReadPos() { return &_buffer[ _readPos ]; }
	BYTE* WritePos() { return &_buffer[ _writePos ]; }
	i32 WritePosOffset() { return _writePos; }
	i32 DataSize() { return _writePos - _readPos; }
	i32 FreeSize() { return _capacity - _writePos; }

	RIO_BUFFERID GetRioBufferId() { return _rioBufferId; }

private:
	i32	_capacity = 0;
	i32	_bufferSize = 0;
	i32	_readPos = 0;
	i32	_writePos = 0;

	BYTE* _buffer;
	RIO_BUFFERID _rioBufferId;
};

}