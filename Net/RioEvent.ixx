﻿export module Mala.Net.RioEvent;

import Mala.Core.Types;
import Mala.Container;
import Mala.Windows;
import Mala.Net.IocpEvent;

using namespace Mala::Container;

export namespace Mala::Net
{

/// <summary>
/// Rio 이벤트
/// </summary>
struct RioEvent : public RIO_BUF
{
    RioEvent( EEventType eventType );
    void Reset();

public:
    const EEventType _eventType;  // 
    RioObjectPtr owner;
};

struct RioRecvEvent : public RioEvent
{
    RioRecvEvent()
    : RioEvent{ EEventType::Recv }
    {
    }
};

struct RioSendEvent : public RioEvent
{
	RioSendEvent();
	~RioSendEvent();
    
	char* _buffer{};
};

/// <summary>
/// Connect Event 
/// </summary>
struct RioConnectEvent : public RioEvent
{
public:
	RioConnectEvent()
	: RioEvent{ EEventType::Connect }
	{
	}
private:
	OVERLAPPED _overlapped{};
};

/// <summary>
/// Disconnect Event 
/// </summary>
struct RioDisConnectEvent : public RioEvent
{
public:
	RioDisConnectEvent()
	: RioEvent{ EEventType::DisConnect }
	{
	}

private:
	OVERLAPPED _overlapped{};
};

/// <summary>
/// Accept Event 
/// </summary>
/// 
struct RioAcceptEvent : public RioEvent
{
public:
	RioAcceptEvent()
	: RioEvent{ EEventType::Accept }
	{
	}

public:
	OVERLAPPED _overlapped{};
	RioSessionPtr session{};
};


} // namespace Mala::Net
