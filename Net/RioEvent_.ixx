﻿export module Mala.Net.RioEvent:impl;

import <stdint.h>;

//import std.core;
import Mala.Core.Types;
import Mala.Windows;
import Mala.Net.IocpEvent;
import Mala.Net.RioEvent;
import Mala.Net.RioSession;
import Mala.Net.SocketHelper;

using namespace Mala::Net;


RioEvent::RioEvent( EEventType eventType ) 
: _eventType{ eventType }
{}


RioSendEvent::RioSendEvent()
: RioEvent{ EEventType::Send }
{
    _buffer = (char*)( Mala::Windows::VirtualAllocEx( 65536 * 2 ) );

    RIO_BUF::BufferId = SocketHelper::RioFunctionTable.RIORegisterBuffer(
        _buffer,
        65536 * 2 );

    RIO_BUF::Offset = 0;
    RIO_BUF::Length = 0;
}

RioSendEvent::~RioSendEvent()
{
    SocketHelper::RioFunctionTable.RIODeregisterBuffer( RIO_BUF::BufferId );

    Mala::Windows::VirtualFreeEx( _buffer );
}

void RioEvent::Reset()
{
    RIO_BUF::BufferId = 0;
    RIO_BUF::Offset = 0;
    RIO_BUF::Length = 0;
}
