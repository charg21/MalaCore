export module Mala.Net.IocpService;

#include "../MalaMacro.h"

import <functional>;
import <memory>;
import <set>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.EndPoint;
import Mala.Net.IocpCore;
import Mala.Net.IocpListener;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Container;

export namespace Mala::Net
{

/// <summary>
/// 서비스 타입 열거형
/// </summary>
enum class EServiceType
{
	Server = 0,
	Client = 1 << 0,
	All = ~0
};

using IocpSessionFactory = std::function< IocpSessionPtr() >;

/// <summary>
/// Server, Client 등.. 여러 정책의 Service클래스
/// </summary>
class IocpService : public std::enable_shared_from_this< IocpService >
{
	using Sessions = Mala::Container::Set< IocpSessionPtr >;

public:
	IocpService( EServiceType type, EndPoint EndPoint, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount = 1 );
	virtual ~IocpService() = default;

	virtual bool Start() abstract; //Start;
	bool CanStart() { return _sessionFactory != nullptr; }
	virtual void CloseService();

	void SetSessionFactory( IocpSessionFactory factory ) { _sessionFactory = factory; }

	IocpSessionPtr CreateSession();
	void AddSession( IocpSessionRef session );
	void ReleaseSession( IocpSessionRef session );
	i32	GetCurrentSessionCount() { return _sessionCount; }
	i32	GetMaxSessionCount() { return _maxSessionCount; }

public:
	EServiceType GetServiceType() { return _type; }
	EndPoint GetEndPoint() { return _endPoint; }
	IocpCorePtr GetCore() { return _iocpCore; }
	IocpCoreRef GetCoreRef() { return _iocpCore; }

protected:
	USE_LOCK;

	EServiceType _type;
	EndPoint _endPoint{};
	IocpCorePtr _iocpCore;

	std::set< IocpSessionPtr > _sessions;
	i32 _sessionCount = 0;
	i32 _maxSessionCount = 0;

	IocpSessionFactory	_sessionFactory;
};

class IocpClientService : public IocpService
{
public:
	IocpClientService( EndPoint address, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount = 1 );
	virtual ~IocpClientService();

	virtual bool Start() override;
};

class IocpServerService : public IocpService
{
public:
	IocpServerService( const EndPoint& address, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount = 1 );
	virtual ~IocpServerService();

	virtual bool Start() override;
	virtual void CloseService() override;

private:
	IocpListenerPtr _listener = nullptr;
};

}