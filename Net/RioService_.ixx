export module Mala.Net.RioService:impl;

#include "../MalaMacro.h"

import <functional>;
import <vector>;
import <string>;
import <memory>;
import <set>;

import Mala.Core.CoreEnum;
import Mala.Core.Types;

import Mala.Net.EndPoint;
import Mala.Net.IocpService;
import Mala.Net.RioCore;
import Mala.Net.RioListener;
import Mala.Net.RioService;
import Mala.Net.RioSession;
import Mala.Memory;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Threading;
using namespace Mala::Net;


RioClientService::RioClientService( EndPoint address, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount )
: RioService( EServiceType::Client, address, core, factory, maxSessionCount )
{
}

RioServerService::RioServerService( EndPoint address, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount )
: RioService( EServiceType::Server, address, core, factory, maxSessionCount )
{
}

bool RioServerService::Start()
{
	if ( !CanStart() )
		return false;

	_listener = MakeShared< RioListener >();
	if ( !_listener )
		return false;

	RioServerServicePtr service = static_pointer_cast< RioServerService >( shared_from_this() );
	if ( !_listener->StartAccept( service ) )
		return false;

	return true;
}

void RioServerService::CloseService()
{
	RioService::CloseService();
}


RioService::RioService( EServiceType type, EndPoint endPoint, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount )
: _type( type )
, _endPoint( endPoint )
, _rioCore( core )
, _sessionFactory( factory )
, _maxSessionCount( maxSessionCount )
{
}

RioSessionPtr RioService::CreateSession()
{
	RioSessionPtr session = _sessionFactory();
	session->SetService( shared_from_this() );
	if ( !_rioCore->Register( session ) )
		return nullptr;

	return session;
}

void RioService::AddSession( RioSessionRef session )
{
	WRITE_LOCK;

	_sessionCount++;
	_sessions.insert( session );
}

void RioService::ReleaseSession( RioSessionRef session )
{
	WRITE_LOCK;

	ASSERT_CRASH( _sessions.erase( session ) != 0 );
	_sessionCount--;
}
