module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <MSWSock.h>

#include "../MalaMacro.h"

#pragma comment( lib, "ws2_32" )

export module Mala.Net.SocketHelper;

import <string>;
import Mala.Core.Types;
import Mala.Core.Crash;
import Mala.Net.EndPoint;

export namespace Mala::Net
{

struct SocketHelper
{
    /// <summary>
    /// 소켓 유틸을 초기화한다
    /// </summary>
    static MALA_NODISCARD bool Initialize();
    static MALA_NODISCARD bool InitializeExtendedApi();

    /// <summary>
    /// 소켓 유틸을 정리한다
    /// </summary>
    static void Finalize();

    /// <summary>
    /// 소켓을 생성한다
    /// </summary>
    static MALA_NODISCARD SOCKET MakeSocket();
    static MALA_NODISCARD SOCKET MakeSocketRio();

    static bool SetLinger( SOCKET socket, u32 onoff, u32 linger );
    static bool SetReuseAddress( SOCKET socket, bool flag );
    static bool SetRecvBufferSize( SOCKET socket, i32 size );
    static bool SetSendBufferSize( SOCKET socket, i32 size );
    static bool SetTcpNoDelay( SOCKET socket, bool flag );					//Nagel Algorithm 
    static bool SetUpdateAcceptSocket( SOCKET socket, SOCKET listenSocket );	//listenSocket의 특성을 Client속성에 전파한다.

    /// <summary>
    /// 소켓을 바인드한다
    /// </summary>
    static MALA_NODISCARD bool Bind( SOCKET sock, const EndPoint& endPoint );
    static bool BindAnyAddress( SOCKET socket, u16 port );

    /// <summary>
    /// 문자열 주소를 통해 세팅 IN_ADDR 구조체에 주소를 설정한다
    /// </summary>
    static IN_ADDR IpToAddress( const std::wstring& address );
    static IN_ADDR IpToAddress( const std::string& address );

    static bool Close( SOCKET& socket );
    static void Clear();
    static bool Listen( SOCKET socket, i32 backlog = SOMAXCONN );
    
    /// <summary>
    /// 윈속 확장 함수 구조체
    /// </summary>
    inline static LPFN_CONNECTEX               ConnectEx       {};
    inline static LPFN_DISCONNECTEX            DisconnectEx    {};
    inline static LPFN_ACCEPTEX                AcceptEx        {};
    inline static RIO_EXTENSION_FUNCTION_TABLE RioFunctionTable{};
};

}