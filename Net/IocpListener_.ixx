module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include "../MalaMacro.h"
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.IocpListener:impl;

import <memory>;

import Mala.Core.Types;

import Mala.Net.IocpCore;
import Mala.Net.IocpEvent;
import Mala.Net.IocpObject;
import Mala.Net.IocpListener;
import Mala.Net.IocpSession;
import Mala.Net.IocpService;
import Mala.Net.RioCore;
import Mala.Net.RioService;
import Mala.Net.EndPoint;
import Mala.Net.SocketHelper;
import Mala.Memory;

export namespace Mala::Net
{

IocpListener::~IocpListener()
{
	SocketHelper::Close( _socket );

	for ( AcceptEvent* acceptEvent : _acceptEvents ) 
	{
		xdelete( acceptEvent );
	}
}

bool IocpListener::StartAccept( IocpServerServiceRef service )
{
	_service = service;
	if ( !_service )
		return false;

	_socket = SocketHelper::MakeSocket();
	if ( _socket == INVALID_SOCKET )
		return false;
	
	////iocp 등록 <-Listen 소켓 iocp 등록
	if ( !_service->GetCoreRef()->Register( shared_from_this() ) )
		return false;

	if ( !SocketHelper::SetReuseAddress( _socket, true ) )
		return false;

	if ( !SocketHelper::SetLinger( _socket, 0, 0 ) )
		return false;

	if ( !SocketHelper::Bind( _socket, _service->GetEndPoint() ) )
		return false;
	
	if ( !SocketHelper::Listen( _socket ) )
		return false;

	////accept 등록
	const i32 acceptCount = _service->GetMaxSessionCount();
	for ( i32 i = 0; i < acceptCount; i++) 
	{
		AcceptEvent* acceptEvent = xnew< AcceptEvent >();
		acceptEvent->owner = shared_from_this();
		_acceptEvents.push_back( acceptEvent );
		RegisterAccept( acceptEvent );
	}

	return true;
}

void IocpListener::CloseSocket()
{
	Mala::Net::SocketHelper::Close( _socket );
}

HANDLE IocpListener::GetHandle()
{
	return reinterpret_cast< HANDLE >( _socket );
}

void IocpListener::Dispatch( IocpEvent* iocpEvent, i32 numofBytes )
{
	ASSERT_CRASH( iocpEvent->eventType == EEventType::Accept );
	AcceptEvent* acceptEvent = static_cast< AcceptEvent* >( iocpEvent );
	ProcessAccept( acceptEvent );
}

//iocp에서 처리할 수 있도록 accept를 등록한다
void IocpListener::RegisterAccept( AcceptEvent* acceptEvent )
{
	IocpSessionPtr session = _service->CreateSession();
	
	acceptEvent->Reset();
	acceptEvent->session = session;
	
	DWORD bytesReceived = 0;

	//당장 실행은 안되지만, Pending상태가 되면 iocp로 부터 오게된다, IocpListener 소켓을 iocp에 등록했기 때문.
	if ( !SocketHelper::AcceptEx( _socket, 
		session->GetSocket(), 
		session->_recvBuffer.WritePos(), 
		0,
		sizeof( SOCKADDR_IN ) + 16, 
		sizeof( SOCKADDR_IN ) + 16, 
		OUT & bytesReceived, 
		static_cast< LPOVERLAPPED >( acceptEvent ) ) ) 
	{
		const i32 errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING ) // Pending 아닐경우.
		{
			//다시 accpet 건다.
			RegisterAccept( acceptEvent );
		}
	}
}

void IocpListener::ProcessAccept( AcceptEvent* acceptEvent )
{
	IocpSessionPtr session = acceptEvent->session;
	
	if ( !SocketHelper::SetUpdateAcceptSocket( session->GetSocket(), _socket ) ) 
	{
		//문제가 발생하면 RegisterAccept를 걸어줘야한다. 재사용 해야 하기 때문.
		RegisterAccept( acceptEvent );
		return;
	}

	SOCKADDR_IN sockAddress;
	i32 sizeofSockAddr = sizeof( sockAddress );
	//상대방의 peer 정보를 얻는 함수
	if ( SOCKET_ERROR == ::getpeername( session->GetSocket(), 
		OUT reinterpret_cast< SOCKADDR* >( &sockAddress ), 
		&sizeofSockAddr ) ) 
	{
		RegisterAccept( acceptEvent );
		return;
	}

	session->SetEndPoint( EndPoint( sockAddress ) );
	session->ProcessConnect();
	//cout << "Client Connected !" << endl;

	RegisterAccept( acceptEvent );
}

}