export module Mala.Net.HttpSession:impl;

import <iostream>;
import <format>;

import Mala.Core.Types;
import Mala.Core.CoreGlobal;
import Mala.Container.String;
import Mala.Net.HttpSession;
import Mala.Net.SendBuffer;
import Mala.Memory;
import Mala.Net.PacketHeader;

export namespace Mala::Net
{

i32 HttpSession::OnRecv( BYTE* buffer, i32 len )
{
	static const char* notFound = "HTTP/1.1 404 NOT FOUND\r\n\r\n";

	i32 processLen = 0;

	for ( ;; )
	{
		// 최소한 헤더는 파싱할 수 있어야 한다.
		// HTTP METHOD 타입( 3 ), 마지막 종료 문자 "\r\n\r\n"( 4 );
		size_t dataSize = len - processLen;
		if ( dataSize < 7 )
			break;

		// 받아온 버퍼를 문자열 뷰로 캐스팅한다
		// 헤더의 끝이 도착하지 않았다면, 추가적으로 수신을 통해 헤더를 완성해야한다.
		std::string_view request{ (char*)( &buffer[ processLen ] ), dataSize };
		auto headerEnd = request.find_last_of( "\r\n\r\n" );
		if ( headerEnd == std::string_view::npos )
			break;

		// 바디 사이즈 체크.
		// Http 요청 조립 성공
		OnRecvRequest( request );

		processLen += request.size();
	}

	return processLen;
}

void HttpSession::OnRecvRequest( const std::string_view& request )
{
	// 임시적으로 404 페이지를 보냄

	static const char* notFound = "HTTP/1.1 404 NOT FOUND\r\nContent-Length:171\r\n\r\n";
	static const char* page404 = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\"><title>Hello!</title></head><body>\
<h1>Oops!</h1><p>Mala.Net.HttpSession Error!</p></body></html>";

	auto sendBuffer = GSendBufferManager->Open( 256 );
	auto page404Length = strlen( page404 );
	auto notFoundLength = strlen( notFound );
	std::memcpy( sendBuffer->Buffer(), notFound, notFoundLength );
	std::memcpy( &sendBuffer->Buffer()[ notFoundLength ], page404, page404Length );

	sendBuffer->Close( notFoundLength + page404Length );
	Send( sendBuffer );

	//ERROR_LOG( L"HTTP/1.1 404 NOT FOUND" );
}

}