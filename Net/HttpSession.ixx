export module Mala.Net.HttpSession;

#pragma once

import Mala.Core.Types;
import Mala.Net;

export namespace Mala::Net
{

class HttpSession : public NetSession
{
public:
	/// <summary>
	/// 持失切
	/// </summary>
	HttpSession() = default;

	/// <summary>
	/// 社瑚切
	/// </summary>
	virtual ~HttpSession() override = default;

	HttpSessionPtr GetHttpSession() { return std::static_pointer_cast< HttpSession >( shared_from_this() ); }

protected:
	i32 OnRecv( BYTE* buffer, i32 len ) override final;

	virtual void OnRecvRequest( const std::string_view& request );
};



}