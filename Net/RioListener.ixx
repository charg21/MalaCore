module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.RioListener;

import <memory>;

import Mala.Core.Types;

import Mala.Container;
import Mala.Net.IocpCore;
import Mala.Net.IocpObject;
import Mala.Net.RioCore;
import Mala.Net.EndPoint;

using namespace Mala::Container;;

export namespace Mala::Net
{

class AcceptEvent;
class ServerService;

/// <summary>
/// Listenr
/// </summary>
class RioListener : public IocpObject
{
public:
	RioListener() = default;
	~RioListener();

public:
	//public Function
	bool StartAccept( RioServerServiceRef service );
	void CloseSocket();

public:
	//interface
	virtual HANDLE GetHandle() override;
	virtual void Dispatch( class IocpEvent* iocpEvent, i32 numofBytes ) override;

private:
	//수신 관련 코드
	void RegisterAccept( AcceptEvent* acceptEvent );
	void ProcessAccept( AcceptEvent* acceptEvent );

protected:
	SOCKET _socket = INVALID_SOCKET;
	Vector< AcceptEvent* > _acceptEvents;

	RioServerServicePtr _service;
};

}