export module Mala.Net.RioCore;

import <vector>;
import <string>;
import <memory>;

import Mala.Core;
import Mala.Container;
import Mala.Core.Types;
import Mala.Windows;
import Mala.Threading.ThreadManager;

using namespace Mala::Threading;
using namespace Mala::Container;

export namespace Mala::Net
{

class Acceptor;
class Connector;
class SessionManager;

/// <summary>
/// Rio �ھ�
/// </summary>
class RioCore
{
public:
    RioCore();
    virtual ~RioCore();

    bool Register( RioObjectRef rioObjectRef );
    bool Register( IocpObjectRef iocpObject );
    bool Dispatch( u32 timeoutMs = TIMEOUT_INFINITE );

    HANDLE GetHandle( int threadId ) { return _iocpHandles[ threadId ]; }
    const RIO_CQ& GetCompletionQueue( int threadId ) { return _completionQueues[ threadId ]; }
    i32 GetConcurrentThreadCount() { return _concurrentThreadCount; }

    void Initialize( i32 threadCount = 1 );

private:
    Vector< RIO_CQ > _completionQueues;
    Vector< HANDLE > _iocpHandles;
    i32 _concurrentThreadCount{};
};

} // namespace Mala::Net