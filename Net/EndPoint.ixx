module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.EndPoint;

import <string>;

export namespace Mala::Net
{

class EndPoint
{
public:
    EndPoint() = default;
    EndPoint( SOCKADDR_IN sockAddr );
    EndPoint( const std::string& address, uint16_t port );
    EndPoint( const std::wstring& address, uint16_t port );

    const sockaddr* AsSockaddr() const;
    sockaddr* AsSockaddr();
    sockaddr_in* AsSockaddrIn();
    const sockaddr_in* AsSockaddrIn() const;

    const uint16_t GetPort() const;
    std::wstring GetIp();
    std::string get_ip();

    constexpr size_t Size() const
    {
        return sizeof( sockaddr );
    }

    std::wstring ToString();

private:
    union
    {
        sockaddr    _sockaddr;
        sockaddr_in _sockaddr_in;
    };
};

} // namespace Mala::Net

