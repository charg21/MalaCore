﻿module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.IocpEvent;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.IocpObject;

using namespace Mala::Container;

export namespace Mala::Net
{

class IocpSession;

enum class EEventType : u8
{
	Connect,
	DisConnect,
	Accept,
	Recv,
	Send,

	RioRecv,
	RioSend,
};

/// <summary>
/// Overlapped
/// </summary>
struct IocpEvent : public OVERLAPPED
{
public:
	IocpEvent( EEventType type );	//virtual 로 만들지 않는 이유, 가상함수 테이블이 있을경우 첫번째 메모리가 Overlapped로 사용 불가
	
	void Reset();

public:
	EEventType eventType;
	IocpObjectPtr owner;
};

/// <summary>
/// Connect Event 
/// </summary>
struct ConnectEvent : public IocpEvent
{
public:
	ConnectEvent()
	: IocpEvent{ EEventType::Connect }
	{
	}

	RioObjectPtr rioOwner;
};

/// <summary>
/// Disconnect Event 
/// </summary>
struct DisConnectEvent : public IocpEvent
{
public:
	DisConnectEvent()
	: IocpEvent{ EEventType::DisConnect }
	{
	}

	RioObjectPtr rioOwner;
};

/// <summary>
/// Accept Event 
/// </summary>
/// 
struct AcceptEvent : public IocpEvent
{
public:
	AcceptEvent()
	: IocpEvent{ EEventType::Accept }
	{
	}

public:
	IocpSessionPtr session{};
	RioSessionPtr rioSession{};
};


/// <summary>
/// Recv Event 
/// </summary>
struct RecvEvent : public IocpEvent
{
public:
	RecvEvent()
	: IocpEvent{ EEventType::Recv }
	{
	}

private:
	//TODO. 인자가 추가로 있을수도 없을 수 있다.
};

/// <summary>
/// Send Event 
/// </summary>
struct SendEvent : public IocpEvent
{
public:
	SendEvent()
	: IocpEvent{ EEventType::Send }
	{
	}

public:
	Vector< SendBufferPtr > sendBuffers;
};

}