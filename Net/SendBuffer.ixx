export module Mala.Net.SendBuffer;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Container;
using namespace Mala::Threading;

export namespace Mala::Net
{

class SendBufferChunk;
using BYTE = unsigned char;

class SendBuffer
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	SendBuffer( SendBufferChunkRef owner, BYTE* buffer, u32 allocSize );
	SendBuffer( SendBufferChunkPtr&& owner, BYTE* buffer, u32 allocSize );

	/// <summary>
	/// 소멸자
	/// </summary>
	~SendBuffer() = default;

	/// <summary>
	/// 버퍼를 반환한다.
	/// </summary>
	/// <returns></returns>
	BYTE* Buffer() { return _buffer; }

	/// <summary>
	/// 
	/// </summary>
	u32	AllocSize() { return _allocSize; }

	/// <summary>
	/// 
	/// </summary>
	i32	WriteSize() { return _writeSize; }

	/// <summary>
	/// 
	/// </summary>
	void Close( u32 writeSize );

	/// <summary>
	/// 
	/// </summary>
	RIO_BUFFERID BufferId();
	i64 BufferOffset();
	
	/// <summary>
	/// 
	/// </summary>
	bool Merge( SendBufferRef sendBuffer );

	/// <summary>
	/// 
	/// </summary>
	u32	_writeSize{};

private:
	/// <summary>
	/// 
	/// </summary>
	BYTE* _buffer{};

	/// <summary>
	/// 
	/// </summary>
	u32	_allocSize{};

	// <summary>
	// 
	//</summary>
	SendBufferChunkPtr _owner;
};

class SendBufferChunk : public std::enable_shared_from_this< SendBufferChunk >
{
public:
	constexpr inline static size_t SEND_BUFFER_CHUNK_SIZE = 64 * 1024 * 10;

public:
	friend class SendBufferManager;
	friend class SendBuffer;

public:
	SendBufferChunk() = default;
	~SendBufferChunk();

	/// <summary>
	/// 초기 상태로 되돌린다.
	/// </summary>
	void Reset();

	/// <summary>
	/// 
	/// </summary>
	SendBufferPtr Open( u32 allocSize );

	/// <summary>
	/// 
	/// </summary>
	void Close( u32 writeSize );

	/// <summary>
	/// 
	/// </summary>
	bool IsOpen() { return _open; }
	BYTE* Buffer() { return &_buffer[ _usedSize ]; }
	u32 FreeSize() { return static_cast< u32 >( _buffer.size() ) - _usedSize; }
	RIO_BUFFERID BufferId() { return _bufferId; }

private:
	/// <summary>
	/// 버퍼 메모리
	/// </summary>
	Array< BYTE, SEND_BUFFER_CHUNK_SIZE > _buffer{};

	/// <summary>
	/// 오픈 여부
	/// </summary>
	bool _open = false;

	/// <summary>
	/// 사용된 크기
	/// </summary>
	u32 _usedSize = 0;

	/// <summary>
	/// 
	/// </summary>
	RIO_BUFFERID _bufferId{};
};

/// <summary>
/// 
/// </summary>
class SendBufferManager
{

public:
	SendBufferPtr Open( u32 size );

private:
	/// <summary>
	/// 
	/// </summary>
	SendBufferChunkPtr Pop();

	void Push( SendBufferChunkRef buffer );

	void Push( SendBufferChunkPtr&& buffer );

	static void	PushGlobal( SendBufferChunk* buffer );

private:
	USE_LOCK;

	Vector< SendBufferChunkPtr > _sendBufferChunks;
};


/// <summary>
/// 
/// </summary>
struct BufferWriter
{
	template< typename T >
	static i32 TryWrite( SendBufferRef buffer, T value, i32 offset = 0 )
	{
		if ( buffer->AllocSize() - ( buffer->WriteSize() + offset ) < sizeof( T ) )
			return 0;

		std::memcpy( &buffer->Buffer()[ offset ], &value, sizeof( T ) );

		return sizeof( T );
	}

	static i32 TryWrite( SendBufferRef buffer, const String& value, i32 offset = 0 )
	{
		u16 strFullLen = (u16)( ( value.size() * 2 ) + 2 );
		if( buffer->AllocSize() - ( buffer->WriteSize() + offset ) < strFullLen )
			return 0;

		u16 strLength = value.size();
		std::memcpy( &buffer->Buffer()[ offset     ], &strLength, 2 );
		std::memcpy( &buffer->Buffer()[ offset + 2 ], value.data(), value.size() * 2 );

		return strFullLen;
	}

};

/// <summary>
/// 
/// </summary>
struct BufferReader
{
	template< typename T >
	static bool TryRead( BYTE* buffer, i32 offset, T& value )
	{
		/*if ( buffer->AllocSize() - ( buffer->WriteSize() + offset ) < sizeof( T ) )
			return 0; */

		std::memcpy( &value, &buffer[ offset ], sizeof( T ) );

		return true;// sizeof( T );
	}

	static bool TryRead( BYTE* buffer, i32 offset, String& value )
	{
		bool success{ true };
		u16 stringSize;
		success &= BufferReader::TryRead( buffer, offset, stringSize );
		offset += sizeof( u16 );
		value = String( (const wchar_t*)( & buffer[ offset ] ), stringSize );

		return success;
	}
};

}
