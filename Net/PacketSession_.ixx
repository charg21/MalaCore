export module Mala.Net.PacketSession:impl;

import <format>;

import Mala.Core.Types;
import Mala.Net.PacketSession;
import Mala.Net.PacketHeader;

export namespace Mala::Net
{

i32 PacketSession::OnRecv( BYTE* buffer, i32 len )
{
	i32 processLen = 0;

	for ( ;; )
	{
		i32 dataSize = len - processLen;
		// 최소한 헤더는 파싱할 수 있어야 한다.
		if ( dataSize < sizeof( PacketHeader ) )
			break;

		// 받아온 버퍼를 헤더로 캐스팅한다
		PacketHeader header = *( reinterpret_cast< PacketHeader* >( &buffer[ processLen ] ) );

		// 헤더에 기록된 패킷 크기를 파싱할 수 있어야 한다
		if ( dataSize < header._length )
			break;

		// 패킷 조립 성공
		// SendBuffer가 풀링 처리되어있어서, 패킷 시작점부터 받아야함
		OnRecvPacket( &buffer[ processLen ], header._length );

		processLen += header._length;
	}

	return processLen;
}

}