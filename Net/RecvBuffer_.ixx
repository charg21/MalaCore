module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
#include <WinSock2.h>
#include <MSWSock.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.RecvBuffer:impl;

import Mala.Core.CoreGlobal;
import Mala.Net.RecvBuffer;
import Mala.Net.RioCore;
import Mala.Net.SocketHelper;
import Mala.Memory;

import <iostream>;

using namespace Mala::Net;

RecvBuffer::RecvBuffer( i32 bufferSize, bool useRio ) 
: _bufferSize{ bufferSize }
, _rioBufferId{}
{
	_capacity = bufferSize;
	_buffer = reinterpret_cast< BYTE* >( ::VirtualAllocEx(
		GetCurrentProcess(), 
		0, 
		bufferSize, 
		MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE ) );
	if ( _buffer == nullptr )
	{
		printf_s( "[DEBUG] VirtualAllocEx Error: %d\n", GetLastError() );
	}

	if ( useRio )
	{
		_rioBufferId = SocketHelper::RioFunctionTable.RIORegisterBuffer( (PCHAR)_buffer, bufferSize );
		if ( _rioBufferId == RIO_INVALID_BUFFERID )
			printf_s( "[DEBUG] RIORegisterBuffer Error: %d\n", GetLastError() );
	}
}

RecvBuffer::~RecvBuffer()
{
	::VirtualFreeEx( GetCurrentProcess(), _buffer, 0, MEM_RELEASE );

	if ( _rioBufferId )
		SocketHelper::RioFunctionTable.RIODeregisterBuffer( _rioBufferId );
}

void RecvBuffer::Clean()
{
	i32 dataSize = DataSize();
	if ( dataSize == 0 )
	{
		// 딱 마침 읽기+쓰기 커서가 동일한 위치라면, 둘 다 리셋.
		_readPos = _writePos = 0;
	}
	else
	{
		// 여유 공간이 버퍼 1개 크기 미만이면, 데이터를 앞으로 땅긴다.
		if ( FreeSize() < _bufferSize )
		{
			::memcpy( &_buffer[ 0 ], &_buffer[ _readPos ], dataSize );
			_readPos = 0;
			_writePos = dataSize;
		}
	}
}

bool RecvBuffer::OnRead( i32 numOfBytes )
{
	if ( numOfBytes > DataSize() )
		return false;

	_readPos += numOfBytes;
	return true;
}

bool RecvBuffer::OnWrite( i32 numOfBytes )
{
	if ( numOfBytes > FreeSize() )
		return false;
		
	_writePos += numOfBytes;
	return true;
}
