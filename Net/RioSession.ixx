module;

#pragma once

#include "../MalaMacro.h"

export module Mala.Net.RioSession;

import <vector>;
import <string>;
import <memory>;

import Mala.Core.Types;
import Mala.Container;
import Mala.Net.DisconnectReason;
import Mala.Net.RioEvent;
import Mala.Net.IocpEvent;
import Mala.Net.EndPoint;
import Mala.Net.RecvBuffer;
import Mala.Windows;
import Mala.Threading.Lock;

using namespace Mala::Container;

export namespace Mala::Net
{

class RioObject : public std::enable_shared_from_this< RioObject >
{
public:
	//interface
	virtual HANDLE GetHandle() { return HANDLE{}; }
	virtual void Dispatch( struct RioEvent* rioEvent, i32 numOfBytes ) {}
	virtual void Dispatch( struct IocpEvent* iocpEvent, i32 numOfBytes ) {}
	i32 GetThreadId() { return _threadId; }
	RIO_RQ GetRequestQueue() { return _requestQueue; }

public: // For Rio
	/// <summary>
	/// 통지 받을 Completion Queue의 스레드 식별자
	/// </summary>
	i32 _threadId{};
	RIO_RQ _requestQueue{};
};

/// <summary>
/// Rio 소켓모델 네트워크 세션
/// </summary>
class RioSession : public RioObject
{
	friend class RioListener;
	friend class RioCore;
	friend class RioService;
	friend class IocpCore;
	friend class IocpService;

	inline static constexpr size_t BUFFER_SIZE = 0x10000;

public:
	RioSession();
	virtual ~RioSession();

public:
	void Send( SendBufferRef sendBuffer );
	void Send( SendBufferPtr&& sendBuffer );
	bool LazySend( SendBufferPtr&& sendBuffer );
	void FlushSendQueue();

	bool Connect();
	void Disconnect( const WCHAR* reason );

	RioServicePtr GetService() { return _service.lock(); }
	void SetService( RioServiceRef service );

public:
	void SetEndPoint( EndPoint address ) { _endPoint = address; }
	EndPoint GetEndPoint() { return _endPoint; }

	SOCKET GetSocket() { return _socket; }
	bool IsConnected() { return _connected; }
	RioSessionPtr GetSessionPtr();

private:
	virtual HANDLE GetHandle() override;
	virtual void Dispatch( struct RioEvent* rioEvent, i32 numOfBytes ) override final;
	virtual void Dispatch( struct IocpEvent* rioEvent, i32 numOfBytes ) override final;

private:
	bool	RegisterConnect();
	bool	RegisterDisConnect();
	void	RegisterRecv();
	void	RegisterSend();

	void	ProcessConnect();
	void	ProcessDisconnect();
	void	ProcessRecv( i32 numOfBytes );
	void	ProcessSend( i32 numOfBytes );

	void	HandleError( i32 errorCode );

protected:
	virtual void OnConnected();
	virtual i32	OnRecv( BYTE* buffer, i32 len ) { return len; }
	virtual void OnSend( i32 len ) { }
	virtual void OnDisconnected() { }

private:
	RioServiceWeakPtr _service;
	SOCKET _socket = INVALID_SOCK;
	EndPoint _endPoint{};
	Atomic< bool > _connected = false;	//접속 여부

private:
	USE_LOCK;

	RecvBuffer _recvBuffer;
	WaitFreeQueue< SendBufferPtr > _sendQueue;
	Atomic< bool > _sendRegistered = false;

private:
	RioSendEvent _sendEvent;
	RioRecvEvent _recvEvent;
	ConnectEvent _connectEvent;
	DisConnectEvent _disConnectEvent;

protected:
	char* _rioBufferPointer;
	RIO_BUFFERID _rioBufferId;

};

} // namespace Mala::Net
