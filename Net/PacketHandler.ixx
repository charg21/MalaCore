export module Mala.Net.PacketDispatcher;

import <array>;

import Mala.Core.Types;
import Mala.Core.Crash;
import Mala.Net.PacketSession;
import Mala.Net.Buffer;
import Mala.Net.PacketHeader;

class PacketHeader;

export namespace Mala::Net
{

template < typename Packet >
extern void Handler( PacketSession* session, Packet& packet )
{
    Mala::Core::Crash();
}

template < typename Packet >
inline void HandlerJob( PacketSession* session, BYTE* buffer, i32 len )
{
    Packet packet;
    packet.Read( buffer, len );

    Handler< Packet >( session, packet );
}

using packet_HandlerJob2 = void( * )( Mala::Net::PacketSession*, BYTE*, i32 );

class PacketDispatcher
{
public:
    static void SetCapacity( size_t capacity )
    {
    }

public:
    inline static size_t _max = 1;

public:
    inline static std::array< packet_HandlerJob2, 512 > Table{};
};

} 

