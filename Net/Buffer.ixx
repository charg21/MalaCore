﻿export module Mala.Net.Buffer;

import <concepts>;
import <memory>;
import <string>;

import Mala.Windows;

export namespace Mala::Net
{

/// <summary>
/// 네트워크 스트림 처리를 위한 버퍼
/// 버퍼의 크기 동적으로 조절
/// </summary>
class StreamBuffer
{
    static constexpr uint16_t MAXIMUM_SEGMENT_SIZE = 1460 * 2;

public:

    /// <summary>
    /// 생성자
    /// </summary>
    StreamBuffer()
    : _buffer          { new char[ MAXIMUM_SEGMENT_SIZE ] }
    , _writer          {                                  }
    , _reader          {                                  }
    , _payloadCapacity { MAXIMUM_SEGMENT_SIZE             }
    {
    }

    /// <summary>
    /// 복사 생성자
    /// </summary>
    StreamBuffer( const StreamBuffer& other )
    : _buffer          { new char[ other._payloadCapacity ] }
    , _writer          { (uint16_t)( other.GetSize() )      }
    , _reader          {                                    }
    , _payloadCapacity { other._payloadCapacity             }
    {
        std::memcpy( _buffer, &other._buffer[ _reader ], other.GetSize() );
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~StreamBuffer()
    {
        delete[] _buffer;

        _reader = 0;
        _writer = 0;
    }

    /// <summary>
    /// 버퍼에 데이터를 쓴다
    /// </summary>
    /// <param name="data"> 데이터 시작 포인터 </param>
    /// <param name="size"> 데이터 크기 </param>
    void Write( const void* data, const size_t size )
    {
        if ( _writer + size >= _payloadCapacity ) // idx니깐 같아져도 문제가 생김.
        {
            size_t new_payload_capacity = _payloadCapacity * 2;
            size_t new_size = _writer + size > new_payload_capacity ? _writer + size : new_payload_capacity;

            Resize( new_size );
        }

        std::memcpy( &_buffer[ _writer ], data, size ); // 현재 인덱스에 복사를 함.

        _writer += (uint16_t)( size );
    }


    /// <summary>
    /// 버퍼로부터 데이터를 읽는다
    /// </summary>
    /// <param name="data"> 데이터 복사 목적지 </param>
    /// <param name="size"> 데이터 크기 </param>
    void Read( void* data, const size_t size )
    {
        if ( _reader + size > _writer ) // idx니깐 같아져도 문제가 생김.
            *( (int*)( nullptr ) ) = 0xDEADBEEF; 

 	    std::memcpy( data, &_buffer[ _reader ], size );

 	    _reader += (uint16_t)( size );
    }

    /// <summary>
    /// 데이터를 읽지만, 데이터 Reader 포지션을 옮기지 않는다.
    /// </summary>
    /// <param name="data"> 데이터 복사 목적지 </param>
    /// <param name="size"> 데이터 크기 </param>
    /// <param name="offset"> 오프셋 크기 </param>
    void Peek( void* data, const size_t size, const size_t offset = 0 )
    {
        if ( _reader + offset + size > _writer ) // idx니깐 같아져도 문제가 생김.
            *( ( int* )( nullptr ) ) = 0xDEADBEEF;

        std::memcpy( data, &_buffer[ _reader + offset ], size );
    }

    /// <summary>
    /// 버퍼를 반환한다
    /// </summary>
    char* GetBuffer() const noexcept
    {
        return _buffer;
    }

    /// <summary>
    /// 다음에 쓸 위치의 버퍼 공간을 반환한다
    /// </summary>
    char* get_writer_buffer() const noexcept
    {
        return &_buffer[ _writer ];
    }
    
    /// <summary>
    /// 버퍼 공간의 사이즈를 조절한다
    /// </summary>
    void Resize( size_t length )
    {
        char*  new_buffer   = new char[ length ] {};
        size_t new_capacity = length > _payloadCapacity ? _payloadCapacity : length;

        std::memcpy( new_buffer, _buffer, new_capacity );

        delete[] _buffer;

        _buffer           = new_buffer;
        _payloadCapacity = length;
        _writer           = (uint16_t)( length > _writer ? _writer : length );
        _reader           = (uint16_t)( length > _reader ? _reader : length );
    }

    /// <summary>
    /// 현재 버퍼에 있는 데이터 크기를 반환한다
    /// </summary>
    constexpr size_t GetSize() const noexcept
    {
        return _writer - _reader;
    }

    /// <summary>
    /// 버퍼에 총 크기를 반환한다
    /// </summary>
    constexpr size_t GetCapacity() const noexcept
    {
        return _payloadCapacity;
    }

    /// <summary>
    /// 버퍼의 다음 쓸 위치를 옮긴다
    /// </summary>
    void CommitOnWrite( size_t length )
    {
        _writer += length;
    }

    /// <summary>
    /// 버퍼의 다음 읽을 위치를 옮긴다
    /// </summary>
    void CommitOnRead( size_t length )
    {
        _reader += length;
    }

    /// <summary>
    /// 버퍼를 초기 상태로 되돌린다
    /// </summary>
    void Reset()
    {
        _writer = 0;
        _reader = 0;
    }

    /// <summary>
    /// 버퍼를 초기 상태로 되돌린다
    /// </summary>
    void Rewind() noexcept
    {
        _writer = _reader = 0;
    }

    /// <summary>
    /// 버퍼를 정리한다
    /// </summary>
    void Clear()
    {
        _writer = 0;
        _reader = 0;
    }

    /// <summary>
    /// 버퍼의 쓰기 가능한 공간 크기를 반환한다
    /// </summary>
    size_t GetWritableSize()
    {
        return _payloadCapacity - _writer;
    }

    /// <summary>
    /// 버퍼의 읽기 가능한 공간 크기를 반환한다
    /// </summary>
    size_t GetReadableSize()
    {
        return _writer - _reader;
    }

    /// <summary>
    /// 버퍼에 데이터를 쓴다
    /// </summary>
    template< typename T >// requires std::integral< T > || std::floating_point< T >// || std::enumeration< T >
    void Write( const T data )
    {
        static_assert( std::is_enum< T >::value || 
            std::is_floating_point< T >::value  || 
            std::numeric_limits< T >::is_integer, "ABCDED" );

        Write( &data, sizeof( T ) );
    }

    /// <summary>
    /// 버퍼에 배열 데이터를 쓴다( 특수화 )
    /// </summary>
    template< typename T, size_t N >
    void Write( const T( &datas )[ N ] )
    {
        Write( ( unsigned short )( N ) * sizeof( T ) );
        Write( &datas[ 0 ], sizeof( T ) * N );
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 쓴다
    /// </summary>
    void Write( const std::string& data )
    {
        Write( (unsigned short)( data.size() ) );
        Write( &data[ 0 ], data.size() );
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 쓴다
    /// </summary>
    void Write( const std::wstring& data )
    {
        Write( ( unsigned short )( data.size() ) );
        Write( &data[ 0 ], data.size() * 2 );
    }

    /// <summary>
    /// 버퍼의 데이터를 읽는다
    /// </summary>
    template< typename T > // requires std::integral< T > || std::floating_point< T > // || std::enumeration< T >
    void Read( T& data )
    {
        static_assert( std::is_enum< T >::value || 
            std::is_floating_point< T >::value  || 
            std::numeric_limits< T >::is_integer, "ABCDED" );

        Read( &data, sizeof( T ) );
    }

    /// <summary>
    /// 버퍼의 데이터를 배열을 통해 읽는다
    /// </summary>
    template< typename T, size_t N > 
    void Read( const T( &datas )[ N ] )
    {
        Read( ( unsigned short )( N ) * sizeof( T ) );
        Read( &datas[ 0 ], sizeof( T ) * N );
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 읽는다
    /// </summary>
    void Read( std::string& data )
    {
        unsigned short length = 0;
        Read( length );
        data.resize( length );
        Read( &data[ 0 ], length );
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 읽는다
    /// </summary>
    void Read( std::wstring& data )
    {
        unsigned short length = 0;
        Read( length );
        data.resize( length );
        Read( &data[ 0 ], length * 2 );
    }

    template< typename Type >
    bool TryRead( Type& out )
    {
        if ( sizeof( Type ) > GetReadableSize() )
            return false;

        Read( (void*)( &out ), sizeof(Type));

        return true;
    }

public:
    char*          _buffer;
    unsigned short _writer;
    unsigned short _reader;
    unsigned short _payloadCapacity;
};

/// <summary>
/// 환형큐, 소비자 생성자가 각각 1개인 상황에서, 스레드 세이프함을 보장한다
/// this buffer class garantee thread safety in 1 producer 1 consumer situation.
/// </summary>
template< size_t BufferSize = 65536, bool UseVirtualAlloc = false >
class CircularBuffer
{
    static constexpr size_t BUFFER_SIZE = BufferSize;
    static constexpr size_t CAPACITY    = BUFFER_SIZE - 1;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    CircularBuffer()
    : _buffer {}
    , _writer {}
    , _reader {}
    {
        if constexpr ( !UseVirtualAlloc )
            _buffer = new char[ BUFFER_SIZE ] {};
        else
            _buffer = (char*)( Mala::Windows::VirtualAllocEx( BUFFER_SIZE ) );
    }

    /// <summary>
    /// 생성자
    /// </summary>
    CircularBuffer( char* buffer, size_t capacity )
    : _buffer { buffer }
    , _writer {        }
    , _reader {        }
    {
    }

    CircularBuffer( const CircularBuffer& other )     = delete;
    CircularBuffer( CircularBuffer&& other ) noexcept = delete;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~CircularBuffer()
    {
        if constexpr ( !UseVirtualAlloc )
            delete[] _buffer;
        else
            Mala::Windows::VirtualFreeEx( _buffer );
    }

    /// <summary>
    /// 큐에 데이터를 추가한다
    /// </summary>
    const size_t Enqueue( const char* src, size_t size )
    {
        const size_t enqueueable_size = std::min( size, get_free_size() );
        if ( enqueueable_size == 0 )
            return 0;

        // 삽입중 타 스레드에서 _reader를 움직여 first free space가 늘어날 수 있지만
        // _writer 위치로부터 위에서 확정된 최대 크기 이상은 넣지 못한다.
        const size_t first_enqueue_size = std::min( get_first_free_space_size(), enqueueable_size );

        // 넣을수잇는 만큼만 넣고
        std::memcpy( &_buffer[ _writer ], src, first_enqueue_size );

        // 추가 Enqueue 가 필요한 경우
        if ( enqueueable_size > first_enqueue_size )
            std::memcpy( &_buffer[ 0 ], src + first_enqueue_size, enqueueable_size - first_enqueue_size );

        return CommitOnEnqueue( enqueueable_size );
    }

    /// <summary>
    /// 큐에 데이터를 읽는다( 읽기 포인터 변경 없음 )
    /// </summary>
    const size_t Peek( char* dest, size_t size )
    {
        const size_t peekable_size = std::min( size, get_stored_size() );
        if ( peekable_size == 0 )
            return false;

        // enuqueue를 하는 스레드로 인해 peekable_size 보다 get_first_stored_space_size() 사이즈가 커질수 있다.
        const size_t first_peek_size = std::min( peekable_size, get_first_stored_space_size() );

        // deque form first space
        std::memcpy( dest, &_buffer[ _reader ], first_peek_size );

        // 추가 peek이 필요한 경우
        if ( peekable_size > first_peek_size )
            std::memcpy( dest + first_peek_size, &_buffer[ 0 ], peekable_size - first_peek_size );

        return peekable_size;
    }

    /// <summary>
    /// 큐에 데이터를 뺀다
    /// </summary>
    const size_t Dequeue( char* dest, size_t size )
    {
        // Peek
        size_t peeked_size = Peek( dest, size );

        return CommitOnDequeue( peeked_size );
    }

    //// <summary>
    /// 버퍼의 쓰기 처리를 한다
    /// </summary>
    size_t CommitOnEnqueue( size_t enqueueSize )
    {
        // 원본에 처리시 타 스레드에서 계산시 문제 발생 가능
        // ex ) 버퍼를 넘는 처리
        size_t writer_capture = _writer;

        writer_capture += enqueueSize;
        writer_capture %= BUFFER_SIZE;

        // conumer thread에서 영향 받지 않도록 한다.
        _writer = writer_capture;

        return enqueueSize;
    }

    //// <summary>
    /// 버퍼의 읽기 및 제거 처리를 한다
    /// </summary>
    size_t CommitOnDequeue( size_t dequeued_size )
    {
        // 원본에 처리시 타 스레드에서 계산시 문제 발생 가능
        size_t reader_capture = _reader;

        reader_capture += dequeued_size;
        reader_capture %= BUFFER_SIZE;

        _reader = reader_capture;

        return dequeued_size;
    }

    //// <summary>
    /// 버퍼를 정리한다
    /// </summary>
    void Clear()
    {
        _writer = 0;
        _reader = 0;
    }

    //// <summary>
    /// 버퍼가 비어있는지 여부를 반환한다
    /// </summary>
    const bool Empty() const
    {
        return _writer == _reader;
    }

    //// <summary>
    /// 
    /// </summary>
    char* GetEnqueueBuffer() const
    {
        return &_buffer[ _writer ];
    }

    //// <summary>
    /// 
    /// </summary>
    char* GetDequeueBuffer() const
    {
        return &_buffer[ _reader ];
    }

    //// <summary>
    /// 버퍼의 포인터를 반환한다
    /// </summary>
    char* GetBuffer() const
    {
        return _buffer;
    }

    constexpr size_t get_first_stored_space_size() const noexcept
    {
        // _reader나 _writer가 조건 비교 이후 다른 스레드에서 값이 변경되어
        // 계산에 영향을 주는것을 방지하기 위해 사본을 통해 계산
        size_t reader_capture = _reader;
        size_t writer_capture = _writer;

        if ( reader_capture > writer_capture )
            return BUFFER_SIZE - reader_capture;
        else
            return writer_capture - reader_capture;
    }

    //// <summary>
    /// 첫번째 공간의 
    /// </summary>
    constexpr size_t get_first_free_space_size() const noexcept
    {
        // _reader나 _writer가 조건 비교 이후 다른 스레드에서 값이 변경되어
        // 계산에 영향을 주는것을 방지하기 위해 사본을 통해 계산
        size_t reader_capture = _reader;
        size_t writer_capture = _writer;

        if ( reader_capture > writer_capture )
            return reader_capture - writer_capture - 1;
        else
            return BUFFER_SIZE - writer_capture;
    }

    //// <summary>
    /// 현재 사용 중인 버퍼의 크기를 반환한다
    /// </summary>
    constexpr size_t get_stored_size() const noexcept
    {
        // _reader나 _writer가 조건 비교 이후 다른 스레드에서 값이 변경되어
        // 계산에 영향을 주는것을 방지하기 위해 사본을 통해 계산
        size_t reader_capture = _reader;
        size_t writer_capture = _writer;

        if ( writer_capture >= reader_capture )
            return writer_capture - reader_capture;
        else
            return BUFFER_SIZE + writer_capture - reader_capture;
    }

    constexpr size_t get_free_size() const noexcept
    {
        return CAPACITY - get_stored_size();
    }

private:
    char*  _buffer; //< 버퍼의 포인터
    size_t _writer; //< 다음에 쓸 버퍼 위치
    size_t _reader; //< 다음에 읽을 버퍼 위치
};

/// <summary>
/// 네트워크 수신 처리를 위한 버퍼
/// </summary>
class ReceiveBuffer
{
    static constexpr uint16_t CAPACITY = 65536;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    ReceiveBuffer()
    : _buffer{}
    , _writer{}
    , _reader{}
    {
        //_buffer = (char*)Windows::VirtualAllocEx( CAPACITY );
        //if ( !_buffer )
        //    printf_s( "[ReceiveBuffer] VirtualAllocEx Error: %d\n", 123 );
    }

    /// <summary>
    /// 복사 생성자
    /// </summary>
    ReceiveBuffer( const ReceiveBuffer& rhs ) = delete;

    /// <summary>
    /// 이동 생성자
    /// </summary>
    ReceiveBuffer( ReceiveBuffer&& rhs ) = delete;

    /// <summary>
    /// 소멸자
    /// </summary>
    ~ReceiveBuffer() = default;

    /// <summary>
    /// 버퍼에 데이터를 쓴다
    /// </summary>
    bool Write( const void* data, const size_t size )
    {
        if ( _writer + size >= CAPACITY ) // idx니깐 같아져도 문제가 생김.
        {
            *( ( int* )( nullptr ) ) = 0xDEADBEEF;
            return false;
        }

        std::memcpy( &_buffer[ _writer ], data, size ); // 현재 인덱스에 복사를 함.

        _writer += (uint16_t)( size );

        return true;
    }

    /// <summary>
    /// 버퍼로부터 데이터를 읽는다
    /// </summary>
    bool Read( void* data, const size_t size )
    {
        if ( _reader + size > _writer ) // idx니깐 같아져도 문제가 생김.
        {
            *( ( int* )( nullptr ) ) = 0xDEADBEEF;
            return false;
        }

 	    std::memcpy( data, &_buffer[ _reader ], size );

 	    _reader += (uint16_t)( size );

        return true;
    }

    /// <summary>
    /// 데이터를 읽지만, 데이터 Reader 포지션을 옮기지 않는다.
    /// </summary>
    bool Peek( void* data, const size_t size, const size_t offset = 0 )
    {
        if ( _reader + offset + size > _writer ) // idx니깐 같아져도 문제가 생김.
        {
            *( ( int* )( nullptr ) ) = 0xDEADBEEF;
            return false;
        }

        std::memcpy( data, &_buffer[ _reader + offset ], size );
    }

    /// <summary>
    /// 버퍼를 반환한다
    /// </summary>
    char* GetBuffer() const noexcept
    {
        return _buffer;
    }

    /// <summary>
    /// 다음에 쓸 위치의 버퍼 공간을 반환한다
    /// </summary>
    char* get_writer_buffer() const noexcept
    {
        return &_buffer[ _writer ];
    }
    
    /// <summary>
    /// 현재 버퍼에 있는 데이터 크기를 반환한다
    /// </summary>
    constexpr size_t GetSize() const noexcept
    {
        return _writer - _reader;
    }

    /// <summary>
    /// 버퍼에 총 크기를 반환한다
    /// </summary>
    constexpr size_t GetCapacity() const noexcept
    {
        return CAPACITY;
    }

    /// <summary>
    /// 버퍼의 다음 쓸 위치를 옮긴다
    /// </summary>
    void CommitOnWrite( size_t length )
    {
        _writer += length;
    }

    /// <summary>
    /// 버퍼의 다음 읽을 위치를 옮긴다
    /// </summary>
    void CommitOnRead( size_t length )
    {
        _reader += length;
    }

    /// <summary>
    /// 버퍼를 초기 상태로 되돌린다
    /// </summary>
    void Reset()
    {
        _writer = 0;
        _reader = 0;
    }

    /// <summary>
    /// 버퍼를 초기 상태로 되돌린다
    /// </summary>
    void Rewind() noexcept
    {
        _writer = _reader = 0;
    }

    /// <summary>
    /// 버퍼를 정리한다
    /// </summary>
    void Clear()
    {
        _writer = 0;
        _reader = 0;
    }

    /// <summary>
    /// 버퍼의 쓰기 가능한 공간 크기를 반환한다
    /// </summary>
    size_t GetWritableSize()
    {
        return CAPACITY - _writer;
    }

    /// <summary>
    /// 버퍼의 읽기 가능한 공간 크기를 반환한다
    /// </summary>
    size_t GetReadableSize()
    {
        return _writer - _reader;
    }

    /// <summary>
    /// 버퍼에 데이터를 쓴다
    /// </summary>
    template< typename T >// requires std::integral< T > || std::floating_point< T >// || std::enumeration< T >
    bool Write( const T data )
    {
        static_assert( std::is_enum< T >::value || 
            std::is_floating_point< T >::value  || 
            std::numeric_limits< T >::is_integer, "ABCDED" );

        return Write( &data, sizeof( T ) );
    }

    /// <summary>
    /// 버퍼에 배열 데이터를 쓴다( 특수화 )
    /// </summary>
    template< typename T, size_t N >
    bool Write( const T( &datas )[ N ] )
    {
        bool success = Write( ( unsigned short )( N ) * sizeof( T ) );
        success &= Write( &datas[ 0 ], sizeof( T ) * N );

        return success;
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 쓴다
    /// </summary>
    bool Write( const std::string& data )
    {
        bool success = Write( (unsigned short)( data.size() ) );
        success &= Write( &data[ 0 ], data.size() );

        return success;
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 쓴다
    /// </summary>
    bool Write( const std::wstring& data )
    {
        bool success = Write( ( unsigned short )( data.size() ) );
        success &= Write( &data[ 0 ], data.size() * 2 );

        return success;
    }

    /// <summary>
    /// 버퍼의 데이터를 읽는다
    /// </summary>
    template< typename T > // requires std::integral< T > || std::floating_point< T > // || std::enumeration< T >
    bool Read( T& data )
    {
        static_assert( std::is_enum< T >::value || 
            std::is_floating_point< T >::value  || 
            std::numeric_limits< T >::is_integer, "ABCDED" );

        return Read( &data, sizeof( T ) );
    }

    /// <summary>
    /// 버퍼의 데이터를 배열을 통해 읽는다
    /// </summary>
    template< typename T, size_t N > 
    bool Read( const T( &datas )[ N ] )
    {
        bool success = Read( ( unsigned short )( N ) * sizeof( T ) );
        success &= Read( &datas[ 0 ], sizeof( T ) * N );

        return success;
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 읽는다
    /// </summary>
    bool Read( std::string& data )
    {
        unsigned short length = 0;
        bool success = Read( length );
        data.resize( length );
        success &= Read( &data[ 0 ], length );

        return success;
    }

    /// <summary>
    /// 버퍼에 문자열 데이터를 읽는다
    /// </summary>
    bool Read( std::wstring& data )
    {
        unsigned short length = 0;
        bool success = Read( length );
        data.resize( length );
        success &= Read( &data[ 0 ], length * 2 );

        return success;
    }

    template< typename Type >
    bool TryRead( Type& out )
    {
        if ( sizeof( Type ) > GetReadableSize() )
            return false;

        Read( (void*)( &out ), sizeof(Type));

        return true;
    }

public:
    char*        _buffer;
    unsigned int _writer;
    unsigned int _reader;
};

} // namespace Mala::Net
