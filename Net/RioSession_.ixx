module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
#include <WinSock2.h>
#include <MSWSock.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

#include <vector>
#include <iostream>

#include "../MalaMacro.h"

export module Mala.Net.RioSession:impl;

import Mala.Core.CoreGlobal;
import Mala.Core.ErrorReason;
import Mala.Core.Types;
import Mala.Container;
import Mala.Container.StaticVector;

import Mala.Memory;

import Mala.Net.Buffer;
import Mala.Net.EndPoint;
import Mala.Net.PacketDispatcher;
import Mala.Net.IocpService;
import Mala.Net.IocpEvent;
import Mala.Net.RioCore;
import Mala.Net.RioEvent;
import Mala.Net.RioSession;
import Mala.Net.RioService;
import Mala.Net.SendBuffer;
import Mala.Net.SocketHelper;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Net;

RioSession::RioSession()
:_recvBuffer{ BUFFER_SIZE, true }
{
	_socket = SocketHelper::MakeSocketRio();
	_rioBufferId = _recvBuffer.GetRioBufferId();
}

RioSession::~RioSession()
{
	SocketHelper::Close( _socket );
}

void RioSession::Send( SendBufferRef sendBuffer )
{
	if ( !IsConnected() )
		return;

	_sendQueue.push( sendBuffer );

	bool registerSend = !_sendRegistered.exchange( true );
	if ( registerSend )
		RegisterSend();
}

void RioSession::Send( SendBufferPtr&& sendBuffer )
{
	if ( !IsConnected() )
		return;

	_sendQueue.push( std::move( sendBuffer ) );

	bool registerSend = !_sendRegistered.exchange( true );
	if ( registerSend )
		RegisterSend();
}

bool RioSession::LazySend( SendBufferPtr&& sendBuffer )
{
	return false;
}

void RioSession::FlushSendQueue()
{
}

bool RioSession::Connect()
{
	return RegisterConnect();
}

void RioSession::Disconnect( const WCHAR* reason )
{
	if ( !_connected.exchange( false ) )
		return;

	int error = ::GetLastError();
	std::wcout << error << L" Disconnet Reason : " << reason << std::endl;

	RegisterDisConnect();
}

void RioSession::SetService( RioServiceRef service )
{
	_service = service;
}

RioSessionPtr RioSession::GetSessionPtr()
{
	return std::static_pointer_cast< RioSession >( RioObject::shared_from_this() );
}

HANDLE RioSession::GetHandle()
{
	return reinterpret_cast< HANDLE >( _socket );
}

void RioSession::Dispatch( RioEvent* rioEvent, i32 numOfBytes )
{
	switch ( rioEvent->_eventType )
	{
	case EEventType::Recv:
		ProcessRecv( numOfBytes );
		break;
	case EEventType::Send:
		ProcessSend( numOfBytes );
		break;
	default:
		break;
	}
}

void RioSession::Dispatch( IocpEvent* iocpEvent, i32 numOfBytes )
{
	switch ( iocpEvent->eventType )
	{
	case EEventType::Connect:
		ProcessConnect();
		break;
	case EEventType::DisConnect:
		ProcessDisconnect();
		break;
	default:
		break;
	}
}

bool RioSession::RegisterConnect()
{
	if ( IsConnected() )
		return false;

	//Client 서비스에서만 가능 
	if ( GetService()->GetServiceType() != EServiceType::Client )
		return false;

	if ( !SocketHelper::SetReuseAddress( _socket, true ) )
		return false;

	if ( !SocketHelper::BindAnyAddress( _socket, 0 ) )
		return false;

	_connectEvent.Reset();
	_connectEvent.rioOwner = shared_from_this();   // Add Ref

	DWORD numOfBytes = 0;
	if ( !SocketHelper::ConnectEx( _socket,
		GetService()->GetEndPoint().AsSockaddr(),
		sizeof( SOCKADDR ),
		nullptr,
		0,
		&numOfBytes,
		&_connectEvent ) )
	{
		i32 errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_connectEvent.rioOwner = nullptr; //Release ref
			return false;
		}
	}

	return true;
}

bool RioSession::RegisterDisConnect()
{
	_disConnectEvent.Reset();
	_disConnectEvent.rioOwner = shared_from_this();    //Add ref

	if ( !SocketHelper::DisconnectEx(
		_socket,
		&_disConnectEvent, 
		TF_REUSE_SOCKET,
		0 ) )
	{
		i32 errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_disConnectEvent.rioOwner = nullptr; //release ref
			return false;
		}
	}

	return true;
}

void RioSession::RegisterRecv()
{
	if ( !IsConnected() )
		return;

	_recvEvent.owner = shared_from_this();
	_recvEvent.BufferId = _rioBufferId;
	_recvEvent.Length = static_cast< ULONG >( _recvBuffer.FreeSize() );
	_recvEvent.Offset = _recvBuffer.WritePosOffset();

	DWORD flags{};

	{
		WRITE_LOCK;

		/// start async recv
		if ( !SocketHelper::RioFunctionTable.RIOReceive(
			_requestQueue,
			(PRIO_BUF)( &_recvEvent ),
			1,
			flags,
			&_recvEvent ) )
		{
			i32 errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );
				_recvEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::RegisterSend()
{
	if ( !IsConnected() )
		return;

	_sendEvent.owner = shared_from_this();
	_sendEvent.Offset = 0;
	_sendEvent.Length = 0;

	{
		WRITE_LOCK;

		SendBufferPtr sendBuffer;
		while ( _sendQueue.TryDequeue( sendBuffer ) )
		{
			if ( _sendEvent.Length + sendBuffer->WriteSize() >= ( 65536 * 2 ) )
				break;

			std::memcpy( &_sendEvent._buffer[ _sendEvent.Length ],
				sendBuffer->Buffer(),
				sendBuffer->WriteSize() );
			_sendEvent.Length += sendBuffer->WriteSize();

			if ( _sendEvent.Length >= ( 65536 * 2 ) )
			{
				printf_s( "[DEBUG] Send Buffer OverFlow!! 0: %d\n", GetLastError() );
				return;
			}
		}

		if ( _sendEvent.Length == 0 )
		{
			printf_s( "[DEBUG] Send Size 0: %d\n", GetLastError() );
			_sendEvent.owner = nullptr;
			_sendRegistered.store( false );

			// 호출 필요할거 같은데 일단 예상 ㅇㅇ
			// RegisterSend
			return;
		}

		DWORD flags{};
		if ( !SocketHelper::RioFunctionTable.RIOSend(
			_requestQueue,
			&_sendEvent,
			1,
			flags,
			&_sendEvent ) )
		{
			i32 errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );

				printf_s( "[DEBUG] RIOSend error: %d\n", GetLastError() );

				_sendEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::ProcessConnect()
{
	_connectEvent.owner = nullptr; //release ref

	_connected.store( true );

	GetService()->AddSession( GetSessionPtr() );

	//컨텐츠코드 오버로딩
	OnConnected();

	//수신 등록 
	RegisterRecv();
}

void RioSession::ProcessDisconnect()
{
	_disConnectEvent.owner = nullptr; //release ref Owner;

	OnDisconnected();
	GetService()->ReleaseSession( GetSessionPtr() );
}

void RioSession::ProcessRecv( i32 numOfBytes )
{
	_recvEvent.owner = nullptr; //Release Ref Owner 에 대한 생명 주기 관리를 위해서 nullptr 로 날려준다.
	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Recv 0" );
		return;
	}

	if ( !_recvBuffer.OnWrite( numOfBytes ) )
	{
		Disconnect( L"OnWrite OverFlow1" );
		return;
	}

	i32 dataSize = _recvBuffer.DataSize();
	i32 processLen = OnRecv( _recvBuffer.ReadPos(), numOfBytes );
	if ( processLen < 0 || dataSize < processLen || !_recvBuffer.OnRead( processLen ) )
	{
		Disconnect( L"OnRead OverFlow2" );
		return;
	}

	_recvBuffer.Clean();

	//수신 등록
	RegisterRecv();
}

void RioSession::ProcessSend( i32 numOfBytes )
{
	_sendEvent.owner = nullptr; //release event

	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Send 0" );
		return;
	}

	OnSend( numOfBytes );

	WRITE_LOCK;
	if ( _sendQueue.UnsafeEmpty() )
	{
		_sendRegistered.store( false );
	}
	else
	{
		//이미 Queue에 쌓여있다. Register 를 바로 호출한다.
		RegisterSend();
	}
}

void RioSession::HandleError( i32 errorCode )
{
	switch ( errorCode )
	{
	case WSAECONNRESET:
	case WSAECONNABORTED:
		Disconnect( L"Handle Error" );
		break;
	default:
		//TODO Log.
		std::wcout << L" Handle Error : " << errorCode << std::endl;
		break;
	}
}

void RioSession::OnConnected()
{
	printf_s( "[DEBUG] Client Connected: ThreadId = %d\n", _threadId );
}
