export module Mala.Net.IocpCore;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Net.IocpObject;
import Mala.Windows;

export namespace Mala::Net
{

/// <summary>
/// Iocp �ھ�
/// </summary>
class IocpCore
{
public:
	IocpCore();
	virtual ~IocpCore();

	HANDLE GetHandle() { return _iocpHandle; }

	bool Register( IocpObjectRef iocpObject );
	bool Dispatch( u32 timeoutMs = TIMEOUT_INFINITE );

	void Initialize( i32 threadCount = 1 );

private:
	HANDLE _iocpHandle;
};

}
