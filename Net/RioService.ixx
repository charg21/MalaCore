export module Mala.Net.RioService;

#include "../MalaMacro.h"

import <functional>;
import <vector>;
import <string>;
import <memory>;
import <set>;

import Mala.Core.CoreEnum;
import Mala.Core.Types;
import Mala.Net.EndPoint;
import Mala.Net.RioCore;
import Mala.Net.IocpService;
import Mala.Net.RioListener;
import Mala.Threading.Lock;
import Mala.Windows;

using namespace Mala::Threading;

export namespace Mala::Net
{

class Acceptor;
class Connector;
class SessionManager;

using RioSessionFactory = std::function< RioSessionPtr() >;

/// <summary>
/// Rio ����
/// </summary>
class RioService : public std::enable_shared_from_this< RioService >
{
    using Sessions = std::set< RioSessionPtr >;

public:
    RioService( EServiceType type, EndPoint EndPoint, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount = 1 );
    virtual ~RioService() = default;

    virtual bool Start() abstract; //Start;
    bool CanStart() { return _sessionFactory != nullptr; }
    virtual void CloseService() {}
    
    RioSessionPtr CreateSession();
    void AddSession( RioSessionRef session );
    void ReleaseSession( RioSessionRef session );
    i32	GetCurrentSessionCount() { return _sessionCount; }
    i32	GetMaxSessionCount() { return _maxSessionCount; }

public:
    EServiceType GetServiceType() { return _type; }
    const EndPoint& GetEndPoint() { return _endPoint; }
    RioCorePtr GetCore() { return _rioCore; }
    RioCoreRef GetCoreRef() { return _rioCore; }

protected:
	USE_LOCK;

	EServiceType _type;
	EndPoint _endPoint{};
	RioCorePtr _rioCore;

	std::set< RioSessionPtr > _sessions;
	i32 _sessionCount = 0;
	i32 _maxSessionCount = 0;

    RioSessionFactory _sessionFactory;
};

class RioClientService : public RioService
{
public:
    RioClientService( EndPoint address, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount = 1 );

    virtual ~RioClientService() {};

    virtual bool Start() override { return true; }
};

class RioServerService : public RioService
{
public:
    RioServerService( EndPoint address, RioCoreRef core, RioSessionFactory factory, i32 maxSessionCount = 1 );
    virtual ~RioServerService() {};

    virtual bool Start() override;
    virtual void CloseService() override;

private:
    RioListenerPtr _listener = nullptr;
};

} // namespace Mala::Net