export module Mala.Net.DisconnectReason;

export namespace Mala::Net
{
    enum class EDisconnectReason
    {
        None,

        ReceivedZeroByte,
        ReceiveBufferFull,
        ReceiveBufferNull,
        SentZeroByte,
        AcceptorBindFailed,
        AcceptorListenFailed,

        Max,
    };
}