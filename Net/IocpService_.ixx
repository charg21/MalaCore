module;

#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment( lib, "ws2_32" )

export module Mala.Net.IocpService:impl;

#include "../MalaMacro.h"

import <memory>;

import Mala.Core.Types;
import Mala.Net.EndPoint;
import Mala.Net.IocpCore;
import Mala.Net.IocpService;
import Mala.Net.IocpSession;
import Mala.Net.IocpListener;
import Mala.Memory;
import Mala.Threading.Lock;

using namespace Mala::Net;


IocpService::IocpService( EServiceType type, EndPoint endPoint, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount )
: _type( type )
, _endPoint( endPoint )
, _iocpCore( core )
, _sessionFactory( factory )
, _maxSessionCount( maxSessionCount )
{

}

void IocpService::CloseService()
{
}

IocpSessionPtr IocpService::CreateSession()
{
	IocpSessionPtr session = _sessionFactory();
	session->SetService( shared_from_this() );
	if ( !_iocpCore->Register( session) )
		return nullptr;

	return session;
}

void IocpService::AddSession( IocpSessionRef session )
{
	WRITE_LOCK;

	_sessionCount++;
	_sessions.insert( session );
}

void IocpService::ReleaseSession( IocpSessionRef session)
{
	WRITE_LOCK;

	ASSERT_CRASH( _sessions.erase( session ) != 0 );
	_sessionCount--;
}


IocpClientService::IocpClientService( EndPoint address, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount )
: IocpService( EServiceType::Client, address, core, factory, maxSessionCount )
{
}

IocpClientService::~IocpClientService()
{
}

bool IocpClientService::Start()
{
	if ( !CanStart() )
		return false;

	const i32 sessionCount = GetMaxSessionCount();
	for ( i32 i = 0; i < sessionCount; i++ )
	{
		IocpSessionPtr session = CreateSession();
		if ( !session->Connect() )
			return false;
	}

	return true;
}

IocpServerService::IocpServerService( const EndPoint& address, IocpCoreRef core, IocpSessionFactory factory, i32 maxSessionCount )
: IocpService( EServiceType::Server, address, core, factory, maxSessionCount )
{
}

IocpServerService::~IocpServerService()
{
}

bool IocpServerService::Start()
{
	if ( !CanStart() )
		return false;

	_listener = MakeShared< IocpListener >();
	if ( !_listener )
		return false;

	IocpServerServicePtr service = static_pointer_cast< IocpServerService >( shared_from_this() );
	if ( !_listener->StartAccept( service ) )
		return false;

	return true;
}

void IocpServerService::CloseService()
{
	IocpService::CloseService();
}
