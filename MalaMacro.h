#pragma once

#include "Core/CoreMacro.h"
#include "Reflection/TypeInfoMacro.h"
#include "Reflection/FieldMacro.h"

#pragma region KEYWORD

#define OUT
#define LOOP       for ( ;; )
#define SUBLOUTINE for ( int charg2__ {}; charg2__ == 0; charg2__ +=1 )
#define NAMESPACE_BEGIN( name ) namespace name {
#define NAMESPACE_END }
#define READONLY  const
#define ALIGN_CACHE alignas( 64 )
#define ALIGN_PAGE  alignas( 4096 )

#pragma endregion

#pragma region ATTRIBUTE
#define    MALA_NODISCARD [[ nodiscard ]]
#pragma endregion


#define _RELEASE
#ifdef _RELEASE
    #define CORE_CRITICAL_LOG( FMT, ... ) { printf( "[CRIT][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_ERROR_LOG( FMT, ... )    { printf( "[ERRO][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_VERBOSE_LOG( FMT, ... )  { printf( "[VERB][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_LOG( FMT, ... )          { printf( "[CORE][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
#else
    #define CORE_CRITICAL_LOG( FMT, ... ) { printf( "[D_CRIT][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_ERROR_LOG( FMT, ... )    { printf( "[D_ERRO][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_VERBOSE_LOG( FMT, ... )  { printf( "[D_VERB][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
    #define CORE_LOG( FMT, ... )     { printf( "[D_CORE][%s:%d] "##FMT##"\n", __FUNCTION__, __LINE__, __VA_ARGS__ ); }
#endif

#define CONSOLE_LOG( Color, Text, ... ) Mala::Log::WriteLog( Mala::Log::EColor::Color, Text __VA_OPT__( , ) __VA_ARGS__ );

#define COUNT_OF( _array ) ( sizeof( _array ) / sizeof( _array[ 0 ] ) )
#define PROFILE_THIS Mala::Diagnostics::profiler_helper _auto_profile( __FUNCTIONW__ );
#define PROFILE_THIS_WITH_TAG( tag ) Mala::Diagnostics::profiler_helper _auto_profile_with_tag( __FUNCTIONW__##tag );

#define NAMESPACE_BEGIN( NameSpace_Name ) export namespace NameSpace_Name {
#define NAMESPACE_END( NS )  }
#define EXPORT_BEGIN export{
#define EXPORT_END  }

#define ASSERT_IF_FALSE( expr ) \
    {    \
        if ( !( expr ) ) \
            *( ( size_t* )( nullptr ) ) = 0xDEADBEEF'DEADBEEF; \
    }

#define EXCLUSIVE_LOCK( Lock ) std::unique_lock exclusive_lock( Lock );

/// <summary>
/// Crash
/// </summary>
#define CRASH( reason )						\
{											\
	u32* crash = nullptr;				    \
	_Analysis_assume_( crash != nullptr );	\
	*crash = 0XDEADBEEF;					\
}

#define ASSERT_CRASH( expr )				\
{											\
	if ( !( expr ) ) [[unlikely]] 			\
	{										\
		CRASH( "ASSERT_CRASH" );			\
		_Analysis_assume_( expr );			\
	}										\
}

/// <summary>
/// Forwarding
/// </summary>
#define MOVE( object ) object = std::move( object );

/// <summary>
/// Smart Pointer
/// </summary>
#define USING_SHARED_PTR( CLASS )                      \
using CLASS##Ptr     = std::shared_ptr< class CLASS >; \
using CLASS##Ref     = const CLASS##Ptr&;              \
using CLASS##WeakPtr = std::weak_ptr< class CLASS >;   \
using CLASS##WeakRef = const CLASS##WeakPtr&;

/// <summary>
/// Lock
/// </summary>
#define USE_MANY_LOCKS( count )	  Mala::Threading::Lock _locks[ count ];
#define USE_LOCK				  USE_MANY_LOCKS( 1 );
#define READ_LOCK_IDX( idx )	  Mala::Threading::ReadLockGuard readLockGuard_##idx( _locks[ idx ] );//, typeid( this ).name() );
#define READ_LOCK				  READ_LOCK_IDX( 0 );
#define WRITE_LOCK_IDX( idx )	  Mala::Threading::WriteLockGuard writeLockGuard_##idx( _locks[ idx ] );//, typeid( this ).name() );
#define WRITE_LOCK				  WRITE_LOCK_IDX( 0 );

/// <summary>
/// Strong Type
/// </summary>
#define USING_STRONG_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongType< Base, Name ## StrongTypeTag >;

#define USING_STRONG_VALUE_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongValueType< Base, Name ## StrongTypeTag >;

