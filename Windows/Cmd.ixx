export module Mala.Windows.Cmd;

import <string>;

export namespace Mala::Windows
{

class Cmd
{
public:
    static void Execute( const std::wstring& command )
    {
        Query( command );
    }

    static std::pair< bool, std::string > Query( const std::wstring& command )
    {
        FILE* file = _wpopen( command.c_str(), L"rt" );
        if ( !file )
            return { false, "" };

        std::string result;
        char buffer[ 1024 + 1 ]{};
        while ( fread( buffer, 1, 1024, file ) )
        {
            result.append( buffer );
            memset( buffer, 0, 1024 );
        }

        fclose( file );

        return { true, std::move( result ) };
    }
};

}
