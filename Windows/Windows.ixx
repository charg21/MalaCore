module;

#pragma warning( push, 0 )

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#ifdef MODULE_TESET
    import <Windows.h>;
    import <WinSock2.h>;
    import <MSWSock.h>;
    import <WS2tcpip.h>;
#else
    #include <Windows.h>
    #include <WinSock2.h>
    #include <MSWSock.h>
    #include <WS2tcpip.h>
#endif

#pragma comment( lib, "ws2_32" )

import <cstdint>;
import <deque>;
import <string>;
import Mala.Container.String;

#pragma pop( 0 )

export module Mala.Windows;

import Mala.Core.Types;
import Mala.Core.Defer;


// types
export 
{

using WCHAR = wchar_t;
using ULONG = ::ULONG;
using ULONG_PTR = ::ULONG_PTR;
using PULONG_PTR = ::PULONG_PTR;

using overlapped_t       = OVERLAPPED;
using OVERLAPPED         = ::OVERLAPPED;
using handle_t           = HANDLE;
using HANDLE             = ::HANDLE;
using SOCKET             = ::SOCKET;
using sockaddr_t         = sockaddr;
using SOCKADDR           = ::SOCKADDR;
using sockaddrin_t       = sockaddr_in;
using WSABUF             = ::WSABUF;
using EXCEPTION_POINTERS = ::EXCEPTION_POINTERS;


using RIO_BUF         = ::RIO_BUF;
using RIO_BUFFERID	  = ::RIO_BUFFERID;
using RIO_RQ          = ::RIO_RQ;
using RIO_CQ          = ::RIO_CQ;
using RIORESULT       = ::RIORESULT;
using RIO_EXTENSION_FUNCTION_TABLE = ::RIO_EXTENSION_FUNCTION_TABLE;
using RIO_NOTIFICATION_COMPLETION  = ::RIO_NOTIFICATION_COMPLETION;

enum
{
    TIMEOUT_INFINITE = INFINITE,
    INVALID_SOCK = INVALID_SOCKET,

    CK_RIO = 0xC0DE,
    RIO_CORRUPT_CQ_CODE = RIO_CORRUPT_CQ,

    SESSION_BUFFER_SIZE = 65536,

    MAX_RIO_RESULT = 1024,
    MAX_SEND_RQ_SIZE_PER_SOCKET = 128,
    MAX_RECV_RQ_SIZE_PER_SOCKET = 8,
    MAX_CLIENT = 5000,
    MAX_CQ_SIZE = ( MAX_SEND_RQ_SIZE_PER_SOCKET + MAX_RECV_RQ_SIZE_PER_SOCKET ) * MAX_CLIENT,
};

//constexpr auto RIO_INVALID_BUFFERID_ = ( (RIO_BUFFERID)(ULONG_PTR)0xFFFFFFFF );
constexpr auto RIO_INVALID_CQ_ = (RIO_CQ)0;
constexpr auto RIO_INVALID_RQ_ = ((RIO_RQ)0); 
//constexpr auto RIO_INVALID_RQ__ = RIO_INVALID_RQ;

// #define RIO_MSG_DONT_NOTIFY           0x00000001
// #define RIO_MSG_DEFER                 0x00000002
// #define RIO_MSG_WAITALL               0x00000004
// #define RIO_MSG_COMMIT_ONLY           0x00000008
// 
// #define RIO_INVALID_BUFFERID          ((RIO_BUFFERID)(ULONG_PTR)0xFFFFFFFF)

// 
// #define RIO_MAX_CQ_SIZE               0x8000000
// #define RIO_CORRUPT_CQ                0xFFFFFFFF
}

/// <summary>
/// 컴플리션 포트 래핑 클래스
/// </summary>
export class CompletionPort
{
public:
    CompletionPort()
    : _nativeHandle { INVALID_HANDLE_VALUE }
    {
    }

    bool Initialize( size_t thread_count = 0 )
    {
        _nativeHandle = ::CreateIoCompletionPort( INVALID_HANDLE_VALUE, nullptr, 0, thread_count );

        return _nativeHandle != INVALID_HANDLE_VALUE;
    }

    bool Register( handle_t handle, id64 completionKey )
    {
        HANDLE returnedHandle = CreateIoCompletionPort( handle, _nativeHandle, completionKey, 0 );
        if ( ( NULL == returnedHandle ) || ( returnedHandle != _nativeHandle ) )
        {
            // TODO : LOG

            return false;
        }

        return true;
    }

    bool Take( overlapped_t*& overlapped_ptr, /* out */ id64& id )
    {
        DWORD     transfered_bytes {};
        ULONG_PTR completion_key_ptr {};

        bool result = ::GetQueuedCompletionStatus(
            _nativeHandle, 
            &transfered_bytes, 
            (ULONG_PTR*)( &id ), 
            &overlapped_ptr, 
            INFINITE );
        if ( !result )
        {
            int32_t last_error = GetLastError();
            if ( WAIT_TIMEOUT == last_error ) // TIMEOUT 인경우
            {
                // TODO
                return false;
            }

            if ( ERROR_OPERATION_ABORTED == last_error )
                return false;

            return false;
        }

        return ( transfered_bytes + overlapped_ptr + completion_key_ptr ) != 0;
    }

    bool TryTake( overlapped_t* overlapped_ptr, /* out */ id64& id, int64_t timeout_value = INFINITE )
    {
        DWORD     transfered_bytes {};
        ULONG_PTR completion_key_ptr {};

        bool result = ::GetQueuedCompletionStatus(
            _nativeHandle, 
            &transfered_bytes, 
            (ULONG_PTR*)( &id ), 
            &overlapped_ptr, 
            timeout_value );
        if ( !result )
        {
            int32_t last_error = GetLastError();
            if ( timeout_value == INFINITE )
            {
                // TODO : Error
                return false;
            }

            if ( WAIT_TIMEOUT == last_error ) // TIMEOUT 인경우
            {
                // TODO
                return true;
            }

            if ( ERROR_OPERATION_ABORTED == last_error )
                return false;

            return false;
        }

        return ( transfered_bytes + overlapped_ptr + completion_key_ptr ) != 0;
    }

    bool Post( overlapped_t* overlapped_ptr, id64 id, int32_t option = 0 )
    {
        return ::PostQueuedCompletionStatus( _nativeHandle, option, (ULONG_PTR)( id ), overlapped_ptr );
    }

    handle_t GetNativeHandle()
    {
        return _nativeHandle;
    }

private:
    handle_t _nativeHandle;
};

export // template< typename T >
    [[ nodiscard ]] void*
    interlocked_exchange_ptr( void* volatile* target, void* value )
{
    return InterlockedExchangePointer( target, value );
}


export std::string get_process_name()
{
    const DWORD  process_id     = GetCurrentProcessId();
    const HANDLE process_handle = OpenProcess( PROCESS_QUERY_LIMITED_INFORMATION, FALSE, process_id );
    if ( INVALID_HANDLE_VALUE == process_handle )
        return "";

    Mala::Core::Defer handle_destroyer = [ process_handle ]()
    {
        CloseHandle( process_handle );
    };

    char  buffer[ MAX_PATH ] = {};
    DWORD bufferSize         = MAX_PATH;
    if ( !QueryFullProcessImageNameA( process_handle, 0, buffer, &bufferSize ) )
        return "";

    std::string_view full_path = buffer;
    size_t           pos       = full_path.find_last_of( "\\" );
    if ( pos == std::string_view::npos )
        return "";

    // +1 [\\], -5 [.][e][x][e][\0]
    std::string_view process_name_view = full_path.substr( pos + 1, full_path.length() - pos - 5 );
    std::string      process_name( process_name_view.data(), process_name_view.length() );

    return std::move( process_name );
}

export std::wstring GetProcessName()
{
    const DWORD  process_id     = GetCurrentProcessId();
    const HANDLE process_handle = OpenProcess( PROCESS_QUERY_LIMITED_INFORMATION, FALSE, process_id );
    if ( INVALID_HANDLE_VALUE == process_handle )
        return L"";

    Mala::Core::Defer handle_destroyer = [ process_handle ]()
    {
        CloseHandle( process_handle );
    };

    wchar_t buffer[ MAX_PATH ] = {};
    DWORD   bufferSize         = MAX_PATH;

    if ( !QueryFullProcessImageNameW( process_handle, 0, buffer, &bufferSize ) )
        return L"";

    std::wstring_view full_path = buffer;
    size_t            pos       = full_path.find_last_of( L"\\" );
    if ( pos == std::string_view::npos )
        return L"";

    // +1은 \\을, -4는 .exe
    std::wstring_view process_name_view = full_path.substr( pos + 1, full_path.length() - pos - 5 );
    std::wstring      process_name( process_name_view.data(), process_name_view.length() );

    return std::move( process_name );
}

/// <summary>
/// 블락킹을 통해 대기
/// </summary>
export inline void sleep( int n )
{
    ::Sleep( n );
}

/// <summary>
/// spin을 통해 대기
/// </summary>
export inline void nanosleep( long long waitUntil )
{
    // 1ns    1
    // 100ns  1'00
    // 1us    1'000
    // 1ms    1'000'000
    // 1sec   1'000'000'000
    waitUntil = max( waitUntil, 100 );
    waitUntil /= 100; // windows 타이머 정밀도 최소 100ns

    size_t start;
    ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &start ) );

    size_t current{ start };

    unsigned long long diff{ current - start };

    do
    {
        ::QueryPerformanceCounter( ( LARGE_INTEGER* )( &current ) );

        diff = current - start;

    } while ( diff <= waitUntil );
}

/// <summary>
/// 타임 퀀텀을 양보한다
/// </summary>
export inline void yield()
{
    ::SwitchToThread();
}

export namespace Mala::Windows
{
inline u64 GetTick()
{
    return ::GetTickCount64();
}

inline void* VirtualAllocEx( usize size )
{
    return ::VirtualAllocEx( GetCurrentProcess(), 0, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
}

inline void VirtualFreeEx( void* bufferPtr )
{
    ::VirtualFreeEx( GetCurrentProcess(), bufferPtr, 0, MEM_RELEASE );
}

inline void SetThisThreadDesc( const String& desc )
{
    ::SetThreadDescription( GetCurrentThread(), desc.c_str() );
}


class Console
{
public:
/// <summary>
/// 콘솔의 제목을 설정한다
/// </summary>
static void SetTitle( String&& title )
{
    SetConsoleTitleW( title.c_str() );
}

};

}
