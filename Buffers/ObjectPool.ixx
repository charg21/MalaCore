module;

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
//
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#define DEALLOC_CHECK

#include <Windows.h>
#include <array>

export module Mala.Buffers.ObjectPool;

import Mala.Core.Types;
import Mala.Memory;
import Mala.Memory.MemoryPool;

using namespace std;

export namespace Mala::Buffers
{
	// 클래스 객체를 넣을 수 있는 Pool
	template< typename Type >
	class ObjectPool
	{
	public:
		template< typename... Args >
		static Type* Pop( Args&&... args )
		{
			// Types.h에 _STOMP 정의가 되어있다면
#ifdef _STOMP
			MemoryHeader* ptr = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( s_allocSize ) );
			Type* memory = static_cast< Type* >( MemoryHeader::AttachHeader( ptr, s_allocSize ) );
			// 정의가 없다면
#else
			Type* memory = static_cast< Type* >( MemoryHeader::AttachHeader( s_pool.Pop(), s_allocSize ) );
#endif

			new( memory )Type( std::forward< Args >( args )... );	// placement new
			return memory;
		}

		static void Push( Type* obj )
		{
			obj->~Type();
#ifdef _STOMP
			StompAllocator::Release( MemoryHeader::DetachHeader( obj ) );
#else
			s_pool.Push( MemoryHeader::DetachHeader( obj ) );
#endif
		}

		template< typename... Args >
		static auto MakeShared( Args&&... args )
		{
			// 두번째 인자로 삭제
			return std::shared_ptr< Type >{ Pop( std::forward< Args >( args )... ), Push };
		}

		template< typename... Args >
		static auto MakeUnique( Args&&... args )
		{
			return std::unique_ptr< Type, void( * )( Type* ) >{ Pop( std::forward< Args >( args )... ), Push };
		}


	private:
		static i32		  s_allocSize;
		static MemoryPool s_pool;
	};

	template< typename Type >
	i32 ObjectPool< Type >::s_allocSize = sizeof( Type ) + sizeof( MemoryHeader );

	template< typename Type >
	MemoryPool ObjectPool< Type >::s_pool{ s_allocSize };
} // namespace Mala::Core